# Stroymarket project installation

1. clone the repo
    ```
    git clone https://skrishtofenko@bitbucket.org/sk_group/stroimarket.git
    ```

2. install composer packages
    ```
    composer install
    ```

3. create an empty database

4. run ```php init``` to deploy needed environment

5. run ```php yii migrate``` to install main project db

6. open the file ```<project_root>\vendor\wbraganca\yii2-dynamicform\DynamicFormWidget.php```, find the ```removeItems``` method declaration (string 195) and change the method scope from ```private``` to ```protected```