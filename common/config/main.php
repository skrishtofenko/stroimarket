<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['GlobalSettings', 'InitVocabularies'],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=62.109.2.43;dbname=stroimarket',
            'username' => 'stroimarket',
            'password' => 'RbF?fmo%@Uf0WNzNV|~X0OwP?m0rTnaw',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'stroymarkettest@yandex.ru',
                'password' => 'bHU-v9k-ByM-GMu',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'GlobalSettings' => [
            'class' => 'common\components\GlobalSettings',
        ],
        'InitVocabularies' => [
            'class' => 'common\components\InitVocabularies',
        ],
    ],
];
