<?php
return [
    'adminEmail' => 'stroymarkettest@yandex.ru',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'senderEmail' => 'stroymarkettest@yandex.ru',
    'mainUrl' => 'stroymarket.ru',
];
