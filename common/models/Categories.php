<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property int $id
 * @property int $parent_id
 * @property int $child_count
 * @property int $products_count
 * @property int $level
 * @property int $min_cost
 * @property int $max_cost
 * @property string $name
 * @property string $image
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Categories $parent
 * @property Categories[] $categories
 * @property Characteristics[] $characteristics
 * @property NoticesCategories[] $noticesCategories
 * @property Users[] $users
 * @property Products[] $products
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'child_count', 'products_count', 'level', 'min_cost', 'max_cost'], 'integer'],
            [['level', 'name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 63],
            [['image'], 'string', 'max' => 32],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'child_count' => Yii::t('app', 'Child Count'),
            'products_count' => Yii::t('app', 'Products Count'),
            'level' => Yii::t('app', 'Level'),
            'min_cost' => Yii::t('app', 'Min Cost'),
            'max_cost' => Yii::t('app', 'Max Cost'),
            'name' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Image'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Categories::className(), ['id' => 'parent_id'])->inverseOf('categories');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['parent_id' => 'id'])->inverseOf('parent');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristics()
    {
        return $this->hasMany(Characteristics::className(), ['category_id' => 'id'])->inverseOf('category');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticesCategories()
    {
        return $this->hasMany(NoticesCategories::className(), ['category_id' => 'id'])->inverseOf('category');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['id' => 'user_id'])->viaTable('{{%notices_categories}}', ['category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['category_id' => 'id'])->inverseOf('category');
    }
}
