<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%ip_ranges}}".
 *
 * @property int $city_id
 * @property string $begin_ip
 * @property string $end_ip
 *
 * @property Cities $city
 */
class IpRanges extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ip_ranges}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'begin_ip', 'end_ip'], 'required'],
            [['city_id', 'begin_ip', 'end_ip'], 'integer'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => Yii::t('app', 'City ID'),
            'begin_ip' => Yii::t('app', 'Begin Ip'),
            'end_ip' => Yii::t('app', 'End Ip'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->inverseOf('ipRanges');
    }
}
