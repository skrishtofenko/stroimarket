<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%bills}}".
 *
 * @property integer $id
 * @property integer $requisite_id
 * @property integer $company_id
 * @property string $type
 * @property integer $cost
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Companies $company
 * @property Requisites $requisite
 */
class Bills extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bills}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requisite_id', 'cost'], 'required'],
            [['requisite_id', 'company_id', 'cost'], 'integer'],
            [['type'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['requisite_id'], 'exist', 'skipOnError' => true, 'targetClass' => Requisites::className(), 'targetAttribute' => ['requisite_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'requisite_id' => Yii::t('app', 'Requisite ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'type' => Yii::t('app', 'Type'),
            'cost' => Yii::t('app', 'Cost'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id'])->inverseOf('bills');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisite()
    {
        return $this->hasOne(Requisites::className(), ['id' => 'requisite_id'])->inverseOf('bills');
    }
}
