<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%admins}}".
 *
 * @property integer $id
 * @property integer $is_active
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 */
class Admins extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%admins}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'integer'],
            [['email'], 'required'],
            [['email'], 'string', 'max' => 31],
            [['password_hash', 'auth_key'], 'string', 'max' => 255],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'is_active' => Yii::t('app', 'Is Active'),
            'email' => Yii::t('app', 'Email'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'auth_key' => Yii::t('app', 'Auth Key'),
        ];
    }
}
