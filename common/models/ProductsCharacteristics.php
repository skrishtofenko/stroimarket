<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%products_characteristics}}".
 *
 * @property int $product_id
 * @property int $characteristic_id
 * @property string $value
 *
 * @property Characteristics $characteristic
 * @property Products $product
 */
class ProductsCharacteristics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products_characteristics}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'characteristic_id'], 'required'],
            [['product_id', 'characteristic_id'], 'integer'],
            [['value'], 'string', 'max' => 63],
            [['product_id', 'characteristic_id'], 'unique', 'targetAttribute' => ['product_id', 'characteristic_id']],
            [['characteristic_id'], 'exist', 'skipOnError' => true, 'targetClass' => Characteristics::className(), 'targetAttribute' => ['characteristic_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => Yii::t('app', 'Product ID'),
            'characteristic_id' => Yii::t('app', 'Characteristic ID'),
            'value' => Yii::t('app', 'Value'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristic()
    {
        return $this->hasOne(Characteristics::className(), ['id' => 'characteristic_id'])->inverseOf('productsCharacteristics');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id'])->inverseOf('productsCharacteristics');
    }
}
