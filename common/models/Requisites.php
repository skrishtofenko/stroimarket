<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%requisites}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $is_default
 * @property string $account
 * @property string $bik
 * @property string $bank
 * @property string $corr_account
 * @property string $legal_address
 * @property string $post_address
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Bills[] $bills
 * @property Users $user
 */
class Requisites extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%requisites}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'is_default'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['account', 'corr_account'], 'string', 'max' => 20],
            [['bik'], 'string', 'max' => 9],
            [['bank'], 'string', 'max' => 127],
            [['legal_address', 'post_address'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'is_default' => Yii::t('app', 'Is Default'),
            'account' => Yii::t('app', 'Account'),
            'bik' => Yii::t('app', 'Bik'),
            'bank' => Yii::t('app', 'Bank'),
            'corr_account' => Yii::t('app', 'Corr Account'),
            'legal_address' => Yii::t('app', 'Legal Address'),
            'post_address' => Yii::t('app', 'Post Address'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills()
    {
        return $this->hasMany(Bills::className(), ['requisite_id' => 'id'])->inverseOf('requisite');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->inverseOf('requisites');
    }
}
