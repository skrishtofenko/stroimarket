<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%persons}}".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $is_main
 * @property string $fio
 * @property string $email
 * @property string $phone
 * @property string $questions
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Companies $company
 */
class Persons extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%persons}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'is_main'], 'integer'],
            [['questions'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['fio', 'email'], 'string', 'max' => 127],
            [['phone'], 'string', 'max' => 10],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'is_main' => Yii::t('app', 'Is Main'),
            'fio' => Yii::t('app', 'Fio'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'questions' => Yii::t('app', 'Questions'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id'])->inverseOf('persons');
    }
}
