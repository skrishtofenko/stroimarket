<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%companies_products}}".
 *
 * @property int $id
 * @property int $company_id
 * @property int $product_id
 * @property int $unit_type_id
 * @property int $is_manufactured_by
 * @property int $is_in_stock
 * @property int $is_by_order
 * @property int $is_promo
 * @property int $is_action
 * @property int $status
 * @property string $name
 * @property string $vendor_code
 * @property int $cost
 * @property string $multiplicity
 * @property string $cnt
 * @property string $url
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Companies $company
 * @property Products $product
 * @property Entities $unitType
 * @property OrdersCompaniesProducts[] $ordersCompaniesProducts
 * @property Orders[] $orders
 * @property ProductsRequests[] $productsRequests
 */
class CompaniesProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%companies_products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'product_id', 'unit_type_id', 'is_manufactured_by', 'is_in_stock', 'is_by_order', 'is_promo', 'is_action', 'status', 'cost'], 'integer'],
            [['multiplicity', 'cnt'], 'number'],
            [['comment'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'url'], 'string', 'max' => 255],
            [['vendor_code'], 'string', 'max' => 63],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['unit_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['unit_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'unit_type_id' => Yii::t('app', 'Unit Type ID'),
            'is_manufactured_by' => Yii::t('app', 'Is Manufactured By'),
            'is_in_stock' => Yii::t('app', 'Is In Stock'),
            'is_by_order' => Yii::t('app', 'Is By Order'),
            'is_promo' => Yii::t('app', 'Is Promo'),
            'is_action' => Yii::t('app', 'Is Action'),
            'status' => Yii::t('app', 'Status'),
            'name' => Yii::t('app', 'Name'),
            'vendor_code' => Yii::t('app', 'Vendor Code'),
            'cost' => Yii::t('app', 'Cost'),
            'multiplicity' => Yii::t('app', 'Multiplicity'),
            'cnt' => Yii::t('app', 'Cnt'),
            'url' => Yii::t('app', 'Url'),
            'comment' => Yii::t('app', 'Comment'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id'])->inverseOf('companiesProducts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id'])->inverseOf('companiesProducts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitType()
    {
        return $this->hasOne(Entities::className(), ['id' => 'unit_type_id'])->inverseOf('companiesProducts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersCompaniesProducts()
    {
        return $this->hasMany(OrdersCompaniesProducts::className(), ['company_product_id' => 'id'])->inverseOf('companyProduct');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['id' => 'order_id'])->viaTable('{{%orders_companies_products}}', ['company_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsRequests()
    {
        return $this->hasMany(ProductsRequests::className(), ['company_product_id' => 'id'])->inverseOf('companyProduct');
    }
}
