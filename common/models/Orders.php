<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%orders}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $company_id
 * @property integer $delivery_type_id
 * @property integer $payment_type_id
 * @property integer $is_deleted_by_buyer
 * @property integer $is_deleted_by_seller
 * @property integer $status
 * @property integer $total_cost
 * @property string $address
 * @property string $flat
 * @property integer $comment
 * @property string $deliver_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Companies $company
 * @property Entities $deliveryType
 * @property Entities $paymentType
 * @property Users $user
 * @property OrdersCompaniesProducts[] $ordersCompaniesProducts
 * @property CompaniesProducts[] $companyProducts
 * @property Reviews[] $reviews
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'company_id', 'delivery_type_id', 'payment_type_id'], 'required'],
            [['user_id', 'company_id', 'delivery_type_id', 'payment_type_id', 'is_deleted_by_buyer', 'is_deleted_by_seller', 'status', 'total_cost', 'comment'], 'integer'],
            [['deliver_at', 'created_at', 'updated_at'], 'safe'],
            [['address'], 'string', 'max' => 255],
            [['flat'], 'string', 'max' => 15],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['delivery_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['delivery_type_id' => 'id']],
            [['payment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['payment_type_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'delivery_type_id' => Yii::t('app', 'Delivery Type ID'),
            'payment_type_id' => Yii::t('app', 'Payment Type ID'),
            'is_deleted_by_buyer' => Yii::t('app', 'Is Deleted By Buyer'),
            'is_deleted_by_seller' => Yii::t('app', 'Is Deleted By Seller'),
            'status' => Yii::t('app', 'Status'),
            'total_cost' => Yii::t('app', 'Total Cost'),
            'address' => Yii::t('app', 'Address'),
            'flat' => Yii::t('app', 'Flat'),
            'comment' => Yii::t('app', 'Comment'),
            'deliver_at' => Yii::t('app', 'Deliver At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id'])->inverseOf('orders');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveryType()
    {
        return $this->hasOne(Entities::className(), ['id' => 'delivery_type_id'])->inverseOf('orders');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentType()
    {
        return $this->hasOne(Entities::className(), ['id' => 'payment_type_id'])->inverseOf('orders0');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->inverseOf('orders');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdersCompaniesProducts()
    {
        return $this->hasMany(OrdersCompaniesProducts::className(), ['order_id' => 'id'])->inverseOf('order');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyProducts()
    {
        return $this->hasMany(CompaniesProducts::className(), ['id' => 'company_product_id'])->viaTable('{{%orders_companies_products}}', ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['order_id' => 'id'])->inverseOf('order');
    }
}
