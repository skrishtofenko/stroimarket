<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%reviews}}".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $rate
 * @property string $dignity
 * @property string $flaw
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ReviewResponses[] $reviewResponses
 * @property Orders $order
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reviews}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['order_id', 'rate'], 'integer'],
            [['dignity', 'flaw', 'comment'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'rate' => Yii::t('app', 'Rate'),
            'dignity' => Yii::t('app', 'Dignity'),
            'flaw' => Yii::t('app', 'Flaw'),
            'comment' => Yii::t('app', 'Comment'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewResponses()
    {
        return $this->hasMany(ReviewResponses::className(), ['review_id' => 'id'])->inverseOf('review');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id'])->inverseOf('reviews');
    }
}
