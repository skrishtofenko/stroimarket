<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%catalogs}}".
 *
 * @property int $id
 * @property int $company_id
 * @property int $is_by_admin
 * @property int $is_processed
 * @property int $tries_count
 * @property string $file_name
 * @property string $upload_name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Companies $company
 */
class Catalogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalogs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'file_name', 'upload_name'], 'required'],
            [['company_id', 'is_by_admin', 'is_processed', 'tries_count'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_name'], 'string', 'max' => 40],
            [['upload_name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_id' => Yii::t('app', 'Company ID'),
            'is_by_admin' => Yii::t('app', 'Is By Admin'),
            'is_processed' => Yii::t('app', 'Is Processed'),
            'tries_count' => Yii::t('app', 'Tries Count'),
            'file_name' => Yii::t('app', 'File Name'),
            'upload_name' => Yii::t('app', 'Upload Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id'])->inverseOf('catalogs');
    }
}
