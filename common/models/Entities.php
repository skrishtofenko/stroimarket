<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%entities}}".
 *
 * @property int $id
 * @property int $vocabulary_id
 * @property string $name
 *
 * @property Characteristics[] $characteristics
 * @property CompaniesProducts[] $companiesProducts
 * @property Vocabularies $vocabulary
 * @property Orders[] $orders
 * @property Orders[] $orders0
 * @property Products[] $products
 * @property Products[] $products0
 * @property Users[] $users
 */
class Entities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%entities}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vocabulary_id', 'name'], 'required'],
            [['vocabulary_id'], 'integer'],
            [['name'], 'string', 'max' => 127],
            [['vocabulary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vocabularies::className(), 'targetAttribute' => ['vocabulary_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'vocabulary_id' => Yii::t('app', 'Vocabulary ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristics()
    {
        return $this->hasMany(Characteristics::className(), ['unit_type_id' => 'id'])->inverseOf('unitType');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesProducts()
    {
        return $this->hasMany(CompaniesProducts::className(), ['unit_type_id' => 'id'])->inverseOf('unitType');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVocabulary()
    {
        return $this->hasOne(Vocabularies::className(), ['id' => 'vocabulary_id'])->inverseOf('entities');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['delivery_type_id' => 'id'])->inverseOf('deliveryType');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders0()
    {
        return $this->hasMany(Orders::className(), ['payment_type_id' => 'id'])->inverseOf('paymentType');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['prod_country_id' => 'id'])->inverseOf('prodCountry');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts0()
    {
        return $this->hasMany(Products::className(), ['unit_type_id' => 'id'])->inverseOf('unitType');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['ownership_type_id' => 'id'])->inverseOf('ownershipType');
    }
}
