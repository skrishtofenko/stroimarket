<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%orders_companies_products}}".
 *
 * @property integer $order_id
 * @property integer $company_product_id
 * @property integer $quantity
 * @property integer $cost
 *
 * @property CompaniesProducts $companyProduct
 * @property Orders $order
 */
class OrdersCompaniesProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%orders_companies_products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'company_product_id', 'quantity', 'cost'], 'required'],
            [['order_id', 'company_product_id', 'quantity', 'cost'], 'integer'],
            [['company_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompaniesProducts::className(), 'targetAttribute' => ['company_product_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('app', 'Order ID'),
            'company_product_id' => Yii::t('app', 'Company Product ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'cost' => Yii::t('app', 'Cost'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyProduct()
    {
        return $this->hasOne(CompaniesProducts::className(), ['id' => 'company_product_id'])->inverseOf('ordersCompaniesProducts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id'])->inverseOf('ordersCompaniesProducts');
    }
}
