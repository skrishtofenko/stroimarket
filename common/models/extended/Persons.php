<?php
namespace common\models\extended;

class Persons extends \common\models\Persons {
    use AppModelTrait;

    public function rules() {
        $rules = parent::rules();
        $rules[] = ['email', 'email'];
        return $rules;
    }

    public function getCompany() {
        return $this->hasOne(Companies::className(), ['id' => 'company_id'])->inverseOf('persons');
    }

    public function beforeSave($insert) {
        if(parent::beforeSave($insert)) {
            if(!$this->isNewRecord) {
                $this->company_id = $this->oldAttributes['company_id'];
            }
            if(!$this->company->mainPerson) {
                $this->is_main = true;
            } else {
                $this->is_main = false;
            }
            return true;
        }
        return false;
    }
}
