<?php
namespace common\models\extended;

use Yii;

class Bills extends \common\models\Bills {
    use AppModelTrait;

    const TYPE_REGISTRATION = 'registration';
    const TYPE_SUBSCRIPTION = 'subscription';

    public static function types() {
        return [
            self::TYPE_REGISTRATION => Yii::t('app', self::TYPE_REGISTRATION),
            self::TYPE_SUBSCRIPTION => Yii::t('app', self::TYPE_SUBSCRIPTION),
        ];
    }

    public function getRequisite() {
        return $this->hasOne(Requisites::className(), ['id' => 'requisite_id'])->inverseOf('bills');
    }

    public static function createForSubscription(Requisites $requisites) {
        $bill = new self;
        $bill->requisite_id = $requisites->id;
        $bill->company_id = $requisites->user->mainCompany->id;
        $bill->type = self::TYPE_SUBSCRIPTION;
        $bill->cost = Yii::$app->params['globalSettings']['subsServCost'];
        $bill->save();
        return $bill;
    }
}
