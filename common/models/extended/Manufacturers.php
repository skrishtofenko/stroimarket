<?php
namespace common\models\extended;

class Manufacturers extends \common\models\Manufacturers {
    use AppModelTrait;

    public function recountProducts() {
        $this->updateAttributes([
            'products_count' => Products::find()->where(['manufacturer_id' => $this->id])->count(),
        ]);
    }
}
