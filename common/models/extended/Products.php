<?php
namespace common\models\extended;

use Yii;
use common\models\queries\ProductsQuery;
use common\components\ImageManager\ImageManager;
use common\components\ImageManager\ImageManagerInterface;

/**
 * @property string $fullName
 * @property string $fullCategory
 * @property string $minCost
 */
class Products extends \common\models\Products implements ImageManagerInterface {
    use AppModelTrait;

    public $imageImage;

    public static function find() {
        return new ProductsQuery(get_called_class());
    }

    public function rules() {
        $rules = parent::rules();
        $rules[] = ImageManager::getRule($this, 'image');
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels = ImageManager::addLabel($labels, 'image');
        return $labels;
    }

    public function afterFind() {
        $this->min_cost = $this->min_cost / 100;
        $this->max_cost = $this->max_cost / 100;
    }

    public static function imagesParams() {
        return [
            'image' => [
                'maxSizeInMB' => 1,
            ]
        ];
    }

    public function getProductsCharacteristics() {
        return $this->hasMany(ProductsCharacteristics::className(), ['product_id' => 'id'])->inverseOf('product');
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'category_id'])->inverseOf('products');
    }

    public function getManufacturer() {
        return $this->hasOne(Companies::className(), ['id' => 'manufacturer_id'])->inverseOf('products');
    }

    public function getNiceName() {
        return "{$this->category->name} {$this->manufacturer->name} \"$this->name\"";
    }

    public function getFullName() {
        $fullName = [$this->name];
        foreach($this->productsCharacteristics as $characteristic) {
            if($characteristic->characteristic->type == Characteristics::TYPE_STRING && $characteristic->characteristic->is_add_to_title && !empty($characteristic->value)) {
                $fullName[] = $characteristic->value . $characteristic->characteristic->unitType->name;
            }
        }
        return implode(', ', $fullName);
    }

    public function getFullCategory() {
        $category = [
            $this->category->parent->parent->name,
            $this->category->parent->name,
            $this->category->name,
        ];
        return implode(' / ', $category);
    }

    public function getMinCost() {
        if($this->min_cost > 0) {
            return $this->min_cost;
        }
        return null;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->category->recountCaches();
    }

    public function recountCaches() {
        $rows = Yii::$app->db->createCommand("
            SELECT IFNULL(MIN(cp.cost), 0) minCost,
                   IFNULL(MAX(cp.cost), 0) maxCost,
                   COUNT(cp.id) assortmentCount,
                   IFNULL(SUM(is_in_stock), 0) availabilityCount
              FROM companies_products cp
             INNER JOIN companies c
                ON c.id = cp.company_id
             INNER JOIN users u
                ON u.id = c.user_id
             WHERE u.status = " . Users::STATUS_ACTIVE . "
               AND c.type = '" . Companies::TYPE_STORE . "'
               AND cp.product_id = {$this->id}
               AND cp.cost > 0
        ")->queryOne();
        if($rows) {
            $this->min_cost = $rows['minCost'];
            $this->max_cost = $rows['maxCost'];
            $this->assortment_count = $rows['assortmentCount'];
            $this->availability_count = $rows['availabilityCount'];
            $this->save();
        }
    }
}
