<?php
namespace common\models\extended;

use Yii;
use common\helpers\SMHelper;
use common\models\queries\CompaniesQuery;

/**
 * @property string $typeAsText
 * @property string $addressAsText
 * @property float $rateAsNum
 */
class Companies extends \common\models\Companies {
    use AppModelTrait;

    const TYPE_PROVIDER = 'provider';
    const TYPE_STORE = 'store';
    const TYPE_PICKER = 'picker';

    public $street;
    public $house;
    public $room_type_id;
    public $room_number;

    public $monFrom, $monTo, $monDinnerFrom, $monDinnerTo;
    public $tueFrom, $tueTo, $tueDinnerFrom, $tueDinnerTo;
    public $wedFrom, $wedTo, $wedDinnerFrom, $wedDinnerTo;
    public $thuFrom, $thuTo, $thuDinnerFrom, $thuDinnerTo;
    public $friFrom, $friTo, $friDinnerFrom, $friDinnerTo;
    public $satFrom, $satTo, $satDinnerFrom, $satDinnerTo;
    public $sunFrom, $sunTo, $sunDinnerFrom, $sunDinnerTo;

    public $phones_array;
    public $emails_array;

    public $delivery_city_id;
    public $delivery_type;

    public function rules() {
        $rules = parent::rules();
        $rules[] = [['street'], 'string', 'max' => 127];
        $rules[] = [['house', 'room_number'], 'string', 'max' => 15];
        $rules[] = [['room_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['room_type_id' => 'id']];
        $rules[] = [['im_url'], 'url', 'defaultScheme' => 'http'];
        $rules[] = [SMHelper::$scheduleAttributes, 'string', 'min' => 5, 'max' => 5];
        $rules[] = [['phones_array'], 'each', 'rule' => ['string', 'max' => 10]];
        $rules[] = [['emails_array'], 'each', 'rule' => ['string', 'max' => 127]];
        $rules[] = [['emails_array'], 'each', 'rule' => ['email']];
        if($this->type == self::TYPE_STORE && $this->is_delivery) {
            $rules[] = [['delivery_city_id'], 'each', 'rule' => ['exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['delivery_city_id' => 'id']]];
            $rules[] = [['delivery_type'], 'each', 'rule' => ['in', 'range' => array_keys(Deliveries::types())]];
        }
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['street'] = Yii::t('app', 'Street');
        $labels['house'] = Yii::t('app', 'House');
        $labels['room_type_id'] = Yii::t('app', 'Room Type');
        $labels['room_number'] = Yii::t('app', 'Room Number');
        $labels['phones_array'] = Yii::t('app', 'Phones');
        $labels['emails_array'] = Yii::t('app', 'Emails');
        $labels['delivery_city_id'] = Yii::t('app', 'City ID');
        $labels['delivery_type'] = Yii::t('app', 'Delivery Type');
        foreach (SMHelper::$scheduleAttributes as $field) {
            $labels[$field] = Yii::t('app', 'Time');
        }
        return $labels;
    }

    public static function types() {
        return [
            self::TYPE_PROVIDER => Yii::t('app', self::TYPE_PROVIDER),
            self::TYPE_STORE => Yii::t('app', self::TYPE_STORE),
            self::TYPE_PICKER => Yii::t('app', self::TYPE_PICKER),
        ];
    }

    public function getUser() {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->inverseOf('companies');
    }

    public function getCity() {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->inverseOf('companies');
    }

    public function getMainPerson() {
        return $this->hasOne(Persons::className(), ['company_id' => 'id'])->andWhere(['is_main' => true]);
    }

    public function getRates() {
        return $this->hasOne(Rates::className(), ['company_id' => 'id'])->inverseOf('company');
    }

    public function getCompaniesProducts() {
        return $this->hasMany(CompaniesProducts::className(), ['company_id' => 'id'])
            ->andWhere(['status' => \frontend\models\CompaniesProducts::OWNER_VISIBLE_PRODUCT_STATUSES])
            ->inverseOf('company');
    }

    public static function find() {
        return new CompaniesQuery(get_called_class());
    }

    public function getTypeAsText() {
        $type = self::types()[$this->type];
        if($this->type == self::TYPE_PROVIDER && $this->is_manufacturer) {
            $type .= ", производитель";
        }
        if($this->type == self::TYPE_STORE) {
            $type = ($this->is_imonly ? 'Интернет-' : 'Розничный ') . $type;
        }
        return $type;
    }

    public function getAddressAsText() {
        if($this->type == self::TYPE_STORE && $this->is_imonly) {
            return $this->im_url;
        }
        $address = [];
        if($this->city) {
            $address[] = "г. {$this->city->name}";
        }
        $address[] = "ул. $this->street";
        if($this->house) {
            $address[] = "д. $this->house";
        }
        return implode(', ', $address);
    }

    public function getRateAsNum() {
        return $this->rates->sum();
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if(!$this->isNewRecord) {
                $this->status = $this->oldAttributes['status'];
                $this->type = $this->oldAttributes['type'];
                $this->user_id = $this->oldAttributes['user_id'];
                $this->is_paid = $this->oldAttributes['is_paid'];
                $this->paid_till = $this->oldAttributes['paid_till'];
                $this->is_main = $this->oldAttributes['is_main'];
            }
            $this->prepareAddress();
            $this->prepareSchedule();
            $this->preparePhones();
            $this->prepareEmails();
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->saveDeliveries();
        if($this->city_id) {
            $this->city->recountCompanies();
        }
        if(!$insert) {
            if(isset($changedAttributes['city_id']) && $changedAttributes['city_id']) {
                Cities::findOne($changedAttributes['city_id'])->recountCompanies();
            }
        }
    }

    public function afterFind() {
        parent::afterFind();
        $this->parseAddress();
        $this->parseSchedule();
        $this->parsePhones();
        $this->parseEmails();
        $this->parseDeliveries();
    }

    private function saveDeliveries() {
        Deliveries::deleteAll(['company_id' => $this->id]);
        if($this->type == self::TYPE_STORE && $this->is_delivery && !empty($this->delivery_city_id)) {
            foreach($this->delivery_city_id as $key => $cityId) {
                $deliveryType = $this->delivery_type[$key];
                if(isset(Deliveries::types()[$deliveryType])) {
                    if(!Deliveries::findOne(['company_id' => $this->id, 'city_id' => $cityId])) {
                        $delivery = new Deliveries([
                            'company_id' => $this->id,
                            'city_id' => $cityId,
                            'type' => $deliveryType,
                        ]);
                        $delivery->save();
                    }
                }
            }
        }
    }

    private function prepareAddress() {
        $address = [
            'street' => $this->street ?: '',
            'house' => $this->house ?: '',
            'room_type_id' => $this->room_type_id ?: '',
            'room_number' => $this->room_number ?: '',
        ];
        $this->address = json_encode($address);
    }

    private function parseAddress() {
        $address = json_decode($this->address);
        if(is_object($address)) {
            foreach ($address as $addressComponent => $value) {
                $this->$addressComponent = $value;
            }
        }
    }

    private function prepareSchedule() {
        if($this->type == self::TYPE_STORE && !$this->is_imonly) {
            $schedule = [];
            foreach (SMHelper::$scheduleAttributes as $field) {
                $schedule[$field] = $this->$field ?: '';
            }
            $this->schedule = json_encode($schedule);
        } else {
            $this->schedule = null;
        }
    }

    private function parseSchedule() {
        $schedule = json_decode($this->schedule);
        if(is_object($schedule)) {
            foreach ($schedule as $scheduleComponent => $value) {
                $this->$scheduleComponent = $value;
            }
        }
    }

    private function preparePhones() {
        $phones = [];
        if(!empty($this->phones_array)) {
            foreach ($this->phones_array as $phone) {
                $phone = preg_replace('/[^0-9]+/', '', $phone);
                if(strlen($phone) > 10) {
                    $phone = substr($phone, -10);
                }
                $phones[] = $phone;
            }
        }
        $this->phones = implode(',', array_unique($phones));
    }

    private function parsePhones() {
        $this->phones_array = !empty($this->phones) ? explode(',', $this->phones) : [];
    }

    private function prepareEmails() {
        $emails = [];
        if(!empty($this->emails_array)) {
            foreach ($this->emails_array as $email) {
                $email = trim($email);
                if($email) {
                    $emails[] = $email;
                }
            }
        }
        $this->emails = implode(',', array_unique($emails));
    }

    private function parseEmails() {
        $this->emails_array = !empty($this->emails) ? explode(',', $this->emails) : [];
    }

    private function parseDeliveries() {
        if($this->type == self::TYPE_STORE && $this->is_delivery) {
            if($deliveries = $this->deliveries) {
                foreach ($deliveries as $delivery) {
                    $this->delivery_city_id[] = $delivery->city_id;
                    $this->delivery_type[] = $delivery->type;
                }
            }
        }
    }

    public static function checkAllPayments() {
        $pageSize = 100;
        $pageNum = 0;
        while(true) {
            $companies = self::find()
                ->select(['id', 'user_id', 'is_paid', 'paid_till', 'address', 'name', 'city_id'])
                ->where(['is_paid' => 1])
                ->limit($pageSize)
                ->offset($pageSize * $pageNum)
                ->orderBy('id')
                ->all();
            if(!$companies) {
                break;
            }
            foreach ($companies as $company) {
                /** @var $company self */
                $company->checkPayment();
            }
            $pageNum++;
        }
    }

    private function checkPayment() {
        $notifyDays = [1, 2, 3, 7, 30];
        $remainingPaidDays = ceil((strtotime($this->paid_till) - time()) / 60 / 60 / 24);
        if($remainingPaidDays <= 0) {
            $this->updateAttributes(['paid_till' => null, 'is_paid' => 0]);
            $this->user->email('companyTurnOff', $this->user->email, [
                'company' => $this,
            ]);
        } elseif(in_array($remainingPaidDays, $notifyDays)) {
            $this->user->email('paymentNotification', $this->user->email, [
                'company' => $this,
                'remainingPaidDays' => $remainingPaidDays,
            ]);
        }
    }

    public static function getManufacturersList() {
        $manufacturers = self::find()
            ->where(['type' => 'provider', 'is_manufacturer' => 1])
            ->all();
        $result = [];
        foreach ($manufacturers as $manufacturer) {
            /** @var $manufacturer self */
            $result[$manufacturer->id] = $manufacturer->user->name;
        }
        return $result;
    }

    public function autoAssortmentFields() {
        if(!in_array($this->type, [self::TYPE_STORE, self::TYPE_PROVIDER])) {
            return [];
        }
        $fields = [
            'Наименование' => [
                'field' => 'product.fullName.name',
                'comment' => 'Название товара в вашей базе',
                'columnWidth' => '50',
            ],
            'Код (артикул)' => [
                'field' => 'vendor_code',
                'comment' => 'Артикул товара в вашей базе',
                'columnWidth' => '18',
            ],
        ];
        $fields = array_merge($fields, $this->type == self::TYPE_STORE
            ? [
                'Ссылка' => [
                    'field' => 'url',
                    'comment' => 'Ссылка на страницу товара на вашем сайте',
                    'columnWidth' => '30',
                ],
                'Наличие' => [
                    'field' => 'is_in_stock',
                    'comment' => 'Если товар есть в наличии, поставьте 1, иначе 0 или оставьте пустым',
                    'columnWidth' => '13',
                ],
                'Под заказ' => [
                    'field' => 'is_by_order',
                    'comment' => 'Если товар доступен под заказ, поставьте 1, иначе 0 или оставьте пустым',
                    'columnWidth' => '13',
                ],
                'Цена' => [
                    'field' => 'cost',
                    'comment' => 'Цена в рублях. Формат: 0.00',
                    'columnWidth' => '10',
                    'format' => '#,##0.00',
                ],
            ]
            : [
                'Акция' => [
                    'field' => 'is_action',
                    'comment' => 'Если на товар объявлена акция, поставьте 1, иначе 0 или оставьте пустым',
                    'columnWidth' => '10',
                ],
                'Цена' => [
                    'field' => 'cost',
                    'comment' => 'Цена по акции в рублях. Формат: 0.00',
                    'columnWidth' => '10',
                    'format' => '#,##0.00',
                ],
                'Количество' => [
                    'field' => 'cnt',
                    'comment' => 'Количество товара, на которое установлена цена по акции',
                    'columnWidth' => '10',
                ],
//                'Кратность' => 'multiplicity',
//                'Единица измерения' => 'unitType.name',
            ]
        );
        $fields = array_merge($fields, [
//            'Признак промо' => 'is_promo',
            'Показывать в каталоге' => [
                'field' => 'status',
                'comment' => 'Если вы хотите, чтобы товар был виден другим пользователям, поставьте 1, иначе 0 или оставьте пустым',
                'columnWidth' => '23',
            ],
            'Код на строймаркете' => [
                'field' => 'product_id',
                'comment' => 'Найдите артикул соответствующего товара в базе Строймаркета',
                'columnWidth' => '23',
            ],
        ]);
        return $fields;
    }
}