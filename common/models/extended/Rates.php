<?php
namespace common\models\extended;

class Rates extends \common\models\Rates {
    use AppModelTrait;

    public function recount() {
        $this->approve_rate = 0;
        $this->paid_rate = 0;
        $this->sm_reg_rate = 0;
        $this->le_reg_rate = 0;
        $company = $this->company;
        $user = $company->user;
        if($user->status == Users::STATUS_ACTIVE) {
            $this->approve_rate = 1;
            if($company->is_paid && $company->paid_till <= date('Y-m-d')) {
                $this->paid_rate = 0.5;
            }
            $userCreatedAt = strtotime($user->created_at);
            $regDays = (int) ((time() - $userCreatedAt) / (60 * 60 * 24));
            $rate = (int) ($regDays / 120) / 10;
            $this->sm_reg_rate = $rate > 1 ? 1.0 : $rate;
            if(!empty($user->legal_registered_at) && $user->legal_registered_at) {
                $legalRegisteredAt = strtotime($user->legal_registered_at);
                $regDays = (int) ((time() - $legalRegisteredAt) / (60 * 60 * 24));
                $rate = (int) ($regDays / 90) / 10;
                $this->le_reg_rate = $rate > 0.5 ? 0.5 : $rate;
            }
        }
        $this->save();
    }

    public static function recountAll() {
        $pageSize = 100;
        $pageNum = 0;
        while(true) {
            if(!$rates = self::find()->limit($pageSize)->offset($pageSize * $pageNum)->orderBy('company_id')->all()) {
                break;
            }
            foreach ($rates as $rate) {
                /** @var $rate self */
                $rate->recount();
            }
            $pageNum++;
        }
    }

    public function sum() {
        return $this->approve_rate + $this->paid_rate + $this->sm_reg_rate + $this->le_reg_rate;
    }
}
