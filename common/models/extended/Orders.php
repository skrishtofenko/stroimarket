<?php
namespace common\models\extended;

use Yii;
use common\helpers\MoneyHelper;

class Orders extends \common\models\Orders {
    use AppModelTrait;

    const STATUS_NEW = 0;
    const STATUS_ACCEPTED = 1;
    const STATUS_COMPLETED = 2;

    public static function statuses() {
        return [
            self::STATUS_NEW => Yii::t('app', 'Status New'),
            self::STATUS_ACCEPTED => Yii::t('app', 'Status Accepted'),
            self::STATUS_COMPLETED => Yii::t('app', 'Status Completed'),
        ];
    }

    public function afterFind() {
        parent::afterFind();
        $this->total_cost = MoneyHelper::intToMoney($this->total_cost);
    }

    public function getUser() {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->inverseOf('orders');
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['user_id'] = Yii::t('app', 'Buyer ID');
        return $labels;
    }
}
