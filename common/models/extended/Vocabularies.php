<?php
namespace common\models\extended;

class Vocabularies extends \common\models\Vocabularies {
    use AppModelTrait;

    const VOCABULARY_LEGAL_ENTITIES = 1;
    const VOCABULARY_ROOMS = 2;
    const VOCABULARY_DOCS = 3;
    const VOCABULARY_PAYMENTS = 4;
    const VOCABULARY_DELIVERIES = 5;
    const VOCABULARY_ORDER_PAYMENTS = 6;
    const VOCABULARY_UNITS = 7;
    const VOCABULARY_PRODUCTION_COUNTRIES = 8;
}
