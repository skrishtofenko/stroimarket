<?php
namespace common\models\extended;

use common\models\queries\CompaniesProductsQuery;

/**
 * @property $availability boolean
 * @property $availabilityAsText string
 */
class CompaniesProducts extends \common\models\CompaniesProducts {
    use AppModelTrait;
    
    const STATUS_HIDDEN = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;
    
    const OWNER_VISIBLE_PRODUCT_STATUSES = [self::STATUS_HIDDEN, self::STATUS_ACTIVE];

    public function rules() {
        $rules = parent::rules();
        foreach($rules as &$rule) {
            if($rule[1] == 'integer') {
                unset($rule[0][array_search('cost', $rule[0])]);
            }
        }
        $rules[] = [['cost'], 'number'];
        return $rules;
    }

    public static function find() {
        return new CompaniesProductsQuery(get_called_class());
    }

    public function beforeSave($insert) {
        if(parent::beforeSave($insert)) {
            $this->cost = (int) ((int) $this->cost * 100);
            return true;
        }
        return false;
    }

    public function afterFind() {
        $this->cost = $this->cost / 100;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->product->recountCaches();
    }

    public function afterDelete() {
        parent::afterDelete();
        $this->product->recountCaches();
    }

    public function getProduct() {
        return $this->hasOne(Products::className(), ['id' => 'product_id'])->inverseOf('companiesProducts');
    }

    public function getAvailability() {
        return $this->is_in_stock || $this->is_by_order;
    }

    public function getAvailabilityAsText() {
        if($this->is_in_stock) {
            return 'Узнайте наличие по телефону';
        } elseif($this->is_by_order) {
            return 'Под заказ';
        }
        return 'Нет в наличии';
    }

    public static function createOrUpdateForManufacturer(Products $product) {
        if(!$manufacturerProduct = self::findOne(['company_id' => $product->manufacturer_id, 'product_id' => $product->id])) {
            $manufacturerProduct = new self;
            $manufacturerProduct->company_id = $product->manufacturer_id;
            $manufacturerProduct->product_id = $product->id;
        }
        $manufacturerProduct->is_manufactured_by = 1;
        $manufacturerProduct->status = self::STATUS_ACTIVE;
        $manufacturerProduct->save();
    }
}
