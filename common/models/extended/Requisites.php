<?php
namespace common\models\extended;

use Yii;
use common\models\validators\CorrAccountValidator;

class Requisites extends \common\models\Requisites {
    use AppModelTrait;

    public function rules() {
        $rules = parent::rules();
        $rules[] = ['account', 'match', 'pattern' => '/^\d{20}$/s', 'message' => Yii::t('app', 'Account must be 20 digits')];
        $rules[] = ['bik', 'match', 'pattern' => '/^(\d{9})$/s', 'message' => Yii::t('app', 'BIK must be 9 digits')];
        $rules[] = ['corr_account', 'match', 'pattern' => '/^(\d{20})$/s', 'message' => Yii::t('app', 'Corr.Account must be 20 digits')];
        $rules[] = ['corr_account', CorrAccountValidator::className()];
        return $rules;
    }

    public function getUser() {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->inverseOf('requisites');
    }

    public function createIfChanged() {
        if(!empty($this->dirtyAttributes)) {
            $newRequisites =  new self;
            $newRequisites->user_id = $this->user_id;
            $newRequisites->is_default = 0;
            $newRequisites->account = $this->account;
            $newRequisites->bik = $this->bik;
            $newRequisites->bank = $this->bank;
            $newRequisites->corr_account = $this->corr_account;
            $newRequisites->legal_address = $this->legal_address;
            $newRequisites->save();
            return $newRequisites;
        }
        return $this;
    }
}
