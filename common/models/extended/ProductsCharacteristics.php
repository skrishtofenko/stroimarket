<?php
namespace common\models\extended;

class ProductsCharacteristics extends \common\models\ProductsCharacteristics {
    use AppModelTrait;

    public function niceValue() {
        switch ($this->characteristic->type) {
            case Characteristics::TYPE_BOOLEAN:
                return $this->value == 1 ? 'Да' : 'Нет';
                break;
            case Characteristics::TYPE_STRING:
                return $this->value . ($this->characteristic->unitType ? ' ' . $this->characteristic->unitType->name : '');
                break;
            case Characteristics::TYPE_LIST:
                return str_replace('|', ', ', trim($this->value, '|'));
                break;
        }
    }
}
