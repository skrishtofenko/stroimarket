<?php
namespace common\models\extended;

use Yii;

class Deliveries extends \common\models\Deliveries {
    use AppModelTrait;

    const TYPE_DELIVERY = 'delivery_delivery';
    const TYPE_PICKUP = 'delivery_pickup';
    const TYPE_BOTH = 'delivery_both';

    public static function types() {
        return [
            self::TYPE_DELIVERY => Yii::t('app', self::TYPE_DELIVERY),
            self::TYPE_PICKUP => Yii::t('app', self::TYPE_PICKUP),
            self::TYPE_BOTH => Yii::t('app', self::TYPE_BOTH),
        ];
    }
}
