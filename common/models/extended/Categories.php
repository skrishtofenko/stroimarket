<?php
namespace common\models\extended;

use Yii;
use common\components\ImageManager\ImageManager;
use common\components\ImageManager\ImageManagerInterface;

class Categories extends \common\models\Categories implements ImageManagerInterface {
    use AppModelTrait;

    const LEVEL_CHAPTER = 1;
    const LEVEL_CATEGORY = 2;
    const LEVEL_SUBCATEGORY = 3;
    const LEVEL_GOODSCATEGORY = 4;

    public $imageImage;

    public static function levels() {
        return [
            self::LEVEL_CHAPTER => Yii::t('app', 'Categories'),
            self::LEVEL_CATEGORY => Yii::t('app', 'Subcategories'),
            self::LEVEL_SUBCATEGORY => Yii::t('app', 'Subsubcategories'),
            self::LEVEL_GOODSCATEGORY => Yii::t('app', 'Goods Categories'),
        ];
    }

    public function rules() {
        $rules = parent::rules();
        $rules[] = ImageManager::getRule($this, 'image');
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels = ImageManager::addLabel($labels, 'image');
        return $labels;
    }

    public function afterFind() {
        $this->min_cost = $this->min_cost / 100;
        $this->max_cost = $this->max_cost / 100;
    }

    public static function imagesParams() {
        return [
            'image' => [
                'maxSizeInMB' => 1,
                'isMultiple' => false,
            ]
        ];
    }

    public function getParent() {
        return $this->hasOne(Categories::className(), ['id' => 'parent_id'])->inverseOf('categories');
    }

    public function getCharacteristics() {
        return $this->hasMany(Characteristics::className(), ['category_id' => 'id'])->inverseOf('category');
    }

    public function recountChild() {
        $this->updateAttributes([
            'child_count' => self::find()->where(['parent_id' => $this->id])->count(),
        ]);
    }

    public function recountCaches() {
        $rows = Yii::$app->db->createCommand("
            SELECT MIN(CASE WHEN min_cost = 0 THEN NULL ELSE min_cost END) minCost,
                   MAX(max_cost) maxCost,
                   COUNT(p.id) productsCount
              FROM products p
             WHERE p.is_active = 1
               AND p.category_id = {$this->id}
        ")->queryOne();
        if($rows) {
            $this->min_cost = $rows['minCost'];
            $this->max_cost = $rows['maxCost'];
            $this->products_count = $rows['productsCount'];
            $this->save();
        }
    }
}
