<?php
namespace common\models\extended;

use Yii;
use common\models\queries\CitiesQuery;
use yii\helpers\ArrayHelper;

class Cities extends \common\models\Cities {
    use AppModelTrait;

    const STATUS_IS_MAIN = 2;
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    public static function find() {
        return new CitiesQuery(get_called_class());
    }

    public static function statuses() {
        return [
            self::STATUS_IS_MAIN => Yii::t('app', 'Status Is Main'),
            self::STATUS_ENABLED => Yii::t('app', 'Status Enabled'),
            self::STATUS_DISABLED => Yii::t('app', 'Status Disabled'),
        ];
    }

    public function recountUsers() {
        $this->updateAttributes([
            'users_count' => Users::find()->where(['city_id' => $this->id])->count(),
        ]);
    }

    public function recountCompanies() {
        $this->updateAttributes([
            'companies_count' => Companies::find()->where(['city_id' => $this->id])->count(),
        ]);
    }

    public static function getList() {
        return ArrayHelper::map(
            self::find()
                ->active()
                ->select(['id', 'name'])
                ->orderBy(['status' => SORT_DESC, 'name' => SORT_ASC])
                ->asArray()
                ->all(),
            'id', 'name'
        );
    }

    public static function getMapWithTop() {
        $cities = self::find()
                ->active()
                ->orderBy(['status' => SORT_DESC, 'name' => SORT_ASC])
                ->all();
        $result = [
            'top' => [],
            'other' => [],
        ];
        foreach($cities as $city) {
            $result[($city->status == self::STATUS_IS_MAIN ? 'top' : 'other')][] = $city;
        }
        return $result;
    }

    public static function getListWithTop() {
        $cities = self::find()
            ->active()
            ->orderBy(['status' => SORT_DESC, 'name' => SORT_ASC])
            ->all();
        $result = [
            'top' => [],
            'other' => [],
        ];
        foreach($cities as $city) {
            /** @var $city self */
            $result[($city->status == self::STATUS_IS_MAIN ? 'top' : 'other')][$city->id] = $city->name;
        }
        return $result;
    }
}
