<?php
namespace common\models\extended;

use yii\helpers\ArrayHelper;

trait AppModelTrait {
    public static function getList($keyField = 'id', $valueField = 'name', $conditions = []) {
        return ArrayHelper::map(
            self::find()->select([$keyField, $valueField])->where($conditions)->orderBy($valueField)->asArray()->all(),
            $keyField,
            $valueField
        );
    }

    public static function getMap($primaryField = 'id') {
        return ArrayHelper::index(
            self::find()->asArray()->all(),
            $primaryField
        );
    }
}