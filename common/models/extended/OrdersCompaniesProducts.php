<?php
namespace common\models\extended;

use Yii;
use common\helpers\MoneyHelper;

class OrdersCompaniesProducts extends \common\models\OrdersCompaniesProducts {
    use AppModelTrait;

    public $totalCost;

    public function afterFind() {
        parent::afterFind();
        $this->cost = MoneyHelper::intToMoney($this->cost);
        $this->totalCost = $this->cost * $this->quantity;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['totalCost'] = Yii::t('app', 'Total Cost');
        return $labels;
    }

    public function getCompanyProduct() {
        return $this->hasOne(CompaniesProducts::className(), ['id' => 'company_product_id'])->inverseOf('ordersCompaniesProducts');
    }
}
