<?php
namespace common\models\extended;

use Yii;
use common\components\Notifier\NotifierTrait;
use common\components\Notifier\Notifyable;

class Admins extends \common\models\Admins implements Notifyable {
    use AppModelTrait;
    use NotifierTrait;

    public static function notifyTemplates() {
        return [
            'newUserRegistered' => [
                'mail_template' => 'adminNewUserRegistered',
                'mail_subject' => 'New user registered on ' . Yii::$app->name,
                'sms_template' => 'New user registered on ' . Yii::$app->name,
            ],
            'userConfirmRequested' => [
                'mail_template' => 'adminUserConfirmRequested',
                'mail_subject' => 'User requested confirmation on ' . Yii::$app->name,
                'sms_template' => 'User requested confirmation on ' . Yii::$app->name,
            ],
            'adminCatalogProcessFailed' => [
                'mail_template' => 'adminCatalogProcessFailed',
                'mail_subject' => 'User catalog processing failed on ' . Yii::$app->name,
                'sms_template' => 'User catalog processing failed on ' . Yii::$app->name,
            ],
            'catalogProcessFailed' => [
                'mail_template' => 'catalogProcessFailed',
                'mail_subject' => 'Произошла ошибка при обновлении вашего ассортимента',
                'sms_template' => 'Your uploaded catalog process is failed on ' . Yii::$app->name,
            ],
            'catalogProcessSuccess' => [
                'mail_template' => 'catalogProcessSuccess',
                'mail_subject' => 'Ваш ассортимент успешно обновлен',
                'sms_template' => 'Your uploaded catalog is successfully processed on ' . Yii::$app->name,
            ],
        ];
    }

}
