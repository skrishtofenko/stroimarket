<?php
namespace common\models\extended;

use Yii;
use yii\web\UploadedFile;

class Catalogs extends \common\models\Catalogs {
    use AppModelTrait;

    const STATUS_NEW = 0;
    const STATUS_COMPLETED = 1;
    const STATUS_ERRORED = 2;

    public static function statuses() {
        return [
            self::STATUS_NEW => Yii::t('app', 'Status New'),
            self::STATUS_COMPLETED => Yii::t('app', 'Status Completed'),
            self::STATUS_ERRORED => Yii::t('app', 'Status Errored'),
        ];
    }

    public function getCompany() {
        return $this->hasOne(Companies::className(), ['id' => 'company_id'])->inverseOf('catalogs');
    }

    public static function touchPath($fileName) {
        $path = self::generateFilePath($fileName);
        if(!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        return $path;
    }

    public static function generateFilePath($fileName, $forURL = false) {
        $fileNameFirst2 = substr($fileName, 0, 2);
        $fileNameFirst4 = substr($fileName, 0, 4);
        $class = explode('\\', self::className());
        $subPath = '/' . array_pop($class) . "/$fileNameFirst2/$fileNameFirst4/";
        if($forURL) {
            return $subPath;
        }
        return Yii::getAlias("@uploads$subPath");
    }

    public static function generateFileName(UploadedFile $catalogFile) {
        return md5(md5_file($catalogFile->tempName) . time()) . ".{$catalogFile->extension}";
    }
}
