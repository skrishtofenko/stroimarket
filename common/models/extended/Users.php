<?php
namespace common\models\extended;

use Yii;
use common\components\Notifier\NotifierTrait;
use common\components\Notifier\Notifyable;
use common\components\ImageManager\ImageManager;
use common\components\ImageManager\ImageManagerInterface;

/**
 * @property Companies $mainCompany
 * @property Requisites $mainRequisites
 * @property string $vcard
 * @property boolean $hasStore
 * @property string $legalEntityVcard
 * @property Bills $registrationBill
 * @property $displayName string
 */
class Users extends \common\models\Users implements Notifyable, ImageManagerInterface {
    use AppModelTrait;
    use NotifierTrait;

    const ROLE_BUYER = 'buyer';
    const ROLE_COMPANY = 'company';

    const STATUS_NEW = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_REQUESTED = 3;
    const STATUS_DECLINED = 4;
    const STATUS_BLOCKED = 5;
    const STATUS_DELETED = 6;

    const REGISTERED_REG_STEP = 3;
    const FILLED_COMPANY_REG_STEP = 4;
    const COMPLETED_REG_STEP = 5;

    const ACTIVE_STATUSES = [self::STATUS_CONFIRMED, self::STATUS_DECLINED, self::STATUS_REQUESTED, self::STATUS_ACTIVE];
    const INACTIVE_STATUSES = [self::STATUS_NEW, self::STATUS_BLOCKED, self::STATUS_DELETED];

    public $clearPassword;

    public $logoImage;
    public $checking_fileImage;

    public static function roles() {
        return [
            self::ROLE_BUYER => Yii::t('app', self::ROLE_BUYER),
            self::ROLE_COMPANY => Yii::t('app', self::ROLE_COMPANY),
        ];
    }

    public static function statuses() {
        return [
            self::STATUS_NEW => Yii::t('app', 'Status New'),
            self::STATUS_ACTIVE => Yii::t('app', 'Status Active'),
            self::STATUS_CONFIRMED => Yii::t('app', 'Status Confirmed'),
            self::STATUS_REQUESTED => Yii::t('app', 'Status Requested'),
            self::STATUS_DECLINED => Yii::t('app', 'Status Declined'),
            self::STATUS_BLOCKED => Yii::t('app', 'Status Blocked'),
            self::STATUS_DELETED => Yii::t('app', 'Status Deleted'),
        ];
    }

    public function rules() {
        $rules = parent::rules();
        foreach ($rules as $index => $rule) {
            if($rule[0] == ['ownership_type_id'] && $rule[1] == 'exist') {
                unset($rules[$index]);
            }
        }
        $rules[] = ['email', 'email'];
        $rules[] = ['kpp', 'match', 'pattern' => '/^\d{9}$/s', 'message' => Yii::t('app', 'KPP must be 9 digits')];
        $rules[] = ['okpo', 'match', 'pattern' => '/^(\d{8}|\d{10})$/s', 'message' => Yii::t('app', 'OKPO must be 8 or 10 digits')];
        $rules[] = ['inn', 'match', 'pattern' => '/^(\d{10}|\d{12})$/s', 'message' => Yii::t('app', 'INN must be 10 or 12 digits')];
        $rules[] = ['ogrn', 'match', 'pattern' => '/^(\d{13}|\d{15})$/s', 'message' => Yii::t('app', 'OGRN must be 13 or 15 digits')];
        $rules[] = [['url'], 'url', 'defaultScheme' => 'http'];
        $rules[] = ImageManager::getRule($this, 'logo');
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels = ImageManager::addLabel($labels, 'logo');
        return $labels;
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord) {
                $this->clearPassword = Yii::$app->security->generateRandomString(8);
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->clearPassword);
                $this->last_completed_registration_step = self::REGISTERED_REG_STEP;
                if($this->ownership_type_id == 0) {
                    $this->ownership_type_id = null;
                }
            } else {
                $this->role = $this->oldAttributes['role'];

                if($this->legal_registered_at != null) {
                    $this->legal_registered_at = date('Y-m-d H:i:s', strtotime($this->legal_registered_at));
                } else {
                    $this->legal_registered_at = null;
                }
            }
            if(!empty($this->phone)) {
                $this->phone = preg_replace('/[^0-9]+/', '', $this->phone);
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if($this->city_id) {
            $this->city->recountUsers();
        }
        if(!$insert) {
            if(isset($changedAttributes['city_id']) && $changedAttributes['city_id']) {
                Cities::findOne($changedAttributes['city_id'])->recountUsers();
            }
        }
    }

    public function afterFind() {
        parent::afterFind();
        $this->legal_registered_at = $this->legal_registered_at ? date('d-M-Y', strtotime($this->legal_registered_at)) : null;
    }

    public static function imagesParams() {
        return [
            'logo' => [
                'maxSizeInMB' => 5,
                'isMultiple' => false,
            ],
            'checking_file' => [
                'maxSizeInMB' => 5,
                'isMultiple' => false,
            ],
        ];
    }

    public function getCity() {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->inverseOf('users');
    }

    public function getVcard() {
        if($this->fio) {
            return "{$this->fio} ({$this->email})";
        } else {
            return $this->email;
        }
    }

    public function getLegalEntityVcard() {
        if($this->ownership_type_id) {
            return "{$this->ownershipType->name} \"{$this->legal_name}\"";
        } else {
            return "Физическое лицо";
        }
    }

    public function getCompanies() {
        return $this->hasMany(Companies::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    public function getMainCompany() {
        return $this->hasOne(Companies::className(), ['user_id' => 'id'])->andWhere(['is_main' => true]);
    }

    public function getMainRequisites() {
        return $this->hasOne(Requisites::className(), ['user_id' => 'id'])->andWhere(['is_default' => true]);
    }

    public function getRegistrationBill() {
        return $this->hasOne(Bills::className(), ['requisite_id' => 'id'])
            ->viaTable('requisites', ['user_id' => 'id'])->andWhere(['type' => Bills::TYPE_REGISTRATION]);
    }

    public function getHasStore() {
        foreach($this->companies as $company) {
            if($company->type == Companies::TYPE_STORE) {
                return true;
            }
        }
        return false;
    }

    public function getDisplayName() {
        return $this->mainCompany->type == Companies::TYPE_PICKER && !$this->ownership_type_id ? $this->fio : $this->name;
    }

    public static function notifyTemplates() {
        return [
            'createdByAdmin' => [
                'mail_template' => 'newUserCreatedByAdmin',
                'mail_subject' => 'Welcome to ' . Yii::$app->name,
                'sms_template' => 'Welcome to ' . Yii::$app->name,
            ],
            'emailUpdatedByAdmin' => [
                'mail_template' => 'userEmailUpdatedByAdmin',
                'mail_subject' => 'Your email was changed',
                'sms_template' => 'Your email was changed',
            ],
            'blockedByAdmin' => [
                'mail_template' => 'userBlockedByAdmin',
                'mail_subject' => 'Your account was blocked',
                'sms_template' => 'Your account was blocked',
            ],
            'unblockedByAdmin' => [
                'mail_template' => 'userUnblockedByAdmin',
                'mail_subject' => 'Your account was unblocked',
                'sms_template' => 'Your account was unblocked',
            ],
            'approvedByAdmin' => [
                'mail_template' => 'userApprovedByAdmin',
                'mail_subject' => 'Ваш аккаунт подтвержден',
                'sms_template' => 'Your account was approved',
            ],
            'declinedByAdmin' => [
                'mail_template' => 'userDeclinedByAdmin',
                'mail_subject' => 'Отправленный вами запрос на подтверждение аккаунта был отклонен',
                'sms_template' => 'Your account was declined',
            ],
            'registered' => [
                'mail_template' => 'newUserSelfRegistered',
                'mail_subject' => 'Добро пожаловать на Строймаркет',
                'sms_template' => 'Welcome to ' . Yii::$app->name,
            ],
            'passwordResetRequest' => [
                'mail_template' => 'passwordResetRequested',
                'mail_subject' => 'Восстановление пароля на Строймаркете',
                'sms_template' => 'Password reset on ' . Yii::$app->name,
            ],
            'paymentNotification' => [
                'mail_template' => 'paymentNotification',
                'mail_subject' => 'Please, check your payment on ' . Yii::$app->name,
                'sms_template' => 'Please, check your payment on ' . Yii::$app->name,
            ],
            'companyTurnOff' => [
                'mail_template' => 'companyTurnOff',
                'mail_subject' => 'Your payment is expired on ' . Yii::$app->name,
                'sms_template' => 'Your payment is expired on ' . Yii::$app->name,
            ],
            'newAction' => [
                'mail_template' => 'newAction',
                'mail_subject' => 'New action is created on ' . Yii::$app->name,
                'sms_template' => 'New action is created on ' . Yii::$app->name,
            ],
            'catalogProcessFailed' => [
                'mail_template' => 'catalogProcessFailed',
                'mail_subject' => 'Произошла ошибка при обновлении вашего ассортимента',
                'sms_template' => 'Your uploaded catalog process is failed on ' . Yii::$app->name,
            ],
            'catalogProcessSuccess' => [
                'mail_template' => 'catalogProcessSuccess',
                'mail_subject' => 'Ваш ассортимент успешно обновлен',
                'sms_template' => 'Your uploaded catalog is successfully processed on ' . Yii::$app->name,
            ],
        ];
    }
}
