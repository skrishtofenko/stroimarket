<?php
namespace common\models\extended;

use Yii;

class Characteristics extends \common\models\Characteristics {
    use AppModelTrait;

    const TYPE_BOOLEAN = 'boolean';
    const TYPE_STRING = 'string';
    const TYPE_LIST = 'list';

    public static function types() {
        return [
            self::TYPE_BOOLEAN => Yii::t('app', self::TYPE_BOOLEAN),
            self::TYPE_STRING => Yii::t('app', self::TYPE_STRING),
            self::TYPE_LIST => Yii::t('app', self::TYPE_LIST),
        ];
    }

    public function getOptions() {
        if($this->type == self::TYPE_LIST) {
            $options = [];
            foreach (explode("\n", $this->values) as $value) {
                $value = trim($value);
                $options[$value] = $value;
            }
            return $options;
        } elseif($this->type == self::TYPE_BOOLEAN) {
            return [
                '' => '',
                1 => 'Да',
                0 => 'Нет',
            ];
        }
        return null;
    }
}
