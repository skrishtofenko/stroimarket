<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%deliveries}}".
 *
 * @property int $company_id
 * @property int $city_id
 * @property string $type
 *
 * @property Cities $city
 * @property Companies $company
 */
class Deliveries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%deliveries}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'city_id'], 'required'],
            [['company_id', 'city_id'], 'integer'],
            [['type'], 'string'],
            [['company_id', 'city_id'], 'unique', 'targetAttribute' => ['company_id', 'city_id']],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->inverseOf('deliveries');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id'])->inverseOf('deliveries');
    }
}
