<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%review_responses}}".
 *
 * @property integer $id
 * @property integer $review_id
 * @property integer $is_from_store
 * @property string $message
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Reviews $review
 */
class ReviewResponses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%review_responses}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['review_id', 'message'], 'required'],
            [['review_id', 'is_from_store'], 'integer'],
            [['message'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['review_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reviews::className(), 'targetAttribute' => ['review_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'review_id' => Yii::t('app', 'Review ID'),
            'is_from_store' => Yii::t('app', 'Is From Store'),
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReview()
    {
        return $this->hasOne(Reviews::className(), ['id' => 'review_id'])->inverseOf('reviewResponses');
    }
}
