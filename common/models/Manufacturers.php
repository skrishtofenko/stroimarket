<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%manufacturers}}".
 *
 * @property int $id
 * @property int $products_count
 * @property string $name
 */
class Manufacturers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%manufacturers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['products_count'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 63],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'products_count' => Yii::t('app', 'Products Count'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}
