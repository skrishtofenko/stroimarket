<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%vocabularies}}".
 *
 * @property integer $id
 * @property string $alias
 * @property string $name
 *
 * @property Entities[] $entities
 */
class Vocabularies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%vocabularies}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'name'], 'required'],
            [['alias'], 'string', 'max' => 15],
            [['name'], 'string', 'max' => 63],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'alias' => Yii::t('app', 'Alias'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntities()
    {
        return $this->hasMany(Entities::className(), ['vocabulary_id' => 'id'])->inverseOf('vocabulary');
    }
}
