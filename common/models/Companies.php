<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%companies}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $country_id
 * @property int $city_id
 * @property int $is_main
 * @property int $is_manufacturer
 * @property int $is_imonly
 * @property int $is_delivery
 * @property int $is_paid
 * @property string $type
 * @property int $status
 * @property string $paid_till
 * @property string $address
 * @property string $im_url
 * @property string $phones
 * @property string $emails
 * @property string $schedule
 * @property string $catalog_updated_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Bills[] $bills
 * @property Catalogs[] $catalogs
 * @property Cities $city
 * @property Countries $country
 * @property Users $user
 * @property CompaniesProducts[] $companiesProducts
 * @property Deliveries[] $deliveries
 * @property Cities[] $cities
 * @property Orders[] $orders
 * @property Persons[] $persons
 * @property Products[] $products
 * @property Rates $rates
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%companies}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'country_id', 'city_id', 'is_main', 'is_manufacturer', 'is_imonly', 'is_delivery', 'is_paid', 'status'], 'integer'],
            [['type', 'address', 'phones', 'emails', 'schedule'], 'string'],
            [['paid_till', 'catalog_updated_at', 'created_at', 'updated_at'], 'safe'],
            [['im_url'], 'string', 'max' => 255],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'is_main' => Yii::t('app', 'Is Main'),
            'is_manufacturer' => Yii::t('app', 'Is Manufacturer'),
            'is_imonly' => Yii::t('app', 'Is Imonly'),
            'is_delivery' => Yii::t('app', 'Is Delivery'),
            'is_paid' => Yii::t('app', 'Is Paid'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'paid_till' => Yii::t('app', 'Paid Till'),
            'address' => Yii::t('app', 'Address'),
            'im_url' => Yii::t('app', 'Im Url'),
            'phones' => Yii::t('app', 'Phones'),
            'emails' => Yii::t('app', 'Emails'),
            'schedule' => Yii::t('app', 'Schedule'),
            'catalog_updated_at' => Yii::t('app', 'Catalog Updated At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBills()
    {
        return $this->hasMany(Bills::className(), ['company_id' => 'id'])->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCatalogs()
    {
        return $this->hasMany(Catalogs::className(), ['company_id' => 'id'])->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->inverseOf('companies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id'])->inverseOf('companies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->inverseOf('companies');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesProducts()
    {
        return $this->hasMany(CompaniesProducts::className(), ['company_id' => 'id'])->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries()
    {
        return $this->hasMany(Deliveries::className(), ['company_id' => 'id'])->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(Cities::className(), ['id' => 'city_id'])->viaTable('{{%deliveries}}', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['company_id' => 'id'])->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPersons()
    {
        return $this->hasMany(Persons::className(), ['company_id' => 'id'])->inverseOf('company');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['manufacturer_id' => 'id'])->inverseOf('manufacturer');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRates()
    {
        return $this->hasOne(Rates::className(), ['company_id' => 'id'])->inverseOf('company');
    }
}
