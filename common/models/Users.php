<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%users}}".
 *
 * @property int $id
 * @property int $city_id
 * @property int $ownership_type_id
 * @property int $is_password_sent
 * @property int $is_request_completed
 * @property int $is_recommended
 * @property int $last_completed_registration_step
 * @property string $role
 * @property int $status
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $registration_token
 * @property string $email
 * @property string $phone
 * @property string $fio
 * @property string $legal_name
 * @property string $name
 * @property string $url
 * @property string $logo
 * @property string $checking_file
 * @property string $inn
 * @property string $ogrn
 * @property string $kpp
 * @property string $okpo
 * @property string $description
 * @property string $legal_registered_at
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Companies[] $companies
 * @property Messages[] $messages
 * @property Messages[] $messages0
 * @property Notices $notices
 * @property NoticesCategories[] $noticesCategories
 * @property Categories[] $categories
 * @property Orders[] $orders
 * @property Requisites[] $requisites
 * @property Cities $city
 * @property Entities $ownershipType
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'ownership_type_id', 'is_password_sent', 'is_request_completed', 'is_recommended', 'last_completed_registration_step', 'status'], 'integer'],
            [['role', 'description'], 'string'],
            [['email'], 'required'],
            [['legal_registered_at', 'created_at', 'updated_at'], 'safe'],
            [['password_hash', 'url'], 'string', 'max' => 255],
            [['password_reset_token'], 'string', 'max' => 63],
            [['registration_token', 'logo', 'checking_file'], 'string', 'max' => 32],
            [['email', 'fio', 'legal_name', 'name'], 'string', 'max' => 127],
            [['phone', 'okpo'], 'string', 'max' => 10],
            [['inn'], 'string', 'max' => 12],
            [['ogrn'], 'string', 'max' => 15],
            [['kpp'], 'string', 'max' => 9],
            [['email'], 'unique'],
            [['registration_token'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['ownership_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['ownership_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'ownership_type_id' => Yii::t('app', 'Ownership Type ID'),
            'is_password_sent' => Yii::t('app', 'Is Password Sent'),
            'is_request_completed' => Yii::t('app', 'Is Request Completed'),
            'is_recommended' => Yii::t('app', 'Is Recommended'),
            'last_completed_registration_step' => Yii::t('app', 'Last Completed Registration Step'),
            'role' => Yii::t('app', 'Role'),
            'status' => Yii::t('app', 'Status'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'registration_token' => Yii::t('app', 'Registration Token'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'fio' => Yii::t('app', 'Fio'),
            'legal_name' => Yii::t('app', 'Legal Name'),
            'name' => Yii::t('app', 'Name'),
            'url' => Yii::t('app', 'Url'),
            'logo' => Yii::t('app', 'Logo'),
            'checking_file' => Yii::t('app', 'Checking File'),
            'inn' => Yii::t('app', 'Inn'),
            'ogrn' => Yii::t('app', 'Ogrn'),
            'kpp' => Yii::t('app', 'Kpp'),
            'okpo' => Yii::t('app', 'Okpo'),
            'description' => Yii::t('app', 'Description'),
            'legal_registered_at' => Yii::t('app', 'Legal Registered At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Messages::className(), ['from_user_id' => 'id'])->inverseOf('fromUser');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages0()
    {
        return $this->hasMany(Messages::className(), ['to_user_id' => 'id'])->inverseOf('toUser');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotices()
    {
        return $this->hasOne(Notices::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticesCategories()
    {
        return $this->hasMany(NoticesCategories::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['id' => 'category_id'])->viaTable('{{%notices_categories}}', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequisites()
    {
        return $this->hasMany(Requisites::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id'])->inverseOf('users');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnershipType()
    {
        return $this->hasOne(Entities::className(), ['id' => 'ownership_type_id'])->inverseOf('users');
    }
}
