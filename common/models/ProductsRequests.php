<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%products_requests}}".
 *
 * @property integer $id
 * @property integer $company_product_id
 * @property integer $product_id
 * @property string $image
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CompaniesProducts $companyProduct
 * @property Products $product
 */
class ProductsRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products_requests}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_product_id'], 'required'],
            [['company_product_id', 'product_id'], 'integer'],
            [['comment'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['image'], 'string', 'max' => 32],
            [['company_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompaniesProducts::className(), 'targetAttribute' => ['company_product_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'company_product_id' => Yii::t('app', 'Company Product ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'image' => Yii::t('app', 'Image'),
            'comment' => Yii::t('app', 'Comment'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyProduct()
    {
        return $this->hasOne(CompaniesProducts::className(), ['id' => 'company_product_id'])->inverseOf('productsRequests');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id'])->inverseOf('productsRequests');
    }
}
