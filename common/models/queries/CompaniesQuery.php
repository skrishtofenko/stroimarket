<?php
namespace common\models\queries;

use Yii;
use yii\db\ActiveQuery;
use common\models\extended\Users;
use common\models\extended\Companies;

class CompaniesQuery extends ActiveQuery {
    public function active() {
//        /** @var Users $user */
//        $user = Yii::$app->user->identity;
//        $userId = $user ? $user->id : null;
        return $this
            ->innerJoin('users', 'users.id = companies.user_id')
            ->andWhere(['users.status' => Users::STATUS_ACTIVE/*, 'is_paid' => 1*/])
//            ->orWhere(['user_id' => $userId])
            ;
    }

    public function providers() {
        return $this
            ->andWhere(['type' => Companies::TYPE_PROVIDER]);
    }

    public function stores() {
        return $this
            ->andWhere(['type' => Companies::TYPE_STORE]);
    }
}