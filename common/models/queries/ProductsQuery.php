<?php
namespace common\models\queries;

use yii\db\ActiveQuery;

class ProductsQuery extends ActiveQuery {
    public function active() {
        return $this->andWhere(['products.is_active' => 1]);
    }
}
