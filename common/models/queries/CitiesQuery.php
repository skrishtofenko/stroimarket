<?php
namespace common\models\queries;

use yii\db\ActiveQuery;
use common\models\extended\Cities;

class CitiesQuery extends ActiveQuery {
    public function active() {
        return $this->andWhere(['status' => [Cities::STATUS_ENABLED, Cities::STATUS_IS_MAIN]]);
    }
}
