<?php
namespace common\models\queries;

use Yii;
use yii\db\ActiveQuery;
use common\models\extended\Users;
use common\models\extended\CompaniesProducts;

class CompaniesProductsQuery extends ActiveQuery {
    public function active($withNoConformity = false) {
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        $userId = $user ? $user->id : null;

        $this
            ->innerJoin('companies', 'companies.id = companies_products.company_id')
            ->andWhere(['companies_products.status' => CompaniesProducts::STATUS_ACTIVE])
            ->orWhere(['companies.user_id' => $userId])
            ->andWhere(['!=', 'companies_products.status', CompaniesProducts::STATUS_DELETED]);
        if($withNoConformity) {
            return $this
                ->leftJoin('products', 'companies_products.product_id = products.id')
                ->andWhere(['products.is_active' => 1])
                ->orWhere(['companies_products.product_id' => null]);
        }
        return $this
            ->innerJoin('products', 'companies_products.product_id = products.id')
            ->andWhere(['products.is_active' => 1]);
    }
}