<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cities}}".
 *
 * @property int $id
 * @property int $country_id
 * @property int $status
 * @property int $users_count
 * @property int $companies_count
 * @property string $name
 *
 * @property Countries $country
 * @property Companies[] $companies
 * @property Deliveries[] $deliveries
 * @property Companies[] $companies0
 * @property IpRanges[] $ipRanges
 * @property Users[] $users
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cities}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'status', 'users_count', 'companies_count'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 63],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'status' => Yii::t('app', 'Status'),
            'users_count' => Yii::t('app', 'Users Count'),
            'companies_count' => Yii::t('app', 'Companies Count'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['id' => 'country_id'])->inverseOf('cities');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['city_id' => 'id'])->inverseOf('city');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeliveries()
    {
        return $this->hasMany(Deliveries::className(), ['city_id' => 'id'])->inverseOf('city');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies0()
    {
        return $this->hasMany(Companies::className(), ['id' => 'company_id'])->viaTable('{{%deliveries}}', ['city_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIpRanges()
    {
        return $this->hasMany(IpRanges::className(), ['city_id' => 'id'])->inverseOf('city');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['city_id' => 'id'])->inverseOf('city');
    }
}
