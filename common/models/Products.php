<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%products}}".
 *
 * @property int $id
 * @property int $category_id
 * @property int $manufacturer_id
 * @property int $prod_country_id
 * @property int $unit_type_id
 * @property int $is_active
 * @property int $assortment_count
 * @property int $availability_count
 * @property int $min_cost
 * @property int $max_cost
 * @property string $name
 * @property string $image
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CompaniesProducts[] $companiesProducts
 * @property Categories $category
 * @property Companies $manufacturer
 * @property Entities $prodCountry
 * @property Entities $unitType
 * @property ProductsCharacteristics[] $productsCharacteristics
 * @property Characteristics[] $characteristics
 * @property ProductsRequests[] $productsRequests
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'unit_type_id', 'name'], 'required'],
            [['category_id', 'manufacturer_id', 'prod_country_id', 'unit_type_id', 'is_active', 'assortment_count', 'availability_count', 'min_cost', 'max_cost'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'image'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['manufacturer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['manufacturer_id' => 'id']],
            [['prod_country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['prod_country_id' => 'id']],
            [['unit_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['unit_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'manufacturer_id' => Yii::t('app', 'Manufacturer ID'),
            'prod_country_id' => Yii::t('app', 'Prod Country ID'),
            'unit_type_id' => Yii::t('app', 'Unit Type ID'),
            'is_active' => Yii::t('app', 'Is Active'),
            'assortment_count' => Yii::t('app', 'Assortment Count'),
            'availability_count' => Yii::t('app', 'Availability Count'),
            'min_cost' => Yii::t('app', 'Min Cost'),
            'max_cost' => Yii::t('app', 'Max Cost'),
            'name' => Yii::t('app', 'Name'),
            'image' => Yii::t('app', 'Image'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesProducts()
    {
        return $this->hasMany(CompaniesProducts::className(), ['product_id' => 'id'])->inverseOf('product');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id'])->inverseOf('products');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(Companies::className(), ['id' => 'manufacturer_id'])->inverseOf('products');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdCountry()
    {
        return $this->hasOne(Entities::className(), ['id' => 'prod_country_id'])->inverseOf('products');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitType()
    {
        return $this->hasOne(Entities::className(), ['id' => 'unit_type_id'])->inverseOf('products0');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsCharacteristics()
    {
        return $this->hasMany(ProductsCharacteristics::className(), ['product_id' => 'id'])->inverseOf('product');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCharacteristics()
    {
        return $this->hasMany(Characteristics::className(), ['id' => 'characteristic_id'])->viaTable('{{%products_characteristics}}', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsRequests()
    {
        return $this->hasMany(ProductsRequests::className(), ['product_id' => 'id'])->inverseOf('product');
    }
}
