<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%characteristics}}".
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $unit_type_id
 * @property integer $is_multiple
 * @property integer $is_add_to_title
 * @property string $type
 * @property string $name
 * @property string $values
 *
 * @property Categories $category
 * @property Entities $unitType
 * @property ProductsCharacteristics[] $productsCharacteristics
 * @property Products[] $products
 */
class Characteristics extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%characteristics}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'name'], 'required'],
            [['category_id', 'unit_type_id', 'is_multiple', 'is_add_to_title'], 'integer'],
            [['type', 'values'], 'string'],
            [['name'], 'string', 'max' => 63],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['unit_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['unit_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category_id' => Yii::t('app', 'Category ID'),
            'unit_type_id' => Yii::t('app', 'Unit Type ID'),
            'is_multiple' => Yii::t('app', 'Is Multiple'),
            'is_add_to_title' => Yii::t('app', 'Is Add To Title'),
            'type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
            'values' => Yii::t('app', 'Values'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id'])->inverseOf('characteristics');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnitType()
    {
        return $this->hasOne(Entities::className(), ['id' => 'unit_type_id'])->inverseOf('characteristics');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductsCharacteristics()
    {
        return $this->hasMany(ProductsCharacteristics::className(), ['characteristic_id' => 'id'])->inverseOf('characteristic');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])->viaTable('{{%products_characteristics}}', ['characteristic_id' => 'id']);
    }
}
