<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%rates}}".
 *
 * @property integer $company_id
 * @property string $approve_rate
 * @property string $paid_rate
 * @property string $sm_reg_rate
 * @property string $le_reg_rate
 *
 * @property Companies $company
 */
class Rates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%rates}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id'], 'integer'],
            [['approve_rate', 'paid_rate', 'sm_reg_rate', 'le_reg_rate'], 'number'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => Yii::t('app', 'Company ID'),
            'approve_rate' => Yii::t('app', 'Approve Rate'),
            'paid_rate' => Yii::t('app', 'Paid Rate'),
            'sm_reg_rate' => Yii::t('app', 'Sm Reg Rate'),
            'le_reg_rate' => Yii::t('app', 'Le Reg Rate'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id'])->inverseOf('rates');
    }
}
