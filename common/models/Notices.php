<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%notices}}".
 *
 * @property integer $user_id
 * @property integer $messages
 * @property integer $actions
 * @property integer $offers
 * @property integer $claims
 * @property integer $specoffers
 * @property integer $purchases
 * @property integer $orders
 * @property integer $reviews
 *
 * @property Users $user
 */
class Notices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notices}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'messages', 'actions', 'offers', 'claims', 'specoffers', 'purchases', 'orders', 'reviews'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'messages' => Yii::t('app', 'Messages'),
            'actions' => Yii::t('app', 'Actions'),
            'offers' => Yii::t('app', 'Offers'),
            'claims' => Yii::t('app', 'Claims'),
            'specoffers' => Yii::t('app', 'Specoffers'),
            'purchases' => Yii::t('app', 'Purchases'),
            'orders' => Yii::t('app', 'Orders'),
            'reviews' => Yii::t('app', 'Reviews'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id'])->inverseOf('notices');
    }
}
