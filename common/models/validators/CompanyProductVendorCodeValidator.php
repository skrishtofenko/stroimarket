<?php
namespace common\models\validators;

use Yii;
use yii\validators\Validator;
use frontend\models\CompaniesProducts;
use frontend\models\forms\ProductForm;

class CompanyProductVendorCodeValidator extends Validator {
    public function init() {
        parent::init();
        $this->message = 'Товар с таким артикулом уже существует';
    }

    public function validateAttribute($model, $attribute) {
        /** @var $model ProductForm */
        if(!empty($model->vendorCode)) {
            if(CompaniesProducts::checkVendorCode($model->vendorCode, $model->id)) {
                $model->addError('vendorCode', $this->message);
            }
        }
    }

    public function clientValidateAttribute($model, $attribute, $view) {
        /** @var $model ProductForm */
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $existsProducts = CompaniesProducts::getVendorCodesList();
        $productsList = [];
        foreach ($existsProducts as $existsProduct) {
            $productsList[] = "'$existsProduct->vendor_code':$existsProduct->id";
        }
        $existsProducts = '{' . implode(',', $productsList) . '}';
        return <<<JS
            var vendorCode = $('#productform-vendorcode').val();
            var existsProducts = $existsProducts;
            if(vendorCode !== '' && typeof existsProducts[vendorCode] !== 'undefined' && existsProducts[vendorCode] !== parseInt($('#productform-id').val())) {
                if(messages.indexOf($message) === -1) {
                    messages.push($message);
                }
            }
JS;
    }
}
