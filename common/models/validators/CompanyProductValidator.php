<?php
namespace common\models\validators;

use Yii;
use yii\validators\Validator;
use frontend\models\forms\ProductForm;

class CompanyProductValidator extends Validator {
    public function init() {
        parent::init();
        $this->message = Yii::t('app', 'At least one of these fields is required');
    }

    public function validateAttribute($model, $attribute) {
        /** @var $model ProductForm */
        if(empty($model->name) && empty($model->vendorCode)) {
            $model->addError('name', $this->message);
            $model->addError('vendorCode', $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view) {
        /** @var $model ProductForm */
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return <<<JS
            var name = $('#productform-name');
            var vendorCode = $('#productform-vendorcode');
            if(name.val() === '' && vendorCode.val() === '') {
                if(messages.indexOf($message) === -1) {
                    messages.push($message);
                }
            }
JS;
    }
}
