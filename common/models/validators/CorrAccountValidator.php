<?php
namespace common\models\validators;

use Yii;
use yii\validators\Validator;
use common\models\extended\Requisites;

class CorrAccountValidator extends Validator {
    public function init() {
        parent::init();
        $this->message = Yii::t('app', 'Corr.Account must begin from 301 and end same 3 symbols as BIK');
    }

    public function validateAttribute($model, $attribute) {
        /** @var $model Requisites */
        if(substr($model->$attribute, 0, 3) != 301) {
            $model->addError('address', $this->message);
        }
        if($model->bik != '' && substr($model->$attribute, -3) != substr($model->bik, -3)) {
            $model->addError('address', $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view) {
        /** @var $model Requisites */
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return <<<JS
            if(value !== '') {
                if (value.substr(0, 3) !== "301" && messages.indexOf($message) === -1) {
                    messages.push($message);
                }
                if(messages.indexOf($message) === -1) {
                    var bik = $('#requisitesinfoform-bik').val();
                    if (bik !== '' && bik.substr(-3) !== value.substr(-3)) {
                        messages.push($message);
                    }
                }
            }
JS;
    }
}
