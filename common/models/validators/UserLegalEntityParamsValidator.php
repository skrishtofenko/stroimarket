<?php
namespace common\models\validators;

use Yii;
use yii\validators\Validator;
use frontend\models\forms\RegistrationStep2Form;

class UserLegalEntityParamsValidator extends Validator {
    public function init() {
        parent::init();
        $this->message = Yii::t('app', ' is required');
    }

    public function validateAttribute($model, $attribute) {
        /** @var $model RegistrationStep2Form */
        $message = $attribute . $this->message;
        if(!isset($model->isLegalEntity) || $model->isLegalEntity) {
            if(!$model->$attribute) {
                $model->addError($attribute, $message);
            }
        }
    }

    public function clientValidateAttribute($model, $attribute, $view) {
        /** @var $model RegistrationStep2Form */
        $message = $attribute . $this->message;
        $message = json_encode($message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return <<<JS
            var isLegalEntity = $('#registrationstep2form-islegalentity');
            if(!isLegalEntity.length || isLegalEntity.prop('checked')) {
                if(value === '') {
                    if(messages.indexOf($message) === -1) {
                        messages.push($message);
                    }
                }
            }
JS;
    }
}
