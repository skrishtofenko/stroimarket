<?php
namespace common\models\validators;

use Yii;
use yii\validators\Validator;
use frontend\models\forms\CompanyInfoForm;

class CompanyAddressValidator extends Validator {
    public function init() {
        parent::init();
        $this->message = Yii::t('app', 'Address is required');
    }

    public function validateAttribute($model, $attribute) {
        /** @var $model CompanyInfoForm */
        if(!!$model->ownership_type_id && !$model->is_imonly && empty($model->$attribute)) {
            $model->addError($attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view) {
        /** @var $model CompanyInfoForm */
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        $isOwnership = !!$model->ownership_type_id;
        $isImOnly = $model->is_imonly ? 'true' : 'false';
        return <<<JS
            var attributeField = $('#companyinfoform-$attribute');
            var isImOnly = $('#companyinfoform-is_imonly');
            isImOnly = isImOnly.length ? isImOnly.prop('checked') : $isImOnly;
            if($isOwnership && !isImOnly && attributeField.val() === '') {
                if(messages.indexOf($message) === -1) {
                    messages.push($message);
                }
            }
JS;
    }
}
