<?php
use yii\helpers\Html;
use common\models\extended\Catalogs;

/* @var $this yii\web\View */
/* @var $catalog Catalogs */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($catalog->company->user->fio)?>!</p>
    <p>Обработка каталога, загруженного вами для компании <?=$catalog->company->user->legal_name?> на портале Строймаркет, завершена.</p>
    <p>Подробный отчет об обработке вы можете посмотреть <a href="http://uploads.<?=Yii::$app->params['mainUrl']?><?=Catalogs::generateFilePath($catalog->file_name, true)?><?=explode('.', $catalog->file_name)[0]?>.txt">по ссылке</a></p>
    <p>Желаем вам приятного и прибыльного использования портала!</p>
</div>
