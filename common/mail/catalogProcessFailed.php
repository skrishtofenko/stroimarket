<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $catalog common\models\extended\Catalogs */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($catalog->company->user->fio)?>!</p>
    <p>К сожалению, обработка каталога, загруженного вами для компании <?=$catalog->company->user->legal_name?> на портале Строймаркет, не удалась.</p>
    <p>Пожалуйста, повторите загрузку файла в личном кабинете. Если ошибка повторится, обратитесь к администратору портала</p>
    <p>Желаем вам приятного и прибыльного использования портала!</p>
</div>
