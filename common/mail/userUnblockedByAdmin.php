<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\extended\Users */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($user->fio)?>,</p>
    <p>Администратор портала Stroimarket.ru разблокировал ваш аккаунт</p>
    <p>Вы снова можете пользоваться порталом, как и раньше. Ваш логин и пароль остались прежними</p>
</div>
