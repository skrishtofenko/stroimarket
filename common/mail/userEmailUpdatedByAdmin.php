<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\extended\Users */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($user->fio)?>,</p>
    <p>Администратор портала Stroimarket.ru изменил ваш логин в системе</p>
    <p>Вы можете войти, используя новый email <?=Html::encode($user->email)?> в качестве логина и ваш старый пароль</p>
</div>
