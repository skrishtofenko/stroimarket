<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\extended\Users */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($user->fio)?>,</p>
    <p>Аккаунт вашей компании на торговой интернет-площадке «Строймаркет» успешно создан.</p>
    <p>Вы можете войти в личный кабинет компании по ссылке <a href="http://mystroymarket.ru/login">mystroymarket.ru</a>, используя ранее указанный вами email и автоматически сгенерированный пароль <?=Html::encode($user->clearPassword)?>.</p>
    <p>Желаем приятного и прибыльного использования возможностей площадки.</p>
</div>
