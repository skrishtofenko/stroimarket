<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\extended\Users */
?>
<div class="password-reset">
    <p>Здравствуйте, Admin!</p>
    <p>Пользователь <?=Html::encode($user->fio)?> (<?=Html::encode($user->email)?>) загрузил скан документа и выставил счет на оплату регистрации.</p>
    <p>Вы можете подтвердить или отклонить его аккаунт в <?=Html::a('профиле пользователя', ["http://admin.{$_SERVER['HTTP_HOST']}/users/view", 'id' => $user->id])?> в админке</p>
</div>
