<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\extended\Users */
/* @var $message string */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($user->fio)?>,</p>
    <p>К сожалению, администратор портала Stroimarket.ru отклонил ваш аккаунт</p>
    <p>Комментарий администратора:</p>
    <p><b><?=$message?></b></p>
    <p>Для подробностей, свяжитесь с администратором портала</p>
</div>
