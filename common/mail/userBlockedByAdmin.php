<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\extended\Users */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($user->fio)?>,</p>
    <p>Администратор портала Stroimarket.ru заблокировал ваш аккаунт</p>
    <p>Вы не сможете авторизоваться и пользоваться порталом</p>
    <p>Для подробностей, свяжитесь с администратором портала</p>
</div>
