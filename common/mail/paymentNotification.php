<?php
use yii\helpers\Html;
use common\helpers\SKHelper;

/* @var $this yii\web\View */
/* @var $company common\models\extended\Companies */
/* @var $remainingPaidDays integer */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($company->user->fio)?>!</p>
    <p>Для вашей компании <?=$company->user->name?> (<?=$company->addressAsText?>) через <?=$remainingPaidDays?> <?=SKHelper::ending($remainingPaidDays, ['день', 'дня', 'дней'])?> заканчивается оплаченный период пользования.</p>
    <p>Если вы планируете продолжить пользование порталом, вам нужно оплатить доступ. Вы можете сделать это в своем профиле.</p>
    <p>Желаем вам приятного и прибыльного использования портала!</p>
</div>
