<?php
use yii\helpers\Html;
use common\models\extended\Catalogs;

/* @var $this yii\web\View */
/* @var $catalog Catalogs */
?>
<div class="password-reset">
    <p>Здравствуйте, Admin!</p>
    <p>К сожалению, обработка каталога, загруженного пользователем <?=$catalog->company->user->legal_name?> завершилась с ошибкой.</p>
    <p>Возможно, требуется ваше внимание</p>
    <p><a href="http://uploads.<?=Yii::$app->params['mainUrl']?><?=Catalogs::generateFilePath($catalog->file_name, true)?><?=$catalog->file_name?>">Загруженный файл</a></p>
    <p><a href="http://uploads.<?=Yii::$app->params['mainUrl']?><?=Catalogs::generateFilePath($catalog->file_name, true)?><?=explode('.', $catalog->file_name)[0]?>.txt">Отчет об обработке</a></p>

    <p>Желаем вам приятного и прибыльного использования портала!</p>
</div>
