<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\extended\Users */
/* @var $product common\models\extended\CompaniesProducts */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($user->fio)?>!</p>
    <p>Вы подписались на уведомления об акциях в категории <?=$product->product->category->name?></p>
    <p>Один из поставщиков объявил акцию на некоторые товары в этой категории.</p>
    <p>Ознакомиться с акциями вы можете <a href="<?="http://{$_SERVER['HTTP_HOST']}/catalog/{$product->product->category_id}"?>">по ссылке</a>.</p>
    <p>Желаем вам приятного и прибыльного использования портала!</p>
</div>
