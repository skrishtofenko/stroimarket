<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\extended\Users */
?>
<div class="password-reset">
    <p>Здравствуйте, <?=Html::encode($user->fio)?>,</p>
    <p>Ваш аккаунт добавлен на портал Stroimarket.ru:</p>
    <p>Вы можете войти, используя ваш email в качестве логина и пароль <?=Html::encode($user->clearPassword)?></p>
</div>
