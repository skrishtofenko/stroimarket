<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\extended\Users */
?>
<div class="password-reset">
    <p>Здравствуйте, Admin!</p>
    <p>На портале Stroimarket.ru зарегистрировался новый пользователь <?=Html::encode($user->fio)?> (<?=Html::encode($user->email)?>).</p>
    <p>Посмотреть <?=Html::a('профиль пользователя', ["http://admin.{$_SERVER['HTTP_HOST']}/users/view", 'id' => $user->id])?> в админке</p>
</div>
