<?php
namespace common\helpers;

use Yii;
use common\models\extended\Users;
use common\models\extended\Companies;
use common\models\extended\Deliveries;
use common\models\extended\Vocabularies;

class SMHelper {
    public static $scheduleAttributes = [
        'monFrom', 'monTo', 'monDinnerFrom', 'monDinnerTo',
        'tueFrom', 'tueTo', 'tueDinnerFrom', 'tueDinnerTo',
        'wedFrom', 'wedTo', 'wedDinnerFrom', 'wedDinnerTo',
        'thuFrom', 'thuTo', 'thuDinnerFrom', 'thuDinnerTo',
        'friFrom', 'friTo', 'friDinnerFrom', 'friDinnerTo',
        'satFrom', 'satTo', 'satDinnerFrom', 'satDinnerTo',
        'sunFrom', 'sunTo', 'sunDinnerFrom', 'sunDinnerTo',
    ];

    public static function renderCompanyPhones($phones) {
        if(!empty($phones)) {
            foreach($phones as &$phone) {
                $phone = SKHelper::formatPhone($phone);
            }
            return implode('<br />', $phones);
        }
        return '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>';
    }

    public static function renderCompanyEmails($emails) {
        if(!empty($emails)) {
            foreach($emails as &$email) {
                $email = SKHelper::formatEmail($email);
            }
            return implode('<br />', $emails);
        }
        return '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>';
    }

    public static function renderCompanyAddress(Companies $company) {
        $address = [];
        foreach(['street', 'house', 'room_type_id', 'room_number'] as $addressComponent) {
            $value = $company->$addressComponent;
            if($addressComponent == 'room_type_id' && $company->$addressComponent && isset(Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_ROOMS]['items'][$company->$addressComponent])) {
                $value = Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_ROOMS]['items'][$company->$addressComponent];
            }
            $address[] = '<b>' . Yii::t('app', $company->attributeLabels()[$addressComponent]) . ':</b> '
                . $value ?: ('<span class="not-set">' . Yii::t('app', '(not set)') . '</span>');
        }
        return implode('<br />', $address);
    }

    public static function renderCompanySchedule(Companies $company) {
        $result = [];
        foreach(SKHelper::getDaysOfWeek() as $short => $full) {
            $from = "{$short}From";
            $to = "{$short}To";
            $dinnerfrom = "{$short}DinnerFrom";
            $dinnerTo = "{$short}DinnerTo";
            $string = '<b>' . Yii::t('app', $full) . ':</b> ';
            if(!empty($company->$from) && !empty($company->$to)) {
                $string .= Yii::t('app', 'From')
                    . " {$company->$from} "
                    . Yii::t('app', 'To')
                    . " {$company->$to} <small><i>(";
                if(!empty($company->$dinnerfrom) && !empty($company->$dinnerTo)) {
                    $string .= Yii::t('app', 'Dinner') . ' '
                        . Yii::t('app', 'From')
                        . " {$company->$dinnerfrom} "
                        . Yii::t('app', 'To')
                        . " {$company->$dinnerTo}";
                } else {
                    $string .= Yii::t('app', 'Without Dinner');
                }
                $string .= ')</i></small>';
            } else {
                $string .= Yii::t('app', 'Output');
            }

            $result[] = $string;
        }
        return implode('<br />', $result);
    }

    public static function renderCompanyDeliveries(Companies $company) {
        $result = [];
        if(!empty($company->deliveries)) {
            foreach ($company->deliveries as $delivery) {
                /* @var $model \backend\models\Deliveries */
                $result[] = "<b>{$delivery->city->name}:</b> " . Deliveries::types()[$delivery->type];
            }
        } else {
            $result[] = Yii::t('app', '(not set)');
        }
        return implode('<br />', $result);
    }

    public static function renderUserStatus($user) {
        /** @var $user Users */
        $statusText = Users::statuses()[$user->status];
        $statusClass = '';
        switch ($user->status) {
            case Users::STATUS_ACTIVE:
                $statusClass = 'message_green';
                break;
            case Users::STATUS_DECLINED: case Users::STATUS_BLOCKED:
            $statusClass = 'message_italic';
            break;
            case Users::STATUS_REQUESTED:
                $statusClass = 'message_red';
                break;
        }
        return $statusClass ? "<span class='$statusClass'>$statusText</span>" : $statusText;
    }

    public static function companyTypeInGenitive($type) {
        switch ($type) {
            case Companies::TYPE_PROVIDER:
                return 'поставщика';
            case Companies::TYPE_STORE:
                return 'магазина';
            case Companies::TYPE_PICKER:
                return 'оптового покупателя';
        }
        return '';
    }

    public static function makeColumnClasses($leftColumnClass) {
        $columns = explode('-', $leftColumnClass);
        return [
            'left' => $leftColumnClass,
            'right' => ((int) $columns[1] - (int) $columns[0]) . '-' . $columns[1],
        ];
    }

    public static function getCompanyScheduleValues(Companies $company) {
        $days = [
            'mon' => 'пн',
            'tue' => 'вт',
            'wed' => 'ср',
            'thu' => 'чт',
            'fri' => 'пт',
            'sat' => 'сб',
            'sun' => 'вс',
        ];
        $result = [];
        foreach ($days as $eng => $ru) {
            $fromField = "{$eng}From";
            $toField = "{$eng}To";
            $dinnerFromField = "{$eng}DinnerFrom";
            $dinnerToField = "{$eng}DinnerTo";
            $headerClasses = [];
            $classes = [];
            $dinnerClasses = [];
            $isEmpty = $isDinnerEmpty = false;
            if(strtolower(date('D')) == $eng) {
                $classes[] = '_is-current';
                $headerClasses[] = '_is-current';
                $dinnerClasses[] = '_is-current';
            }
            if(empty($company->$fromField) || empty($company->$toField)) {
                $classes[] = '_is-empty';
                $headerClasses[] = 'day-off';
                $dinnerClasses[] = '_is-empty';
                $isEmpty = $isDinnerEmpty = true;
            } else {
                if(empty($company->$dinnerFromField) || empty($company->$dinnerToField)) {
                    $dinnerClasses[] = '_is-empty';
                    $isDinnerEmpty = true;
                }
            }
            $result[$eng] = [
                'from' => $company->$fromField,
                'to' => $company->$toField,
                'dinnerFrom' => $company->$dinnerFromField,
                'dinnerTo' => $company->$dinnerToField,
                'classes' => [
                    'header' => $headerClasses,
                    'day' => $classes,
                    'dinner' => $dinnerClasses,
                ],
                'isEmpty' => $isEmpty,
                'isDinnerEmpty' => $isDinnerEmpty,
                'ru' => $ru,
            ];
        }
        return $result;
    }
}