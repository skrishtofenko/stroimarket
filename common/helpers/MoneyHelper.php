<?php
namespace common\helpers;

use Yii;

class MoneyHelper {
    public static function intToMoney($count) {
        return $count / 100;
    }

    public static function moneyToInt($count) {
        return $count * 100;
    }
}