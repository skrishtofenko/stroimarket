<?php
namespace common\helpers;

class DateHelper {
    private static $shortEMonthToFullR = [
        'Jan' => 'января',
        'Feb' => 'февраля',
        'Mar' => 'марта',
        'Apr' => 'апреля',
        'May' => 'мая',
        'Jun' => 'июня',
        'Jul' => 'июля',
        'Aug' => 'августа',
        'Sep' => 'сентября',
        'Oct' => 'октября',
        'Nov' => 'ноября',
        'Dec' => 'декабря',
    ];

    public static function rusDate($date) {
        $date = strtotime($date);
        return date('d ', $date) . (self::$shortEMonthToFullR[date('M', $date)]) . date(' Y', $date);
    }
}