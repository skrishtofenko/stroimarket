<?php
namespace common\helpers;

use Yii;

class SKHelper {
    public static function formatPhone($phone) {
        if(!empty($phone)) {
            $code = substr($phone, 0, 3);
            $first3 = substr($phone, 3, 3);
            $second2 = substr($phone, 6, 2);
            $last2 = substr($phone, 8);
            return "+7 ($code) $first3-$second2-$last2";
        }
        return '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>';
    }

    public static function formatEmail($email) {
        if(!empty($email)) {
            return "<a href='mailto:$email' target='_blank'>$email</a>";
        }
        return '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>';
    }

    public static function getBooleanFilter() {
        return [1 => 'Да', 0 => 'Нет'];
    }

    public static function getDaysOfWeek() {
        return [
            'mon' => 'Monday',
            'tue' => 'Tuesday',
            'wed' => 'Wednesday',
            'thu' => 'Thursday',
            'fri' => 'Friday',
            'sat' => 'Saturday',
            'sun' => 'Sunday',
        ];
    }

    public static function smartCut($text, $length = 100) {
        $text = trim($text);
        if(mb_strlen($text, 'utf-8') > $length) {
            $end = mb_strpos($text, ' ', $length, 'utf-8');
            $end = !$end || $end > ($length * 1.1) ? $length : $end;
            return mb_substr($text, 0, $end, 'utf-8') . '...';
        } else {
            return $text;
        }
    }

    public static function costToStr($cost) {
        $zero = 'ноль';
        $ranks = [
            ['миллиард', 'миллиарда', 'миллиардов'],
            ['миллион', 'миллиона', 'миллионов'],
            ['тысяча', 'тысячи', 'тысяч'],
        ];
        $units = [
            'kop' => ['копейка' ,'копейки' ,'копеек'],
            'rub' => ['рубль'   ,'рубля'   ,'рублей'],
        ];
        $result = '';

        list($rub, $kop) = explode('.', sprintf("%015.2f", (float) $cost));
        if((int) $rub > 0) {
            $rubbles = [];
            foreach (str_split($rub,3) as $rank => $value) {
                $value = (int) $value;
                if(!$value) {
                    continue;
                }
                $subResult = self::hundredsToStr($value, (int) ($rank == 2));
                if($rank < 3) {
                    $subResult .= (' ' . self::ending($value, $ranks[$rank]));
                }
                $rubbles[] = $subResult;
            }
            $result = implode(' ', $rubbles);
        } else {
            $result = $zero;
        }
        $result .= (' ' . self::ending($rub, $units['rub']));
        $result .= (' ' . $kop . ' ' . self::ending($kop, $units['kop']));
        return $result;
    }

    public static function hundredsToStr($value, $gender = 0) {
        $digits = [
            ['', 'один', 'два', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
            ['', 'одна', 'две', 'три', 'четыре', 'пять', 'шесть', 'семь', 'восемь', 'девять'],
        ];
        $tens = [2 => 'двадцать', 'тридцать', 'сорок', 'пятьдесят', 'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто'];
        $hundreds = ['', 'сто', 'двести', 'триста', 'четыреста', 'пятьсот', 'шестьсот', 'семьсот', 'восемьсот', 'девятьсот'];
        $teens = ['десять', 'одиннадцать', 'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать', 'шестнадцать', 'семьнадцать', 'восемьнадцать', 'девятнадцать'];
        $result = [];
        $value %= 1000;

        if($hundred = (int) ($value / 100)) {
            $result[] = $hundreds[$hundred];
        }
        $value = $value % 100;
        if($value) {
            if ($value > 9 && $value < 20) {
                $result[] = $teens[$value % 10];
            } else {
                if($ten = (int) ($value / 10)) {
                    $result[] = $tens[$ten];
                }
                if($digit = $value % 10) {
                    $result[] = $digits[$gender][$digit];
                }
            }
        }
        return implode(' ', $result);
    }

    public static function ending($cnt, $endings) {
        if(($cnt / 1) > 0 && ($cnt / 1) < 1) {
            return $endings[1];
        }
        $cnt %= 100;
        if($cnt <= 10 || $cnt > 20) {
            $cnt %= 10;
            switch ($cnt) {
                case 1:
                    return $endings[0];
                case 2: case 3: case 4:
                    return $endings[1];
                default:
                    return $endings[2];
            }
        } else {
            return $endings[2];
        }
    }

    public static function clearText($text) {
        $text = strip_tags($text);
        $text = str_replace('&nbsp;', ' ', $text);
        $text = preg_replace("/(\s){2,}/", "</p><p>", $text);
//        $text = nl2br($text);
        return '<p>' . $text . '</p>';
    }
}