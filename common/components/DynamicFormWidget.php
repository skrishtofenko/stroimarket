<?php
namespace common\components;

use Symfony\Component\DomCrawler\Crawler;

class DynamicFormWidget extends \wbraganca\dynamicform\DynamicFormWidget {
    protected function removeItems($content) {
        $crawler = new Crawler();
        $crawler->addHTMLContent($content, \Yii::$app->charset);
        $crawler->filter($this->widgetItem)->each(function ($nodes) {
            foreach ($nodes as $node) {
                $node->parentNode->removeChild($node);
            }
        });
        return $crawler->html();
    }
}
