<?php
namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use common\models\extended\Vocabularies;
use common\models\extended\Entities;

class InitVocabularies extends Component {
    public function init() {
        if (Yii::$app->db->schema->getTableSchema(Vocabularies::tableName()) !== null) {
            $vocabularies = Vocabularies::getMap();
            foreach ($vocabularies as &$vocabulary) {
                $vocabulary['items'] = Entities::getList('id', 'name', ['vocabulary_id' => $vocabulary['id']]);
            }
            Yii::$app->params['vocabulariesFront'] = ArrayHelper::index($vocabularies, 'alias');
            $vocabularies[Vocabularies::VOCABULARY_LEGAL_ENTITIES]['items'][0] = Yii::t('app', 'Individual');
            Yii::$app->params['vocabularies'] = $vocabularies;
        }
        parent::init();
    }
}