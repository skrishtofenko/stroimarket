<?php
namespace common\components;

class FlashMessages {
    const FLASH_SUCCESS = 'success';
    const FLASH_FAILED = 'fail';
    const FLASH_INFO = 'info';

    const FLASH_TYPES = [
        self::FLASH_SUCCESS => 'Успешно!',
        self::FLASH_FAILED => 'Ошибка!',
        self::FLASH_INFO => 'Информация',
    ];
}