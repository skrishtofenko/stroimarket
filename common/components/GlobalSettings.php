<?php
namespace common\components;

use Yii;
use yii\base\Component;
use common\models\extended\Settings;

class GlobalSettings extends Component {
    public function init() {
        if (Yii::$app->db->schema->getTableSchema(Settings::tableName()) !== null) {
            Yii::$app->params['globalSettings'] = Settings::getList('alias', 'value');
        }
        parent::init();
    }
}