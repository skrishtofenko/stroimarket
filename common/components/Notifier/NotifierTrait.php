<?php
namespace common\components\Notifier;

use Yii;

trait NotifierTrait {
    private static function commonTemplates() {
        return [
            'default' => [
                'mail_template' => 'default',
                'mail_subject' => Yii::$app->name . ' feedback',
                'sms_template' => 'Please, tell us, that you got this message',
            ],
        ];
    }

    public function notify($template, $params = [], $email = false) {
        $email = $email ?: $this->email;
        $templates = self::getNotifyTemplates($template);
        if (!empty($email) && $templates['mail_template']) {
            return self::mail($templates['mail_template'], $email, $templates['mail_subject'], $params);
        } elseif (!empty($this->phone) && $templates['sms_template']) {
            return self::sms($this->phone, $templates['sms_template'], $params);
        }
        return false;
    }

    public static function email($eventType, $to, $params = [], $contents = []) {
        $templates = self::getNotifyTemplates($eventType);
        return self::sendEmail($templates['mail_template'], $to, $templates['mail_subject'], $params, $contents);
    }

    private static function getNotifyTemplates($type) {
        $commonTemplates = self::commonTemplates();
        if (is_callable('self::notifyTemplates')) {
            $commonTemplates = array_merge($commonTemplates, self::notifyTemplates());
        }
        return isset($commonTemplates[$type]) ? $commonTemplates[$type] : $commonTemplates['default'];
    }

    private static function sendEmail($template, $to, $subject, $templateParams, $contents = []) {
        $mail = \Yii::$app->mailer
            ->compose($template, $templateParams)
            ->setTo($to)
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->name])
            ->setSubject($subject);
        foreach ($contents as $content) {
            if(isset($content['content'])) {
                $attachment = $content['content'];
                unset($content['content']);
                $mail->attachContent($attachment, $content);
            }
        }
        return $mail->send();

    }

//    public static function sms($to, $template, $templateParams = []) {
//        $templateParams = self::parametrizeTemplateParams($templateParams);
//        $smsText = str_replace(array_keys($templateParams), $templateParams, $template);
//        $to = "+$to";
//        return self::sendSms($to, $smsText);
//    }
//
//    private static function parametrizeTemplateParams($params) {
//        $newParams = [];
//        foreach ($params as $name => $value) {
//            $newParams["%$name%"] = $value;
//        }
//        return $newParams;
//    }
//
//    private static function sendSms($to, $smsText) {
//        $smsParams = Yii::$app->params['smsc'];
//        $ch = curl_init(
//            "https://smsc.ru/sys/send.php?login={$smsParams['login']}&psw={$smsParams['password']}&phones=$to&mes=$smsText&charset=utf-8&sender={$smsParams['sender']}"
//        );
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
//        $response = curl_exec($ch);
//        curl_close($ch);
//        return (bool)preg_match('#^OK#', $response);
//    }
}