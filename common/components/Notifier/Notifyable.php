<?php
namespace common\components\Notifier;

interface Notifyable {
    public static function notifyTemplates();
}