<?php
namespace common\components\BillGenerator;

use common\helpers\DateHelper;
use common\helpers\SKHelper;
use Yii;
use Dompdf\Dompdf;
use common\models\extended\Bills;

class BillGenerator {
    public static function generateBill(Bills $bill) {
        $params = self::generateParams($bill);
        $html = file_get_contents(Yii::getAlias('@common/components/BillGenerator/bill.html'));
        $html = str_replace(array_keys($params), $params, $html);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4');
        $dompdf->render();
        $dompdf->stream("stroimarket_bill_{$bill->id}.pdf");
    }

    private static function generateParams(Bills $bill) {
        return array_merge([
            '%logoSrc%' => Yii::getAlias('@common/components/BillGenerator/logo.png'),
            '%recipientAddress%' => Yii::$app->params['globalSettings']['smAddress'],
            '%recipientBookkeepingPhone%' => Yii::$app->params['globalSettings']['smBKPhone'],
            '%recipientBookkeepingFax%' => Yii::$app->params['globalSettings']['smBKFax'],
            '%recipientBookkeepingEmail%' => Yii::$app->params['globalSettings']['smBKEmail'],
            '%recipientTechDeptPhone%' => Yii::$app->params['globalSettings']['smTechPhone'],
            '%recipientTechDeptFax%' => Yii::$app->params['globalSettings']['smTechFax'],
            '%recipientTechDeptEmail%' => Yii::$app->params['globalSettings']['smTechEmail'],
            '%recipientUrl%' => Yii::$app->params['globalSettings']['smUrl'],
            '%billId%' => $bill->id,
            '%billDate%' => DateHelper::rusDate($bill->created_at),
            '%recipientRequisites%' => self::generateRecipientBlock(),
            '%vendorRequisites%' => self::generateVendorBlock($bill),
            '%billContent%' => '',
            '%billTotal%' => '',
            '%billTotalCount%' => '',
            '%billTotalWords%' => '',
            '%recipientTitle%' => Yii::$app->params['globalSettings']['smTitle'],
            '%recipientDirector%' => Yii::$app->params['globalSettings']['smDirector'],
            '%recipientBookkeeper%' => Yii::$app->params['globalSettings']['smBKName'],
            '%signSrc%' => Yii::getAlias('@common/components/BillGenerator/sign.png'),
        ], self::generateBillContent($bill));
    }

    private static function generateBillContent(Bills $bill) {
        $billContent = [];
        $billTotalCount = 0;
        $billTotal = 0;
        $tdStyle = 'padding: 0 3px; border-bottom: 1px solid black; border-right: 1px solid black;';
        $tdStyleRight = "style='$tdStyle text-align: right;'";
        $tdStyle = "style='$tdStyle'";
        switch ($bill->type) {
            case Bills::TYPE_REGISTRATION:
                $billContent[] =
                    "<td $tdStyle>1.</td>"
                    . "<td $tdStyle>" . Yii::$app->params['globalSettings']['regServTitle'] . "</td>"
                    . "<td $tdStyleRight><nobr>{$bill->cost}.00</nobr></td>"
                    . "<td $tdStyleRight><nobr>Без НДС*</nobr></td>"
                    . "<td $tdStyleRight><nobr>-</nobr></td>"
                    . "<td $tdStyleRight><nobr>{$bill->cost}.00</nobr></td>";
                $billTotalCount++;
                $billTotal += $bill->cost;
                break;
            case Bills::TYPE_SUBSCRIPTION:
                $billContent[] =
                    "<td $tdStyle>1.</td>"
                    . "<td $tdStyle>" . Yii::$app->params['globalSettings']['subsServTitle'] . "</td>"
                    . "<td $tdStyleRight><nobr>{$bill->cost}.00</nobr></td>"
                    . "<td $tdStyleRight><nobr>Без НДС*</nobr></td>"
                    . "<td $tdStyleRight><nobr>-</nobr></td>"
                    . "<td $tdStyleRight><nobr>{$bill->cost}.00</nobr></td>";
                $billTotalCount++;
                $billTotal += $bill->cost;
                break;
        }
        return [
            '%billContent%' => !empty($billContent) ? '<tr>' . implode('</tr><tr>', $billContent) . '</tr>' : '',
            '%billTotal%' => $billTotal . '.00',
            '%billTotalCount%' => $billTotalCount,
            '%billTotalWords%' => SKHelper::costToStr($billTotal),
        ];
    }

    private static function generateRecipientBlock() {
        return implode('<br />', [
            Yii::$app->params['globalSettings']['smTitle'],
            'ИНН ' . Yii::$app->params['globalSettings']['smINN'],
            'КПП ' . Yii::$app->params['globalSettings']['smKPP'],
            'р/с ' . Yii::$app->params['globalSettings']['smAccount'],
            Yii::$app->params['globalSettings']['smBank'],
            'к/с ' . Yii::$app->params['globalSettings']['smCorrAccount'],
            'БИК ' . Yii::$app->params['globalSettings']['smBIK'],
            'ОКПО ' . Yii::$app->params['globalSettings']['smOKPO'],
            'ОГРН ' . Yii::$app->params['globalSettings']['smOGRN'],
        ]);
    }

    private static function generateVendorBlock(Bills $bill) {
        $result = [
            $bill->requisite->user->getLegalEntityVcard(),
            'ИНН ' . $bill->requisite->user->inn,
            'КПП ' . $bill->requisite->user->kpp,
            'ОГРН ' . $bill->requisite->user->ogrn,
            SKHelper::formatPhone($bill->requisite->user->phone),
        ];
        return implode('<br />', $result);
    }
}