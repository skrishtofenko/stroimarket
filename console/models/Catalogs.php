<?php
namespace console\models;

use frontend\models\Admins;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class Catalogs extends \common\models\extended\Catalogs {
    const CHECKING_FILE = 'catalog_parse_iteration_started';
    const ITERATION_LIFETIME_IN_SECONDS = 300;
    const MAX_TRIES_COUNT = 3;
    const AVAILABLE_FILE_TYPES = [
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'Xlsx',
        'application/vnd.ms-excel' => 'Xls',
    ];
    const READER_CLASS_NAMESPACCE = 'PhpOffice\\PhpSpreadsheet\\Reader\\';

    private $logFile = null;

    public static function process() {
        if(self::startIsPossible()) {
            if($catalog = self::getNextCatalog()) {
                $fileContent = $catalog->getFileContent();
                if(empty($fileContent['error'])) {
                    $fieldsAliases = ArrayHelper::getColumn($catalog->company->autoAssortmentFields(), 'field');
                    $needleFields = array_keys($fieldsAliases);
                    if($fileContent['headers'] == $needleFields) {
                        if(!empty($fileContent['rows'])) {
                            $i = 3;
                            foreach($fileContent['rows'] as $row) {
                                $row = array_combine($fieldsAliases, $row);
                                $row['company_id'] = $catalog->company_id;
                                $catalog->parseLog("Строка $i: " . CompaniesProducts::tryToUpdateOrCreate($row));
                                $i++;
                            }
                            $catalog->completeProcess();
                        } else {
                            $catalog->parseLog('В файле не обнаружено ни одного товара');
                        }
                    } else {
                        $catalog->parseLog('Неправильный набор полей в загруженном файле. Ожидаемый набор полей (названия полей должны совпадать с точностью до знака): ' . implode(', ', $needleFields));
                    }
                } else {
                    $catalog->parseLog($fileContent['error']);
                }
            }
        }
    }

    private static function startIsPossible() {
        $iterationFlagFileName = Yii::getAlias('@runtime') . DIRECTORY_SEPARATOR . self::CHECKING_FILE;
        if(file_exists($iterationFlagFileName)) {
            if(((int)microtime(true)) - filemtime($iterationFlagFileName) > self::ITERATION_LIFETIME_IN_SECONDS) {
                unlink($iterationFlagFileName);
                return true;
            }
            return false;
        }
        return true;
    }

    private static function getNextCatalog() {
        /** @var Catalogs $catalog */
        $catalog = self::find()
            ->where(['is_processed' => self::STATUS_NEW])
            ->orderBy(['created_at' => SORT_ASC])
            ->limit(1)
            ->one();
        if($catalog) {
            if($catalog->tries_count >= self::MAX_TRIES_COUNT) {
                $catalog->completeProcess(false);
                return self::getNextCatalog();
            }
            $catalog->updateAttributes(['tries_count' => $catalog->tries_count + 1]);
            return $catalog;
        }
        return false;
    }

    private function getFileContent() {
        $filePath = self::generateFilePath($this->file_name) . $this->file_name;
        $result = [
            'headers' => [],
            'rows' => [],
            'error' => '',
        ];
        if(file_exists($filePath)) {
            $fileType = mime_content_type($filePath);
            if(isset(self::AVAILABLE_FILE_TYPES[$fileType])) {
                $readerClass = self::READER_CLASS_NAMESPACCE . self::AVAILABLE_FILE_TYPES[$fileType];
                /** @var Xlsx|Xls $reader */
                $reader = new $readerClass();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($filePath);
                $worksheet = $spreadsheet->getActiveSheet();
                $commentsPassed = false;
                foreach($worksheet->getRowIterator() as $row) {
                    $cellIterator = $row->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(false);
                    if(empty($result['headers'])) {
                        foreach ($cellIterator as $cell) {
                            $result['headers'][] = $cell->getValue();
                        }
                    } else {
                        if($commentsPassed) {
                            $row = [];
                            foreach ($cellIterator as $cell) {
                                $row[] = $cell->getValue();
                            }
                            $result['rows'][] = $row;
                        } else {
                            $commentsPassed = true;
                        }
                    }
                }
            } else {
                $result['error'] = 'Incorrect file type. Available xlsx or xls.';
            }
        } else {
            $result['error'] = 'File not found';
        }
        return $result;
    }

    private function parseLog($logText) {
        if(!$this->logFile) {
            $logFileName = self::generateFilePath($this->file_name) . explode('.', $this->file_name)[0] . '.txt';
            if(file_exists($logFileName)) {
                unlink($logFileName);
            }
            $this->logFile = fopen($logFileName, 'a');
        }
        $logText = mb_convert_encoding ( $logText, 'windows-1251');
        fwrite($this->logFile, "$logText\n");
    }

    private function completeProcess($isSuccess = true) {
        $processedStatus = self::STATUS_ERRORED;
        $mailTemplate = 'Failed';
        if($isSuccess) {
            $this->company->updateAttributes(['catalog_updated_at' => new Expression('NOW()')]);
            $processedStatus = self::STATUS_COMPLETED;
            $mailTemplate = 'Success';
        }
        $this->updateAttributes(['is_processed' => $processedStatus]);
        if(!$this->is_by_admin) {
            $this->company->user->email(
                "catalogProcess$mailTemplate",
                $this->company->user->email,
                ['catalog' => $this]
            );
            if ($processedStatus == self::STATUS_ERRORED) {
                Admins::email('adminCatalogProcessFailed', Yii::$app->params['adminEmail'], ['catalog' => $this]);
            }
        } else {
            Admins::email("catalogProcess$mailTemplate", Yii::$app->params['adminEmail'], ['catalog' => $this]);
        }
    }

    public function __destruct() {
        if($this->logFile) {
            fclose($this->logFile);
        }
    }
}
