<?php
namespace console\models;

use Yii;
use common\models\extended\Vocabularies;

class CompaniesProducts extends \common\models\extended\CompaniesProducts {
    const STATUS_NEW = 0;
    const STATUS_UPDATED = 1;
    const STATUS_CREATE_ERROR = 2;
    const STATUS_UPDATE_ERROR = 3;

    public static function tryToUpdateOrCreate($attributes) {
        $product = null;
        if(!empty($attributes['vendor_code'])) {
            $product = self::findOne(['vendor_code' => $attributes['vendor_code'], 'company_id' => $attributes['company_id']]);
        } elseif(!empty($attributes['product.fullName.name'])) {
            $product = self::findOne(['name' => $attributes['product.fullName.name'], 'company_id' => $attributes['company_id']]);
        } else {
            return 'ОШИБКА! Обязательно должно быть указано название товара или артикул';
        }
        $status = self::STATUS_UPDATED;
        if(!$product) {
            if(!empty($attributes['product_id'])) {
                if(self::findOne(['product_id' => $attributes['product_id'], 'company_id' => $attributes['company_id']])) {
                    return "ОШИБКА! Соответствие для данного товара уже выбрано";
                }
            }
            $product = new self;
            $status = self::STATUS_NEW;
        }
        $unitTypes = Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_UNITS]['items'];
        foreach($attributes as $attributeName => $attributeValue) {
            if($attributeName == 'unitType.name') {
                if(in_array($attributeValue, $unitTypes)) {
                    $product->unit_type_id = array_search($attributeValue, $unitTypes);
                } else {
                    return "ОШИБКА! Единица измерения \"$attributeValue\" некорректна. Допустимые единицы измерения: " . implode(', ', $unitTypes);
                }
            } else {
                if ($attributeName == 'product.fullName.name') {
                    $attributeName = 'name';
                }
                switch ($attributeName) {
                    case 'vendor_code':
                        $product->$attributeName = (string) $attributeValue;
                        break;
                    case 'status':
                        $product->$attributeName = (int) $attributeValue;
                        break;
                    default:
                        $product->$attributeName = $attributeValue;
                        break;
                }
            }
        }
        if($product->validate()) {
            if($product->save()) {
                return "Товар {$product->name} ($product->vendor_code) успешно " . ($status == self::STATUS_UPDATED ? 'обновлен' : 'создан');
            }
            return 'Не удалось ' . ($status == self::STATUS_UPDATED ? 'обновить информацию о товаре' : 'создать товар') . ". Неизвестная ошибка.";
        }
        $errorStatus = 'ОШИБКА! ' . ($status == self::STATUS_UPDATED ? 'Обновление' : 'Создание') . ' товара не удалось';
        return "$errorStatus - " . implode('; ', array_column($product->errors, 0));
    }
}
