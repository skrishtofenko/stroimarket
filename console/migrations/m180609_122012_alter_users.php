<?php
use yii\db\Migration;

class m180609_122012_alter_users extends Migration {
    public function safeUp() {
        $this->addColumn('{{%users}}', 'is_password_sent', $this->boolean()->notNull()->defaultValue(0)->after('ownership_type_id'));
        $this->update('{{%users}}', ['is_password_sent' => 1]);
    }

    public function safeDown() {
        $this->dropColumn('{{%users}}', 'is_password_sent');
    }
}
