<?php
use yii\db\Migration;

class m180524_054513_alter_companies extends Migration {
    public function safeUp() {
        $this->addColumn('{{%companies}}', 'country_id', $this->integer()->null()->after('user_id'));
        $this->addForeignKey(
            'FK_companies_country_id',
            '{{%companies}}',
            'country_id',
            '{{%countries}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_companies_country_id', '{{%companies}}');
        $this->dropColumn('{{%companies}}', 'country_id');
    }
}
