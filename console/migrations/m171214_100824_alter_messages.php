<?php
use yii\db\Migration;

class m171214_100824_alter_messages extends Migration {
    public function safeUp() {
        $this->dropForeignKey('FK_messages_from_company_id', '{{%messages}}');
        $this->renameColumn('{{%messages}}', 'from_company_id', 'from_user_id');
        $this->addForeignKey(
            'FK_messages_from_user_id',
            '{{%messages}}',
            'from_user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropForeignKey('FK_messages_to_company_id', '{{%messages}}');
        $this->renameColumn('{{%messages}}', 'to_company_id', 'to_user_id');
        $this->addForeignKey(
            'FK_messages_to_user_id',
            '{{%messages}}',
            'to_user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_messages_from_user_id', '{{%messages}}');
        $this->renameColumn('{{%messages}}', 'from_user_id', 'from_company_id');
        $this->addForeignKey(
            'FK_messages_from_company_id',
            '{{%messages}}',
            'from_company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropForeignKey('FK_messages_to_user_id', '{{%messages}}');
        $this->renameColumn('{{%messages}}', 'to_user_id', 'to_company_id');
        $this->addForeignKey(
            'FK_messages_to_company_id',
            '{{%messages}}',
            'to_company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
