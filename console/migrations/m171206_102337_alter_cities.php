<?php
use yii\db\Migration;

class m171206_102337_alter_cities extends Migration {
    public function safeUp() {
        $this->addColumn('{{%cities}}', 'users_count', $this->integer()->notNull()->defaultValue(0)->after('id'));
        $this->addColumn('{{%cities}}', 'companies_count', $this->integer()->notNull()->defaultValue(0)->after('users_count'));
    }

    public function safeDown() {
        $this->dropColumn('{{%cities}}', 'users_count');
        $this->dropColumn('{{%cities}}', 'companies_count');
    }
}
