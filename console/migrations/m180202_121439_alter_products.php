<?php
use yii\db\Migration;

class m180202_121439_alter_products extends Migration {
    public function safeUp() {
        $this->addColumn('{{%products}}', 'is_active', $this->boolean()->defaultValue(1)->after('unit_type_id'));
    }

    public function safeDown() {
        $this->dropColumn('{{%products}}', 'is_active');
    }
}
