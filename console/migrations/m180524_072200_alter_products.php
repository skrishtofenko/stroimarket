<?php
use yii\db\Migration;

class m180524_072200_alter_products extends Migration {
    public function safeUp() {
        $this->dropForeignKey('FK_products_manufacturer_id', '{{%products}}');
        $this->alterColumn('{{%products}}', 'manufacturer_id', $this->integer()->null());
        $this->update('{{%products}}', ['manufacturer_id' => null]);
        $this->addForeignKey(
            'FK_products_manufacturer_id',
            '{{%products}}',
            'manufacturer_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_products_manufacturer_id', '{{%products}}');
        $this->addForeignKey(
            'FK_products_manufacturer_id',
            '{{%products}}',
            'manufacturer_id',
            '{{%manufacturers}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
