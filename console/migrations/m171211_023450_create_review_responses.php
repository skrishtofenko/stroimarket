<?php
use yii\db\Migration;

class m171211_023450_create_review_responses extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%review_responses}}', [
            'id' => $this->primaryKey(),
            'review_id' => $this->integer()->notNull(),
            'is_from_store' => $this->boolean()->notNull()->defaultValue(0),
            'message' => $this->text()->notNull(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_review_responses_review_id',
            '{{%review_responses}}',
            'review_id',
            '{{%reviews}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_review_responses_review_id', '{{%review_responses}}');
        $this->dropTable('{{%review_responses}}');
    }
}
