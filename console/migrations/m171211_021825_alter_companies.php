<?php
use yii\db\Migration;

class m171211_021825_alter_companies extends Migration {
    public function safeUp() {
        $this->addColumn('{{%companies}}', 'original_id', $this->integer()->null()->defaultValue(null)->after('city_id'));
        $this->addForeignKey(
            'FK_companies_original_id',
            '{{%companies}}',
            'original_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_companies_original_id', '{{%companies}}');
        $this->dropColumn('{{%companies}}', 'original_id');
    }
}
