<?php
use yii\db\Migration;

class m171205_061733_create_products_requests extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%products_requests}}', [
            'id' => $this->primaryKey(),
            'company_product_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->null()->defaultValue(null),
            'image' => $this->string(31)->null()->defaultValue(null),
            'comment' => $this->text()->null(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_products_requests_company_product_id',
            '{{%products_requests}}',
            'company_product_id',
            '{{%companies_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_products_requests_product_id',
            '{{%products_requests}}',
            'product_id',
            '{{%products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_products_requests_company_product_id', '{{%products_requests}}');
        $this->dropForeignKey('FK_products_requests_product_id', '{{%products_requests}}');
        $this->dropTable('{{%products_requests}}');
    }
}
