<?php
use yii\db\Migration;

class m171205_051116_create_requisites extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%requisites}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'is_default' => $this->boolean()->notNull()->defaultValue(0),
            'account' => $this->string(20)->null()->defaultValue(null),
            'bik' => $this->string(9)->null()->defaultValue(null),
            'bank' => $this->string(127)->null()->defaultValue(null),
            'corr_account' => $this->string(20)->null()->defaultValue(null),
            'legal_address' => $this->string()->null()->defaultValue(null),
            'post_address' => $this->string()->null()->defaultValue(null),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_requisites_company_id',
            '{{%requisites}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_requisites_company_id', '{{%requisites}}');
        $this->dropTable('{{%requisites}}');
    }
}
