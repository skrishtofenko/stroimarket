<?php
use yii\db\Migration;

class m171205_082952_alter_users extends Migration {
    public function safeUp() {
        $this->alterColumn('{{%users}}', 'status', $this->smallInteger(1)->notNull()->defaultValue(0));
    }

    public function safeDown() {
        $this->alterColumn('{{%users}}', 'status', $this->smallInteger(1));
    }
}
