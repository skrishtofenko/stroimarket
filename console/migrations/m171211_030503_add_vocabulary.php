<?php
use yii\db\Migration;

class m171211_030503_add_vocabulary extends Migration {
    public function safeUp() {
        $this->insert('vocabularies', ['id' => 7, 'alias' => 'units', 'name' => 'Единицы измерения']);
    }

    public function safeDown() {
        $this->delete('vocabularies', ['id' => 7]);
    }
}
