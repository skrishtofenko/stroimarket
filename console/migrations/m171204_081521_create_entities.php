<?php
use yii\db\Migration;

class m171204_081521_create_entities extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%entities}}', [
            'id' => $this->primaryKey(),
            'vocabulary_id' => $this->integer()->notNull(),
            'name' => $this->string(127)->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_entities_vocabulary_id',
            'entities',
            'vocabulary_id',
            'vocabularies',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_entities_vocabulary_id', '{{%entities}}');
        $this->dropTable('{{%entities}}');
    }
}
