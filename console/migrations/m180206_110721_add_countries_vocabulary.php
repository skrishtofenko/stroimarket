<?php
use yii\db\Migration;

class m180206_110721_add_countries_vocabulary extends Migration {
    public function safeUp() {
        $this->insert('vocabularies', ['id' => 8, 'alias' => 'prod_countries', 'name' => 'Страны производства']);

        $this->addColumn('{{%products}}', 'prod_country_id', $this->integer()->null()->defaultValue(null)->after('manufacturer_id'));
        $this->addForeignKey(
            'FK_products_prod_country_id',
            '{{%products}}',
            'prod_country_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->delete('vocabularies', ['id' => 8]);

        $this->dropForeignKey('FK_products_prod_country_id', '{{%products}}');
        $this->dropColumn('{{%products}}', 'prod_country_id');
    }
}
