<?php
use yii\db\Migration;

class m171205_055811_create_products_characteristics extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%products_characteristics}}', [
            'product_id' => $this->integer()->notNull(),
            'characteristic_id' => $this->integer()->notNull(),
            'value' => $this->string(63)->notNull(),
        ], $tableOptions);
        $this->addPrimaryKey('PK_products_characteristics', '{{%products_characteristics}}', ['product_id', 'characteristic_id']);

        $this->addForeignKey(
            'FK_products_characteristics_product_id',
            '{{%products_characteristics}}',
            'product_id',
            '{{%products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_products_characteristics_city_id',
            '{{%products_characteristics}}',
            'characteristic_id',
            '{{%characteristics}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_products_characteristics_product_id', '{{%products_characteristics}}');
        $this->dropForeignKey('FK_products_characteristics_city_id', '{{%products_characteristics}}');
        $this->dropTable('{{%products_characteristics}}');
    }
}
