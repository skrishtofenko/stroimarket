<?php
use yii\db\Migration;

class m180524_050715_create_countries extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%countries}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(63)->notNull(),
        ], $tableOptions);

        $this->addColumn('{{%cities}}', 'country_id', $this->integer()->null()->after('id'));
        $this->addForeignKey(
            'FK_cities_country_id',
            '{{%cities}}',
            'country_id',
            '{{%countries}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->insert('{{%countries}}', ['id' => 1, 'name' => 'Россия']);
        $this->update('{{%cities}}', ['country_id' => 1]);
    }

    public function safeDown() {
        $this->dropForeignKey('FK_cities_country_id', '{{%cities}}');
        $this->dropColumn('{{%cities}}', 'country_id');
        $this->dropTable('{{%countries}}');
    }
}
