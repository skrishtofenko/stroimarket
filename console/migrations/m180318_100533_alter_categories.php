<?php
use yii\db\Migration;

class m180318_100533_alter_categories extends Migration {
    public function safeUp() {
        $this->addColumn('{{%categories}}', 'min_cost', $this->integer()->notNull()->defaultValue(0)->after('level'));
        $this->addColumn('{{%categories}}', 'max_cost', $this->integer()->notNull()->defaultValue(0)->after('min_cost'));
    }

    public function safeDown() {
        $this->dropColumn('{{%categories}}', 'min_cost');
        $this->dropColumn('{{%categories}}', 'max_cost');
    }
}
