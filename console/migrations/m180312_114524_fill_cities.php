<?php
use yii\db\Migration;
use console\components\CitiesList;

class m180312_114524_fill_cities extends Migration {
    public function safeUp() {
        $this->batchInsert(
            '{{%cities}}',
            ['id', 'status', 'users_count', 'companies_count', 'name'],
            CitiesList::$citiesArray
        );
    }

    public function safeDown() {
        $this->delete('{{%cities}}');
    }
}
