<?php
use yii\db\Migration;

class m171206_100545_alter_companies extends Migration {
    public function safeUp() {
        $this->addColumn('{{%companies}}', 'url', $this->string()->null()->defaultValue(null)->after('emails'));
        $this->addColumn('{{%companies}}', 'logo', $this->string(31)->null()->defaultValue(null)->after('schedule'));
        $this->addColumn('{{%companies}}', 'legal_registered_at', $this->timestamp()->null()->defaultValue(null)->after('okpo'));
        $this->addColumn('{{%companies}}', 'description', $this->text()->null()->after('okpo'));
        $this->addColumn('{{%companies}}', 'is_request_completed', $this->boolean()->notNull()->defaultValue(0)->after('ownership_type_id'));
        $this->addColumn('{{%companies}}', 'checking_file', $this->string(31)->null()->defaultValue(null)->after('logo'));
        $this->addColumn('{{%companies}}', 'catalog_updated_at', $this->timestamp()->null()->defaultValue(null)->after('legal_registered_at'));
    }

    public function safeDown() {
        $this->dropColumn('{{%companies}}', 'url');
        $this->dropColumn('{{%companies}}', 'logo');
        $this->dropColumn('{{%companies}}', 'legal_registered_at');
        $this->dropColumn('{{%companies}}', 'description');
        $this->dropColumn('{{%companies}}', 'is_request_completed');
        $this->dropColumn('{{%companies}}', 'checking_file');
        $this->dropColumn('{{%companies}}', 'catalog_updated_at');
    }
}
