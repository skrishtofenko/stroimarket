<?php
use yii\db\Migration;

class m180201_085439_replace_company_name extends Migration {
    public function safeUp() {
        $this->addColumn('{{%users}}', 'name', $this->string(127)->notNull()->defaultValue('')->after('legal_name'));
        $this->dropColumn('{{%companies}}', 'name');
    }

    public function safeDown() {
        $this->addColumn('{{%companies}}', 'name', $this->string(127)->notNull()->defaultValue('')->after('paid_till'));
        $this->dropColumn('{{%users}}', 'name');
    }
}
