<?php
use yii\db\Migration;

class m171213_021505_alter_requisites extends Migration {
    public function safeUp() {
        $this->dropForeignKey('FK_requisites_company_id', '{{%requisites}}');
        $this->renameColumn('{{%requisites}}', 'company_id', 'legal_entity_id');
        $this->addForeignKey(
            'FK_requisites_legal_entity_id',
            '{{%requisites}}',
            'legal_entity_id',
            '{{%legal_entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_requisites_legal_entity_id', '{{%requisites}}');
        $this->renameColumn('{{%requisites}}', 'legal_entity_id', 'company_id');
        $this->addForeignKey(
            'FK_requisites_company_id',
            '{{%requisites}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
