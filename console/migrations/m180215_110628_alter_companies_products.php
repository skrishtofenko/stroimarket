<?php
use yii\db\Migration;

class m180215_110628_alter_companies_products extends Migration {
    public function safeUp() {
        $this->alterColumn('{{%companies_products}}', 'product_id', $this->integer()->null());
    }

    public function safeDown() {
        return true;
    }
}
