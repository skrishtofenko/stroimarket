<?php
use yii\db\Migration;

class m171205_044118_create_companies extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%companies}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'ownership_type_id' => $this->integer()->notNull(),
            'is_manufacturer' => $this->boolean()->null()->defaultValue(null),
            'is_imonly' => $this->boolean()->null()->defaultValue(null),
            'is_delivery' => $this->boolean()->null()->defaultValue(null),
            'type' => "ENUM('provider', 'store', 'picker')",
            'name' => $this->string(127)->notNull()->defaultValue(''),
            'legal_name' => $this->string(127)->notNull()->defaultValue(''),
            'address' => $this->text()->null(),
            'phones' => $this->text()->null(),
            'emails' => $this->text()->null(),
            'schedule' => $this->text()->null(),
            'inn' => $this->string(12)->null()->defaultValue(null),
            'ogrn' => $this->string(15)->null()->defaultValue(null),
            'kpp' => $this->string(9)->null()->defaultValue(null),
            'okpo' => $this->string(10)->null()->defaultValue(null),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_companies_user_id',
            '{{%companies}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_companies_city_id',
            '{{%companies}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_companies_ownership_type_id',
            '{{%companies}}',
            'ownership_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_companies_user_id', '{{%companies}}');
        $this->dropForeignKey('FK_companies_city_id', '{{%companies}}');
        $this->dropForeignKey('FK_companies_ownership_type_id', '{{%companies}}');
        $this->dropTable('{{%companies}}');
    }
}
