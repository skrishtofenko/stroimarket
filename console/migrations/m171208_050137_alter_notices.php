<?php
use yii\db\Migration;

class m171208_050137_alter_notices extends Migration {
    public function safeUp() {
        $this->addColumn('{{%notices}}', 'orders', $this->boolean()->notNull()->defaultValue(0));
        $this->addColumn('{{%notices}}', 'reviews', $this->boolean()->notNull()->defaultValue(0));
    }

    public function safeDown() {
        $this->dropColumn('{{%notices}}', 'orders');
        $this->dropColumn('{{%notices}}', 'reviews');
    }
}
