<?php
use yii\db\Migration;

class m171213_021940_alter_bills extends Migration {
    public function safeUp() {
        $this->addColumn('{{%bills}}', 'company_id', $this->integer()->null()->defaultValue(null)->after('requisite_id'));
        $this->addForeignKey(
            'FK_bills_company_id',
            '{{%bills}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_bills_company_id', '{{%bills}}');
        $this->dropColumn('{{%bills}}', 'company_id');
    }
}
