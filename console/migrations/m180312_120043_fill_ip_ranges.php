<?php
use yii\db\Migration;
use console\components\IpsList;

class m180312_120043_fill_ip_ranges extends Migration {
    public function safeUp() {
        for($i = 1; $i <= 5; $i++) {
            $array = "ipsArray$i";
            $this->batchInsert(
                '{{%ip_ranges}}',
                ['city_id', 'begin_ip', 'end_ip'],
                IpsList::$$array
            );
        }
    }

    public function safeDown() {
        $this->delete('{{%ip_ranges}}');
    }
}
