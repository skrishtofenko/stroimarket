<?php
use yii\db\Migration;

class m180206_084636_alter_products_characteristics extends Migration {
    public function safeUp() {
        $this->alterColumn('{{%products_characteristics}}', 'value', $this->string(63)->null()->defaultValue(''));
    }

    public function safeDown() {
        $this->alterColumn('{{%products_characteristics}}', 'value', $this->string(63)->notNull());
    }
}
