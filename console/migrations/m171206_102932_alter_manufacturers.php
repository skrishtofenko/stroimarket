<?php
use yii\db\Migration;

class m171206_102932_alter_manufacturers extends Migration {
    public function safeUp() {
        $this->addColumn('{{%manufacturers}}', 'products_count', $this->integer()->notNull()->defaultValue(0)->after('id'));
    }

    public function safeDown() {
        $this->dropColumn('{{%manufacturers}}', 'products_count');
    }
}
