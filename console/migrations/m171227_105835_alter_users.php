<?php
use yii\db\Migration;

class m171227_105835_alter_users extends Migration {
    public function safeUp() {
        $this->renameColumn('{{%users}}', 'type', 'role');
    }

    public function safeDown() {
        $this->renameColumn('{{%users}}', 'role', 'type');
    }
}
