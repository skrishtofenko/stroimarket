<?php
use yii\db\Migration;

class m180612_065314_alter_users extends Migration {
    public function safeUp() {
        $this->addColumn('{{%users}}', 'is_recommended', $this->boolean()->notNull()->defaultValue(0)->after('is_request_completed'));
    }

    public function safeDown() {
        $this->dropColumn('{{%users}}', 'is_recommended');
    }
}
