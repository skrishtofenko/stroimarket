<?php
use yii\db\Migration;

class m171214_101716_alter_notices_categories extends Migration {
    public function safeUp() {
        $this->dropForeignKey('FK_notices_categories_company_id', '{{%notices_categories}}');
        $this->dropForeignKey('FK_notices_categories_city_id', '{{%notices_categories}}');

        $this->dropPrimaryKey('PK_notices_categories', '{{%notices_categories}}');
        $this->renameColumn('{{%notices_categories}}', 'company_id', 'user_id');
        $this->addPrimaryKey('PK_notices_categories', '{{%notices_categories}}', ['user_id', 'category_id']);

        $this->addForeignKey(
            'FK_notices_categories_user_id',
            '{{%notices_categories}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_notices_categories_category_id',
            '{{%notices_categories}}',
            'category_id',
            '{{%categories}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_notices_categories_user_id', '{{%notices_categories}}');
        $this->dropForeignKey('FK_notices_categories_category_id', '{{%notices_categories}}');

        $this->dropPrimaryKey('PK_notices_categories', '{{%notices_categories}}');
        $this->renameColumn('{{%notices_categories}}', 'user_id', 'company_id');
        $this->addPrimaryKey('PK_notices_categories', '{{%notices_categories}}', ['company_id', 'category_id']);

        $this->addForeignKey(
            'FK_notices_categories_company_id',
            '{{%notices_categories}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_notices_categories_city_id',
            '{{%notices_categories}}',
            'category_id',
            '{{%categories}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
