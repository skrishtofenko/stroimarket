<?php
use yii\db\Migration;

class m171227_081423_alter_companies extends Migration {
    public function safeUp() {
        $this->dropForeignKey('FK_companies_city_id', '{{%companies}}');
        $this->alterColumn('{{%companies}}', 'city_id', $this->integer()->null());
        $this->addForeignKey(
            'FK_companies_city_id',
            '{{%companies}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_companies_city_id', '{{%companies}}');
        $this->alterColumn('{{%companies}}', 'city_id', $this->integer()->notNull());
        $this->addForeignKey(
            'FK_companies_city_id',
            '{{%companies}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
