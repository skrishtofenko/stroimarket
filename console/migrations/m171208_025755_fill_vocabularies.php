<?php
use yii\db\Migration;

class m171208_025755_fill_vocabularies extends Migration {
    public function safeUp() {
        $this->insert('vocabularies', ['id' => 1, 'alias' => 'legal_entities', 'name' => 'Формы собственности']);
        $this->insert('vocabularies', ['id' => 2, 'alias' => 'rooms', 'name' => 'Типы помещений']);
        $this->insert('vocabularies', ['id' => 3, 'alias' => 'docs', 'name' => 'Типы подтверждающих документов']);
        $this->insert('vocabularies', ['id' => 4, 'alias' => 'payments', 'name' => 'Способы оплаты подписки']);
        $this->insert('vocabularies', ['id' => 5, 'alias' => 'deliveries', 'name' => 'Способы доставки']);
        $this->insert('vocabularies', ['id' => 6, 'alias' => 'order_payments', 'name' => 'Способы оплаты заказов']);
    }

    public function safeDown() {
        $this->delete('vocabularies', ['id' => 1]);
        $this->delete('vocabularies', ['id' => 2]);
        $this->delete('vocabularies', ['id' => 3]);
        $this->delete('vocabularies', ['id' => 4]);
        $this->delete('vocabularies', ['id' => 5]);
        $this->delete('vocabularies', ['id' => 6]);
    }
}
