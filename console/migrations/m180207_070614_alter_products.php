<?php
use yii\db\Migration;

class m180207_070614_alter_products extends Migration {
    public function safeUp() {
        $this->alterColumn('{{%products}}', 'name', $this->string()->notNull());
        $this->alterColumn('{{%products}}', 'image', $this->string()->null()->defaultValue(null));
    }

    public function safeDown() {
        $this->alterColumn('{{%products}}', 'name', $this->string(63)->notNull());
        $this->alterColumn('{{%products}}', 'image', $this->string(31)->null()->defaultValue(null));
    }
}
