<?php
use yii\db\Migration;

class m171213_020348_create_legal_entities extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%legal_entities}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'ownership_type_id' => $this->integer()->notNull(),
            'is_request_completed' => $this->boolean()->notNull()->defaultValue(0),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'legal_name' => $this->string(127)->notNull()->defaultValue(''),
            'logo' => $this->string(31)->null()->defaultValue(null),
            'checking_file' => $this->string(31)->null()->defaultValue(null),
            'inn' => $this->string(12)->null()->defaultValue(null),
            'ogrn' => $this->string(15)->null()->defaultValue(null),
            'kpp' => $this->string(9)->null()->defaultValue(null),
            'okpo' => $this->string(10)->null()->defaultValue(null),
            'legal_registered_at' => $this->timestamp()->null()->defaultValue(null),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);


        $this->addForeignKey(
            'FK_legal_entities_user_id',
            '{{%legal_entities}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_legal_entities_city_id',
            '{{%legal_entities}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_legal_entities_ownership_type_id',
            '{{%legal_entities}}',
            'ownership_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_legal_entities_user_id', '{{%legal_entities}}');
        $this->dropForeignKey('FK_legal_entities_city_id', '{{%legal_entities}}');
        $this->dropForeignKey('FK_legal_entities_ownership_type_id', '{{%legal_entities}}');
        $this->dropTable('{{%legal_entities}}');
    }
}
