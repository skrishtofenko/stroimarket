<?php
use yii\db\Migration;

class m171205_054313_create_bills extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%bills}}', [
            'id' => $this->primaryKey(),
            'requisite_id' => $this->integer()->notNull(),
            'type' => "ENUM('registration', 'subscription')",
            'cost' => $this->integer()->notNull(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_bills_requisite_id',
            '{{%bills}}',
            'requisite_id',
            '{{%requisites}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_bills_requisite_id', '{{%bills}}');
        $this->dropTable('{{%bills}}');
    }
}
