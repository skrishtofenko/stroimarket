<?php
use yii\db\Migration;

class m171227_093004_alter_users extends Migration {
    public function safeUp() {
        $this->addColumn('{{%users}}', 'registration_token', $this->string(32)->null()->defaultValue(null)->unique()->after('password_hash'));
    }

    public function safeDown() {
        $this->dropColumn('{{%users}}', 'registration_token');
    }
}
