<?php
use yii\db\Migration;

class m180318_122405_alter_deliveries extends Migration {
    public function safeUp() {
        $this->addColumn('{{%deliveries}}', 'type', "ENUM('delivery_delivery', 'delivery_pickup', 'delivery_both') NOT NULL DEFAULT 'delivery_both' AFTER city_id");

        $this->dropForeignKey('FK_deliveries_delivery_type_id', '{{%deliveries}}');
        $this->dropColumn('{{%deliveries}}', 'delivery_type_id');
    }

    public function safeDown() {
        $this->dropColumn('{{%deliveries}}', 'type');
        $this->addColumn('{{%deliveries}}', 'delivery_type_id', $this->integer()->notNull()->defaultValue(1)->after('city_id'));
        $this->addForeignKey(
            'FK_deliveries_delivery_type_id',
            '{{%deliveries}}',
            'delivery_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
