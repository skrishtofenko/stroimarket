<?php
use yii\db\Migration;

class m171205_050702_create_messages extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%messages}}', [
            'id' => $this->primaryKey(),
            'from_company_id' => $this->integer()->notNull(),
            'to_company_id' => $this->integer()->notNull(),
            'text' => $this->text()->notNull(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_messages_from_company_id',
            '{{%messages}}',
            'from_company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_messages_to_company_id',
            '{{%messages}}',
            'to_company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_messages_from_company_id', '{{%messages}}');
        $this->dropForeignKey('FK_messages_to_company_id', '{{%messages}}');
        $this->dropTable('{{%messages}}');
    }
}
