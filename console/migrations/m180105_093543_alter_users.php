<?php
use yii\db\Migration;

class m180105_093543_alter_users extends Migration {
    public function safeUp() {
        $this->addColumn('{{%users}}', 'password_reset_token', $this->string(63)->defaultValue(null)->unique()->after('password_hash'));
    }

    public function safeDown() {
        $this->dropColumn('{{%users}}', 'password_reset_token');
    }
}
