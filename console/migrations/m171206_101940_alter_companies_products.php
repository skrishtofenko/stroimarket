<?php
use yii\db\Migration;

class m171206_101940_alter_companies_products extends Migration {
    public function safeUp() {
        $this->addColumn('{{%companies_products}}', 'is_promo', $this->boolean()->notNull()->defaultValue(0)->after('unit_type_id'));
        $this->addColumn('{{%companies_products}}', 'status', $this->smallInteger(1)->notNull()->defaultValue(0)->after('is_action'));
    }

    public function safeDown() {
        $this->dropColumn('{{%companies_products}}', 'is_promo');
        $this->dropColumn('{{%companies_products}}', 'status');
    }
}
