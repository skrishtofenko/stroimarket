<?php
use yii\db\Migration;

class m171204_081942_create_users extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%users}}', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer()->null()->defaultValue(null),
            'type' => "ENUM('buyer', 'company')",
            'status' => $this->smallInteger(1),
            'password_hash' => $this->string(),
            'email' => $this->string(127)->notNull()->unique(),
            'phone' => $this->string(10)->null(),
            'fio' => $this->string(127)->null(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_users_city_id',
            '{{%users}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_users_city_id', '{{%users}}');
        $this->dropTable('{{%users}}');
    }
}
