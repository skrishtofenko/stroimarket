<?php
use yii\db\Migration;

class m171204_075827_create_cities extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%cities}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(63)->notNull(),
        ], $tableOptions);
    }

    public function safeDown() {
        $this->dropTable('{{%cities}}');
    }
}
