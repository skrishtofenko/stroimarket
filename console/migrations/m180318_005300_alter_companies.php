<?php
use yii\db\Migration;

class m180318_005300_alter_companies extends Migration {
    public function safeUp() {
        $this->addColumn('{{%companies}}', 'im_url', $this->string()->null()->defaultValue(null)->after('address'));
    }

    public function safeDown() {
        $this->dropColumn('{{%companies}}', 'im_url');
    }
}
