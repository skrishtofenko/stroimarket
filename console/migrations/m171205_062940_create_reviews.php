<?php
use yii\db\Migration;

class m171205_062940_create_reviews extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%reviews}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'rate' => $this->integer()->notNull()->defaultValue(0),
            'dignity' => $this->text()->null(),
            'flaw' => $this->text()->null(),
            'comment' => $this->text()->null(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_reviews_order_id',
            '{{%reviews}}',
            'order_id',
            '{{%orders}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_reviews_order_id', '{{%reviews}}');
        $this->dropTable('{{%reviews}}');
    }
}
