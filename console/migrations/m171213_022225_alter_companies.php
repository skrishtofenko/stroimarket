<?php
use yii\db\Migration;

class m171213_022225_alter_companies extends Migration {
    public function safeUp() {
        $this->dropForeignKey('FK_companies_user_id', '{{%companies}}');
        $this->dropForeignKey('FK_companies_original_id', '{{%companies}}');
        $this->dropForeignKey('FK_companies_ownership_type_id', '{{%companies}}');

        $this->dropColumn('{{%companies}}', 'user_id');
        $this->dropColumn('{{%companies}}', 'original_id');
        $this->dropColumn('{{%companies}}', 'ownership_type_id');
        $this->dropColumn('{{%companies}}', 'is_request_completed');
        $this->dropColumn('{{%companies}}', 'legal_name');
        $this->dropColumn('{{%companies}}', 'logo');
        $this->dropColumn('{{%companies}}', 'checking_file');
        $this->dropColumn('{{%companies}}', 'inn');
        $this->dropColumn('{{%companies}}', 'ogrn');
        $this->dropColumn('{{%companies}}', 'kpp');
        $this->dropColumn('{{%companies}}', 'okpo');
        $this->dropColumn('{{%companies}}', 'legal_registered_at');

        $this->addColumn('{{%companies}}', 'legal_entity_id', $this->integer()->notNull()->after('id'));
        $this->addForeignKey(
            'FK_companies_legal_entity_id',
            '{{%companies}}',
            'legal_entity_id',
            '{{%legal_entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_companies_legal_entity_id', '{{%companies}}');
        $this->dropColumn('{{%companies}}', 'legal_entity_id');

        $this->addColumn('{{%companies}}', 'user_id', $this->integer()->notNull()->after('id'));
        $this->addColumn('{{%companies}}', 'original_id', $this->integer()->null()->defaultValue(null)->after('city_id'));
        $this->addColumn('{{%companies}}', 'ownership_type_id', $this->integer()->notNull()->after('original_id'));
        $this->addColumn('{{%companies}}', 'is_request_completed', $this->boolean()->notNull()->defaultValue(0)->after('ownership_type_id'));
        $this->addColumn('{{%companies}}', 'legal_name', $this->string(127)->notNull()->defaultValue('')->after('name'));
        $this->addColumn('{{%companies}}', 'logo', $this->string(31)->null()->defaultValue(null)->after('schedule'));
        $this->addColumn('{{%companies}}', 'checking_file', $this->string(31)->null()->defaultValue(null)->after('logo'));
        $this->addColumn('{{%companies}}', 'inn', $this->string(12)->null()->defaultValue(null)->after('checking_file'));
        $this->addColumn('{{%companies}}', 'ogrn', $this->string(15)->null()->defaultValue(null)->after('inn'));
        $this->addColumn('{{%companies}}', 'kpp', $this->string(9)->null()->defaultValue(null)->after('ogrn'));
        $this->addColumn('{{%companies}}', 'okpo', $this->string(10)->null()->defaultValue(null)->after('kpp'));
        $this->addColumn('{{%companies}}', 'legal_registered_at', $this->timestamp()->null()->defaultValue(null)->after('description'));

        $this->addForeignKey(
            'FK_companies_user_id',
            '{{%companies}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_companies_ownership_type_id',
            '{{%companies}}',
            'ownership_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_companies_original_id',
            '{{%companies}}',
            'original_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
