<?php
use yii\db\Migration;

class m171204_041607_add_admin extends Migration {
    public function safeUp() {
        $this->insert('{{%admins}}', [
            'email' => 'admin@stroimarket.ru',
            'password_hash' => Yii::$app->security->generatePasswordHash('123123'),
            'auth_key' => Yii::$app->security->generateRandomString(),
        ]);
    }

    public function safeDown() {
        $this->delete('{{%admins}}', ['email' => 'admin@stroimarket.ru']);
    }
}
