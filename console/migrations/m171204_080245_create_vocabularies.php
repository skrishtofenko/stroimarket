<?php
use yii\db\Migration;

class m171204_080245_create_vocabularies extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vocabularies}}', [
            'id' => $this->primaryKey(),
            'alias' => $this->string(15)->notNull(),
            'name' => $this->string(63)->notNull(),
        ], $tableOptions);
    }

    public function safeDown() {
        $this->dropTable('{{%vocabularies}}');
    }
}
