<?php
use yii\db\Migration;

class m171229_115218_alter_users extends Migration {
    public function safeUp() {
        $this->addColumn('{{%users}}', 'last_completed_registration_step', $this->smallInteger()->notNull()->defaultValue(0)->after('is_request_completed'));
    }

    public function safeDown() {
        $this->dropColumn('{{%users}}', 'last_completed_registration_step');
    }
}
