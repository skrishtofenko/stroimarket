<?php
use yii\db\Migration;

class m171218_090231_fix_filename_length extends Migration {
    public function safeUp() {
        $this->alterColumn('{{%categories}}', 'image', $this->string(32)->null()->defaultValue(null));
        $this->alterColumn('{{%files}}', 'filename', $this->string(32)->null()->defaultValue(null));
        $this->alterColumn('{{%products}}', 'image', $this->string(32)->null()->defaultValue(null));
        $this->alterColumn('{{%products_requests}}', 'image', $this->string(32)->null()->defaultValue(null));
        $this->alterColumn('{{%users}}', 'logo', $this->string(32)->null()->defaultValue(null));
        $this->alterColumn('{{%users}}', 'checking_file', $this->string(32)->null()->defaultValue(null));
    }

    public function safeDown() {
        $this->alterColumn('{{%categories}}', 'image', $this->string(31)->null()->defaultValue(null));
        $this->alterColumn('{{%files}}', 'filename', $this->string(31)->null()->defaultValue(null));
        $this->alterColumn('{{%products}}', 'image', $this->string(31)->null()->defaultValue(null));
        $this->alterColumn('{{%products_requests}}', 'image', $this->string(31)->null()->defaultValue(null));
        $this->alterColumn('{{%users}}', 'logo', $this->string(31)->null()->defaultValue(null));
        $this->alterColumn('{{%users}}', 'checking_file', $this->string(31)->null()->defaultValue(null));
    }
}
