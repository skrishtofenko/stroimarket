<?php
use yii\db\Migration;

class m180224_122543_alter_catalogs extends Migration {
    public function safeUp() {
        $this->addColumn('{{%catalogs}}', 'file_name', $this->string(40)->notNull()->after('tries_count'));
        $this->addColumn('{{%catalogs}}', 'upload_name', $this->string()->notNull()->after('file_name'));
    }

    public function safeDown() {
        $this->dropColumn('{{%catalogs}}', 'file_name');
        $this->dropColumn('{{%catalogs}}', 'upload_name');
    }
}
