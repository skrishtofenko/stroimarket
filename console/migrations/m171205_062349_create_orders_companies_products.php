<?php
use yii\db\Migration;

class m171205_062349_create_orders_companies_products extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%orders_companies_products}}', [
            'order_id' => $this->integer()->notNull(),
            'company_product_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'cost' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addPrimaryKey('PK_products_characteristics', '{{%orders_companies_products}}', ['order_id', 'company_product_id']);

        $this->addForeignKey(
            'FK_orders_companies_products_order_id',
            '{{%orders_companies_products}}',
            'order_id',
            '{{%orders}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_orders_companies_products_company_product_id',
            '{{%orders_companies_products}}',
            'company_product_id',
            '{{%companies_products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_orders_companies_products_order_id', '{{%orders_companies_products}}');
        $this->dropForeignKey('FK_orders_companies_products_company_product_id', '{{%orders_companies_products}}');
        $this->dropTable('{{%orders_companies_products}}');
    }
}
