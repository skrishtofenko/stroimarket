<?php
use yii\db\Migration;

class m171206_102720_alter_categories extends Migration {
    public function safeUp() {
        $this->addColumn('{{%categories}}', 'child_count', $this->integer()->notNull()->defaultValue(0)->after('parent_id'));
        $this->addColumn('{{%categories}}', 'products_count', $this->integer()->notNull()->defaultValue(0)->after('child_count'));
    }

    public function safeDown() {
        $this->dropColumn('{{%categories}}', 'child_count');
        $this->dropColumn('{{%categories}}', 'products_count');
    }
}
