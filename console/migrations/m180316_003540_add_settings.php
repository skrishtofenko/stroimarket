<?php
use yii\db\Migration;

class m180316_003540_add_settings extends Migration {
    public function safeUp() {
        $this->insert('{{%settings}}', ['alias' => 'contactPhone', 'name' => 'Контактный телефон', 'value' => '+7 (383) 235-99-25']);
        $this->insert('{{%settings}}', ['alias' => 'contactEmail', 'name' => 'Контактный email', 'value' => 'info@mystroymarket.ru']);
    }

    public function safeDown() {
        $this->delete('{{%settings}}', ['alias' => 'contactPhone']);
        $this->delete('{{%settings}}', ['alias' => 'contactEmail']);
    }
}
