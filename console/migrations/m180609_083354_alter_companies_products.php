<?php
use yii\db\Migration;

class m180609_083354_alter_companies_products extends Migration {
    public function safeUp() {
        $this->addColumn('{{%companies_products}}', 'is_manufactured_by', $this->boolean()->notNull()->defaultValue(0)->after('unit_type_id'));
    }

    public function safeDown() {
        $this->dropColumn('{{%companies_products}}', 'is_manufactured_by');
    }
}
