<?php
use yii\db\Migration;

class m180318_100036_alter_products extends Migration {
    public function safeUp() {
        $this->addColumn('{{%products}}', 'assortment_count', $this->integer()->notNull()->defaultValue(0)->after('is_active'));
        $this->addColumn('{{%products}}', 'availability_count', $this->integer()->notNull()->defaultValue(0)->after('assortment_count'));
        $this->addColumn('{{%products}}', 'min_cost', $this->integer()->notNull()->defaultValue(0)->after('availability_count'));
        $this->addColumn('{{%products}}', 'max_cost', $this->integer()->notNull()->defaultValue(0)->after('min_cost'));
    }

    public function safeDown() {
        $this->dropColumn('{{%products}}', 'assortment_cnt');
        $this->dropColumn('{{%products}}', 'availability_cnt');
        $this->dropColumn('{{%products}}', 'min_cost');
        $this->dropColumn('{{%products}}', 'max_cost');
    }
}
