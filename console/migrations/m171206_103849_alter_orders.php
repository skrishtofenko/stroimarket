<?php
use yii\db\Migration;

class m171206_103849_alter_orders extends Migration {
    public function safeUp() {
        $this->addColumn('{{%orders}}', 'delivery_type_id', $this->integer()->notNull()->after('company_id'));
        $this->addColumn('{{%orders}}', 'payment_type_id', $this->integer()->notNull()->after('delivery_type_id'));
        $this->addColumn('{{%orders}}', 'is_deleted_by_buyer', $this->boolean()->notNull()->defaultValue(0)->after('payment_type_id'));
        $this->addColumn('{{%orders}}', 'is_deleted_by_seller', $this->boolean()->notNull()->defaultValue(0)->after('is_deleted_by_buyer'));
        $this->addColumn('{{%orders}}', 'deliver_at', $this->timestamp()->null()->defaultValue(null)->after('total_cost'));
        $this->addColumn('{{%orders}}', 'address', $this->string()->null()->defaultValue(null)->after('total_cost'));
        $this->addColumn('{{%orders}}', 'flat', $this->string(15)->null()->defaultValue(null)->after('address'));
        $this->addColumn('{{%orders}}', 'comment', $this->integer()->notNull()->defaultValue(0)->after('flat'));

        $this->addForeignKey(
            'FK_orders_delivery_type_id',
            '{{%orders}}',
            'delivery_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_orders_payment_type_id',
            '{{%orders}}',
            'payment_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_orders_delivery_type_id', '{{%orders}}');
        $this->dropForeignKey('FK_orders_payment_type_id', '{{%orders}}');

        $this->dropColumn('{{%orders}}', 'delivery_type_id');
        $this->dropColumn('{{%orders}}', 'payment_type_id');
        $this->dropColumn('{{%orders}}', 'is_deleted_by_buyer');
        $this->dropColumn('{{%orders}}', 'is_deleted_by_seller');
        $this->dropColumn('{{%orders}}', 'deliver_at');
        $this->dropColumn('{{%orders}}', 'address');
        $this->dropColumn('{{%orders}}', 'flat');
        $this->dropColumn('{{%orders}}', 'comment');
    }
}
