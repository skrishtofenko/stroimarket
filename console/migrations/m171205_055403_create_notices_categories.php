<?php
use yii\db\Migration;

class m171205_055403_create_notices_categories extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notices_categories}}', [
            'company_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addPrimaryKey('PK_notices_categories', '{{%notices_categories}}', ['company_id', 'category_id']);

        $this->addForeignKey(
            'FK_notices_categories_company_id',
            '{{%notices_categories}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_notices_categories_city_id',
            '{{%notices_categories}}',
            'category_id',
            '{{%categories}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_notices_categories_company_id', '{{%notices_categories}}');
        $this->dropForeignKey('FK_notices_categories_city_id', '{{%notices_categories}}');
        $this->dropTable('{{%notices_categories}}');
    }
}
