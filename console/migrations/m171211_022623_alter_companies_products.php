<?php
use yii\db\Migration;

class m171211_022623_alter_companies_products extends Migration {
    public function safeUp() {
        $this->dropColumn('{{%companies_products}}', 'stock_balance');
        $this->addColumn('{{%companies_products}}', 'is_in_stock', $this->boolean()->null()->defaultValue(0)->after('unit_type_id'));
    }

    public function safeDown() {
        $this->dropColumn('{{%companies_products}}', 'is_in_stock');
        $this->addColumn('{{%companies_products}}', 'stock_balance', $this->integer()->null()->defaultValue(null)->after('cost'));
    }
}
