<?php
use yii\db\Migration;

class m171204_080541_create_categories extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(11)->null()->defaultValue(null),
            'level' => $this->smallInteger(1)->notNull(),
            'name' => $this->string(63)->notNull(),
            'image' => $this->string(31)->null()->defaultValue(null),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_categories_parent_id',
            'categories',
            'parent_id',
            'categories',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_categories_parent_id', '{{%categories}}');
        $this->dropTable('{{%categories}}');
    }
}
