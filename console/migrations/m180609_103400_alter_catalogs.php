<?php
use yii\db\Migration;

class m180609_103400_alter_catalogs extends Migration {
    public function safeUp() {
        $this->addColumn('{{%catalogs}}', 'is_by_admin', $this->boolean()->notNull()->defaultValue(0)->after('company_id'));
    }

    public function safeDown() {
        $this->dropColumn('{{%catalogs}}', 'is_by_admin');
    }
}
