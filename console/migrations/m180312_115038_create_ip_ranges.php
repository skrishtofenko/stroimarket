<?php
use yii\db\Migration;

class m180312_115038_create_ip_ranges extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ip_ranges}}', [
            'city_id' => $this->integer(11)->notNull(),
            'begin_ip' => $this->bigInteger()->notNull(),
            'end_ip' => $this->bigInteger()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_ip_ranges_city_id',
            '{{%ip_ranges}}',
            'city_id',
            'cities',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('IDX_ip_ranges_begin_ip', '{{%ip_ranges}}', 'begin_ip');
        $this->createIndex('IDX_ip_ranges_end_ip', '{{%ip_ranges}}', 'end_ip');
    }

    public function safeDown() {
        $this->dropIndex('IDX_ip_ranges_begin_ip', '{{%ip_ranges}}');
        $this->dropIndex('IDX_ip_ranges_end_ip', '{{%ip_ranges}}');
        $this->dropForeignKey('FK_ip_ranges_city_id', '{{%ip_ranges}}');
        $this->dropTable('{{%ip_ranges}}');
    }
}
