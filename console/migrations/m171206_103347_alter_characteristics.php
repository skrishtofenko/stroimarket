<?php
use yii\db\Migration;

class m171206_103347_alter_characteristics extends Migration {
    public function safeUp() {
        $this->addColumn('{{%characteristics}}', 'unit_type_id', $this->integer()->null()->defaultValue(null)->after('category_id'));
        $this->addColumn('{{%characteristics}}', 'is_multiple', $this->boolean()->notNull()->defaultValue(0)->after('unit_type_id'));
        $this->addColumn('{{%characteristics}}', 'is_add_to_title', $this->boolean()->notNull()->defaultValue(0)->after('is_multiple'));

        $this->addForeignKey(
            'FK_characteristics_unit_type_id',
            '{{%characteristics}}',
            'unit_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_characteristics_unit_type_id', '{{%characteristics}}');

        $this->dropColumn('{{%characteristics}}', 'unit_type_id');
        $this->dropColumn('{{%characteristics}}', 'is_multiple');
        $this->dropColumn('{{%characteristics}}', 'is_add_to_title');
    }
}
