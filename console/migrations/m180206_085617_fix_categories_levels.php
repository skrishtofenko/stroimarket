<?php
use yii\db\Migration;

class m180206_085617_fix_categories_levels extends Migration {
    public function safeUp() {
        $this->update('{{%categories}}', ['level' => 4], ['level' => 3]);
    }

    public function safeDown() {
        $this->update('{{%categories}}', ['level' => 3], ['level' => 4]);
    }
}
