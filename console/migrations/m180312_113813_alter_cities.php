<?php
use yii\db\Migration;

class m180312_113813_alter_cities extends Migration {
    public function safeUp() {
        $this->update('{{%users}}', ['city_id' => null]);
        $this->update('{{%companies}}', ['city_id' => null]);
        $this->truncateTable('{{%deliveries}}');
        $this->delete('{{%cities}}');

        $this->addColumn('{{%cities}}', 'status', $this->smallInteger(1)->notNull()->defaultValue(1)->after('id'));
    }

    public function safeDown() {
        $this->dropColumn('{{%cities}}', 'status');
    }
}
