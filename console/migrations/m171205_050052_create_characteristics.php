<?php
use yii\db\Migration;

class m171205_050052_create_characteristics extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%characteristics}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'type' => "ENUM('boolean', 'string', 'list')",
            'name' => $this->string(63)->notNull(),
            'values' => $this->text()->null(),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_characteristics_category_id',
            '{{%characteristics}}',
            'category_id',
            '{{%categories}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_characteristics_category_id', '{{%characteristics}}');
        $this->dropTable('{{%characteristics}}');
    }
}
