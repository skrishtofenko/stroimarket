<?php
use yii\db\Migration;

class m171229_110820_alter_companies extends Migration {
    public function safeUp() {
        $this->addColumn('{{%companies}}', 'is_main', $this->boolean()->notNull()->defaultValue(0)->after('city_id'));
    }

    public function safeDown() {
        $this->dropColumn('{{%companies}}', 'is_main');
    }
}
