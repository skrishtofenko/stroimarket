<?php
use yii\db\Migration;

class m171213_075810_alter_legal_entities extends Migration {
    public function safeUp() {
        $this->alterColumn('{{%legal_entities}}', 'ownership_type_id', $this->integer()->null());
    }

    public function safeDown() {
        $this->alterColumn('{{%legal_entities}}', 'ownership_type_id', $this->integer()->notNull());
    }
}
