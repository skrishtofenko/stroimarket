<?php
use yii\db\Migration;

class m171214_101304_alter_notices extends Migration {
    public function safeUp() {
        $this->dropForeignKey('FK_notices_company_id', '{{%notices}}');
        $this->renameColumn('{{%notices}}', 'company_id', 'user_id');
        $this->addForeignKey(
            'FK_notices_user_id',
            '{{%notices}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_notices_user_id', '{{%notices}}');
        $this->renameColumn('{{%notices}}', 'user_id', 'company_id');
        $this->addForeignKey(
            'FK_notices_company_id',
            '{{%notices}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }
}
