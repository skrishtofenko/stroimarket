<?php
use yii\db\Migration;

class m171206_103147_alter_messages extends Migration {
    public function safeUp() {
        $this->addColumn('{{%messages}}', 'is_read', $this->boolean()->notNull()->defaultValue(0)->after('to_company_id'));
    }

    public function safeDown() {
        $this->dropColumn('{{%messages}}', 'is_read');
    }
}
