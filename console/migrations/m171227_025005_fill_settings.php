<?php
use yii\db\Migration;

class m171227_025005_fill_settings extends Migration {
    public function safeUp() {
        $this->alterColumn('{{%settings}}', 'name', $this->string(63)->notNull());

        $this->insert('{{%settings}}', ['alias' => 'smAddress', 'name' => 'Почтовый адрес']);
        $this->insert('{{%settings}}', ['alias' => 'smBKPhone', 'name' => 'Телефон бухгалтерии']);
        $this->insert('{{%settings}}', ['alias' => 'smBKFax', 'name' => 'Факс бухгалтерии']);
        $this->insert('{{%settings}}', ['alias' => 'smBKEmail', 'name' => 'Email бухгалтерии']);
        $this->insert('{{%settings}}', ['alias' => 'smTechPhone', 'name' => 'Телефон техотдела']);
        $this->insert('{{%settings}}', ['alias' => 'smTechFax', 'name' => 'Факс техотдела']);
        $this->insert('{{%settings}}', ['alias' => 'smTechEmail', 'name' => 'Email техотдела']);
        $this->insert('{{%settings}}', ['alias' => 'smUrl', 'name' => 'Адрес сайта']);
        $this->insert('{{%settings}}', ['alias' => 'smTitle', 'name' => 'Наименование юрлица']);
        $this->insert('{{%settings}}', ['alias' => 'smDirector', 'name' => 'Имя директора']);
        $this->insert('{{%settings}}', ['alias' => 'smBKName', 'name' => 'Имя главного бухгалтера']);
        $this->insert('{{%settings}}', ['alias' => 'regServTitle', 'name' => 'Наименование услуги подтверждения аккаунта']);
        $this->insert('{{%settings}}', ['alias' => 'regServCost', 'name' => 'Стоимость подтверждения аккаунта']);
        $this->insert('{{%settings}}', ['alias' => 'subsServTitle', 'name' => 'Наименование услуги подписки']);
        $this->insert('{{%settings}}', ['alias' => 'subsServCost', 'name' => 'Стоимость подписки (в месяц)']);
        $this->insert('{{%settings}}', ['alias' => 'smINN', 'name' => 'ИНН']);
        $this->insert('{{%settings}}', ['alias' => 'smKPP', 'name' => 'КПП']);
        $this->insert('{{%settings}}', ['alias' => 'smAccount', 'name' => 'Рассчетный счет']);
        $this->insert('{{%settings}}', ['alias' => 'smBank', 'name' => 'Наименование банка']);
        $this->insert('{{%settings}}', ['alias' => 'smCorrAccount', 'name' => 'Корреспондентский счет']);
        $this->insert('{{%settings}}', ['alias' => 'smBIK', 'name' => 'БИК']);
        $this->insert('{{%settings}}', ['alias' => 'smOKPO', 'name' => 'ОКПО']);
        $this->insert('{{%settings}}', ['alias' => 'smOGRN', 'name' => 'ОГРН']);
    }

    public function safeDown() {
        $this->delete('{{%settings}}', ['alias' => 'smAddress']);
        $this->delete('{{%settings}}', ['alias' => 'smBKPhone']);
        $this->delete('{{%settings}}', ['alias' => 'smBKFax']);
        $this->delete('{{%settings}}', ['alias' => 'smBKEmail']);
        $this->delete('{{%settings}}', ['alias' => 'smTechPhone']);
        $this->delete('{{%settings}}', ['alias' => 'smTechFax']);
        $this->delete('{{%settings}}', ['alias' => 'smTechEmail']);
        $this->delete('{{%settings}}', ['alias' => 'smUrl']);
        $this->delete('{{%settings}}', ['alias' => 'smTitle']);
        $this->delete('{{%settings}}', ['alias' => 'smDirector']);
        $this->delete('{{%settings}}', ['alias' => 'smBKName']);
        $this->delete('{{%settings}}', ['alias' => 'regServTitle']);
        $this->delete('{{%settings}}', ['alias' => 'regServCost']);
        $this->delete('{{%settings}}', ['alias' => 'subsServTitle']);
        $this->delete('{{%settings}}', ['alias' => 'subsServCost']);
        $this->delete('{{%settings}}', ['alias' => 'smINN']);
        $this->delete('{{%settings}}', ['alias' => 'smKPP']);
        $this->delete('{{%settings}}', ['alias' => 'smAccount']);
        $this->delete('{{%settings}}', ['alias' => 'smBank']);
        $this->delete('{{%settings}}', ['alias' => 'smCorrAccount']);
        $this->delete('{{%settings}}', ['alias' => 'smBIK']);
        $this->delete('{{%settings}}', ['alias' => 'smOKPO']);
        $this->delete('{{%settings}}', ['alias' => 'smOGRN']);

        $this->alterColumn('{{%settings}}', 'name', $this->string(31)->notNull());
    }
}
