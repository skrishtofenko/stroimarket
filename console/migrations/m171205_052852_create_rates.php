<?php
use yii\db\Migration;

class m171205_052852_create_rates extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%rates}}', [
            'company_id' => $this->integer()->notNull(),
            'approve_rate' => $this->decimal(2, 1)->notNull()->defaultValue(0),
            'paid_rate' => $this->decimal(2, 1)->notNull()->defaultValue(0),
            'sm_reg_rate' => $this->decimal(2, 1)->notNull()->defaultValue(0),
            'le_reg_rate' => $this->decimal(2, 1)->notNull()->defaultValue(0),
        ], $tableOptions);
        $this->addPrimaryKey('PK_rates', '{{%rates}}', ['company_id']);

        $this->addForeignKey(
            'FK_rates_company_id',
            '{{%rates}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_rates_company_id', '{{%rates}}');
        $this->dropTable('{{%rates}}');
    }
}
