<?php
use yii\db\Migration;

class m171214_045648_join_users_legal_entities extends Migration {
    public function safeUp() {
        $this->addColumn('{{%users}}', 'ownership_type_id', $this->integer()->null()->after('city_id'));
        $this->addColumn('{{%users}}', 'is_request_completed', $this->boolean()->notNull()->defaultValue(0)->after('ownership_type_id'));
        $this->addColumn('{{%users}}', 'legal_name', $this->string(127)->notNull()->defaultValue('')->after('fio'));
        $this->addColumn('{{%users}}', 'logo', $this->string(31)->null()->defaultValue(null)->after('legal_name'));
        $this->addColumn('{{%users}}', 'checking_file', $this->string(31)->null()->defaultValue(null)->after('logo'));
        $this->addColumn('{{%users}}', 'inn', $this->string(12)->null()->defaultValue(null)->after('checking_file'));
        $this->addColumn('{{%users}}', 'ogrn', $this->string(15)->null()->defaultValue(null)->after('inn'));
        $this->addColumn('{{%users}}', 'kpp', $this->string(9)->null()->defaultValue(null)->after('ogrn'));
        $this->addColumn('{{%users}}', 'okpo', $this->string(10)->null()->defaultValue(null)->after('kpp'));
        $this->addColumn('{{%users}}', 'legal_registered_at', $this->timestamp()->null()->defaultValue(null)->after('okpo'));

        $this->addForeignKey(
            'FK_users_ownership_type_id',
            '{{%users}}',
            'ownership_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropForeignKey('FK_companies_legal_entity_id', '{{%companies}}');
        $this->renameColumn('{{%companies}}', 'legal_entity_id', 'user_id');
        $this->addForeignKey(
            'FK_companies_user_id',
            '{{%companies}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropForeignKey('FK_requisites_legal_entity_id', '{{%requisites}}');
        $this->renameColumn('{{%requisites}}', 'legal_entity_id', 'user_id');
        $this->addForeignKey(
            'FK_requisites_user_id',
            '{{%requisites}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropForeignKey('FK_legal_entities_user_id', '{{%legal_entities}}');
        $this->dropForeignKey('FK_legal_entities_city_id', '{{%legal_entities}}');
        $this->dropForeignKey('FK_legal_entities_ownership_type_id', '{{%legal_entities}}');
        $this->dropTable('{{%legal_entities}}');
    }

    public function safeDown() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%legal_entities}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'ownership_type_id' => $this->integer()->null(),
            'is_request_completed' => $this->boolean()->notNull()->defaultValue(0),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'legal_name' => $this->string(127)->notNull()->defaultValue(''),
            'logo' => $this->string(31)->null()->defaultValue(null),
            'checking_file' => $this->string(31)->null()->defaultValue(null),
            'inn' => $this->string(12)->null()->defaultValue(null),
            'ogrn' => $this->string(15)->null()->defaultValue(null),
            'kpp' => $this->string(9)->null()->defaultValue(null),
            'okpo' => $this->string(10)->null()->defaultValue(null),
            'legal_registered_at' => $this->timestamp()->null()->defaultValue(null),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_legal_entities_user_id',
            '{{%legal_entities}}',
            'user_id',
            '{{%users}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_legal_entities_city_id',
            '{{%legal_entities}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_legal_entities_ownership_type_id',
            '{{%legal_entities}}',
            'ownership_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropForeignKey('FK_companies_user_id', '{{%companies}}');
        $this->renameColumn('{{%companies}}', 'user_id', 'legal_entity_id');
        $this->addForeignKey(
            'FK_companies_legal_entity_id',
            '{{%companies}}',
            'legal_entity_id',
            '{{%legal_entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropForeignKey('FK_requisites_user_id', '{{%requisites}}');
        $this->renameColumn('{{%requisites}}', 'user_id', 'legal_entity_id');
        $this->addForeignKey(
            'FK_requisites_legal_entity_id',
            '{{%requisites}}',
            'legal_entity_id',
            '{{%legal_entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->dropForeignKey('FK_users_ownership_type_id', '{{%users}}');
        $this->dropColumn('{{%users}}', 'ownership_type_id');
        $this->dropColumn('{{%users}}', 'is_request_completed');
        $this->dropColumn('{{%users}}', 'legal_name');
        $this->dropColumn('{{%users}}', 'logo');
        $this->dropColumn('{{%users}}', 'checking_file');
        $this->dropColumn('{{%users}}', 'inn');
        $this->dropColumn('{{%users}}', 'ogrn');
        $this->dropColumn('{{%users}}', 'kpp');
        $this->dropColumn('{{%users}}', 'okpo');
        $this->dropColumn('{{%users}}', 'legal_registered_at');
    }
}
