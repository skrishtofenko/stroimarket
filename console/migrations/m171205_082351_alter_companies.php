<?php
use yii\db\Migration;

class m171205_082351_alter_companies extends Migration {
    public function safeUp() {
        $this->addColumn('{{%companies}}', 'is_paid', $this->boolean()->notNull()->defaultValue(0)->after('is_delivery'));
        $this->addColumn('{{%companies}}', 'paid_till', $this->timestamp()->null()->defaultValue(null)->after('type'));
        $this->addColumn('{{%companies}}', 'status', $this->smallInteger(1)->notNull()->defaultValue(0)->after('type'));
    }

    public function safeDown() {
        $this->dropColumn('{{%companies}}', 'is_paid');
        $this->dropColumn('{{%companies}}', 'paid_till');
        $this->dropColumn('{{%companies}}', 'status');
    }
}
