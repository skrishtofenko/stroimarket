<?php
use yii\db\Migration;

class m171205_061523_create_catalogs extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%catalogs}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'is_processed' => $this->boolean()->defaultValue(0),
            'tries_count' => $this->smallInteger(1)->defaultValue(0),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_catalogs_company_id',
            '{{%catalogs}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_catalogs_company_id', '{{%catalogs}}');
        $this->dropTable('{{%catalogs}}');
    }
}
