<?php
use yii\db\Migration;

class m180215_063945_alter_companies_products extends Migration {
    public function safeUp() {
        $this->addColumn('{{%companies_products}}', 'is_by_order', $this->boolean()->null()->defaultValue(0)->after('is_in_stock'));
        $this->addColumn('{{%companies_products}}', 'name', $this->string()->null()->defaultValue(null)->after('status'));
        $this->addColumn('{{%companies_products}}', 'url', $this->string()->null()->defaultValue(null)->after('cnt'));
    }

    public function safeDown() {
        $this->dropColumn('{{%companies_products}}', 'is_by_order');
        $this->dropColumn('{{%companies_products}}', 'name');
        $this->dropColumn('{{%companies_products}}', 'url');
    }
}
