<?php
use yii\db\Migration;

class m171205_054709_create_files extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%files}}', [
            'id' => $this->primaryKey(),
            'message_id' => $this->integer()->notNull(),
            'filename' => $this->string(31)->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_files_message_id',
            '{{%files}}',
            'message_id',
            '{{%messages}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_files_message_id', '{{%files}}');
        $this->dropTable('{{%files}}');
    }
}
