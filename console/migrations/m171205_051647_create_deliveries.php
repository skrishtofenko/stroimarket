<?php
use yii\db\Migration;

class m171205_051647_create_deliveries extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%deliveries}}', [
            'company_id' => $this->integer()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'delivery_type_id' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addPrimaryKey('PK_deliveries', '{{%deliveries}}', ['company_id', 'city_id']);

        $this->addForeignKey(
            'FK_deliveries_company_id',
            '{{%deliveries}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_deliveries_city_id',
            '{{%deliveries}}',
            'city_id',
            '{{%cities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_deliveries_delivery_type_id',
            '{{%deliveries}}',
            'delivery_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_deliveries_company_id', '{{%deliveries}}');
        $this->dropForeignKey('FK_deliveries_city_id', '{{%deliveries}}');
        $this->dropForeignKey('FK_deliveries_delivery_type_id', '{{%deliveries}}');
        $this->dropTable('{{%deliveries}}');
    }
}
