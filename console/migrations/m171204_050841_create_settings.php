<?php
use yii\db\Migration;

class m171204_050841_create_settings extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%settings}}', [
            'alias' => $this->string(15)->notNull(),
            'name' => $this->string(31)->notNull(),
            'value' => $this->string(255)->null(),
        ], $tableOptions);
        $this->addPrimaryKey('PK_settings', 'settings', ['alias']);
    }

    public function safeDown() {
        $this->dropTable('{{%settings}}');
    }
}
