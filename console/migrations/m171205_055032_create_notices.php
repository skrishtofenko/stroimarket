<?php
use yii\db\Migration;

class m171205_055032_create_notices extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%notices}}', [
            'company_id' => $this->integer()->notNull(),
            'messages' => $this->boolean()->notNull()->defaultValue(0),
            'actions' => $this->boolean()->notNull()->defaultValue(0),
            'offers' => $this->boolean()->notNull()->defaultValue(0),
            'claims' => $this->boolean()->notNull()->defaultValue(0),
            'specoffers' => $this->boolean()->notNull()->defaultValue(0),
            'purchases' => $this->boolean()->notNull()->defaultValue(0),
        ], $tableOptions);
        $this->addPrimaryKey('PK_notices', '{{%notices}}', ['company_id']);

        $this->addForeignKey(
            'FK_notices_company_id',
            '{{%notices}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_notices_company_id', '{{%notices}}');
        $this->dropTable('{{%notices}}');
    }
}
