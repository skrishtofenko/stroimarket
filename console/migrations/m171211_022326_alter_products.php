<?php
use yii\db\Migration;

class m171211_022326_alter_products extends Migration {
    public function safeUp() {
        $this->addColumn('{{%products}}', 'unit_type_id', $this->integer()->notNull()->after('manufacturer_id'));
        $this->addForeignKey(
            'FK_products_unit_type_id',
            '{{%products}}',
            'unit_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_products_unit_type_id', '{{%products}}');
        $this->dropColumn('{{%products}}', 'unit_type_id');
    }
}
