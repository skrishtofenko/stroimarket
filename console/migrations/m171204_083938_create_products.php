<?php
use yii\db\Migration;

class m171204_083938_create_products extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'manufacturer_id' => $this->integer()->notNull(),
            'name' => $this->string(63)->notNull(),
            'image' => $this->string(31)->null()->defaultValue(null),
            'description' => $this->text(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_products_category_id',
            '{{%products}}',
            'category_id',
            '{{%categories}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_products_manufacturer_id',
            '{{%products}}',
            'manufacturer_id',
            '{{%manufacturers}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_products_category_id', '{{%products}}');
        $this->dropForeignKey('FK_products_manufacturer_id', '{{%products}}');
        $this->dropTable('{{%products}}');
    }
}
