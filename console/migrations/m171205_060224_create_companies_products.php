<?php
use yii\db\Migration;

class m171205_060224_create_companies_products extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%companies_products}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'unit_type_id' => $this->integer()->null()->defaultValue(null),
            'is_action' => $this->boolean()->null()->defaultValue(null),
            'vendor_code' => $this->string(63)->null()->defaultValue(null),
            'cost' => $this->integer()->null()->defaultValue(null),
            'stock_balance' => $this->integer()->null()->defaultValue(null),
            'multiplicity' => $this->decimal(10,2)->null()->defaultValue(null),
            'cnt' => $this->decimal(10,2)->null()->defaultValue(null),
            'comment' => $this->text()->null(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_companies_products_company_id',
            '{{%companies_products}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_companies_products_product_id',
            '{{%companies_products}}',
            'product_id',
            '{{%products}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_companies_products_unit_type_id',
            '{{%companies_products}}',
            'unit_type_id',
            '{{%entities}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_companies_products_company_id', '{{%companies_products}}');
        $this->dropForeignKey('FK_companies_products_product_id', '{{%companies_products}}');
        $this->dropForeignKey('FK_companies_products_unit_type_id', '{{%companies_products}}');
        $this->dropTable('{{%companies_products}}');
    }
}
