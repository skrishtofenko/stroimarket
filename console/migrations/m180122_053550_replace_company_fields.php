<?php
use yii\db\Migration;

class m180122_053550_replace_company_fields extends Migration {
    public function safeUp() {
        $this->addColumn('{{%users}}', 'url', $this->string()->null()->defaultValue(null)->after('legal_name'));
        $this->addColumn('{{%users}}', 'description', $this->text()->null()->after('okpo'));

        $this->dropColumn('{{%companies}}', 'url');
        $this->dropColumn('{{%companies}}', 'description');
    }

    public function safeDown() {
        $this->addColumn('{{%companies}}', 'url', $this->string()->null()->defaultValue(null)->after('emails'));
        $this->addColumn('{{%companies}}', 'description', $this->text()->null()->after('schedule'));

        $this->dropColumn('{{%users}}', 'url');
        $this->dropColumn('{{%users}}', 'description');
    }
}
