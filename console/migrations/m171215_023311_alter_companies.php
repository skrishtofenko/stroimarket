<?php
use yii\db\Migration;

class m171215_023311_alter_companies extends Migration {
    public function safeUp() {
        $this->alterColumn('{{%companies}}', 'address', $this->text()->null());
        $this->alterColumn('{{%companies}}', 'phones', $this->text()->null());
        $this->alterColumn('{{%companies}}', 'emails', $this->text()->null());
    }

    public function safeDown() {
        $this->alterColumn('{{%companies}}', 'address', $this->text()->notNull());
        $this->alterColumn('{{%companies}}', 'phones', $this->text()->notNull());
        $this->alterColumn('{{%companies}}', 'emails', $this->text()->notNull());
    }
}
