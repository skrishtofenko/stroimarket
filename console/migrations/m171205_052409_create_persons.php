<?php
use yii\db\Migration;

class m171205_052409_create_persons extends Migration {
    public function safeUp() {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%persons}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'is_main' => $this->boolean()->notNull()->defaultValue(0),
            'fio' => $this->string(127)->null(),
            'email' => $this->string(127)->null(),
            'phone' => $this->string(10)->null(),
            'questions' => $this->text()->null(),

            'created_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->null()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
        ], $tableOptions);

        $this->addForeignKey(
            'FK_persons_company_id',
            '{{%persons}}',
            'company_id',
            '{{%companies}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown() {
        $this->dropForeignKey('FK_persons_company_id', '{{%persons}}');
        $this->dropTable('{{%persons}}');
    }
}
