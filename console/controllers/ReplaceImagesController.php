<?php
namespace console\controllers;

use yii\console\Controller;

class ReplaceImagesController extends Controller {
    public function actionIndex() {
        $imagesRootPath = dirname(dirname(__DIR__)) . '/files';
        $imagesNewPath = dirname(dirname(__DIR__)) . '/files1';
        if(!file_exists($imagesRootPath)) {
            return;
        }

        $files = scandir($imagesRootPath);
        for($i = 2; $i < count($files); $i++) {
            $fileName = $files[$i];
            $path = "$imagesRootPath/$fileName";
            if(is_file($path)) {
                $fileFirst2 = mb_substr($fileName, 0, 2, 'utf-8');
                $fileFirst4 = mb_substr($fileName, 0, 4, 'utf-8');
                $newImageDir = "$imagesNewPath/$fileFirst2/$fileFirst4";
                if(!file_exists($newImageDir)) {
                    mkdir($newImageDir, 0777, true);
                }

                $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);
                $fileMime = mime_content_type($path);
                $newFileName = str_replace(".$fileExt", '.jpg', $fileName);
                $newImagePath = "$newImageDir/$newFileName";
                if(!file_exists($newImagePath)) {
                    if($fileMime == 'image/jpeg') {
                        rename($path, $newImagePath);
                    } else {
                        $sourceSizes = getimagesize($path);
                        $createImageFuncName = "imagecreatefrom" . explode('/', $fileMime)[1];
                        $sourceFile = $createImageFuncName($path);
                        $finalImage = imagecreatetruecolor($sourceSizes[0], $sourceSizes[1]);
                        if($createImageFuncName == "imagecreatefrompng") {
                            $bgRectangle = imagecolorallocate($finalImage, 255, 255, 255);
                            imagefilledrectangle($finalImage, 0, 0, $sourceSizes[0], $sourceSizes[1], $bgRectangle);
                        }
                        imagecopyresampled(
                            $finalImage, $sourceFile,
                            0, 0,
                            0, 0,
                            $sourceSizes[0], $sourceSizes[1],
                            $sourceSizes[0], $sourceSizes[1]
                        );
                        imagejpeg($finalImage, $newImagePath, 85);
                        unlink($path);
                    }
                }
                echo "$i: $fileName renamed to $newFileName\n";
            }
        }
    }
}
