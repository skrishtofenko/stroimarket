<?php
namespace console\controllers;

use common\models\extended\Rates;
use yii\console\Controller;

class RecountRatesController extends Controller {
    public function actionIndex() {
        Rates::recountAll();
    }
}
