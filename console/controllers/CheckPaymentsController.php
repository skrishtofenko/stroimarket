<?php
namespace console\controllers;

use common\models\extended\Companies;
use yii\console\Controller;

class CheckPaymentsController extends Controller {
    public function actionIndex() {
        Companies::checkAllPayments();
    }
}
