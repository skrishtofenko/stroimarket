<?php
namespace console\controllers;

use common\models\extended\Products;
use console\models\CompaniesProducts;
use yii\console\Controller;

class SetManufacturersController extends Controller {
    const PAGE_SIZE = 100;

    public function actionIndex() {
        $page = 0;
        while(
            $products = Products::find()
                ->orderBy('id')
                ->limit(self::PAGE_SIZE)
                ->offset($page * self::PAGE_SIZE)
                ->all()
        ) {
            foreach($products as $product) {
                /** @var $product Products */
                if($product->manufacturer_id) {
                    CompaniesProducts::createOrUpdateForManufacturer($product);
                }
            }
            $page++;
        }
    }
}
