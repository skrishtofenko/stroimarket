<?php
namespace console\controllers;

use console\models\Catalogs;
use yii\console\Controller;

class ProcessCatalogController extends Controller {
    public function actionIndex() {
        Catalogs::process();
    }
}
