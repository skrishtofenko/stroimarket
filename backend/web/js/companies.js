$(function() {
    $('#companies-type').on('change', function() {
        var companyType = $(this).val();
        $('.custom-block').slideUp();
        $('.for-' + companyType).slideDown();
        if(companyType !== 'store' || !$('#companies-is_imonly').prop('checked')) {
            $('.address-block').slideDown();
        } else {
            $('.address-block').slideUp();
        }
    });

    $('#companies-is_imonly').on('change', function() {
        if($(this).prop('checked')) {
            $('.address-block').slideUp();
            $('.im-block').slideDown();
        } else {
            $('.address-block').slideDown();
            $('.im-block').slideUp();
        }
    });

    $('#companies-is_delivery').on('change', function() {
        if($(this).prop('checked')) {
            $('#delivery-block').slideDown();
        } else {
            $('#delivery-block').slideUp();
        }
    });

    $('#add-phone').on('click', function() {
        var phoneField = $('.phone-field:last').clone();
        phoneField.find('input[type=text]').val('');
        $('#phones').append(phoneField).find('div.remove-phone').slideDown();
        bindPhoneMask();
        bindRemoveAction('phone');
    });

    $('#add-email').on('click', function() {
        var emailField = $('.email-field:last').clone();
        emailField.find('input[type=text]').val('');
        $('#emails').append(emailField).find('div.remove-email').slideDown();
        bindRemoveAction('email');
    });

    $('#add-delivery_city').on('click', function() {
        var deliveryCityRow = $('.delivery_city-field:last').clone();
        deliveryCityRow.find('select').val(-1);
        $('#delivery_cities tbody').append(deliveryCityRow).find('div.remove-delivery_city').slideDown();
        bindRemoveAction('delivery_city');
    });

    $('input.time-input').inputmask({
        'mask': '99:99',
    });

    bindPhoneMask();
    bindRemoveAction('phone');
    bindRemoveAction('email');
    bindRemoveAction('delivery_city');
});

function bindPhoneMask() {
    $('.phone-field input[type=text]').each(function() {
        $(this).inputmask({
            'mask': '+7 (999) 999-99-99',
            'removeMaskOnSubmit': true,
            'autoUnmask': true
        });
    });
}

function bindRemoveAction(type) {
    $('div.remove-' + type).on('click', function() {
        var div = $(this).parent();
        if(type === 'delivery_city') {
            div = div.parent();
        }
        div.slideUp(400, function() {
            div.remove();
            if($('.' + type + '-field').length < 2) {
                $('div.remove-' + type).off('click').slideUp();
            }
        });
    });
}
