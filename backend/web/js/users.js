$(function() {
    $('#users-ownership_type_id').on('change', function() {
        var legalEntityTypeId = parseInt($(this).val());
        if(legalEntityTypeId === 0) {
            $('#fileds_for_legal_entity_only').slideUp();
        } else {
            $('#fileds_for_legal_entity_only').slideDown();
        }
    });

    $('#show-decline-form').click(function() {
        $('#declineFormDiv').slideDown(200);
    });

    $('#hide-decline-form').click(function() {
        $('#declineFormDiv').slideUp(200);
    });
});
