<?php
namespace backend\models\forms;

use Yii;
use yii\base\Model;

class DeclineForm extends Model {
    public $message;

    public function rules() {
        return [
            [['message'], 'required'],
            ['message', 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'message' => Yii::t('app', 'Your message for user'),
        ];
    }
}
