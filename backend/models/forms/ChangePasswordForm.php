<?php
namespace backend\models\forms;

use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model {
	public $old_password;
	public $new_password;
	public $repeat_new_password;

	public function rules() {
		return [
			[['old_password', 'new_password', 'repeat_new_password'], 'required'],
			['old_password', 'findPasswords'],
			['repeat_new_password', 'compare', 'compareAttribute' => 'new_password'],
		];
	}

	public function findPasswords($attribute, $params) {
		if(Yii::$app->user->identity->password_hash != Yii::$app->user->identity->validatePassword($this->old_password)) {
			$this->addError($attribute, Yii::t('app', 'Wrong old password'));
		}
	}

	public function attributeLabels() {
		return [
			'old_password' => Yii::t('app', 'Current password'),
			'new_password' => Yii::t('app', 'New password'),
			'repeat_new_password' => Yii::t('app', 'Repeat new password'),
		];
	}
}