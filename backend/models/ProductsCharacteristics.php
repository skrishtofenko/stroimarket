<?php
namespace backend\models;

use Yii;

class ProductsCharacteristics extends \common\models\extended\ProductsCharacteristics {
    public function rules() {
        $rules = parent::rules();
        foreach ($rules as $index => $rule) {
            if($rule[1] == 'required') {
                $rules[$index][0] = ['characteristic_id'];
            }
            if($rule[1] == 'string' || $rule[1] == 'unique') {
                unset($rules[$index]);
            }
        }
        if($this->characteristic->type == Characteristics::TYPE_LIST && $this->characteristic->is_multiple) {
            $rules[] = ['value', 'each', 'rule' => ['string']];
        } else {
            $rules[] = ['value', 'string'];
        }
        return $rules;
    }

    public function getCharacteristic() {
        return $this->hasOne(Characteristics::className(), ['id' => 'characteristic_id'])->inverseOf('productsCharacteristics');
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if(is_array($this->value) && !empty($this->value)) {
                $values = $this->value;
                foreach($values as &$value) {
                    $value = trim($value);
                }
                $this->value = ('|' . implode('|', $values) . '|');
            }
            return true;
        }
        return false;
    }

    public static function getEmptyForCategory(Categories $category) {
        $characteristics = [];
        foreach($category->characteristics as $characteristic) {
            $newCharacteristic = new ProductsCharacteristics();
            $newCharacteristic->characteristic_id = $characteristic->id;
            $characteristics[$characteristic->id] = $newCharacteristic;
        }
        return $characteristics;
    }

    public static function fillFromPost(&$characteristics) {
        /** @var ProductsCharacteristics[] $characteristics */
        $postCharacteristics = Yii::$app->request->post('ProductsCharacteristics', []);
        $valid = true;
        if(!empty($postCharacteristics)) {
            foreach($postCharacteristics as $postCharacteristic) {
                $characteristicId = $postCharacteristic['ProductsCharacteristics']['characteristic_id'];
                $characteristics[$characteristicId]->load($postCharacteristic);
                if(!$characteristics[$characteristicId]->validate()) {
                    $valid = false;
                }
            }
        }
        return $valid;
    }

    public static function fillFromProduct(Products $product) {
        /** @var ProductsCharacteristics[] $characteristics */
        $characteristics = ProductsCharacteristics::getEmptyForCategory($product->category);
        $productCharacteristics = $product->productsCharacteristics;
        if(!empty($productCharacteristics)) {
            foreach($productCharacteristics as $productCharacteristic) {
                $characteristicId = $productCharacteristic->characteristic_id;
                if(isset($characteristics[$characteristicId])) {
                    $characteristics[$characteristicId]->load(['ProductsCharacteristics' => $productCharacteristic->attributes]);
                }
            }
        }
        return $characteristics;
    }

    public function getSelected() {
        $result = [];
        if($this->characteristic->type == Characteristics::TYPE_LIST && $this->characteristic->is_multiple) {
            $values = explode('|', trim($this->value, '|'));
            foreach ($values as $value) {
                $result[$value] = ['selected' => true];
            }
        }
        return $result;
    }

    public function getNiceValue() {
        if($this->characteristic->type == Characteristics::TYPE_LIST && $this->characteristic->is_multiple) {
            return str_replace('|', '<br />', trim($this->value, '|'));
        }
        if($this->characteristic->type == Characteristics::TYPE_BOOLEAN) {
            if($this->value === '') {
                return '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>';
            }
            return $this->value ? 'Да' : 'Нет';
        }
        return $this->value;
    }
}
