<?php
namespace backend\models;

class Manufacturers extends \common\models\extended\Manufacturers {
    public function isEmpty() {
        return !$this->products_count;
    }
}
