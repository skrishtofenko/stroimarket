<?php
namespace backend\models;

use Yii;
use common\components\ImageManager\ImageManager;

class Users extends \common\models\extended\Users {
    public function getMainRequisites() {
        return $this->hasOne(Requisites::className(), ['user_id' => 'id'])->andWhere(['is_default' => true]);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord) {
                $this->status = self::STATUS_ACTIVE;
                $this->role = self::ROLE_COMPANY;
            }
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if($insert && $this->is_password_sent) {
            $this->email('createdByAdmin', $this->email, [
                'user' => $this
            ]);
        } else {
            if(isset($changedAttributes['email'])) {
                $this->email('emailUpdatedByAdmin', $this->email, [
                    'user' => $this
                ]);
            }
        }
        ImageManager::upload($this, 'logo');
    }
}
