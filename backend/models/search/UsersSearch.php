<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Users;

/**
 * UsersSearch represents the model behind the search form about `backend\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'status', 'ownership_type_id', 'is_request_completed'], 'integer'],
            [['role', 'password_hash', 'email', 'phone', 'fio', 'created_at', 'updated_at', 'legal_name', 'logo', 'checking_file', 'inn', 'ogrn', 'kpp', 'okpo', 'legal_registered_at', 'url', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find()->where(['!=', 'status', Users::STATUS_DELETED])->andWhere(['role' => Users::ROLE_COMPANY]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'is_request_completed' => $this->is_request_completed,
            'legal_registered_at' => $this->legal_registered_at,
        ]);

        if($this->ownership_type_id) {
            $query->andFilterWhere([
                'ownership_type_id' => $this->ownership_type_id,
            ]);
        } elseif($this->ownership_type_id != '') {
            $query->andWhere(['ownership_type_id' => null]);
        }

        $query->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', preg_replace('/[^0-9]+/', '', $this->phone)])
            ->andFilterWhere(['like', 'fio', $this->fio])
            ->andFilterWhere(['like', 'legal_name', $this->legal_name])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'checking_file', $this->checking_file])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'ogrn', $this->ogrn])
            ->andFilterWhere(['like', 'kpp', $this->kpp])
            ->andFilterWhere(['like', 'okpo', $this->okpo])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
