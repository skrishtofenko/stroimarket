<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Companies;

/**
 * CompaniesSearch represents the model behind the search form about `backend\models\Companies`.
 */
class CompaniesSearch extends Companies
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'city_id', 'is_manufacturer', 'is_imonly', 'is_delivery', 'is_paid', 'status'], 'integer'],
            [['type', 'paid_till', 'name', 'address', 'phones', 'emails', 'url', 'schedule', 'description', 'catalog_updated_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Companies::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'city_id' => $this->city_id,
            'is_manufacturer' => $this->is_manufacturer,
            'is_imonly' => $this->is_imonly,
            'is_delivery' => $this->is_delivery,
            'is_paid' => $this->is_paid,
            'status' => $this->status,
            'paid_till' => $this->paid_till,
            'catalog_updated_at' => $this->catalog_updated_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phones', $this->phones])
            ->andFilterWhere(['like', 'emails', $this->emails])
            ->andFilterWhere(['like', 'schedule', $this->schedule]);

        return $dataProvider;
    }
}
