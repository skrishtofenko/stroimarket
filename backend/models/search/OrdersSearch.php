<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `backend\models\Orders`.
 */
class OrdersSearch extends Orders
{
    public $companyName;
    public $buyerName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'company_id', 'delivery_type_id', 'payment_type_id', 'is_deleted_by_buyer', 'is_deleted_by_seller', 'status', 'total_cost', 'comment'], 'integer'],
            [['address', 'flat', 'deliver_at', 'created_at', 'updated_at', 'companyName', 'buyerName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find()->joinWith(['company', 'user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['companyName'] = [
            'asc' => ['users.name' => SORT_ASC],
            'desc' => ['users.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['buyerName'] = [
            'asc' => ['users.fio' => SORT_ASC],
            'desc' => ['users.fio' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'company_id' => $this->company_id,
            'delivery_type_id' => $this->delivery_type_id,
            'payment_type_id' => $this->payment_type_id,
            'is_deleted_by_buyer' => $this->is_deleted_by_buyer,
            'is_deleted_by_seller' => $this->is_deleted_by_seller,
            'status' => $this->status,
            'total_cost' => $this->total_cost,
            'comment' => $this->comment,
            'deliver_at' => $this->deliver_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'flat', $this->flat])
            ->andFilterWhere(['like', 'users.name', $this->companyName]);
        if($this->buyerName) {
            $query->andWhere("users.fio LIKE '%{$this->buyerName}%' OR users.email LIKE '%{$this->buyerName}%'");
        }

        return $dataProvider;
    }
}
