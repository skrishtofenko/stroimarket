<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Catalogs;

/**
 * CatalogsSearch represents the model behind the search form of `backend\models\Catalogs`.
 */
class CatalogsSearch extends Catalogs
{
    public $companyName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'is_by_admin', 'is_processed', 'tries_count'], 'integer'],
            [['file_name', 'upload_name', 'created_at', 'updated_at', 'companyName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Catalogs::find()->joinWith(['company']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['companyName'] = [
            'asc' => ['users.name' => SORT_ASC],
            'desc' => ['users.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'is_by_admin' => $this->is_by_admin,
            'is_processed' => $this->is_processed,
            'tries_count' => $this->tries_count,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'file_name', $this->file_name])
            ->andFilterWhere(['like', 'upload_name', $this->upload_name])
            ->andFilterWhere(['like', 'users.name', $this->companyName]);

        return $dataProvider;
    }
}
