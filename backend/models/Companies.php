<?php
namespace backend\models;

class Companies extends \common\models\extended\Companies {
    public static function getAssortmentList() {
        $companies = self::find()
            ->where(['type' => ['provider', 'store']])
            ->all();
        $result = [];
        foreach ($companies as $company) {
            /** @var $company self */
            $result[$company->id] = $company->user->legalEntityVcard;
        }
        return $result;
    }
}
