<?php
namespace backend\models;

use Yii;

class Catalogs extends \common\models\extended\Catalogs {
    public $catalogFile;

    public function rules() {
        $rules = parent::rules();
        $rules[] =  [
            'catalogFile',
            'file',
            'skipOnEmpty' => false,
            'extensions' => 'xls, xlsx',
            'mimeTypes' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel'],
            'maxSize' => 10 * 1024 * 1024
        ];
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['catalogFile'] = Yii::t('app', 'Catalog File');
        return $labels;
    }
}
