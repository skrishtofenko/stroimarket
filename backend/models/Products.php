<?php
namespace backend\models;

use Yii;
use common\components\ImageManager\ImageManager;

class Products extends \common\models\extended\Products {
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        ImageManager::upload($this, 'image');
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'category_id'])->inverseOf('products');
    }

    public function saveWithCharacteristics($characteristics) {
        $transaction = Yii::$app->db->beginTransaction();
        $oldManufacturerId = $this->oldAttributes['manufacturer_id'];
        if($this->save()) {
            ProductsCharacteristics::deleteAll(['product_id' => $this->id]);
            foreach($characteristics as $characteristic) {
                /** @var ProductsCharacteristics $characteristic */
                $characteristic->product_id = $this->id;
                if(!$characteristic->save(false)) {
                    $transaction->rollBack();
                    return false;
                }
            }
            $this->updateManufacturerProduct($oldManufacturerId);
        } else {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }

    public static function checkCategory($categoryId) {
        $category = null;
        if(!$categoryId) {
            return false;
        } else {
            $category = Categories::findOne($categoryId);
            if(!$category || $category->level != Categories::LEVEL_GOODSCATEGORY) {
                return false;
            }
        }
        return $category;
    }

    public function getProductsCharacteristics() {
        return $this->hasMany(ProductsCharacteristics::className(), ['product_id' => 'id'])->inverseOf('product');
    }

    private function updateManufacturerProduct($oldManufacturerId) {
        if($oldManufacturerId != $this->manufacturer_id) {
            if($oldManufacturerId) {
                CompaniesProducts::updateAll(['is_manufactured_by' => 0], ['company_id' => $oldManufacturerId, 'product_id' => $this->id]);
            }
            if($this->manufacturer_id) {
                CompaniesProducts::createOrUpdateForManufacturer($this);
            }
        }
    }
}
