<?php
namespace backend\models;

use Yii;
use yii\helpers\ArrayHelper;
use common\components\ImageManager\ImageManager;

class Categories extends \common\models\extended\Categories {
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        ImageManager::upload($this, 'image');
        if($this->level != self::LEVEL_CHAPTER) {
            $this->parent->recountChild();
        }
    }

    public function afterDelete() {
        parent::afterDelete();
        if($this->level != self::LEVEL_CHAPTER) {
            $this->parent->recountChild();
        }
    }

    public function isEmpty() {
        return !$this->products_count && !$this->child_count;
    }

    public function canHasChild() {
        return in_array($this->level, [self::LEVEL_CHAPTER, self::LEVEL_CATEGORY, self::LEVEL_SUBCATEGORY]);
    }

    public function safeUpdate($characteristics) {
        $transaction = Yii::$app->db->beginTransaction();
        if($this->save()) {
            if($this->level == self::LEVEL_GOODSCATEGORY) {
                $oldIDs = ArrayHelper::map($this->characteristics, 'id', 'id');
                $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($characteristics, 'id', 'id')));
                if(!empty($deletedIDs)) {
                    Characteristics::deleteAll(['id' => $deletedIDs]);
                }
                foreach($characteristics as $characteristic) {
                    /** @var Characteristics $characteristic */
                    $characteristic->category_id = $this->id;
                    if($characteristic->id) {
                        $attributes = ['Characteristics' => $characteristic->attributes];
                        $characteristic = Characteristics::findOne($characteristic->id);
                        $characteristic->load($attributes);
                    }
                    if(!$characteristic->save(false)) {
                        $transaction->rollBack();
                        return false;
                    }
                }
            }
        } else {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }
}
