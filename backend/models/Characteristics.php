<?php
namespace backend\models;

class Characteristics extends \common\models\extended\Characteristics {
    public function rules() {
        $rules = parent::rules();
        foreach ($rules as $index => $rule) {
            if($rule[1] == 'required') {
                $rules[$index][0] = ['name'];
            }
        }
        $rules[] = [['id'], 'integer'];
        return $rules;
    }

    public function getLabelText() {
        return $this->name . ($this->type == Characteristics::TYPE_STRING ? ", {$this->unitType->name}" : '');
    }
}
