<?php
namespace backend\models;

class Cities extends \common\models\extended\Cities {
    public function isEmpty() {
        return !$this->users_count && !$this->companies_count;
    }
}
