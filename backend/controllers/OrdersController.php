<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use backend\models\Orders;
use backend\models\search\OrdersSearch;
use backend\models\search\OrdersCompaniesProductsSearch;

class OrdersController extends AppController {
    public function behaviors() {
        return parent::behaviors();
    }

    public function actionIndex() {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $searchModel = new OrdersCompaniesProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['order_id' => $id]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'productsDataProvider' => $dataProvider,
        ]);
    }

    protected function findModel($id) {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
