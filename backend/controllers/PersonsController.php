<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use backend\models\Persons;
use backend\models\Companies;

class PersonsController extends AppController {
    public function behaviors() {
        return parent::behaviors();
    }

    public function actionCreate() {
        if(!$companyId = Yii::$app->request->get('company_id', false)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if(!$company = Companies::findOne($companyId)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new Persons();
        if($model->load(Yii::$app->request->post())) {
            $model->company_id = $companyId;
            if($model->save()) {
                return $this->redirect(['/companies/view', 'id' => $model->company_id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'company' => $company,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                return $this->redirect(['/companies/view', 'id' => $model->company_id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $person = $this->findModel($id);
        if(!$person->is_main) {
            $person->delete();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id) {
        if (($model = Persons::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionSetMain($id) {
        if($model = $this->findModel($id)) {
            Persons::updateAll(['is_main' => 0], ['company_id' => $model->company_id]);
            $model->updateAttributes(['is_main' => 1]);
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
