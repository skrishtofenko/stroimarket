<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use backend\models\Cities;
use backend\models\Users;
use backend\models\Companies;
use backend\models\Rates;
use backend\models\search\CompaniesSearch;
use backend\models\search\PersonsSearch;

class CompaniesController extends AppController {
    public function behaviors() {
        return parent::behaviors();
    }

    public function actionView($id) {
        $company = $this->findModel($id);

        $searchModel = new PersonsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['company_id' => $id]);

        return $this->render('view', [
            'model' => $company,
            'personsDataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        if(!$userId = Yii::$app->request->get('user_id', false)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $user = Users::findOne($userId);
        if(!$user || !$user->ownership_type_id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new Companies();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = $userId;
            $model->is_main = $user->mainCompany ? 0 : 1;
            if($model->save()) {
                $rates = new Rates();
                $rates->company_id = $model->id;
                $rates->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'cities' => Cities::getList(),
            'user' => $user,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'cities' => Cities::getList(),
        ]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id) {
        if (($model = Companies::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
