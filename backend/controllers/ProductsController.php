<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use common\components\ImageManager\ImageManager;
use backend\models\Products;
use backend\models\Categories;
use backend\models\ProductsCharacteristics;
use backend\models\search\ProductsSearch;

class ProductsController extends AppController {
    public function behaviors() {
        return parent::behaviors();
    }

    public function actionIndex() {
        $searchModel = new ProductsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        if(!$category = Products::checkCategory(Yii::$app->request->get('category_id', false))) {
            return $this->render('select-category', [
                'categories' => Categories::getList('id', 'name', ['level' => Categories::LEVEL_GOODSCATEGORY]),
            ]);
        }

        $model = new Products();
        $model->category_id = $category->id;
        /** @var ProductsCharacteristics[] $characteristics */
        $characteristics = ProductsCharacteristics::getEmptyForCategory($category);
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $valid = ProductsCharacteristics::fillFromPost($characteristics);
            if($valid && $model->saveWithCharacteristics($characteristics)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'category' => $category,
            'characteristics' => $characteristics,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $characteristics = ProductsCharacteristics::fillFromProduct($model);
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $valid = ProductsCharacteristics::fillFromPost($characteristics);
            if($valid && $model->saveWithCharacteristics($characteristics)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'characteristics' => $characteristics,
        ]);
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->updateAttributes(['is_active' => 0]);
        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id) {
        if (($model = Products::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionRemoveImage($id, $attribute, $filename) {
        $model = $this->findModel($id);
        ImageManager::removeImage($model, $attribute, $filename);
        return $this->redirect(Yii::$app->request->referrer);
    }
}
