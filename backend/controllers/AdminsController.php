<?php
namespace backend\controllers;

use Yii;
use yii\base\Exception;
use yii\web\NotFoundHttpException;
use backend\models\Admins;
use backend\models\search\AdminsSearch;
use backend\models\forms\ChangePasswordForm;

/**
 * AdminsController implements the CRUD actions for Admins model.
 */
class AdminsController extends AppController
{
//    public function actionIndex() {
//        $searchModel = new AdminsSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//        return $this->render('index', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//        ]);
//    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

//    public function actionCreate() {
//        $model = new Admins();
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }

//    public function actionUpdate($id) {
//        $model = $this->findModel($id);
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

//    public function actionDelete($id) {
//        $this->findModel($id)->delete();
//        return $this->redirect(['index']);
//    }

    protected function findModel($id) {
        if (($model = Admins::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangePassword() {
        $model = new ChangePasswordForm;
        $user = Yii::$app->user->identity;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                try {
                    $user->password_hash = Yii::$app->security->generatePasswordHash($model->new_password);
                    if ($user->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Password changed successfully'));
                        return $this->redirect(['site/index']);
                    } else {
                        Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Password change failed'));
                        return $this->redirect(['site/index']);
                    }
                } catch (Exception $e) {
                    Yii::$app->getSession()->setFlash('error', "{$e->getMessage()}");
                    return $this->render('change-password', [
                        'model' => $model,
                    ]);
                }
            }
        }
        return $this->render('change-password', [
            'model' => $model,
        ]);
    }
}
