<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use backend\models\Users;
use backend\models\search\BuyersSearch;
use backend\models\search\OrdersSearch;

class BuyersController extends AppController {
    public function behaviors() {
        return parent::behaviors();
    }

    public function actionIndex() {
        $searchModel = new BuyersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['orders.user_id' => $id]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'ordersDataProvider' => $dataProvider,
        ]);
    }

    protected function findModel($id) {
        if ($model = Users::findOne($id)) {
            /** @var $model Users */
            if($model->role == Users::ROLE_BUYER) {
                return $model;
            }
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
