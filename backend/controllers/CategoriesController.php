<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use common\components\ImageManager\ImageManager;
use backend\models\Categories;
use backend\models\Characteristics;
use backend\models\search\CategoriesSearch;
use backend\models\search\ProductsSearch;
use backend\models\search\CharacteristicsSearch;

class CategoriesController extends AppController {
    public function behaviors() {
        return parent::behaviors();
    }

    public function actionIndex() {
        $searchModel = new CategoriesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Categories::LEVEL_CHAPTER);
        $dataProvider->pagination = false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $dataProvider = null;
        $searchModel = null;
        $productsDataProvider = null;
        $productsSearchModel = null;
        $category = $this->findModel($id);
        if($category->canHasChild()) {
            $subLevel = $category->level + 1;
            $searchModel = new CategoriesSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $subLevel);
            $dataProvider->query->andWhere(['parent_id' => $id]);
        } else {
            $searchModel = new CharacteristicsSearch();
            $dataProvider = $searchModel->search([]);
            $dataProvider->query->andWhere(['category_id' => $id]);
            $productsSearchModel = new ProductsSearch();
            $productsDataProvider = $productsSearchModel->search(Yii::$app->request->queryParams);
            $productsDataProvider->query->andWhere(['category_id' => $id]);
            $productsDataProvider->pagination = false;
        }
        $dataProvider->pagination = false;

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'productsSearchModel' => $productsSearchModel,
            'productsDataProvider' => $productsDataProvider,
        ]);
    }

    public function actionCreateChapter() {
        $model = new Categories();

        if($model->load(Yii::$app->request->post())) {
            $model->level = Categories::LEVEL_CHAPTER;
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'level' => Categories::LEVEL_CHAPTER,
            'parent' => null,
            'characteristics' => null,
        ]);
    }

    public function actionCreateCategory() {
        if(!$categoryId = Yii::$app->request->get('id', false)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $category = Categories::findOne($categoryId);
        if(!$category || $category->level != Categories::LEVEL_CHAPTER) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new Categories();
        if($model->load(Yii::$app->request->post())) {
            $model->parent_id = $categoryId;
            $model->level = Categories::LEVEL_CATEGORY;
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'level' => Categories::LEVEL_CATEGORY,
            'parent' => $category,
            'characteristics' => null,
        ]);
    }

    public function actionCreateSubcategory() {
        if(!$categoryId = Yii::$app->request->get('id', false)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $category = Categories::findOne($categoryId);
        if(!$category || $category->level != Categories::LEVEL_CATEGORY) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new Categories();
        if($model->load(Yii::$app->request->post())) {
            $model->parent_id = $categoryId;
            $model->level = Categories::LEVEL_SUBCATEGORY;
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'level' => Categories::LEVEL_SUBCATEGORY,
            'parent' => $category,
            'characteristics' => null,
        ]);
    }

    public function actionCreateGoodsCategory() {
        if(!$categoryId = Yii::$app->request->get('id', false)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $category = Categories::findOne($categoryId);
        if(!$category || $category->level != Categories::LEVEL_SUBCATEGORY) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model = new Categories();
        $model->parent_id = $categoryId;
        $model->level = Categories::LEVEL_GOODSCATEGORY;
        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $postCharacteristics = Yii::$app->request->post('Characteristics', []);
            $characteristics = [];
            $valid = true;
            if(!empty($postCharacteristics)) {
                foreach($postCharacteristics as $postCharacteristic) {
                    $characteristic = new Characteristics();
                    $characteristic->load($postCharacteristic);
                    if($characteristic->validate()) {
                        $characteristics[] = $characteristic;
                    } else {
                        $valid = false;
                        break;
                    }
                }
            }
            if($valid) {
                $model->safeUpdate($characteristics);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
            'level' => Categories::LEVEL_GOODSCATEGORY,
            'parent' => $category,
            'characteristics' => [new Characteristics()],
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if($model->load(Yii::$app->request->post()) && $model->validate()) {
            $postCharacteristics = Yii::$app->request->post('Characteristics', []);
            $characteristics = [];
            $valid = true;
            if(!empty($postCharacteristics)) {
                foreach($postCharacteristics as $postCharacteristic) {
                    $characteristic = new Characteristics();
                    $characteristic->load($postCharacteristic);
                    $characteristic->category_id = $model->id;
                    if($characteristic->validate()) {
                        $characteristics[] = $characteristic;
                    } else {
                        $valid = false;
                        break;
                    }
                }
            }
            if($valid) {
                $model->safeUpdate($characteristics);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
            'level' => $model->level,
            'characteristics' => $model->level == Categories::LEVEL_GOODSCATEGORY
                ? (!empty($model->characteristics) ? $model->characteristics : [new Characteristics()])
                : null,
        ]);
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        if($model->isEmpty()) {
            $model->delete();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id) {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionRemoveImage($id, $attribute, $filename) {
        $model = $this->findModel($id);
        ImageManager::removeImage($model, $attribute, $filename);
        return $this->redirect(Yii::$app->request->referrer);
    }
}
