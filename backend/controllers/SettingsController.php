<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use backend\models\Settings;
use backend\models\search\SettingsSearch;

class SettingsController extends AppController {
    public function behaviors() {
        return parent::behaviors();
    }

    public function actionIndex() {
        $searchModel = new SettingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->alias = $model->oldAttributes['alias'];
            $model->name = $model->oldAttributes['name'];
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->alias]);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    protected function findModel($id) {
        if (($model = Settings::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
