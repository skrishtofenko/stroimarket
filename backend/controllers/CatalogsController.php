<?php
namespace backend\controllers;

use Yii;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use backend\models\Catalogs;
use backend\models\search\CatalogsSearch;

class CatalogsController extends AppController {
    public function actionIndex() {
        $searchModel = new CatalogsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        $model = new Catalogs();

        if($model->load(Yii::$app->request->post())) {
            $catalogFile = UploadedFile::getInstance($model, 'catalogFile');
            $newFileName = Catalogs::generateFileName($catalogFile);
            $newFilePath = Catalogs::touchPath($newFileName);
            $catalogFile->saveAs($newFilePath . $newFileName);
            $model->file_name = $newFileName;
            $model->upload_name = mb_substr($catalogFile->name, 0, 255, 'utf-8');
            $model->is_by_admin = 1;
            if($model->save(false)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    protected function findModel($id) {
        if (($model = Catalogs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
