<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use common\components\ImageManager\ImageManager;
use backend\models\Users;
use backend\models\Cities;
use backend\models\Requisites;
use backend\models\Notices;
use backend\models\search\UsersSearch;
use backend\models\search\CompaniesSearch;
use backend\models\forms\DeclineForm;

class UsersController extends AppController {
    public function behaviors() {
        return parent::behaviors();
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @throws NotFoundHttpException if the model cannot be found
     * @return mixed
     */
    public function actionView($id)
    {
        if(!$requisites = Requisites::findOne(['user_id' => $id, 'is_default' => true])) {
            $requisites = new Requisites();
        }
        $searchModel = new CompaniesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['user_id' => $id]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'requisites' => $requisites,
            'companiesDataProvider' => $dataProvider,
            'declineForm' => new DeclineForm(),
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user = new Users();
        $requisites = new Requisites();
        if($user->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            if($user->save()) {
                $notices = new Notices();
                $notices->user_id = $user->id;
                $notices->save();
                if($user->ownership_type_id) {
                    if($requisites->load(Yii::$app->request->post())) {
                        $requisites->user_id = $user->id;
                        $requisites->is_default = true;
                        if ($requisites->save()) {
                            $transaction->commit();
                            return $this->redirect(['view', 'id' => $user->id]);
                        } else {
                            $transaction->rollBack();
                        }
                    }
                } else {
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $user->id]);
                }
            } else {
                $transaction->rollBack();
            }
        }
        return $this->render('create', [
            'user' => $user,
            'requisites' => $requisites,
            'cities' => Cities::getList(),
        ]);
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @throws NotFoundHttpException if the model cannot be found
     * @return mixed
     */
    public function actionUpdate($id) {
        $user = $this->findModel($id);
        if($user->role != Users::ROLE_COMPANY) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if(!$requisites = Requisites::findOne(['user_id' => $id, 'is_default' => true])) {
            $requisites = new Requisites();
        }
        if($user->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            if($user->save()) {
                if($user->ownership_type_id) {
                    if($requisites->load(Yii::$app->request->post())) {
                        $requisites->user_id = $user->id;
                        $requisites->is_default = true;
                        if ($requisites->save()) {
                            $transaction->commit();
                            return $this->redirect(['view', 'id' => $user->id]);
                        } else {
                            $transaction->rollBack();
                        }
                    }
                } else {
                    $transaction->commit();
                    return $this->redirect(['view', 'id' => $user->id]);
                }
            } else {
                $transaction->rollBack();
            }
        }
        return $this->render('update', [
            'user' => $user,
            'requisites' => $requisites,
            'cities' => Cities::getList(),
        ]);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ($model = Users::findOne($id)) {
            /** @var $model Users */
            if($model->role == Users::ROLE_COMPANY) {
                return $model;
            }
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionBlock($id) {
        $model = $this->findModel($id);
        if($model->role == Users::ROLE_COMPANY) {
            $model->updateAttributes(['status' => Users::STATUS_BLOCKED]);
            $model->email('blockedByAdmin', $model->email, [
                'user' => $model
            ]);
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUnblock($id) {
        $model = $this->findModel($id);
        if($model->role == Users::ROLE_COMPANY) {
            $model->updateAttributes(['status' => Users::STATUS_CONFIRMED]);
            $model->email('unblockedByAdmin', $model->email, [
                'user' => $model
            ]);
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        if($model->role == Users::ROLE_COMPANY) {
            $model->updateAttributes(['status' => Users::STATUS_DELETED]);
            return $this->redirect(Yii::$app->request->referrer);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionApprove($id) {
        $model = $this->findModel($id);
        $model->updateAttributes(['status' => Users::STATUS_ACTIVE]);
        $model->email('approvedByAdmin', $model->email, [
            'user' => $model
        ]);
        $model->mainCompany->rates->recount();
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDecline($id) {
        $declineForm = new DeclineForm();
        if($declineForm->load(Yii::$app->request->post()) && $model = $this->findModel($id)) {
            $model->updateAttributes(['status' => Users::STATUS_DECLINED]);
            $model->email('declinedByAdmin', $model->email, [
                'user' => $model,
                'message' => $declineForm->message,
            ]);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Your message was sent'));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRemoveImage($id, $attribute, $filename) {
        $model = $this->findModel($id);
        ImageManager::removeImage($model, $attribute, $filename);
        return $this->redirect(Yii::$app->request->referrer);
    }
}
