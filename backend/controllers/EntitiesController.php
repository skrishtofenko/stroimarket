<?php
namespace backend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use backend\models\Entities;
use backend\models\search\EntitiesSearch;

/**
 * EntitiesController implements the CRUD actions for Entities model.
 */
class EntitiesController extends AppController {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return parent::behaviors();
    }

    /**
     * Lists all Entities models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new EntitiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'vocabulary' => $this->getCurrentVocabulary(),
        ]);
    }

    /**
     * Creates a new Entities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Entities();
        $vocabulary = $this->getCurrentVocabulary();

        if ($model->load(Yii::$app->request->post())) {
            $model->vocabulary_id = $vocabulary['id'];
            if($model->save()) {
                return $this->redirect(['index?EntitiesSearch[vocabulary_id]=' . $model->vocabulary_id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
            'vocabulary' => $vocabulary,
        ]);
    }

    /**
     * Updates an existing Entities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index?EntitiesSearch[vocabulary_id]=' . $model->vocabulary_id]);
        }
        return $this->render('update', [
            'model' => $model,
            'vocabulary' => $this->getCurrentVocabulary($model->vocabulary_id),
        ]);
    }

    /**
     * Deletes an existing Entities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Entities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Entities::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function getCurrentVocabulary($vocabularyId = 0) {
        if(!$vocabularyId) {
            $vocabularyId = Yii::$app->request->get('vocabulary_id', 0);
            if (!$vocabularyId) {
                $entitiesSearch = Yii::$app->request->get('EntitiesSearch', []);
                if (!empty($entitiesSearch) && isset($entitiesSearch['vocabulary_id']) && $entitiesSearch['vocabulary_id']) {
                    $vocabularyId = $entitiesSearch['vocabulary_id'];
                }
            }
        }
        if(!isset(Yii::$app->params['vocabularies'][$vocabularyId])) {
            $vocabularyId = array_keys(Yii::$app->params['vocabularies'])[0];
        }
        return Yii::$app->params['vocabularies'][$vocabularyId];
    }
}
