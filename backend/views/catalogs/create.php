<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Catalogs */
$this->title = Yii::t('app', 'Create Catalogs');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Catalogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogs-create">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
    ])?>
</div>
