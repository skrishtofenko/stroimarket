<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use backend\models\Companies;

/* @var $this yii\web\View */
/* @var $model backend\models\Catalogs */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="catalogs-form">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model, 'company_id')->widget(Select2::classname(), [
        'data' => Companies::getAssortmentList(),
        'options' => ['placeholder' => 'Select company ...'],
    ])?>
    <?=$form->field($model, 'catalogFile')->fileInput()?>
    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
