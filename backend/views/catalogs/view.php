<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\models\Catalogs;

/* @var $this yii\web\View */
/* @var $model backend\models\Catalogs */
$this->title = $model->file_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Catalogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogs-view">
    <h1><?=Html::encode($this->title)?></h1>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'company_id',
                'format' => 'raw',
                'value' =>  Html::a($model->company->user->name, ['companies/view', 'id' => $model->company_id]),
            ],
            [
                'attribute' => 'is_by_admin',
                'format' => 'raw',
                'value' => $model->is_by_admin ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'is_processed',
                'format' => 'raw',
                'value' => Catalogs::statuses()[$model->is_processed],
            ],
            'tries_count',
            [
                'attribute' => 'file_name',
                'format' => 'raw',
                'value' => '<a href="http://uploads.' . Yii::$app->params['mainUrl'] . Catalogs::generateFilePath($model->file_name, true) . "/{$model->file_name}\">{$model->file_name}</a>",
            ],
            'upload_name',
            'created_at',
        ],
    ])?>
</div>
