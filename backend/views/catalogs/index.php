<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\SKHelper;
use backend\models\Catalogs;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CatalogsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::t('app', 'Catalogs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="catalogs-index">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Create Catalogs'), ['create'], ['class' => 'btn btn-success'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'companyName',
                'label' => Yii::t('app', 'Company Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Orders */
                    return Html::a($data->company->user->name, ['companies/view', 'id' => $data->company_id]);
                },
            ],
            [
                'attribute' => 'is_by_admin',
                'filter' => SKHelper::getBooleanFilter(),
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Catalogs */
                    return $data->is_by_admin ? 'Да' : 'Нет';
                },
            ],
            [
                'attribute' => 'is_processed',
                'format' => 'raw',
                'filter' => Catalogs::statuses(),
                'value' => function ($data) {
                    /** @var $data Catalogs */
                    return Catalogs::statuses()[$data->is_processed];
                },
            ],
            'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]);?>
</div>
