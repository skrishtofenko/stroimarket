<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\models\Orders;
use backend\models\OrdersCompaniesProducts;

/* @var $this yii\web\View */
/* @var $model backend\models\Orders */
/* @var $productsDataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Order #{number}', ['number' => $model->id]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Buyers'), 'url' => ['/buyers/index']];
$this->params['breadcrumbs'][] = ['label' => $model->user->vcard, 'url' => ['/buyers/view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Orders'), 'url' => ['/buyers/view', 'id' => $model->user_id, '#' => 'buyer-orders']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">
    <h1><?=Html::encode($this->title)?></h1>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'user_id',
                'format' => 'raw',
                'value' =>  Html::a($model->user->vcard, ['buyers/view', 'id' => $model->user_id]),
            ],
            [
                'attribute' => 'company_id',
                'format' => 'raw',
                'value' =>  Html::a($model->company->user->name, ['companies/view', 'id' => $model->company_id]),
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => Orders::statuses()[$model->status],
            ],
            'total_cost',
            [
                'attribute' => 'delivery_type_id',
                'format' => 'raw',
                'value' => $model->deliveryType->name,
            ],
            [
                'attribute' => 'payment_type_id',
                'format' => 'raw',
                'value' => $model->paymentType->name,
            ],
            [
                'attribute' => 'is_deleted_by_buyer',
                'format' => 'raw',
                'value' => Yii::t('app', $model->is_deleted_by_buyer ? 'Yes' : 'No'),
            ],
            [
                'attribute' => 'is_deleted_by_seller',
                'format' => 'raw',
                'value' => Yii::t('app', $model->is_deleted_by_seller ? 'Yes' : 'No'),
            ],
            'address',
            'flat',
            'comment',
            'deliver_at',
            'created_at',
            'updated_at',
        ],
    ])?>

    <h2><?=Yii::t('app', 'Products in order')?></h2>
    <?=GridView::widget([
        'dataProvider' => $productsDataProvider,
        'columns' => [
            [
                'attribute' => 'company_product_id',
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data OrdersCompaniesProducts */
                    return $data->companyProduct->product->niceName;
                },
            ],
            'cost',
            'quantity',
            'totalCost',
        ],
    ]);?>
</div>
