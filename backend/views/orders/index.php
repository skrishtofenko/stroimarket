<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Orders;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">
    <h1><?=Html::encode($this->title)?></h1>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Order num'),
            ],
            [
                'attribute' => 'companyName',
                'label' => Yii::t('app', 'Company Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Orders */
                    return Html::a($data->company->user->name, ['companies/view', 'id' => $data->company_id]);
                },
            ],
            [
                'attribute' => 'buyerName',
                'format' => 'raw',
                'label' => Yii::t('app', 'Buyer Name'),
                'value' => function ($data) {
                    /** @var $data Orders */
                    return Html::a($data->user->vcard, ['buyers/view', 'id' => $data->user_id]);
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Orders::statuses(),
                'value' => function ($data) {
                    /** @var $data Orders */
                    return Orders::statuses()[$data->status];
                },
            ],
            [
                'attribute' => 'total_cost',
                'filter' => false,
            ],
            'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]);?>
</div>
