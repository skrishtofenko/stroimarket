<?php
use yii\helpers\Html;
use backend\models\Categories;
use backend\models\Characteristics;

/* @var $this yii\web\View */
/* @var $model Categories */
/* @var $level integer */
/* @var $parent Categories|null */
/* @var $characteristics Characteristics|null */

$title = Yii::t('app', 'Create Chapter');
switch ($level) {
    case Categories::LEVEL_CATEGORY:
        $title = Yii::t('app', 'Create Category for "{chapter}"', [
            'chapter' => $parent->name,
        ]);
        break;
    case Categories::LEVEL_SUBCATEGORY:
        $title = Yii::t('app', 'Create Subcategory for "{category}"', [
            'category' => $parent->name,
        ]);
        break;
    case Categories::LEVEL_GOODSCATEGORY:
        $title = Yii::t('app', 'Create Goods Category');
        break;
}

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Catalog'), 'url' => ['index']];
if($parent) {
    if($parent->level > Categories::LEVEL_CHAPTER) {
        if($parent->level == Categories::LEVEL_SUBCATEGORY) {
            $this->params['breadcrumbs'][] = ['label' => $parent->parent->parent->name, 'url' => ['/categories/view', 'id' => $parent->parent->parent_id]];
        }
        $this->params['breadcrumbs'][] = ['label' => $parent->parent->name, 'url' => ['/categories/view', 'id' => $parent->parent_id]];
    }
    $this->params['breadcrumbs'][] = ['label' => $parent->name, 'url' => ['/categories/view', 'id' => $parent->id]];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-create">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
        'level' => $level,
        'characteristics' => $characteristics,
    ])?>
</div>
