<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\DynamicFormWidget;
use common\components\ImageManager\ImageManager;
use backend\models\Categories;
use backend\models\Characteristics;
use backend\models\Vocabularies;

/* @var $this yii\web\View */
/* @var $model Categories */
/* @var $form yii\widgets\ActiveForm */
/* @var $level integer */
/* @var $characteristics Characteristics|null */

$js = <<<JS
    $('.dynamicform_wrapper').on('afterInsert', function(e, item) {
        fillIndex();
        showOrHideFields();
    }).on('afterDelete', function(e) {
        fillIndex();
        showOrHideFields();
    }).on("limitReached", function(e, item) {
        alert("Нахрена столько характеристик?");
    }).on("beforeDelete", function(e, item) {
        return confirm("Точно удалить?");
    }).on('change', 'select.characteristic-type', function() {
        showOrHideFields();
    });

    showOrHideFields();

    function fillIndex() {
        $('.dynamicform_wrapper .panel-title-address').each(function(index) {
            $(this).html('Характеристика ' + (index + 1));
        });
    }
    
    function showOrHideFields() {
        $('.dynamicform_wrapper .item .panel-body').each(function(index) {
            var type = $('#characteristics-' + index + '-characteristics-type');
            if(type.length) {
                $(this).find('div.custom-fields').hide();
                $(this).find('div.custom-fields-' + type.val()).show();
            }
        });
    }
JS;
$this->registerJs($js);
?>

<div class="categories-form">
    <?php $form = ActiveForm::begin(['id' => 'dynamic-form']); ?>
    <?=$form->field($model, 'name')->textInput(['maxlength' => true])?>
    <?php if($level == Categories::LEVEL_CATEGORY):?>
        <?php ImageManager::field($form, $model, 'image')?>
    <?php endif;?>
    <?php if($level == Categories::LEVEL_GOODSCATEGORY):?>
        <h2><?=Yii::t('app', 'Characteristics')?></h2>
        <div class="padding-v-md">
            <div class="line line-dashed"></div>
        </div>
        <?php DynamicFormWidget::begin([
            'widgetContainer' => 'dynamicform_wrapper',
            'widgetBody' => '.container-items',
            'widgetItem' => '.item',
            'limit' => 25,
            'min' => 0,
            'insertButton' => '.add-item',
            'deleteButton' => '.remove-item',
            'model' => $characteristics[0],
            'formId' => 'dynamic-form',
            'formFields' => [
                'category_id',
                'unit_type_id',
                'is_multiple',
                'is_add_to_title',
                'type',
                'name',
                'values',
            ],
        ]); ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-envelope"></i>Характеристики товара в категории
                <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i>Добавить характеристику</button>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body container-items">
                <?php foreach($characteristics as $index => $characteristic):?>
                    <div class="item panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title-address">Характеристика <?=($index + 1)?></span>
                            <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php if(!$characteristic->isNewRecord):?>
                                <?=Html::activeHiddenInput($characteristic, "[{$index}][Characteristics]id")?>
                            <?php endif;?>
                            <?=$form->field($characteristic, "[{$index}][Characteristics]type")->dropDownList(Characteristics::types(), ['class' => 'form-control characteristic-type'])?>
                            <?=$form->field($characteristic, "[{$index}][Characteristics]name")->textInput(['maxlength' => true])?>
                            <div class="custom-fields custom-fields-string">
                                <?=$form->field($characteristic, "[{$index}][Characteristics]unit_type_id")->dropDownList(Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_UNITS]['items'])?>
                                <?=$form->field($characteristic, "[{$index}][Characteristics]is_add_to_title")->checkbox()?>
                            </div>
                            <div class="custom-fields custom-fields-list">
                                <?=$form->field($characteristic, "[{$index}][Characteristics]is_multiple")->checkbox()?>
                                <?=$form->field($characteristic, "[{$index}][Characteristics]values")->textarea(['rows' => 6])?>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
        <?php DynamicFormWidget::end();?>
    <?php endif;?>
    <div class="form-group">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
