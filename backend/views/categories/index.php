<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Categories;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Catalog');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Create Chapter'), ['create-chapter'], ['class' => 'btn btn-success'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'name',
                'enableSorting' => false,
            ],
            [
                'attribute' => 'child_count',
                'enableSorting' => false,
                'filter' => false,
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {create-category} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        /** @var $model Categories */
                        if ($model->isEmpty()) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'data-method' => 'post',
                                'title' => Yii::t('app', 'Delete'),
                                'aria-label' => Yii::t('app', 'Delete'),
                                'data-pjax' => 0,
                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            ]);
                        }
                        return '';
                    },
                    'create-category' => function ($url, $model) {
                        /** @var $model Categories */
                        if($model->canHasChild()) {
                            $options = [
                                'title' => Yii::t('app', 'Create Categories'),
                                'aria-label' => Yii::t('app', 'Create Categories'),
                            ];
                            return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, $options);
                        }
                        return '';
                    },
                ]
            ],
        ],
    ]);?>
</div>
