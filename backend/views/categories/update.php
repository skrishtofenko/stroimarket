<?php
use yii\helpers\Html;
use backend\models\Categories;
use backend\models\Characteristics;

/* @var $this yii\web\View */
/* @var $model backend\models\Categories */
/* @var $level integer */
/* @var $characteristics Characteristics|null */

$this->title = Yii::t('app', 'Update Categories') . ": {$model->name}";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Catalog'), 'url' => ['index']];
if($model->level > Categories::LEVEL_CHAPTER) {
    if($model->level > Categories::LEVEL_CATEGORY) {
        if($model->level == Categories::LEVEL_GOODSCATEGORY) {
            $this->params['breadcrumbs'][] = ['label' => $model->parent->parent->parent->name, 'url' => ['/categories/view', 'id' => $model->parent->parent->parent_id]];
        }
        $this->params['breadcrumbs'][] = ['label' => $model->parent->parent->name, 'url' => ['/categories/view', 'id' => $model->parent->parent_id]];
    }
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['/categories/view', 'id' => $model->parent_id]];
}
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="categories-update">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
        'level' => $level,
        'characteristics' => $characteristics,
    ])?>
</div>
