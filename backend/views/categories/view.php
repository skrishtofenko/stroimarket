<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\components\ImageManager\ImageManager;
use common\components\ImageManager\ImageManagerAsset;
use backend\models\Products;
use backend\models\Companies;
use backend\models\Categories;
use backend\models\Vocabularies;
use backend\models\Characteristics;

/* @var $this yii\web\View */
/* @var $model backend\models\Categories */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\models\search\CategoriesSearch */
/* @var $productsDataProvider yii\data\ActiveDataProvider|null */
/* @var $productsSearchModel backend\models\search\ProductsSearch|null */

$this->title = $model->name . ' | Каталог';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Catalog'), 'url' => ['index']];
if($model->level > Categories::LEVEL_CHAPTER) {
    if($model->level > Categories::LEVEL_CATEGORY) {
        if($model->level == Categories::LEVEL_GOODSCATEGORY) {
            $this->params['breadcrumbs'][] = ['label' => $model->parent->parent->parent->name, 'url' => ['/categories/view', 'id' => $model->parent->parent->parent_id]];
        }
        $this->params['breadcrumbs'][] = ['label' => $model->parent->parent->name, 'url' => ['/categories/view', 'id' => $model->parent->parent_id]];
    }
    $this->params['breadcrumbs'][] = ['label' => $model->parent->name, 'url' => ['/categories/view', 'id' => $model->parent_id]];
}
$this->params['breadcrumbs'][] = $model->name;

$subCaption = 'Category';
$subCaptionMult = 'Categories';
switch ($model->level) {
    case Categories::LEVEL_CATEGORY:
        $subCaption = 'Subcategory';
        $subCaptionMult = 'Subcategories';
        break;
    case Categories::LEVEL_SUBCATEGORY:
        $subCaption = 'Goods Category';
        $subCaptionMult = 'Goods Categories';
        break;
}
$createSubLink = str_replace(' ', '-', strtolower($subCaption));
?>
<div class="categories-view">
    <h1><?=Html::encode($model->name)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
        <?php if ($model->isEmpty()):?>
            <?=Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])?>
        <?php endif;?>
    </p>
    <?php
    $attributes = [
        'name',
    ];
    if($model->level == Categories::LEVEL_CATEGORY) {
        $attributes[] = ImageManager::getViewAttribute($model, 'image', 200);
    }
    if($model->level != Categories::LEVEL_CHAPTER) {
        $attributes[] = [
            'attribute' => 'parent_id',
            'format' => 'raw',
            'value' => Html::a($model->parent->name, ['categories/view', 'id' => $model->parent_id]),
        ];
    }
    if($model->level == Categories::LEVEL_GOODSCATEGORY) {
        $attributes[] = 'products_count';
    } else {
        $attributes[] = 'child_count';
    }
    ?>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ])?>

    <?php if($model->canHasChild()):?>
        <h2 id="user-companies"><?=Yii::t('app', $subCaptionMult)?></h2>
        <p>
            <?=Html::a(
                Yii::t('app', "Create $subCaption"),
                ["categories/create-$createSubLink?id={$model->id}"],
                ['class' => 'btn btn-primary']
            )?>
        </p>
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'attribute' => 'id',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'name',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'child_count',
                    'enableSorting' => false,
                    'filter' => false,
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {create-subcategory} {create-goods-category} {delete}',
                    'buttons' => [
                        'delete' => function ($url, $model) {
                            /** @var $model Categories */
                            if ($model->isEmpty()) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'data-method' => 'post',
                                    'title' => Yii::t('app', 'Delete'),
                                    'aria-label' => Yii::t('app', 'Delete'),
                                    'data-pjax' => 0,
                                    'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                                ]);
                            }
                            return '';
                        },
                        'create-subcategory' => function ($url, $model) {
                            /** @var $model Categories */
                            if($model->level == Categories::LEVEL_CATEGORY) {
                                $message = Yii::t('app', "Create Subcategory");
                                $options = [
                                    'title' => $message,
                                    'aria-label' => $message,
                                ];
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, $options);
                            }
                            return '';
                        },
                        'create-goods-category' => function ($url, $model) {
                            /** @var $model Categories */
                            if($model->level == Categories::LEVEL_SUBCATEGORY) {
                                $message = Yii::t('app', "Create Goods Category");
                                $options = [
                                    'title' => $message,
                                    'aria-label' => $message,
                                ];
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, $options);
                            }
                            return '';
                        },
                    ]
                ],
            ],
        ]);?>
    <?php else:?>
        <h2 id="user-companies"><?=Yii::t('app', 'Characteristics')?></h2>
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'name',
                    'enableSorting' => false,
                ],
                [
                    'attribute' => 'type',
                    'enableSorting' => false,
                    'value' => function ($data) {
                        /** @var $data Characteristics */
                        return Characteristics::types()[$data->type];
                    },
                ],
                [
                    'attribute' => 'is_add_to_title',
                    'enableSorting' => false,
                    'value' => function ($data) {
                        /** @var $data Characteristics */
                        return $data->type == Characteristics::TYPE_STRING ? ($data->is_add_to_title ? 'Да' : 'Нет') : '--';
                    },
                ],
                [
                    'attribute' => 'unit_type_id',
                    'enableSorting' => false,
                    'value' => function ($data) {
                        /** @var $data Characteristics */
                        return $data->type == Characteristics::TYPE_STRING ? $data->unitType->name : '--';
                    },
                ],
                [
                    'attribute' => 'values',
                    'format' => 'ntext',
                    'enableSorting' => false,
                    'value' => function ($data) {
                        /** @var $data Characteristics */
                        return $data->type == Characteristics::TYPE_LIST ? $data->values : '--';
                    },
                ],
                [
                    'attribute' => 'is_multiple',
                    'enableSorting' => false,
                    'value' => function ($data) {
                        /** @var $data Characteristics */
                        return $data->type == Characteristics::TYPE_LIST ? ($data->is_multiple ? 'Да' : 'Нет') : '--';
                    },
                ],
            ],
        ]);?>

        <h2 id="category-products"><?=Yii::t('app', 'Products')?></h2>
        <p>
            <?=Html::a(Yii::t('app', 'Create {modelClass}', [
                'modelClass' => Yii::t('app', 'Products')
            ]), ['products/create?category_id=' . $model->id], ['class' => 'btn btn-primary'])?>
        </p>
        <?=GridView::widget([
            'dataProvider' => $productsDataProvider,
            'filterModel' => $productsSearchModel,
            'columns' => [
                'name',
                [
                    'attribute' => 'manufacturer_id',
                    'format' => 'raw',
                    'filter' => Companies::getManufacturersList(),
                    'value' => function ($data) {
                        /** @var $data Products */
                        return $data->manufacturer_id
                            ? Html::a($data->manufacturer->user->legalEntityVcard, ['companies/view', 'id' => $data->manufacturer_id])
                            : null;
                    },
                ],
                [
                    'attribute' => 'unit_type_id',
                    'format' => 'raw',
                    'filter' => Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_UNITS]['items'],
                    'value' => function ($data) {
                        /** @var $data Products */
                        return $data->unitType->name;
                    },
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            $options = [
                                'title' => Yii::t('app', 'View'),
                                'aria-label' => Yii::t('app', 'View'),
                            ];
                            $url = str_replace('categories', 'products', $url);
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                        },
                        'update' => function ($url, $model) {
                            $options = [
                                'title' => Yii::t('app', 'Update'),
                                'aria-label' => Yii::t('app', 'Update'),
                            ];
                            $url = str_replace('categories', 'products', $url);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                        },
                        'delete' => function ($url, $model, $key) {
                            $options = [
                                'title' => Yii::t('app', 'Delete'),
                                'aria-label' => Yii::t('app', 'Delete'),
                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this product? This action is unrecoverable!'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ];
                            $url = str_replace('categories', 'products', $url);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                        },
                    ]
                ],
            ],
        ]);?>
    <?php endif;?>
</div>
<?php
ImageManagerAsset::register($this);
?>
