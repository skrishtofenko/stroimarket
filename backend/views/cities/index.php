<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Cities;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cities-index">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Create Cities'), ['create'], ['class' => 'btn btn-success'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Cities::statuses(),
                'value' => function ($data) {
                    /** @var $model Cities */
                    return Cities::statuses()[$data->status];
                },
            ],
            'users_count',
            'companies_count',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        /** @var $model Cities */
                        if ($model->isEmpty()) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'data-method' => 'post',
                                'title' => Yii::t('app', 'Delete'),
                                'aria-label' => Yii::t('app', 'Delete'),
                                'data-pjax' => 0,
                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            ]);
                        }
                        return '';
                    },
                ]
            ],
        ],
    ]);?>
</div>
