<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Cities;
use backend\models\Countries;

/* @var $this yii\web\View */
/* @var $model Cities */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="cities-form">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model, 'name')->textInput(['maxlength' => true])?>
    <?=$form->field($model, 'country_id')->dropDownList(Countries::getList())?>
    <?=$form->field($model, 'status')->dropDownList(Cities::statuses())?>
    <div class="form-group">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
