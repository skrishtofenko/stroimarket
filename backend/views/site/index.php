<?php
/* @var $this yii\web\View */

$this->title = 'Welcome to ' . Yii::$app->name;
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Greetings!</h1>
    </div>
    <div class="body-content text-center">
        Let's surf the admin panel!
    </div>
</div>
