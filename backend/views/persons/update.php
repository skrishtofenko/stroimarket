<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Persons */

$this->title = Yii::t('app', 'Update Person') . " $model->fio ({$model->company->user->name})";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['/users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->company->user->vcard, 'url' => ['/users/view', 'id' => $model->company->user_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Companies'), 'url' => ['/users/view', 'id' => $model->company->user_id, '#' => 'user-companies']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Company "{name}"', ['name' => $model->company->user->name]), 'url' => ['/companies/view', 'id' => $model->company->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['/companies/view', 'id' => $model->company->id, '#' => 'company-persons']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update Person "{name}"', ['name' => $model->fio]);
?>
<div class="persons-update">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
    ])?>
</div>
