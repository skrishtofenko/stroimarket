<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\models\Persons */
/* @var $form yii\widgets\ActiveForm */
/* @var $company backend\models\Companies */
?>
<div class="persons-form">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model, 'fio')->textInput(['maxlength' => true])?>
    <?=$form->field($model, 'email')->textInput(['maxlength' => true])?>
    <?=$form->field($model, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+7 (999) 999-99-99',
        'clientOptions' => [
            'removeMaskOnSubmit' => true,
            'autoUnmask' => true,
        ]
    ]);?>
    <?=$form->field($model, 'questions')->textarea(['rows' => 6])?>
    <div class="form-group">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
