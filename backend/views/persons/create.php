<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Persons */
/* @var $company backend\models\Companies */

$this->title = Yii::t('app', 'Create Persons');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['/users/index']];
$this->params['breadcrumbs'][] = ['label' => $company->user->vcard, 'url' => ['/users/view', 'id' => $company->user_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Companies'), 'url' => ['/users/view', 'id' => $company->user_id, '#' => 'user-companies']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Company "{name}"', ['name' => $company->user->name]), 'url' => ['/companies/view', 'id' => $company->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Persons'), 'url' => ['/companies/view', 'id' => $company->id, '#' => 'company-persons']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="persons-create">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
    ])?>
</div>
