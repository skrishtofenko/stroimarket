<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\components\ImageManager\ImageManager;
use common\components\ImageManager\ImageManagerAsset;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['/categories']];
$this->params['breadcrumbs'][] = ['label' => $model->category->parent->parent->parent->name, 'url' => ['/categories/view', 'id' => $model->category->parent->parent->parent_id]];
$this->params['breadcrumbs'][] = ['label' => $model->category->parent->parent->name, 'url' => ['/categories/view', 'id' => $model->category->parent->parent_id]];
$this->params['breadcrumbs'][] = ['label' => $model->category->parent->name, 'url' => ['/categories/view', 'id' => $model->category->parent_id]];
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => ['/categories/view', 'id' => $model->category_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['/categories/view', 'id' => $model->category_id, '#' => 'category-products']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-view">
    <h1><?=Html::encode($this->title)?></h1>
    <?php if($model->is_active):?>
        <p>
            <?=Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
            <?=Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])?>
        </p>
    <?php endif;?>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'attribute' => 'category_id',
                'format' => 'raw',
                'value' => Html::a($model->category->name, ['categories/view', 'id' => $model->category_id]),
            ],
            [
                'attribute' => 'manufacturer_id',
                'format' => 'raw',
                'value' => $model->manufacturer_id
                    ? Html::a($model->manufacturer->user->legalEntityVcard, ['companies/view', 'id' => $model->manufacturer_id])
                    : '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>',
            ],
            [
                'attribute' => 'prod_country_id',
                'format' => 'raw',
                'value' => $model->prod_country_id ? $model->prodCountry->name : '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>',
            ],
            [
                'attribute' => 'unit_type_id',
                'format' => 'raw',
                'value' => $model->unitType->name,
            ],
            ImageManager::getViewAttribute($model, 'image', 200),
            'description:html',
            'created_at',
            'updated_at',
        ],
    ])?>

    <?php
    if(!empty($model->category->characteristics)) {
        echo '<h2>' . Yii::t('app', 'Characteristics') . '</h2>';
        $attributes = [];
        foreach ($model->productsCharacteristics as $characteristic) {
            $attributes[] = [
                'attribute' => false,
                'format' => 'raw',
                'value' => $characteristic->getNiceValue(),
                'label' => $characteristic->characteristic->getLabelText(),
            ];
        }
        echo DetailView::widget([
            'model' => $model,
            'attributes' => $attributes,
        ]);
    }
    ?>
</div>
<?php
ImageManagerAsset::register($this);
?>

