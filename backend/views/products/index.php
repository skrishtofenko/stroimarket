<?php
use yii\helpers\Html;
use yii\grid\GridView;
use backend\models\Companies;
use backend\models\Categories;
use backend\models\Vocabularies;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Create Products'), ['create'], ['class' => 'btn btn-success'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'category_id',
                'format' => 'raw',
                'filter' => Categories::getList('id', 'name', ['level' => Categories::LEVEL_GOODSCATEGORY]),
                'value' => function ($data) {
                    /** @var $data Products */
                    return Html::a($data->category->name, ['categories/view', 'id' => $data->category_id]);
                },
            ],
            [
                'attribute' => 'manufacturer_id',
                'format' => 'raw',
                'filter' => Companies::getManufacturersList(),
                'value' => function ($data) {
                    /** @var $data Products */
                    return $data->manufacturer_id
                        ? Html::a($data->manufacturer->user->legalEntityVcard, ['companies/view', 'id' => $data->manufacturer_id])
                        : null;
                },
            ],
            [
                'attribute' => 'unit_type_id',
                'format' => 'raw',
                'filter' => Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_UNITS]['items'],
                'value' => function ($data) {
                    /** @var $data Products */
                    return $data->unitType->name;
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);?>
</div>
