<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $characteristics backend\models\ProductsCharacteristics[] */

$this->title = Yii::t('app', 'Update Products: {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['/categories']];
$this->params['breadcrumbs'][] = ['label' => $model->category->parent->parent->parent->name, 'url' => ['/categories/view', 'id' => $model->category->parent->parent->parent_id]];
$this->params['breadcrumbs'][] = ['label' => $model->category->parent->parent->name, 'url' => ['/categories/view', 'id' => $model->category->parent->parent_id]];
$this->params['breadcrumbs'][] = ['label' => $model->category->parent->name, 'url' => ['/categories/view', 'id' => $model->category->parent_id]];
$this->params['breadcrumbs'][] = ['label' => $model->category->name, 'url' => ['/categories/view', 'id' => $model->category_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['/categories/view', 'id' => $model->category_id, '#' => 'category-products']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="products-update">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
        'category' => $model->category,
        'characteristics' => $characteristics,
    ])?>
</div>
