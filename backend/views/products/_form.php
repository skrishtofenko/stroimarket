<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\components\ImageManager\ImageManager;
use backend\models\Companies;
use backend\models\Vocabularies;
use backend\models\Characteristics;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $form yii\widgets\ActiveForm */
/* @var $category backend\models\Categories */
/* @var $characteristics backend\models\ProductsCharacteristics[] */
?>
<div class="products-form">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model, 'name')->textInput(['maxlength' => true])?>
    <?=$form->field($model, 'manufacturer_id')->dropDownList(Companies::getManufacturersList(), ['prompt' => ''])?>
    <?=$form->field($model, 'prod_country_id')->dropDownList(Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_PRODUCTION_COUNTRIES]['items'])?>
    <?=$form->field($model, 'unit_type_id')->dropDownList(Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_UNITS]['items'])?>
    <?=$form->field($model, 'description')->textarea(['rows' => 6])?>
    <?php ImageManager::field($form, $model, 'image')?>

    <?php if(!empty($category->characteristics)):?>
        <h2><?=Yii::t('app', 'Characteristics')?></h2>
        <?php foreach($characteristics as $characteristic):?>
            <?php $baseIputName = "[{$characteristic->characteristic->id}][ProductsCharacteristics]"?>
            <?php $labelText = $characteristic->characteristic->getLabelText();?>
            <?=$form->field($characteristic, "{$baseIputName}characteristic_id")->hiddenInput()->label(false)?>
            <?php
            switch($characteristic->characteristic->type) {
                case Characteristics::TYPE_STRING:
                    echo $form->field($characteristic, "{$baseIputName}value")->textInput(['maxlength' => true])->label($labelText);
                    break;
                case Characteristics::TYPE_BOOLEAN:
                    echo $form->field($characteristic, "{$baseIputName}value")
                        ->dropDownList($characteristic->characteristic->getOptions())
                        ->label($labelText);
                    break;
                case Characteristics::TYPE_LIST:
                    if($characteristic->characteristic->is_multiple) {
                        echo $form->field($characteristic, "{$baseIputName}value")
                            ->listBox($characteristic->characteristic->getOptions(), [
                                'multiple' => !!$characteristic->characteristic->is_multiple,
                                'options' => $characteristic->getSelected(),
                            ])->label($labelText);
                    } else {
                        echo $form->field($characteristic, "{$baseIputName}value")
                            ->dropDownList($characteristic->characteristic->getOptions())
                            ->label($labelText);
                    }
                    break;
                default:
                    echo 'Обработка такого типа данных еще не предусмотрена!';
            }
            ?>
        <?php endforeach;?>
    <?php endif;?>

    <div class="form-group">
        <?=Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
