<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Products */
/* @var $category backend\models\Categories */
/* @var $characteristics backend\models\ProductsCharacteristics[] */

$this->title = Yii::t('app', 'Create Products');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Categories'), 'url' => ['/categories']];
$this->params['breadcrumbs'][] = ['label' => $category->parent->parent->parent->name, 'url' => ['/categories/view', 'id' => $category->parent->parent->parent_id]];
$this->params['breadcrumbs'][] = ['label' => $category->parent->parent->name, 'url' => ['/categories/view', 'id' => $category->parent->parent_id]];
$this->params['breadcrumbs'][] = ['label' => $category->parent->name, 'url' => ['/categories/view', 'id' => $category->parent_id]];
$this->params['breadcrumbs'][] = ['label' => $category->name, 'url' => ['/categories/view', 'id' => $category->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['/categories/view', 'id' => $category->id, '#' => 'category-products']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-create">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
        'category' => $category,
        'characteristics' => $characteristics,
    ])?>
</div>
