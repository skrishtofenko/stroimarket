<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $categories backend\models\Categories[] */

$this->title = Yii::t('app', 'Create Products');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-create">
    <h1>Выбор категории</h1>
    <?php $form = ActiveForm::begin(['method' => 'get']);?>
    <div class="form-group required">
        <label class="control-label" for="category_id">Категория</label>
        <?=Html::dropDownList('category_id', null, $categories, ['id' => 'category_id', 'aria-required' => 'true', 'class' => 'form-control'])?>
        <div class="help-block"></div>
    </div>


    <div class="form-group">
        <?=Html::submitButton('Продолжить', ['class' => 'btn btn-success'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
