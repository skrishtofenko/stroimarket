<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\helpers\SKHelper;
use common\helpers\SMHelper;
use backend\models\Users;
use backend\models\Orders;

/* @var $this yii\web\View */
/* @var $model backend\models\Users */
/* @var $ordersDataProvider yii\data\ActiveDataProvider */

$this->title = $model->vcard;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Buyers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">
    <h1><?=Html::encode($this->title)?></h1>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fio',
            [
                'attribute' => 'role',
                'format' => 'raw',
                'value' => Users::roles()[$model->role],
            ],
            [
                'attribute' => 'city_id',
                'format' => 'raw',
                'value' => $model->city_id ? $model->city->name : '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>',
            ],
            'email:email',
            [
                'attribute' => 'phone',
                'format' => 'raw',
                'value' => SKHelper::formatPhone($model->phone),
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => SMHelper::renderUserStatus($model),
            ],
            'created_at',
            'updated_at',
        ],
    ])?>

    <h2 id="buyer-orders"><?=Yii::t('app', 'Orders')?></h2>
    <?=GridView::widget([
        'dataProvider' => $ordersDataProvider,
        'columns' => [
            [
                'attribute' => 'id',
                'label' => Yii::t('app', 'Order num'),
            ],

            [
                'attribute' => 'companyName',
                'label' => Yii::t('app', 'Company Name'),
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Orders */
                    return Html::a($data->company->user->name, ['companies/view', 'id' => $data->company_id]);
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Orders */
                    return Orders::statuses()[$data->status];
                },
            ],
            'total_cost',
            'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        $options = [
                            'title' => Yii::t('app', 'View'),
                            'aria-label' => Yii::t('app', 'View'),
                        ];
                        $url = str_replace('buyers', 'orders', $url);
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                    },
                ]
            ],
        ],
    ]);?>
</div>
