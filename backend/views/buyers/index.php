<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\SKHelper;
use backend\models\Cities;
use backend\models\Users;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BuyersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Buyers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
    <h1><?=Html::encode($this->title)?></h1>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'fio',
            'email:email',
            [
                'attribute' => 'phone',
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Users */
                    return SKHelper::formatPhone($data->phone);
                },
            ],
            [
                'attribute' => 'city_id',
                'format' => 'raw',
                'filter' => Cities::getList(),
                'value' => function ($data) {
                    /** @var $data Users */
                    return $data->city_id ? Html::a(
                        $data->city->name,
                        ['cities/view', 'id' => $data->city_id]
                    ) : '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>';
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}'
            ],
        ],
    ]);?>
</div>
