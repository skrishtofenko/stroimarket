<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Admins */

$this->title = $model->email;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Admins')];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admins-view">
    <h1><?=Html::encode($this->title)?></h1>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'email:email',
            [
                'attribute' => 'is_active',
                'value' => $model->is_active ? 'Да' : 'Нет',
            ],
        ],
    ])?>
</div>
