<?php
use common\helpers\SKHelper;
use backend\models\Companies;

/* @var $model Companies */
/* @var $form yii\widgets\ActiveForm */
$timeTypes = ['From', 'DinnerFrom', 'DinnerTo', 'To'];
?>
<table style="width: 100%">
    <thead>
        <tr>
            <th style="text-align: center;" colspan="<?=(count($timeTypes) + 1)?>">
                <h3><?=Yii::t('app', 'Schedule')?></h3>
            </th>
        </tr>
        <tr>
            <th style="text-align: center; width: 20%;"><?=Yii::t('app', 'Day of week')?></th>
            <?php foreach($timeTypes as $type):?>
                <th style="text-align: center; width: 20%;"><?=Yii::t('app', $type)?></th>
            <?php endforeach;?>
        </tr>
    </thead>
    <tbody>
        <?php foreach(SKHelper::getDaysOfWeek() as $short => $full):?>
            <tr>
                <th><?=Yii::t('app', $full)?></th>
                <?php foreach($timeTypes as $type):?>
                    <td>
                        <?=$form->field($model, "$short$type")
                            ->textInput(['maxlength' => true, 'class' => 'form-control time-input'])
                            ->label(false)?>
                    </td>
                <?php endforeach;?>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>