<?php
use backend\models\Companies;
use backend\models\Deliveries;

/* @var $model Companies */
/* @var $form yii\widgets\ActiveForm */
/* @var $cities array */
/* @var $cityId int */
/* @var $deliveryTypeId int*/
/* @var $deliveriesCount int*/

$deliveryCitiesList = [-1 => Yii::t('app', "Select City")] + $cities;
$deliveryTypesList
    = [-1 => Yii::t('app', "Select Delivery Type")]
    + Deliveries::types();
$cityId = isset($cityId) ? $cityId : -1;
$deliveryTypeId = isset($deliveryTypeId) ? $deliveryTypeId : -1;
$deliveriesCount = isset($deliveriesCount) ? $deliveriesCount : 0;
?>
<tr class="delivery_city-field" style="margin-bottom: 5px;">
    <td>
        <?=$form->field($model, "delivery_city_id[]")->dropDownList($deliveryCitiesList, ['value' => $cityId])->label(false);?>
    </td>
    <td>
        <?=$form->field($model, "delivery_type[]")->dropDownList($deliveryTypesList, ['value' => $deliveryTypeId])->label(false);?>
    </td>
    <td>
        <div class="remove-delivery_city" style="margin-top: -14px; cursor: pointer;<?=($deliveriesCount < 2 ? ' display: none;' : '')?>" title="<?=Yii::t('app', "Remove Delivery City")?>">
            <span class="glyphicon glyphicon-minus"></span><?=Yii::t('app', "Remove")?>
        </div>
    </td>
</tr>
