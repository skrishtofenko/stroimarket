<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use common\helpers\SMHelper;
use common\helpers\SKHelper;
use backend\models\Companies;
use backend\models\Persons;

/* @var $this yii\web\View */
/* @var $model backend\models\Companies */

$this->title = $model->user->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['/users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->user->vcard, 'url' => ['/users/view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Companies'), 'url' => ['/users/view', 'id' => $model->user_id, '#' => 'user-companies']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Company "{name}"', ['name' => $model->user->name]);
?>
<div class="companies-view">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
    </p>
    <?php
    $attributes = [
        [
            'attribute' => 'type',
            'format' => 'raw',
            'value' => Companies::types()[$model->type],
        ],
        [
            'attribute' => 'user_id',
            'format' => 'raw',
            'value' => Html::a($model->user->vcard, ['users/view', 'id' => $model->user_id]),
        ],
        [
            'attribute' => 'city_id',
            'format' => 'raw',
            'value' => $model->city_id
                ? Html::a($model->city->name, ['cities/view', 'id' => $model->city_id])
                : '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>',
        ],
        [
            'attribute' => 'is_paid',
            'format' => 'raw',
            'value' => $model->is_paid ? 'Да' : 'Нет',
        ],
        'paid_till',
        [
            'attribute' => 'phones',
            'format' => 'raw',
            'value' => SMHelper::renderCompanyPhones($model->phones_array),
        ],
        [
            'attribute' => 'emails',
            'format' => 'raw',
            'value' => SMHelper::renderCompanyEmails($model->emails_array),
        ],
    ];
    if($model->is_imonly) {
        $attributes = array_merge($attributes, [
            'im_url:url'
        ]);
    } else {
        $attributes = array_merge($attributes, [
            [
                'attribute' => 'address',
                'format' => 'raw',
                'value' => SMHelper::renderCompanyAddress($model),
            ],
        ]);
    }
    if($model->type != Companies::TYPE_PICKER) {
        $attributes = array_merge($attributes, [
            'catalog_updated_at',
        ]);
    }
    if($model->type == Companies::TYPE_PROVIDER) {
        $attributes = array_merge($attributes, [
            [
                'attribute' => 'is_manufacturer',
                'format' => 'raw',
                'value' => $model->is_manufacturer ? 'Да' : 'Нет',
            ],
        ]);
    }
    if($model->type == Companies::TYPE_STORE) {
        $attributes = array_merge($attributes, [
            [
                'attribute' => 'is_imonly',
                'format' => 'raw',
                'value' => $model->is_imonly ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'schedule',
                'format' => 'raw',
                'value' => SMHelper::renderCompanySchedule($model),
            ],
            [
                'attribute' => 'is_delivery',
                'format' => 'raw',
                'value' => $model->is_delivery ? 'Да' : 'Нет',
            ],
        ]);
        if($model->is_delivery) {
            $attributes = array_merge($attributes, [
                [
                    'attribute' => 'is_delivery',
                    'format' => 'raw',
                    'label' => Yii::t('app', 'Delivery Cities'),
                    'value' => SMHelper::renderCompanyDeliveries($model),
                ],
            ]);
        }
    }
    $attributes = array_merge($attributes, [
        'created_at',
        'updated_at',
    ]);
    ?>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ])?>

    <h2 id="company-persons"><?=Yii::t('app', 'Persons')?></h2>
    <p>
        <?=Html::a(Yii::t('app', 'Create Persons'), ['persons/create?company_id=' . $model->id], ['class' => 'btn btn-primary'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $personsDataProvider,
        'columns' => [
            'fio',
            [
                'attribute' => 'is_main',
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Persons */
                    return $data->is_main ? 'Да' : 'Нет';
                },
            ],
            'email:email',
            [
                'attribute' => 'phone',
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Persons */
                    return SKHelper::formatPhone($data->phone);
                },
            ],
            [
                'attribute' => 'questions',
                'value' => function($data) {return SKHelper::smartCut($data->questions, 70);}
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {set-main}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        $options = [
                            'title' => Yii::t('app', 'Update'),
                            'aria-label' => Yii::t('app', 'Update'),
                        ];
                        $url = str_replace('companies', 'persons', $url);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'delete' => function ($url, $model) {
                        /** @var $model Persons */
                        if(!$model->is_main) {
                            $options = [
                                'title' => Yii::t('app', 'Delete'),
                                'aria-label' => Yii::t('app', 'Delete'),
                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this person? This action is unrecoverable!'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ];
                            $url = str_replace('companies', 'persons', $url);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                        }
                        return '';
                    },
                    'set-main' => function ($url, $model) {
                        /** @var $model Persons */
                        if(!$model->is_main) {
                            $options = [
                                'title' => Yii::t('app', 'Set Main'),
                                'aria-label' => Yii::t('app', 'Set Main'),
                            ];
                            $url = str_replace('companies', 'persons', $url);
                            return Html::a('<span class="glyphicon glyphicon-check"></span>', $url, $options);
                        }
                        return '';
                    },
                ]
            ],
        ],
    ]);?>
</div>
