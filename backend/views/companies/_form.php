<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\assets\CompaniesAsset;
use backend\models\Companies;
use backend\models\Countries;
use backend\models\Vocabularies;

/* @var $this yii\web\View */
/* @var $model Companies */
/* @var $form yii\widgets\ActiveForm */
/* @var $cities array */
?>
<div class="companies-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php if($model->isNewRecord):?>
        <?=$form->field($model, 'type')->dropDownList(Companies::types())?>
    <?php endif;?>
    <?php if($model->isNewRecord || $model->type == Companies::TYPE_PROVIDER):?>
        <div class="custom-block for-provider">
            <?=$form->field($model, 'is_manufacturer')->checkbox()?>
            <?=$form->field($model, 'country_id')->dropDownList(Countries::getList(), ['prompt' => ''])?>
        </div>
    <?php endif;?>
    <?php if($model->isNewRecord || $model->type == Companies::TYPE_STORE):?>
        <div class="custom-block for-store" <?=($model->isNewRecord ? 'style="display: none"' : '')?>>
            <?=$form->field($model, 'is_imonly')->checkbox()?>
        </div>
    <?php endif;?>
    <div class="address-block" <?=(!$model->isNewRecord && $model->type == Companies::TYPE_STORE && $model->is_imonly ? 'style="display: none"' : '')?>>
        <?=$form->field($model, 'city_id')->dropDownList($cities, ['prompt' => ''])?>
        <?=$form->field($model, 'street')->textInput(['maxlength' => true])?>
        <?=$form->field($model, 'house')->textInput(['maxlength' => true])?>
        <?=$form->field($model, 'room_type_id')->dropDownList(
            Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_ROOMS]['items']
        )?>
        <?=$form->field($model, 'room_number')->textInput(['maxlength' => true])?>
    </div>
    <?php if($model->isNewRecord || $model->type == Companies::TYPE_STORE):?>
        <div class="im-block" <?=($model->isNewRecord || !$model->is_imonly ? 'style="display: none"' : '')?>>
            <?=$form->field($model, 'im_url')->textInput(['maxlength' => true])?>
        </div>
    <?php endif;?>

    <?php foreach(['Phone', 'Email'] as $contactType):?>
        <?=$this->render('_contacts_list', [
            'model' => $model,
            'form' => $form,
            'type' => $contactType,
        ])?>
    <?php endforeach;?>

    <?php if($model->isNewRecord || $model->type == Companies::TYPE_STORE):?>
        <div class="custom-block for-store" <?=($model->isNewRecord ? 'style="display: none"' : '')?>>
            <?=$this->render('_schedule', ['model' => $model, 'form' => $form])?>
            <h3 class="text-center"><?=Yii::t('app', 'Delivery Info')?></h3>
            <?=$form->field($model, 'is_delivery')->checkbox()?>
            <div id="delivery-block" <?=($model->isNewRecord || !$model->is_delivery ? 'style="display: none"' : '')?>>
                <?=$this->render('_deliveries_list', ['model' => $model, 'form' => $form, 'cities' => $cities])?>
            </div>
        </div>
    <?php endif;?>
    <div class="form-group">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
<?php
CompaniesAsset::register($this);
?>
