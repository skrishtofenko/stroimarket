<?php
use backend\models\Companies;

/* @var $model Companies */
/* @var $form yii\widgets\ActiveForm */
/* @var $type string */

$lcType = strtolower($type);
$contactsListType = "{$lcType}s_array";
$contactsList = $model->$contactsListType;
?>
<div id="<?=$lcType?>s">
    <label class="control-label"><?=Yii::t('app', "{$type}s")?></label>
    (<div id="add-<?=$lcType?>" style="display: inline-block; cursor: pointer;" title="<?=Yii::t('app', "Add $type")?>">
        <span class="glyphicon glyphicon-plus"></span><?=Yii::t('app', "Add")?>
    </div>)
    <?php if(!$model->isNewRecord && !empty($contactsList)):?>
        <?php foreach($contactsList as $item):?>
            <?=$this->render('_contact_item', [
                'model' => $model,
                'form' => $form,
                'type' => $type,
                'item' => $item,
                'contactsCount' => count($contactsList),
            ])?>
        <?php endforeach;?>
    <?php else:?>
        <?=$this->render('_contact_item', [
            'model' => $model,
            'form' => $form,
            'type' => $type,
        ])?>
    <?php endif;?>
</div><br />