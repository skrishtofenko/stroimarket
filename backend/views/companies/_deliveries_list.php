<?php
use backend\models\Companies;

/* @var $model Companies */
/* @var $form yii\widgets\ActiveForm */
/* @var $cities array */
?>
<label class="control-label"><?=Yii::t('app', "Delivery Cities")?></label>
(<div id="add-delivery_city" style="display: inline-block; cursor: pointer;" title="<?=Yii::t('app', "Add Delivery City")?>">
    <span class="glyphicon glyphicon-plus"></span><?=Yii::t('app', "Add")?>
</div>)
<table id="delivery_cities" style="width: 100%">
    <thead>
    <tr>
        <th style="text-align: center; width: 45%;"><?=Yii::t('app', 'City ID')?></th>
        <th style="text-align: center; width: 45%;"><?=Yii::t('app', 'Delivery Type ID')?></th>
        <th style="text-align: center; width: 10%;"></th>
    </tr>
    </thead>
    <tbody>
        <?php if(!$model->isNewRecord && !empty($model->delivery_city_id)):?>
            <?php foreach($model->delivery_city_id as $key => $cityId):?>
                <?=$this->render('_delivery_item', [
                    'model' => $model,
                    'form' => $form,
                    'cities' => $cities,
                    'cityId' => $cityId,
                    'deliveryTypeId' => $model->delivery_type[$key],
                    'deliveriesCount' => count($model->delivery_city_id),
                ])?>
            <?php endforeach;?>
        <?php else:?>
            <?=$this->render('_delivery_item', ['model' => $model, 'form' => $form, 'cities' => $cities])?>
        <?php endif;?>
    </tbody>
</table>