<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Companies */
/* @var $cities array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Companies'),
]) . $model->user->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['/users/index']];
$this->params['breadcrumbs'][] = ['label' => $model->user->vcard, 'url' => ['/users/view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Companies'), 'url' => ['/users/view', 'id' => $model->user_id, '#' => 'user-companies']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Company "{name}"', ['name' => $model->user->name]), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="companies-update">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
        'cities' => $cities,
    ])?>
</div>
