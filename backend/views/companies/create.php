<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Companies */
/* @var $cities array */
/* @var $user backend\models\Users */

$this->title = Yii::t('app', 'Create Companies');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['/users/index']];
$this->params['breadcrumbs'][] = ['label' => $user->vcard, 'url' => ['/users/view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User Companies'), 'url' => ['/users/view', 'id' => $user->id, '#' => 'user-companies']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="companies-create">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
        'cities' => $cities,
    ])?>
</div>
