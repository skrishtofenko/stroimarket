<?php
use backend\models\Companies;

/* @var $model Companies */
/* @var $form yii\widgets\ActiveForm */
/* @var $type string */
/* @var $contactsCount integer */
/* @var $item string */

$lcType = strtolower($type);
$item = isset($item) ? $item : false;
$inputOptions = $item ? ['value' => $item] : [];
$contactsCount = isset($contactsCount) ? $contactsCount : 0;
?>
<div class="<?=$lcType?>-field" style="margin-bottom: 5px;">
    <?=$form->field($model, "{$lcType}s_array[]")->textInput($inputOptions)->label(false);?>
    <div class="remove-<?=$lcType?>" style="margin-top: -16px; cursor: pointer;<?=($contactsCount < 2 ? ' display: none;' : '')?>" title="<?=Yii::t('app', "Remove $type")?>">
        <span class="glyphicon glyphicon-minus"></span><?=Yii::t('app', "Remove")?>
    </div>
</div>
