<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use common\helpers\SKHelper;
use common\helpers\SMHelper;
use backend\assets\UsersAsset;
use common\components\ImageManager\ImageManager;
use common\components\ImageManager\ImageManagerAsset;
use backend\models\Users;
use backend\models\Companies;

/* @var $this yii\web\View */
/* @var $model backend\models\Users */
/* @var $requisites backend\models\Requisites */
/* @var $companiesDataProvider yii\data\ActiveDataProvider */
/* @var $declineForm backend\models\forms\DeclineForm */

$this->title = $model->vcard;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
        <span style="float: right;">
            <?php if($model->status == Users::STATUS_REQUESTED):?>
                <?=Html::a(Yii::t('app', 'Approve'), ['approve', 'id' => $model->id], ['class' => 'btn btn-success'])?>
                <?=Html::button(Yii::t('app', 'Decline'), ['class' => 'btn btn-danger', 'id' => 'show-decline-form'])?>
            <?php endif;?>
        </span>
    </p>
    <?php if($model->status == Users::STATUS_REQUESTED):?>
        <div id="declineFormDiv">
            <?php $form = ActiveForm::begin(['action' => '/users/decline?id=' . $model->id]);?>
            <?=$form->field($declineForm, 'message')->textarea(['rows' => 6])?>
            <?=Html::submitButton(Yii::t('app', 'Send feedback'), ['class' => 'btn btn-success'])?>
            <?=Html::button(Yii::t('app', 'Cancel'), ['class' => 'btn btn-danger', 'id' => 'hide-decline-form'])?>
            <?php ActiveForm::end();?>
        </div>
    <?php endif;?>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'fio',
            [
                'attribute' => 'role',
                'format' => 'raw',
                'value' => Users::roles()[$model->role],
            ],
            [
                'attribute' => 'is_recommended',
                'format' => 'raw',
                'value' => $model->is_recommended ? 'Да' : 'Нет',
            ],
            [
                'attribute' => 'city_id',
                'format' => 'raw',
                'value' => $model->city_id ? $model->city->name : '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>',
            ],
            'email:email',
            [
                'attribute' => 'phone',
                'format' => 'raw',
                'value' => SKHelper::formatPhone($model->phone),
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => SMHelper::renderUserStatus($model),
            ],
            'created_at',
            'updated_at',
        ],
    ])?>

    <?php if($model->ownership_type_id):?>
        <h2>Информация о юрлице <?=$model->legalEntityVcard?></h2>
        <?=DetailView::widget([
            'model' => $model,
            'attributes' => [
                'legal_name',
                'name',
                [
                    'attribute' => 'ownership_type_id',
                    'format' => 'raw',
                    'value' => $model->ownership_type_id ? $model->ownershipType->name : 'Физ.лицо',
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => Users::statuses()[$model->status],
                ],
                'url:url',
                ImageManager::getViewAttribute($model, 'logo', 200),
                'description:ntext',
                [
                    'attribute' => 'checking_file',
                    'format' => 'raw',
                    'value' => $model->checking_file
                        ? '<a href="' . ImageManager::getUrl($model, 'checking_file', 800) . '" target="_blank"><img src="' . ImageManager::getUrl($model, 'checking_file', 300) . '"></a>'
                        : null,
                ],
                'legal_registered_at',
            ],
        ])?>
        <h2>Реквизиты</h2>
        <?=DetailView::widget([
            'model' => $model,
            'attributes' => [
                'inn',
                'ogrn',
                'kpp',
                'okpo',
                [
                    'attribute' => 'account',
                    'label' => Yii::t('app', 'Account'),
                    'format' => 'raw',
                    'value' => $model->mainRequisites ? $model->mainRequisites->account : null,
                ],
                [
                    'attribute' => 'bik',
                    'label' => Yii::t('app', 'Bik'),
                    'format' => 'raw',
                    'value' => $model->mainRequisites ? $model->mainRequisites->bik : null,
                ],
                [
                    'attribute' => 'bank',
                    'label' => Yii::t('app', 'Bank'),
                    'format' => 'raw',
                    'value' => $model->mainRequisites ? $model->mainRequisites->bank : null,
                ],
                [
                    'attribute' => 'corr_account',
                    'label' => Yii::t('app', 'Corr Account'),
                    'format' => 'raw',
                    'value' => $model->mainRequisites ? $model->mainRequisites->corr_account : null,
                ],
                [
                    'attribute' => 'legal_address',
                    'label' => Yii::t('app', 'Legal Address'),
                    'format' => 'raw',
                    'value' => $model->mainRequisites ? $model->mainRequisites->legal_address : null,
                ],
                [
                    'attribute' => 'post_address',
                    'label' => Yii::t('app', 'Post Address'),
                    'format' => 'raw',
                    'value' => $model->mainRequisites ? $model->mainRequisites->post_address : null,
                ],
            ],
        ])?>
        <h2 id="user-companies"><?=Yii::t('app', 'Companies')?></h2>
        <p>
            <?=Html::a(Yii::t('app', 'Create {modelClass}', [
                'modelClass' => Yii::t('app', 'Companies')
            ]), ['companies/create?user_id=' . $model->id], ['class' => 'btn btn-primary'])?>
        </p>
        <?=GridView::widget([
            'dataProvider' => $companiesDataProvider,
            'columns' => [
                [
                    'attribute' => 'type',
                    'format' => 'raw',
                    'value' => function ($data) {
                        /** @var $data Companies */
                        return Companies::types()[$data->type];
                    },
                ],
                [
                    'attribute' => 'city_id',
                    'format' => 'raw',
                    'value' => function ($data) {
                        /** @var $data Companies */
                        return $data->city_id ? Html::a(
                            $data->city->name,
                            ['cities/view', 'id' => $data->city_id]
                        ) : '<span class="not-set">' . Yii::t('app', '(not set)') . '</span>';
                    },
                ],
                [
                    'attribute' => 'is_paid',
                    'format' => 'raw',
                    'value' => function ($data) {
                        /** @var $data Companies */
                        return $data->is_paid ? 'Да' : 'Нет';
                    },
                ],
                'paid_till',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            $options = [
                                'title' => Yii::t('app', 'View'),
                                'aria-label' => Yii::t('app', 'View'),
                            ];
                            $url = str_replace('users', 'companies', $url);
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, $options);
                        },
                        'update' => function ($url, $model) {
                            $options = [
                                'title' => Yii::t('app', 'Update'),
                                'aria-label' => Yii::t('app', 'Update'),
                            ];
                            $url = str_replace('users', 'companies', $url);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                        },
                        'delete' => function ($url, $model, $key) {
                            $options = [
                                'title' => Yii::t('app', 'Delete'),
                                'aria-label' => Yii::t('app', 'Delete'),
                                'data-confirm' => Yii::t('app', 'Are you sure you want to delete this company? This action is unrecoverable!'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ];
                            $url = str_replace('users', 'companies', $url);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                        },
                    ]
                ],
            ],
        ]);?>
    <?php endif;?>
</div>
<?php
UsersAsset::register($this);
ImageManagerAsset::register($this);
?>
