<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user backend\models\Users */
/* @var $requisites backend\models\Requisites */
/* @var $cities array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Users'),
]) . $user->vcard;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $user->vcard, 'url' => ['view', 'id' => $user->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="users-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'user' => $user,
        'requisites' => $requisites,
        'cities' => $cities,
    ]) ?>

</div>
