<?php
use yii\helpers\Html;
use yii\grid\GridView;
use common\helpers\SMHelper;
use backend\models\Users;
use backend\models\Vocabularies;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Create Users'), ['create'], ['class' => 'btn btn-success'])?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fio',
            'email:email',
            [
                'attribute' => 'ownership_type_id',
                'format' => 'raw',
                'filter' => Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_LEGAL_ENTITIES]['items'],
                'value' => function ($data) {
                    /** @var $data Users */
                    return $data->ownership_type_id ? $data->ownershipType->name : 'Физ.лицо';
                },
            ],
            [
                'attribute' => 'legal_name',
                'format' => 'raw',
                'value' => function ($data) {
                    /** @var $data Users */
                    return $data->ownership_type_id ? $data->legal_name : '--' ;
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => Users::statuses(),
                'value' => function ($data) {
                    return SMHelper::renderUserStatus($data);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {
                        /** @var $model Users */
                        $options = [
                            'title' => Yii::t('app', 'Update'),
                            'aria-label' => Yii::t('app', 'Update'),
                        ];
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, $options);
                    },
                    'block' => function ($url, $model, $key) {
                        if ($model->status != Users::STATUS_BLOCKED) {
                            $options = [
                                'title' => Yii::t('app', 'Block'),
                                'aria-label' => Yii::t('app', 'Block'),
                                'data-confirm' => Yii::t('app', 'Are you sure you want to block this user?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ];
                            return Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', $url, $options);
                        } else return '';
                    },
                    'unblock' => function ($url, $model, $key) {
                        if ($model->status == Users::STATUS_BLOCKED) {
                            $options = [
                                'title' => Yii::t('app', 'Unblock'),
                                'aria-label' => Yii::t('app', 'Unblock'),
                                'data-confirm' => Yii::t('app', 'Are you sure you want to unblock this user?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ];
                            return Html::a('<span class="glyphicon glyphicon-ok-circle"></span>', $url, $options);
                        } else return '';
                    },
                    'delete' => function ($url, $model, $key) {
                        $options = [
                            'title' => Yii::t('app', 'Delete'),
                            'aria-label' => Yii::t('app', 'Delete'),
                            'data-confirm' => Yii::t('app', 'Are you sure you want to delete this user? This action is unrecoverable!'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, $options);
                    },
                ],
                'template' => '{view} {update} {block} {unblock} {delete}'
            ],
        ],
    ]);?>
</div>
