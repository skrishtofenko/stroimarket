<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\DatePicker;
use common\components\ImageManager\ImageManager;
use backend\assets\UsersAsset;
use backend\models\Vocabularies;
use backend\models\Users;

/* @var $this yii\web\View */
/* @var $user Users */
/* @var $requisites backend\models\Requisites */
/* @var $form yii\widgets\ActiveForm */
/* @var $cities array */
?>

<div class="users-form">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($user, 'fio')->textInput(['maxlength' => true])?>
    <?php if($user->isNewRecord):?>
        <?=$form->field($user, 'is_password_sent')->checkbox()?>
    <?php endif;?>
    <?=$form->field($user, 'status')->dropDownList(Users::statuses())?>
    <?=$form->field($user, 'is_recommended')->checkbox()?>
    <?=$form->field($user, 'email')->textInput(['maxlength' => true])?>
    <?=$form->field($user, 'phone')->widget(MaskedInput::className(), [
        'mask' => '+7 (999) 999-99-99',
        'clientOptions' => [
            'removeMaskOnSubmit' => true,
            'autoUnmask' => true,
        ]
    ]);?>
    <?=$form->field($user, 'city_id')->dropDownList($cities, ['prompt' => ''])?>

    <?php if($user->isNewRecord || $user->ownership_type_id):?>
        <h2><?=Yii::t('app', 'Legal Entity Info')?><?=(!$user->isNewRecord ? " $user->legalEntityVcard" : '')?></h2>
    <?php endif;?>
    <?php if($user->isNewRecord):?>
        <?=$form->field($user, 'ownership_type_id')->dropDownList(
            Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_LEGAL_ENTITIES]['items']
        )?>
    <?php endif;?>
    <?php if($user->isNewRecord || $user->ownership_type_id):?>
        <div id="fileds_for_legal_entity_only">
            <?=$form->field($user, 'legal_name')->textInput(['maxlength' => true])?>
            <?=$form->field($user, 'name')->textInput(['maxlength' => true])?>
            <?=$form->field($user, 'legal_registered_at')->widget(DatePicker::className(), [
                'value' => $user->legal_registered_at,
                'language' => 'en',
                'type' => DatePicker::TYPE_INPUT,
                'readonly' => true,
                'pluginOptions' => [
                    'format' => 'dd-M-yyyy',
                    'endDate' => '0d',
                    'autoclose' => true,
                ],
            ])?>
            <?=$form->field($user, 'url')->textInput(['maxlength' => true])?>
            <?php ImageManager::field($form, $user, 'logo')?>
            <?=$form->field($user, 'description')->textarea(['rows' => 6])?>

            <h2><?=Yii::t('app', 'Requisites')?></h2>
            <?=$form->field($user, 'inn')->textInput(['maxlength' => true, 'type' => 'number'])?>
            <?=$form->field($user, 'ogrn')->textInput(['maxlength' => true, 'type' => 'number'])?>
            <?=$form->field($user, 'kpp')->textInput(['maxlength' => true, 'type' => 'number'])?>
            <?=$form->field($user, 'okpo')->textInput(['maxlength' => true, 'type' => 'number'])?>

            <?=$form->field($requisites, 'account')->textInput(['maxlength' => true])?>
            <?=$form->field($requisites, 'bik')->textInput(['maxlength' => true])?>
            <?=$form->field($requisites, 'bank')->textInput(['maxlength' => true])?>
            <?=$form->field($requisites, 'corr_account')->textInput(['maxlength' => true])?>
            <?=$form->field($requisites, 'legal_address')->textInput(['maxlength' => true])?>
            <?=$form->field($requisites, 'post_address')->textInput(['maxlength' => true])?>
        </div>
    <?php endif;?>

    <div class="form-group">
        <?=Html::submitButton($user->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $user->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
<?php
UsersAsset::register($this);
?>
