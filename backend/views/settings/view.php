<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Settings */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-view">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->alias], ['class' => 'btn btn-primary'])?>
    </p>
    <?=DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'value',
        ],
    ])?>
</div>
