<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\EntitiesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $vocabulary array */

$this->title = $vocabulary['name'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entities-index">
    <h1><?=Html::encode($this->title)?></h1>
    <p>
        <?=Html::a(
            Yii::t('app', 'Create {modelClass}', [
                'modelClass' => $vocabulary['name'],
            ]),
            ['create?vocabulary_id=' . $vocabulary['id']], ['class' => 'btn btn-success']
        )?>
    </p>
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
            ],
        ],
    ]);?>
</div>
