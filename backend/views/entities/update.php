<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Entities */
/* @var $vocabulary array */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => $vocabulary['name'],
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', $model->vocabulary->name), 'url' => ["index?EntitiesSearch[vocabulary_id]={$model->vocabulary->id}"]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="entities-update">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
        'vocabulary' => $vocabulary,
    ])?>
</div>
