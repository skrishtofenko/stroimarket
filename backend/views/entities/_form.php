<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Entities */
/* @var $form yii\widgets\ActiveForm */
/* @var $vocabulary array */
?>
<div class="entities-form">
    <?php $form = ActiveForm::begin();?>
    <?=$form->field($model, 'name')->textInput(['maxlength' => true])?>
    <div class="form-group">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>
    <?php ActiveForm::end();?>
</div>
