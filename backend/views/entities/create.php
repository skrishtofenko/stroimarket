<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Entities */
/* @var $vocabulary array */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => $vocabulary['name'],
]);
$this->params['breadcrumbs'][] = ['label' => $vocabulary['name'], 'url' => ["index?EntitiesSearch[vocabulary_id]={$vocabulary['id']}"]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entities-create">
    <h1><?=Html::encode($this->title)?></h1>
    <?=$this->render('_form', [
        'model' => $model,
        'vocabulary' => $vocabulary,
    ])?>
</div>
