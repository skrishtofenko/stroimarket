<?php
namespace backend\assets;

use yii\web\AssetBundle;

class UsersAsset extends AssetBundle {
    public $sourcePath = '@backend/web/js';
    public $css = [];
    public $js = [
        'users.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}