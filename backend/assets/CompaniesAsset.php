<?php
namespace backend\assets;

use yii\web\AssetBundle;

class CompaniesAsset extends AssetBundle {
    public $sourcePath = '@backend/web/js';
    public $css = [];
    public $js = [
        'companies.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\widgets\MaskedInputAsset',
    ];
}