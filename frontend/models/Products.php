<?php
namespace frontend\models;

use Yii;
use frontend\models\forms\PS;

/**
 * @property Companies $providers
 * @property Companies $stores
 * */
class Products extends \common\models\extended\Products {
    const EDIT_SORTS = [
        'art' => 'vendor_code_for_sort',
        'sm_art' => 'product_id_for_sort',
        'product' => 'product_name_for_sort',
        'category' => 'category_name_for_sort',
    ];
    const DEFAULT_EDIT_SORT = 'art';

    const GET_SEARCH_RESULT = 0;
    const GET_SEARCH_RESULT_WITH_COUNT = 1;
    const GET_SEARCH_RESULT_ONLY_COUNT = 2;

    public $providerCnt;
    public $storeCnt;
    public $actionsCnt;

    public function getProviders() {
        return Companies::find()
            ->active()
            ->providers()
            ->innerJoin('companies_products', 'companies_products.company_id=companies.id')
            ->andWhere(['product_id' => $this->id])
            ->all();
    }

    public function getStores() {
        return Companies::find()
            ->active()
            ->stores()
            ->innerJoin('companies_products', 'companies_products.company_id=companies.id')
            ->andWhere(['product_id' => $this->id])
            ->all();
    }

    public function getForCompany($companyId) {
        return CompaniesProducts::findOne(['product_id' => $this->id, 'company_id' => $companyId]);
    }

    public static function getHintsByName($query) {
        $products = self::find()
            ->active()
            ->select(['id', 'name'])
            ->andWhere(['like', 'name', $query])
            ->all();

        $result = [];
        foreach ($products as $product) {
            /** @var $product self */
            $result[] = [
                'id' => $product->id,
                'name' => $product->name,
            ];
        }
        return $result;
    }

    public static function getByFilter(PS &$searchForm, $searchType = self::GET_SEARCH_RESULT) {
        //todo: разбить метод на несколько отдельных
        $products = self::find()
            ->select([
                'products.id',
                'products.category_id',
                'products.manufacturer_id',
                'products.prod_country_id',
                'products.unit_type_id',
                'products.name',
                'products.image',
                'products.description',
                'products.min_cost',
                'products.max_cost',
                'products.assortment_count',
                'products.availability_count',
                self::companiesCountField(Companies::TYPE_PROVIDER),
                self::companiesCountField(Companies::TYPE_STORE),
                self::actionsCountField(),
            ])
            ->leftJoin('companies_products', 'products.id = companies_products.product_id AND companies_products.status = 1')
            ->leftJoin('companies', 'companies_products.company_id = companies.id')
            ->leftJoin('users', 'companies.user_id = users.id')
            ->active()
            ->groupBy(['products.id', 'cost']);
        // TODO: не помню, почему здесь идет группировка по cost, но из-зв нее неправильно считается количество товаров в категории. Нужно разобраться

        if(!empty($searchForm->q)) {
            $products->andWhere(['like', 'products.name', $searchForm->q]);
        }

        if(!empty($searchForm->cat)) {
            $products->andWhere(['products.category_id' => $searchForm->cat]);
            $filterCharacteristics = $searchForm->getClearCharacteristicsFilter();
            if(!empty($filterCharacteristics)) {
                foreach($filterCharacteristics as $characteristicId => $characteristicValue) {
                    $products->leftJoin(["ps$characteristicId" => 'products_characteristics'], "products.id = ps$characteristicId.product_id AND ps$characteristicId.characteristic_id = $characteristicId");
                    if(is_numeric($characteristicValue)) {
                        $products->andWhere(["ps$characteristicId.value" => $characteristicValue]);
                    } else {
                        $products->andWhere(['REGEXP', "ps$characteristicId.value",  $characteristicValue]);
                    }
                }
            }
        }

        if(!empty($searchForm->c)) {
            $selectedCities = array_keys(
                array_filter(
                    $searchForm->c,
                    function($value) {return $value == 1;}
                )
            );
            if(!empty($selectedCities)) {
                $products->andWhere(['companies.city_id' => $selectedCities]);
            }
        }

        if(!empty($searchForm->a) && $searchForm->a) {
            $products->andWhere(['companies_products.is_action' => 1]);
        }

        if(!empty($searchForm->companyId)) {
            $products->andWhere(['companies_products.company_id' => $searchForm->companyId]);
        }

        if(!empty($searchForm->m)) {
            $selectedManufacturers = array_keys(
                array_filter(
                    $searchForm->m,
                    function($value) {return $value == 1;}
                )
            );
            if(!empty($selectedManufacturers)) {
                $products->andWhere(['products.manufacturer_id' => $selectedManufacturers]);
            }
        }

        if($userCity = Users::getCurrentUserCity()) {
            if($searchForm->d) {
                $products
                    ->innerJoin(["deliveries_delivery" => 'deliveries'], "companies.id = deliveries_delivery.company_id AND deliveries_delivery.city_id = $userCity")
                    ->andWhere(['deliveries_delivery.type' => [Deliveries::TYPE_DELIVERY, Deliveries::TYPE_BOTH]]);
            }
            if($searchForm->p) {
                $products
                    ->innerJoin(["deliveries_pickup" => 'deliveries'], "companies.id = deliveries_pickup.company_id AND deliveries_pickup.city_id = $userCity")
                    ->andWhere(['deliveries_pickup.type' => [Deliveries::TYPE_PICKUP, Deliveries::TYPE_BOTH]]);
            }
        }

        if(!empty($searchForm->pf)) {
            $products->andWhere(['>=', 'companies_products.cost', (int) $searchForm->pf * 100]);
        }
        if(!empty($searchForm->pt)) {
            $products->andWhere(['<=', 'companies_products.cost', (int) $searchForm->pt * 100]);
        }

        if(in_array($searchType, [self::GET_SEARCH_RESULT_ONLY_COUNT, self::GET_SEARCH_RESULT_WITH_COUNT])) {
            $searchForm->found = $products->count();
            if($searchType == self::GET_SEARCH_RESULT_ONLY_COUNT) {
                return $searchForm->found;
            }
        }

        $products->orderBy($searchForm->getSort());

        if($limit = $searchForm->getOnPage()) {
            $products->limit($limit);
            $page = !empty($searchForm->page) ? $searchForm->page : 1;
            $page = min($page, $searchForm->found / $limit);
            $products->offset(($page - 1) * $limit);
        }
        return $products->all();
    }

    public static function companiesCountField($companyType) {
//        return "SUM(CASE WHEN companies.type = '$companyType' AND companies.is_paid = 1 AND users.status = " . Users::STATUS_ACTIVE . " THEN 1 ELSE 0 END) {$companyType}Cnt";
        return "SUM(CASE WHEN companies.type = '$companyType' AND users.status = " . Users::STATUS_ACTIVE . " THEN 1 ELSE 0 END) {$companyType}Cnt";
    }

    public static function actionsCountField() {
        return 'SUM(companies_products.is_action) actionsCnt';
    }

    public static function getForConformity($categoryId = null, $query = null) {
        if(!$categoryId && !$query) {
            return [];
        }
        /** @var Users $user */
        /** @var Companies $company */
        $user = Yii::$app->user->identity;
        $company = $user->getCurrentCompany();

        $products = self::find()
            ->leftJoin('companies_products', 'companies_products.product_id = products.id AND companies_products.company_id = ' . $company->id)
            ->where(['is_active' => 1])
            ->andWhere(['companies_products.id' => null]);
        if($categoryId) {
            $products->andWhere(['category_id' => $categoryId]);
        } else {
            $products->andWhere(['like', 'products.name', $query]);
        }
        return $products->all();
    }

    public static function getRealEditSort($sort) {
        return isset(self::EDIT_SORTS[$sort]) ? self::EDIT_SORTS[$sort] : self::EDIT_SORTS[self::DEFAULT_EDIT_SORT];
    }
}
