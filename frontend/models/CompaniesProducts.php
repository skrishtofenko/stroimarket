<?php
namespace frontend\models;

use Yii;
use yii\db\Expression;
use frontend\models\forms\ProductForm;

class CompaniesProducts extends \common\models\extended\CompaniesProducts {
    public static function safeCreateOrUpdate(ProductForm $productForm) {
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        /** @var Companies $company */
        $company = $user->getCurrentCompany();
        if($company->type == Companies::TYPE_PICKER) {
            return null;
        }
        if($productForm->id) {
            $product = self::findOne($productForm->id);
            if(!$product || $product->company_id != $company->id) {
                return null;
            }
        } else {
            $product = new self;
            $product->company_id = $company->id;
        }
        $product->product_id = $productForm->productId;
        $product->is_promo = $productForm->isPromo;
        $product->status = $productForm->isVisible;
        $product->vendor_code = $productForm->vendorCode ?: null;
        $product->name = $productForm->name ?: null;
        $product->url = $productForm->url;
        $product->cost = $productForm->cost;
        $product->cnt = $productForm->cnt;

        if($company->type == Companies::TYPE_PROVIDER) {
            $product->is_action = $productForm->isAction;
            $product->unit_type_id = $productForm->unitTypeId;
            $product->multiplicity = $productForm->multiplicity;
            $product->is_in_stock = 0;
            $product->is_by_order = 0;
        } else {
            $product->is_action = null;
            $product->unit_type_id = null;
            $product->multiplicity = null;
            $product->is_in_stock = $productForm->isInStock;
            $product->is_by_order = $productForm->isByOrder;
        }
        $isNewRecord = $product->isNewRecord;
        $isNewAction = ($isNewRecord || !$product->oldAttributes['is_action']) && $product->is_action;
        if($product->save()) {
            if($product->company->is_paid && $isNewAction) {
                $product->actionNotify();
            }
            return true;
        }
        return false;
    }

    public static function getRandom($companyId, $limit = 3, $isPromo = true) {
        return $promoProducts = CompaniesProducts::find()
            ->active()
            ->andWhere(['companies_products.is_promo' => (int) $isPromo, 'company_id' => $companyId])
            ->limit($limit)
            ->orderBy(new Expression('rand()'))
            ->all();
    }

    private function actionNotify() {
        if($this->product) {
            /** @var Users[] $users */
            $users = Users::find()
                ->innerJoin('notices_categories', 'notices_categories.user_id = users.id AND notices_categories.category_id = ' . $this->product->category_id)
                ->andWhere(['users.status' => Users::STATUS_ACTIVE])
                ->all();
            if ($users) {
                foreach ($users as $user) {
                    $user->email('newAction', $user->email, ['user' => $user, 'product' => $this]);
                }
            }
        }
    }

    public static function checkVendorCode($vendorCode, $id = null) {
        /** @var Users $user */
        /** @var Companies $company */
        $user = Yii::$app->user->identity;
        $company = $user->getCurrentCompany();
        $existsProduct = self::find()->where(['vendor_code' => $vendorCode, 'company_id' => $company->id]);
        if($id) {
            $existsProduct->andWhere(['!=', 'id', $id]);
        }
        return $existsProduct->limit(1)->one();
    }

    public static function getVendorCodesList() {
        /** @var Users $user */
        /** @var Companies $company */
        $user = Yii::$app->user->identity;
        $company = $user->getCurrentCompany();
        return self::find()
            ->select(['id', 'vendor_code'])
            ->where(['company_id' => $company->id])
            ->andWhere(['!=', 'vendor_code', ''])
            ->all();
    }
}
