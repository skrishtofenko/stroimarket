<?php
namespace frontend\models;

use Yii;

class Notices extends \common\models\extended\Notices {
    public $categoriesIds;

    public function rules() {
        $rules = parent::rules();
        $rules[] = ['categoriesIds', 'each', 'rule' => ['integer']];
        $rules[] = ['categoriesIds', 'each', 'rule' => ['exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['categoriesIds' => 'id']]];
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['messages'] = 'Новые сообщения:';
        $labels['actions'] = 'Акции:';
        $labels['offers'] = '';
        $labels['claims'] = '';
        $labels['specoffers'] = 'Спецпредложения от строймаркета:';
        $labels['purchases'] = '';
        $labels['orders'] = 'Новые заказы:';
        $labels['reviews'] = 'Новые отзывы:';
        return $labels;
    }

    public function fillCategories() {
        $categories = $this->user->noticesCategories;
        if(!empty($categories)) {
            foreach($categories as $category) {
                $this->categoriesIds[$category->category_id] = 1;
            }
        }
    }

    public function saveWithCategories() {
        $transaction = Yii::$app->db->beginTransaction();
        unset($this->categoriesIds[0]);
        if(empty($this->categoriesIds)) {
            $this->actions = 0;
        }
        if(!$this->save()) {
            $transaction->rollBack();
            return false;
        }
        NoticesCategories::deleteAll(['user_id' => $this->user_id]);
        if($this->actions && !empty($this->categoriesIds)) {
            foreach($this->categoriesIds as $categoryId => $v) {
                $newCategoryNotice = new NoticesCategories();
                $newCategoryNotice->category_id = $categoryId;
                $newCategoryNotice->user_id = $this->user_id;
                if(!$newCategoryNotice->save()) {
                    $transaction->rollBack();
                    return false;
                }
            }
        }
        $transaction->commit();
        return true;
    }
}
