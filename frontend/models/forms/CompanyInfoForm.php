<?php
namespace frontend\models\forms;

use yii\base\Model;
use common\helpers\SMHelper;
use common\models\validators\CompanyAddressValidator;
use common\models\validators\CompanyUrlValidator;
use frontend\models\Users;
use frontend\models\Cities;
use frontend\models\Entities;
use frontend\models\Companies;
use frontend\models\Deliveries;

class CompanyInfoForm extends Model {
    public $type;
    public $name;
    public $city_id;
    public $street;
    public $house;
    public $room_type_id;
    public $room_number;
    public $im_url;
    public $is_imonly;
    public $phone;
    public $email;
    public $is_delivery;
    public $delivery_city_id;
    public $delivery_type;

    public $monFrom, $monTo, $monDinnerFrom, $monDinnerTo;
    public $tueFrom, $tueTo, $tueDinnerFrom, $tueDinnerTo;
    public $wedFrom, $wedTo, $wedDinnerFrom, $wedDinnerTo;
    public $thuFrom, $thuTo, $thuDinnerFrom, $thuDinnerTo;
    public $friFrom, $friTo, $friDinnerFrom, $friDinnerTo;
    public $satFrom, $satTo, $satDinnerFrom, $satDinnerTo;
    public $sunFrom, $sunTo, $sunDinnerFrom, $sunDinnerTo;

    public $legal_name;
    public $description;
    public $url;
    public $fio;
    public $ownership_type_id;

    public $isOnSettings = false;

    public function rules() {
        return [
            [['type', 'phone', 'email'], 'required'],
            [['name'], 'required', 'when' => function() {
                return !!$this->ownership_type_id;
            }],
            [['city_id'], 'required', 'when' => function() {
                return !$this->ownership_type_id;
            }, 'whenClient' => "function (attribute, value) {
                return false;
            }"],
            [['street', 'house'], CompanyAddressValidator::className(), 'skipOnEmpty' => false],
            [['im_url'], CompanyUrlValidator::className(), 'skipOnEmpty' => false],
            [['legal_name'], 'required', 'when' => function() {
                return $this->isOnSettings;
            }],
            [['is_imonly', 'is_delivery'], 'integer'],
            [SMHelper::$scheduleAttributes, 'string', 'min' => 5, 'max' => 5],
            [['type', 'description'], 'string'],
            [['phone'], 'string', 'max' => 10],
            [['house', 'room_number'], 'string', 'max' => 15],
            [['email', 'name', 'street', 'fio', 'legal_name'], 'string', 'max' => 127],
            [['url'], 'string', 'max' => 255],
            ['email', 'email'],
            ['email', 'trim'],
            [['url'], 'url', 'defaultScheme' => 'http'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['room_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['room_type_id' => 'id']],
            [
                ['delivery_city_id'],
                'each',
                'rule' => [
                    'exist',
                    'skipOnError' => true,
                    'targetClass' => Cities::className(),
                    'targetAttribute' => ['delivery_city_id' => 'id'],
                ],
                'when' => function() {
                    return $this->type == Companies::TYPE_STORE && $this->is_delivery;
                },
            ],
            [
                ['delivery_type'],
                'each',
                'rule' => ['in', 'range' => array_keys(Deliveries::types())],
                'when' => function() {
                    return $this->type == Companies::TYPE_STORE && $this->is_delivery;
                },
            ],
        ];
    }

    public function fill(Users $user, Companies $company = null) {
        if(!$company) {
            $company = $user->mainCompany ?: new Companies();
        }
        $this->type = $company->type;
        $this->name = $user->name;
        $this->city_id = $company->city_id;
        $this->street = $company->street;
        $this->house = $company->house;
        $this->room_type_id = $company->room_type_id;
        $this->room_number = $company->room_number;
        $this->im_url = $company->im_url;
        $this->is_imonly = $company->is_imonly;
        $this->is_delivery = $company->is_delivery;
        $this->phone = !empty($company->phones_array) ? $company->phones_array[0] : '';
        $this->email = !empty($company->emails_array) ? $company->emails_array[0] : '';
        $this->delivery_city_id = $company->delivery_city_id;
        $this->delivery_type = $company->delivery_type;
        foreach (SMHelper::$scheduleAttributes as $attribute) {
            $this->$attribute = $company->$attribute;
        }

        $this->description = $user->description;
        $this->url = $user->url;
        $this->fio = $user->fio;
        $this->ownership_type_id = $user->ownership_type_id;
        $this->legal_name = $user->legal_name;
    }
}