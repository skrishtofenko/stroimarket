<?php
namespace frontend\models\forms;

use yii\base\Model;

// имя класса сокращено для уменьшения длины генерируемого URL
class CS extends Model { // ProductsSearchForm
    // имена переменных сокращены для уменьшения длины генерируемого URL
    public $q; // query
    public $c; // cityId
    public $m; // isManufacturer
    public $mc; // onlyMyCity
    public $id; // isDelivery
    public $cat; // categoryId

    public $type;
    public $productId;
    public $isRecommended;

    public $s; // sort
    const DEFAULT_SORT_FIELD = 'legal_name';

    public $so; // sortOrder
    const AVAILABLE_SORT_ORDERS = [1 => SORT_ASC, -1 => SORT_DESC];
    const DEFAULT_SORT_ORDER = 1;

    public $page;

    public $op; // onPage
    const AVAILABLE_ON_PAGE = [12];
    const DEFAULT_ON_PAGE = 12;

    public $found;

    public function rules() {
        return [
            [['c', 'cat', 'page'], 'integer'],
            [['q', 's', 'so'], 'string'],
            [['q'], 'trim'],
            [['m', 'mc', 'id'], 'boolean']
        ];
    }

    public static function getAvailableSortFields($isBuyer = false) {
        $sorts = [
            'legal_name' => 'по названию',
            'totalRate' => 'по рейтингу',
        ];
        if($isBuyer) {
            $sorts['cost'] = 'по цене';
        }
        return $sorts;
    }

    public function getSort() {
        $sortField = self::DEFAULT_SORT_FIELD;
        if(isset($this->s) && isset(self::getAvailableSortFields(true)[$this->s])) {
            $sortField = $this->s;
        }
        $sortField = $sortField == 'legal_name' ? "users.$sortField" : $sortField;
        $sortOrder = self::DEFAULT_SORT_ORDER;
        if(isset($this->so) && isset(self::AVAILABLE_SORT_ORDERS[$this->so])) {
            $sortOrder = $this->so;
        }
        $sortOrder = self::AVAILABLE_SORT_ORDERS[$sortOrder];

        return [$sortField => $sortOrder];
    }

    public function getOnPage() {
        if(!isset($this->op) || !in_array($this->op, self::AVAILABLE_ON_PAGE)) {
            $this->op = self::DEFAULT_ON_PAGE;
        }
        return $this->op;
    }
}
