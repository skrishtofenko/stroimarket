<?php
namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\Users;

class PasswordResetRequestForm extends Model {
    public $email;

    public function rules() {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\frontend\models\Users',
                'filter' => ['in', 'status', Users::ACTIVE_STATUSES],
                'message' => 'Пользователь с таким email не зарегистрирован или заблокирован администратором.'
            ],
        ];
    }
}
