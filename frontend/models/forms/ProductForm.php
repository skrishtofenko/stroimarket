<?php
namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\Products;
use frontend\models\Entities;
use common\models\validators\CompanyProductValidator;
use common\models\validators\CompanyProductVendorCodeValidator;

class ProductForm extends Model {
    public $id;

    public $isVisible;
    public $isInStock;
    public $isByOrder;
    public $isPromo;
    public $isAction;

    public $productId;
    public $unitTypeId;

    public $name;
    public $vendorCode;
    public $url;
    public $cnt;
    public $cost;
    public $multiplicity;

    public function rules() {
        return [
            [['name', 'vendorCode'], CompanyProductValidator::className()],
            [['isVisible', 'isInStock', 'isByOrder', 'isPromo', 'isAction'], 'boolean'],
            ['unitTypeId', 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['unitTypeId' => 'id']],
            ['productId', 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['productId' => 'id']],
            [['name', 'url'], 'string', 'max' => 255],
            [['vendorCode'], 'string', 'max' => 63],
            [['cnt', 'multiplicity'], 'number'],
            [['id', 'cost'], 'integer'],
            [['url'], 'url', 'defaultScheme' => 'http'],
            [['vendorCode'], CompanyProductVendorCodeValidator::className()]
        ];
    }
}
