<?php
namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\Users;

class PasswordResetForm extends Model {
    public $password;
    public $passwordRepeat;
    public $token;

    public function rules() {
        return [
            ['token', 'validateToken'],
            [['password', 'passwordRepeat', 'token'], 'required'],
            [['password', 'passwordRepeat'], 'string', 'min' => 8],
            ['passwordRepeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли должны совпадать'],
        ];
    }

    public function attributeLabels() {
        return [
            'password' => 'Пароль',
            'passwordRepeat' => 'Повтор пароля',
        ];
    }

    public function validateToken($attribute, $params, $validator) {
        if(empty($this->$attribute) || !is_string($this->$attribute)) {
            $this->addError('token', 'Некорректный формат ссылки.');
        }
        if(!Users::findByPasswordResetToken($this->$attribute)) {
            $this->addError('token', 'Ссылка недействительна.');
        }
    }
}
