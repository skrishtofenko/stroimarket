<?php
namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\Users;

class LoginForm extends Model {
    public $email;
    public $password;
    public $rememberMe = true;
    public $isRegistrationFirstLogin = false;

    public function rules() {
        return [
            [['email', 'password'], 'required'],
            ['email', 'email'],
            [['rememberMe', 'isRegistrationFirstLogin'], 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels() {
        return [
            'email' => 'Email',
            'password' => 'Пароль'
        ];
    }

    public function validatePassword($attribute, $params) {
        if (!$this->hasErrors()) {
            $user = Users::findByEmail($this->email);
            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Недействительная пара логин/пароль.');
            }
        }
    }
}
