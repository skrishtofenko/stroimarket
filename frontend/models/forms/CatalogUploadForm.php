<?php
namespace frontend\models\forms;

use yii\base\Model;

class CatalogUploadForm extends Model {
    public $catalogFile;

    public function rules() {
        return [[
            'catalogFile',
            'file',
            'skipOnEmpty' => false,
            'extensions' => 'xls, xlsx',
            'mimeTypes' => ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel'],
            'maxSize' => 10 * 1024 * 1024
        ]];
    }
}
