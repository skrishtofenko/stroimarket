<?php
namespace frontend\models\forms;

use Yii;
use yii\base\Model;
use common\models\validators\CorrAccountValidator;
use frontend\models\Users;
use frontend\models\Requisites;

class RequisitesInfoForm extends Model {
    public $legal_name;
    public $inn;
    public $ogrn;
    public $kpp;
    public $okpo;
    public $account;
    public $bik;
    public $bank;
    public $corr_account;
    public $legal_address;
    public $post_address;

    public $isOnSettings = false;

    public function rules() {
        return [
            [['inn', 'ogrn', 'kpp', 'okpo', 'account', 'bik', 'bank', 'corr_account', 'legal_address', 'post_address'], 'required'],
            [['legal_name'], 'required', 'when' => function() {
                return $this->isOnSettings;
            }],
            [['inn', 'ogrn', 'kpp', 'okpo', 'account', 'bik', 'corr_account'], 'string'],
            [['bank', 'legal_name'], 'string', 'max' => 127],
            [['legal_address', 'post_address'], 'string', 'max' => 255],
            ['kpp', 'match', 'pattern' => '/^\d{9}$/s', 'message' => Yii::t('app', 'KPP must be 9 digits')],
            ['okpo', 'match', 'pattern' => '/^(\d{8}|\d{10})$/s', 'message' => Yii::t('app', 'OKPO must be 8 or 10 digits')],
            ['inn', 'match', 'pattern' => '/^(\d{10}|\d{12})$/s', 'message' => Yii::t('app', 'INN must be 10 or 12 digits')],
            ['ogrn', 'match', 'pattern' => '/^(\d{13}|\d{15})$/s', 'message' => Yii::t('app', 'OGRN must be 13 or 15 digits')],
            ['account', 'match', 'pattern' => '/^\d{20}$/s', 'message' => Yii::t('app', 'Account must be 20 digits')],
            ['bik', 'match', 'pattern' => '/^(\d{9})$/s', 'message' => Yii::t('app', 'BIK must be 9 digits')],
            ['corr_account', 'match', 'pattern' => '/^(\d{20})$/s', 'message' => Yii::t('app', 'Corr.Account must be 20 digits')],
            ['corr_account', CorrAccountValidator::className()],
        ];
    }

    public function attributeLabels() {
        return [
            'legal_name' => Yii::t('app', 'Legal Name') . ':',
            'inn' => Yii::t('app', 'Inn') . ':',
            'ogrn' => Yii::t('app', 'Ogrn') . ':',
            'kpp' => Yii::t('app', 'Kpp') . ':',
            'okpo' => Yii::t('app', 'Okpo') . ':',
            'account' => Yii::t('app', 'Account') . ':',
            'bik' => Yii::t('app', 'Bik') . ':',
            'bank' => Yii::t('app', 'Bank') . ':',
            'corr_account' => Yii::t('app', 'Corr Account') . ':',
            'legal_address' => Yii::t('app', 'Legal Address') . ':',
            'post_address' => Yii::t('app', 'Post Address') . ':',
        ];
    }

    public function fill(Users $user) {
        $requisites = $user->mainRequisites ?: new Requisites();
        $this->legal_name = $user->legal_name;
        $this->inn = $user->inn;
        $this->ogrn = $user->ogrn;
        $this->kpp = $user->kpp;
        $this->okpo = $user->okpo;

        $this->account = $requisites->account;
        $this->bik = $requisites->bik;
        $this->bank = $requisites->bank;
        $this->corr_account = $requisites->corr_account;
        $this->legal_address = $requisites->legal_address;
        $this->post_address = $requisites->post_address;
    }
}
