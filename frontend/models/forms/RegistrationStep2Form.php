<?php
namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\Entities;
use common\models\validators\UserLegalEntityParamsValidator;

class RegistrationStep2Form extends Model {
    public $isManufacturer;
    public $isLegalEntity;

    public $ownershipTypeId;
    public $legalEntityName;

    public $fio;
    public $email;
    public $phoneCode;
    public $phoneNumber;

    public $type;

    public function rules() {
        return [
            [['fio', 'email', 'phoneCode', 'phoneNumber', 'type'], 'required'],
            ['ownershipTypeId', 'exist', 'skipOnError' => true, 'targetClass' => Entities::className(), 'targetAttribute' => ['ownershipTypeId' => 'id']],
            [['ownershipTypeId', 'legalEntityName'], UserLegalEntityParamsValidator::className()],
            [['isLegalEntity', 'isManufacturer'], 'boolean'],
            ['email', 'unique', 'targetClass' => '\frontend\models\Users', 'message' => 'This email address has already been taken.'],
            ['email', 'email'],
            ['email', 'trim'],
            [['email', 'fio'], 'string', 'max' => 127],
            [['phoneCode'], 'string', 'max' => 3],
            [['phoneNumber'], 'string', 'max' => 7],
            [['phoneCode'], 'match', 'pattern' => '/^[\d]{3}$/', 'message' => 'Phone code must be 3 digits'],
            [['phoneNumber'], 'match', 'pattern' => '/^[\d]{7}$/', 'message' => 'Phone must be 7 digits'],
        ];
    }

    public function attributeLabels() {
        return [
            'isManufacturer' => 'Вы являетесь производителем',
            'isLegalEntity' => 'Вы являетесь юр.лицом или ИП',
            'ownershipTypeId' => 'Форма собственности',
            'legalEntityName' => 'Название юридического лица',
            'fio' => 'ФИО',
            'email' => 'Электронная почта',
            'phoneCode' => 'Код',
            'phoneNumber' => 'Номер',
            'type' => 'Тип компании',
        ];
    }
}
