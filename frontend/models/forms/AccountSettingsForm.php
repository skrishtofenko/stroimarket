<?php
namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\Users;

class AccountSettingsForm extends Model {
    public $fio;
    public $phone;

    public function rules() {
        return [
            [['phone'], 'required'],
            [['phone'], 'string', 'max' => 10],
            [['fio'], 'string', 'max' => 127],
        ];
    }

    public function fill(Users $user) {
        $this->fio = $user->fio;
        $this->phone = $user->phone;
    }
}
