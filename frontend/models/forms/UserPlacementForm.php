<?php
namespace frontend\models\forms;

use yii\base\Model;
use frontend\models\Cities;

class UserPlacementForm extends Model {
    public $city_id;

    public function rules() {
        return [
            [['city_id'], 'required'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city_id' => 'id']],
        ];
    }
}