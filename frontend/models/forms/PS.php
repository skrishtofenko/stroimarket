<?php
namespace frontend\models\forms;

use frontend\models\Users;
use yii\base\Model;

// имя класса сокращено для уменьшения длины генерируемого URL
class PS extends Model { // ProductsSearchForm
    // имена переменных сокращены для уменьшения длины генерируемого URL
    public $cat; // categoryId
    public $q; // query
    public $c; // cityId
    public $a; // isAction
    public $m; // manufacturerId
    public $ch; // characteristics
    public $pf; // priceFrom
    public $pt; // priceTo
    public $d; // delivery
    public $p; // pickup

    public $page;

    public $op; // onPage
    const AVAILABLE_ON_PAGE = [30, 50, 0];
    const DEFAULT_ON_PAGE = 30;

    public $s; // sort
    const DEFAULT_SORT_FIELD = 'name';

    public $so; // sortOrder
    const AVAILABLE_SORT_ORDERS = [1 => SORT_ASC, -1 => SORT_DESC];
    const DEFAULT_SORT_ORDER = 1;

    public $companyId;
    public $found;

    public function rules() {
        return [
            [['cat', 'c', 'pf', 'pt', 'page'], 'integer'],
            [['m', 'op'], 'each', 'rule' => ['integer']],
            [['ch'], 'each', 'rule' => ['string']],
            [['a', 'd', 'p'], 'boolean'],
            [['q', 's', 'so'], 'string'],
            [['q'], 'trim'],
        ];
    }

    public function getClearCharacteristicsFilter() {
        if(!is_array($this->ch) || empty($this->ch)) {
            return [];
        }
        $newCharacteristics = [];
        foreach($this->ch as $characteristicId => $characteristicValue) {
            if(is_array($characteristicValue)) {
                $fullValue = [];
                foreach($characteristicValue as $value => $isChecked) {
                    if($isChecked) $fullValue[] = $value;
                }
                if(!empty($fullValue)) {
                    $newCharacteristics[$characteristicId] = implode('|', $fullValue);
                }
            } else {
                if($characteristicValue === '0' || $characteristicValue === '1') {
                    $newCharacteristics[$characteristicId] = $characteristicValue;
                }
            }
        }
        return $newCharacteristics;
    }

    public function getSort() {
        $sortField = self::DEFAULT_SORT_FIELD;
        if(isset($this->s) && isset(self::getAvailableSortFields()[$this->s])) {
            $sortField = $this->s;
        }
        $sortField = $sortField == 'name' ? "products.$sortField" : $sortField;
        $sortField = $sortField == 'top' ? "products.assortment_count" : $sortField;
        $sortOrder = self::DEFAULT_SORT_ORDER;
        if(isset($this->so) && isset(self::AVAILABLE_SORT_ORDERS[$this->so])) {
            $sortOrder = $this->so;
        }
        $sortOrder = self::AVAILABLE_SORT_ORDERS[$sortOrder];

        return [$sortField => $sortOrder];
    }

    public static function getAvailableSortFields($userRole = null) {
        $fields = [];
        if(!$userRole || $userRole == Users::ROLE_COMPANY) {
            $fields['name'] = 'по названию';
            $fields['providerCnt'] = 'по количеству поставщиков';
            $fields['storeCnt'] = 'по количеству магазинов';
        }
        if(!$userRole || $userRole == Users::ROLE_BUYER) {
            $fields['cost'] = 'по цене';
            $fields['top'] = 'по популярности';
//            $fields['novels'] = 'новинки';
        }
        return $fields;
    }

    public function getOnPage() {
        if(!isset($this->op) || !in_array($this->op, self::AVAILABLE_ON_PAGE)) {
            $this->op = self::DEFAULT_ON_PAGE;
        }
        return $this->op;
    }
}
