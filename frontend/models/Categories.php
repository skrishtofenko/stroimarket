<?php
namespace frontend\models;

use yii\helpers\ArrayHelper;
use frontend\models\forms\PS;

/**
 * @property Manufacturers[] $manufacturers
 */
class Categories extends \common\models\extended\Categories {
    public $visibleProductsCount = 0;
    public $children;

    public static function getCategoriesList() {
        return self::find()
            ->where(['level' => self::LEVEL_CHAPTER])
            ->orderBy('name')
            ->all();
    }

    public function fillChildren() {
        if($this->level == self::LEVEL_GOODSCATEGORY) return null;
        $level = self::LEVEL_CATEGORY;
        switch ($this->level) {
            case self::LEVEL_CATEGORY:
                $level = self::LEVEL_SUBCATEGORY;
                break;
            case self::LEVEL_SUBCATEGORY:
                $level = self::LEVEL_GOODSCATEGORY;
                break;
        }
        $this->children = self::find()
            ->where(['level' => $level, 'parent_id' => $this->id])
            ->orderBy('name')
            ->all();
    }

    public static function getFullTree(PS $searchForm = null, $isWhole = false) {
        $chapters = self::getCategoriesList();
        foreach($chapters as &$chapter) {
            /** @var $chapter self */
            $chapter->fillChildren();
            foreach($chapter->children as &$category) {
                /** @var $category Categories */
                $category->fillChildren();
                foreach($category->children as &$subcategory) {
                    /** @var $subcategory Categories */
                    $subcategory->fillChildren();
                    foreach($subcategory->children as &$goodscategory) {
                        /** @var $goodscategory Categories */
                        if($searchForm) {
                            $searchForm->cat = $goodscategory->id;
                            $goodscategory->visibleProductsCount = Products::getByFilter($searchForm, Products::GET_SEARCH_RESULT_ONLY_COUNT);
                        } else {
                            $goodscategory->visibleProductsCount = $goodscategory->products_count;
                        }
                        $subcategory->visibleProductsCount += $goodscategory->visibleProductsCount;
                    }
                    $category->visibleProductsCount += $subcategory->visibleProductsCount;
                }
                $chapter->visibleProductsCount += $category->visibleProductsCount;
            }
        }
        return $isWhole ? $chapters : self::filterEmpty($chapters);
    }

    public function getManufacturers() {
        $companies = Companies::find()
            ->select(['companies.*', 'users.*'])
            ->innerJoin('products', 'products.manufacturer_id = companies.id')
            ->innerJoin('users', 'users.id = companies.user_id')
            ->where(['products.category_id' => $this->id, 'products.is_active' => 1])
            ->orderBy('users.name')
            ->all();
        $result = [];
        foreach ($companies as $company) {
            /** @var $company Companies */
            $result[$company->id] = $company->user->name;
        }
        return $result;
    }

    private static function filterEmpty($chapters) {
        $result = [];
        foreach($chapters as $chapterIndex => $chapter) {
            /** @var $chapter self */
            if(!$chapter->visibleProductsCount) {
                continue;
            }
            $chapterChildren = $chapter->children;
            $chapter->children = [];
            foreach($chapterChildren as &$category) {
                /** @var $category Categories */
                if(!$category->visibleProductsCount) {
                    continue;
                }
                $categoryChildren = $category->children;
                $category->children = [];
                foreach ($categoryChildren as &$subcategory) {
                    /** @var $subcategory Categories */
                    if(!$subcategory->visibleProductsCount) {
                        continue;
                    }
                    $subcategoryChildren = $subcategory->children;
                    $subcategory->children = [];
                    foreach ($subcategoryChildren as &$goodscategory) {
                        /** @var $goodscategory Categories */
                        if(!$goodscategory->visibleProductsCount) {
                            continue;
                        }
                        $subcategory->children[] = $goodscategory;
                    }
                    $category->children[] = $subcategory;
                }
                $chapter->children[] = $category;
            }
            $result[] = $chapter;
        }
        return $result;
    }
}
