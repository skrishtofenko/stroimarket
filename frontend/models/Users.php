<?php
namespace frontend\models;

use Yii;
use yii\web\Cookie;
use yii\web\IdentityInterface;
use yii\base\NotSupportedException;
use common\helpers\SMHelper;
use common\components\FlashMessages;
use common\components\ImageManager\ImageManager;
use frontend\models\forms\RegistrationStep2Form;
use frontend\models\forms\LoginForm;
use frontend\models\forms\PasswordResetRequestForm;
use frontend\models\forms\PasswordResetForm;
use frontend\models\forms\CompanyInfoForm;
use frontend\models\forms\RequisitesInfoForm;
use frontend\models\forms\AccountSettingsForm;

class Users extends \common\models\extended\Users implements IdentityInterface {
    const MY_CITY_COOKIE_NAME = 'myCity';
    const DEFAULT_USER_CITY = 23541; // Москва

    const MY_LANGUAGE_COOKIE_NAME = 'myLanguage';
    const DEFAULT_USER_LANGUAGE = 'ru';
    const USER_LANGUAGES = [
        'ru' => [
            'locale' => 'ru-RU',
            'shortName' => 'ru',
            'selfName' => 'РУ',
        ],
        'en' => [
            'locale' => 'en-US',
            'shortName' => 'en',
            'selfName' => 'EN',
        ],
    ];

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord) {
                $this->status = self::STATUS_NEW;
                $this->is_request_completed = 0;
                $this->registration_token = Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    public static function findIdentity($id) {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByEmail($email) {
        return static::findOne(['email' => $email]);
    }

    public function getId() {
        return $this->getPrimaryKey();
    }

    public function getAuthKey() {
        return '';
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password) {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function getMainCompany() {
        return $this->hasOne(Companies::className(), ['user_id' => 'id'])->andWhere(['is_main' => true]);
    }

    public function getMainRequisites() {
        return $this->hasOne(Requisites::className(), ['user_id' => 'id'])->andWhere(['is_default' => true]);
    }

    public function getNotices() {
        return $this->hasOne(Notices::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    public static function signUp(RegistrationStep2Form $registrationStep2Form) {
        $user = new self;
        $user->email = $registrationStep2Form->email;
        $user->phone = $registrationStep2Form->phoneCode . $registrationStep2Form->phoneNumber;
        $user->fio = $registrationStep2Form->fio;
        $user->ownership_type_id
            = $registrationStep2Form->type != Companies::TYPE_PICKER || $registrationStep2Form->isLegalEntity
            ? $registrationStep2Form->ownershipTypeId
            : null;
        $user->legal_name = $registrationStep2Form->legalEntityName;
        $user->role = self::ROLE_COMPANY;

        $company = new Companies();
        $company->is_manufacturer = $registrationStep2Form->isManufacturer;
        $company->type = $registrationStep2Form->type;
        $company->is_main = 1;

        $requisites = new Requisites();
        $requisites->is_default = 1;

        $notices = new Notices();
        $rates = new Rates();

        $transaction = Yii::$app->db->beginTransaction();
        if($user->save()) {
            $company->user_id = $user->id;
            $requisites->user_id = $user->id;
            $notices->user_id = $user->id;
            if($company->save() && $requisites->save() && $notices->save()) {
                $rates->company_id = $company->id;
                if($rates->save()) {
                    $transaction->commit();
                    $user->email('registered', $user->email, ['user' => $user]);
                    Admins::email('newUserRegistered', Yii::$app->params['adminEmail'], ['user' => $user]);
                    return $user->registration_token;
                }
            }
        }
        $transaction->rollBack();
        return false;
    }

    public static function login(LoginForm $loginForm) {
        $user = self::findByEmail($loginForm->email);
        if(Yii::$app->user->login($user, $loginForm->rememberMe ? 3600 * 24 * 7 : 0)) {
            if($loginForm->isRegistrationFirstLogin) {
                $user->registration_token = null;
                $user->legal_registered_at = null;
                $user->status = self::STATUS_CONFIRMED;
                $user->save();
            }
            return true;
        }
        return false;
    }

    public static function requestPasswordReset(PasswordResetRequestForm $form) {
        if(!$user = self::findByEmail($form->email)) {
            return false;
        }

        if(!self::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if(!$user->save()) {
                return false;
            }
        }

        return $user->email('passwordResetRequest', $user->email, ['user' => $user]);
    }

    public static function isPasswordResetTokenValid($token) {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function generatePasswordResetToken() {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public static function findByPasswordResetToken($token) {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::find()
            ->where(['password_reset_token' => $token])
            ->andWhere(['in', 'status', self::ACTIVE_STATUSES])
            ->one();
    }

    public static function passwordReset(PasswordResetForm $form) {
        /** @var $user self*/
        $user = self::findByPasswordResetToken($form->token);
        $user->setPassword($form->password);
        $user->removePasswordResetToken();
        return ($user->save() && Yii::$app->user->login($user));
    }

    public function setPassword($password) {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function removePasswordResetToken() {
        $this->password_reset_token = null;
    }

    public function getCurrentCompany() {
        $currentCompany = $this->mainCompany;
        if($companyId = Yii::$app->session->get('currentCompany', false)) {
            if($sessionCompany = Companies::findOne($companyId)) {
                if($sessionCompany->user_id == $this->id) {
                    $currentCompany = $sessionCompany;
                }
            }
        }
        return $currentCompany;
    }

    public function checkRegistrationComplete() {
        if($this->status != self::STATUS_REQUESTED) {
            if(!empty($this->checking_file) && $this->registrationBill) {
                $this->updateAttributes(['status' => self::STATUS_REQUESTED]);
                Admins::email('userConfirmRequested', Yii::$app->params['adminEmail'], ['user' => $this]);
            }
        }
    }

    public function getOrCreateRegistrationBill() {
        if(!$bill = $this->registrationBill) {
            $bill = new Bills();
            $bill->type = Bills::TYPE_REGISTRATION;
            $bill->requisite_id = $this->mainRequisites->id;
            $bill->cost = Yii::$app->params['globalSettings']['regServCost'];
            $bill->save();
            $this->checkRegistrationComplete();
        }
        return $bill;
    }

    public function updateCompanyInfo(CompanyInfoForm $companyInfoForm, $isOnReg = false, Companies $company = null) {
        if(!$company) {
            $company = $this->mainCompany;
        }
        if(!$companyInfoForm->isOnSettings) {
            $company->city_id = $companyInfoForm->city_id;
            $company->street = $companyInfoForm->street;
            $company->house = $companyInfoForm->house;
            $company->room_type_id = $companyInfoForm->room_type_id;
            $company->room_number = $companyInfoForm->room_number;
            $company->im_url = $companyInfoForm->im_url;
        }
        if($company->type == Companies::TYPE_STORE) {
            $company->is_imonly = $companyInfoForm->is_imonly;
            $company->is_delivery = $companyInfoForm->is_delivery;
            $company->delivery_city_id = $companyInfoForm->delivery_city_id;
            $company->delivery_type = $companyInfoForm->delivery_type;
        }
        $company->phones_array = [$companyInfoForm->phone];
        $company->emails_array = [$companyInfoForm->email];
        foreach (SMHelper::$scheduleAttributes as $attribute) {
            $company->$attribute = $companyInfoForm->$attribute;
        }

        $this->fio = $companyInfoForm->fio;
        $this->name = $companyInfoForm->name;
        $this->description = $companyInfoForm->description;
        $this->url = $companyInfoForm->url;
        if($companyInfoForm->isOnSettings && $this->status != self::STATUS_ACTIVE) {
            $this->legal_name = $companyInfoForm->legal_name;
        }
        if($isOnReg) {
            $this->last_completed_registration_step = $company->type == Companies::TYPE_PICKER && !$this->ownership_type_id ? self::COMPLETED_REG_STEP : self::FILLED_COMPANY_REG_STEP;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if($this->save() && $company->save()) {
            ImageManager::upload($this, 'logo');
            $transaction->commit();
            return true;
        }
        $transaction->rollBack();
        return false;
    }

    public function addStore(CompanyInfoForm $companyInfoForm, Companies $company = null) {
        if(!$company) {
            $company = new Companies();
        }
        $company->type = Companies::TYPE_STORE;
        $company->user_id = $this->id;
        $company->is_main = 0;
        $company->city_id = $companyInfoForm->city_id;
        $company->street = $companyInfoForm->street;
        $company->house = $companyInfoForm->house;
        $company->room_type_id = $companyInfoForm->room_type_id;
        $company->room_number = $companyInfoForm->room_number;
        $company->im_url = $companyInfoForm->im_url;
        $company->is_imonly = $companyInfoForm->is_imonly;
        $company->is_delivery = $companyInfoForm->is_delivery;
        $company->delivery_city_id = $companyInfoForm->delivery_city_id;
        $company->delivery_type = $companyInfoForm->delivery_type;
        $company->phones_array = [$companyInfoForm->phone];
        $company->emails_array = [$companyInfoForm->email];
        foreach (SMHelper::$scheduleAttributes as $attribute) {
            $company->$attribute = $companyInfoForm->$attribute;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if($company->save()) {
            $rates = new Rates();
            $rates->company_id = $company->id;
            if($rates->save()) {
                $transaction->commit();
                return $company->id;
            }
        }
        $transaction->rollBack();
        return false;
    }

    public function updateRequisitesInfo(RequisitesInfoForm $requisitesInfoForm, $isOnReg = false) {
        $requisites = $this->mainRequisites;
        if($this->status != self::STATUS_ACTIVE) {
            $this->legal_name = $requisitesInfoForm->legal_name;
            $this->inn = $requisitesInfoForm->inn;
            $this->ogrn = $requisitesInfoForm->ogrn;
            $this->kpp = $requisitesInfoForm->kpp;
            $this->okpo = $requisitesInfoForm->okpo;
        }

        $requisites->account = $requisitesInfoForm->account;
        $requisites->bik = $requisitesInfoForm->bik;
        $requisites->bank = $requisitesInfoForm->bank;
        $requisites->corr_account = $requisitesInfoForm->corr_account;
        $requisites->legal_address = $requisitesInfoForm->legal_address;
        $requisites->post_address = $requisitesInfoForm->post_address;
        if($isOnReg) {
            $this->last_completed_registration_step = self::COMPLETED_REG_STEP;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if($this->save() && $requisites->save()) {
            $transaction->commit();
            return true;
        }
        $transaction->rollBack();
        return false;
    }

    public function updateAccount(AccountSettingsForm $accountSettingsForm) {
        $this->fio = $accountSettingsForm->fio;
        $this->phone = $accountSettingsForm->phone;
        if($this->save()) {
            if(ImageManager::upload($this, 'checking_file')) {
                Yii::$app->session->setFlash(FlashMessages::FLASH_SUCCESS, 'Документ загружен, ожидайте проверки администратора');
            }
            $this->checkRegistrationComplete();
            return true;
        }
        return false;
    }

    public static function getCurrentUserCity() {
        $cookieCityId = Yii::$app->request->cookies->getValue(self::MY_CITY_COOKIE_NAME, false);
        if($cookieCityId && Cities::findOne($cookieCityId)) {
            return $cookieCityId;
        }
        return false;
    }

    public static function setCurrentUserCity($cityId = false) {
        if(!$cityId) {
            $cityId = self::getCurrentCityByIp();
        }
        Yii::$app->response->cookies->add(new Cookie([
            'name' => self::MY_CITY_COOKIE_NAME,
            'value' => $cityId,
            'expire' => time() + 30 * 24 * 60 * 60, // 30 days
        ]));
        return $cityId;
    }

    private static function getCurrentCityByIp() {
        return self::DEFAULT_USER_CITY; // todo: drop this line when will connected other cities
        $userIp = $_SERVER['REMOTE_ADDR'];
        if($userIp) {
            $int = sprintf("%u", ip2long($userIp));
            $ipRange = IpRanges::find()
                ->where(['<=', 'begin_ip', $int])
                ->andWhere(['>=', 'end_ip', $int])
                ->orderBy(['begin_ip' => SORT_DESC])
                ->limit(1)
                ->one();
            if($ipRange && $city = Cities::findOne($ipRange->city_id)) {
                return $city->id;
            }
        }
        return self::DEFAULT_USER_CITY;
    }

    public static function getCurrentUserLanguage() {
        $lang = Yii::$app->request->cookies->getValue(self::MY_LANGUAGE_COOKIE_NAME, false);
        if(!$lang || !isset(self::USER_LANGUAGES[$lang])) {
            return self::setCurrentUserLanguage();
        } else {
            return self::USER_LANGUAGES[$lang]['locale'];
        }
    }

    public static function setCurrentUserLanguage($lang = false) {
        if(!$lang || !isset(self::USER_LANGUAGES[$lang])) {
            $lang = self::DEFAULT_USER_LANGUAGE;
        }
        Yii::$app->response->cookies->add(new Cookie([
            'name' => self::MY_LANGUAGE_COOKIE_NAME,
            'value' => $lang,
            'expire' => time() + 30 * 24 * 60 * 60, // 30 days
        ]));
        return self::USER_LANGUAGES[$lang]['locale'];
    }
}
