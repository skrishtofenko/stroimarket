<?php
namespace frontend\models;

use Yii;
use frontend\models\forms\PS;
use frontend\models\forms\CS;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * @property array $categoriesTreeWithCounts
 * @property CompaniesProducts[] $promoProducts
 * @property Categories[] $manufacturingCategories
 * @property Categories[] $manufacturingChapters
 */
class Companies extends \common\models\extended\Companies {
    public $totalRate;

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord) {
                $this->is_paid = 0;
                $this->status = 0;
                $this->paid_till = null;
            }
            return true;
        }
        return false;
    }

    public function getCategoriesTreeWithCounts(PS $searchForm) {
        $chapters = Categories::getCategoriesList();
        foreach($chapters as $chapterIndex => &$chapter) {
            /** @var $chapter Categories */
            $chapter->fillChildren();
            foreach($chapter->children as $categoryIndex => &$category) {
                /** @var $category Categories */
                $category->fillChildren();
                foreach($category->children as $subcategoryIndex => &$subcategory) {
                    /** @var $subcategory Categories */
                    $subcategory->fillChildren();
                    foreach($subcategory->children as $goodscategoryIndex => &$goodscategory) {
                        /** @var $goodscategory Categories */
                        $searchForm->companyId = $this->id;
                        $searchForm->cat = $goodscategory->id;

                        if($productsCount = Products::getByFilter($searchForm, Products::GET_SEARCH_RESULT_ONLY_COUNT)) {
                            $goodscategory->visibleProductsCount = $productsCount;
                        } else {
                            unset($subcategory->children[$goodscategoryIndex]);
                        }
                    }
                    if(empty($subcategory->children)) {
                        unset($category->children[$subcategoryIndex]);
                    }
                }
                if(empty($category->children)) {
                    unset($chapter->children[$categoryIndex]);
                }
            }
            if(empty($chapter->children)) {
                unset($chapters[$chapterIndex]);
            }
        }
        return $chapters;
    }

    public function findProducts($categoryId = null, $onlyCount = false, $sort = Products::DEFAULT_EDIT_SORT) {
        $query = CompaniesProducts::find()
            ->select([
                'companies_products.*',
                'COALESCE(companies_products.vendor_code, "яяяяяяяяя") vendor_code_for_sort',
                'COALESCE(products.id, 4000000000) product_id_for_sort',
                'COALESCE(categories.name, "яяяяяяяяя") category_name_for_sort',
                'COALESCE(products.name, companies_products.name, "яяяяяяяяя") product_name_for_sort',
            ])
            ->active(true)
            ->leftJoin('categories', 'categories.id = products.category_id')
            ->andWhere(['company_id' => $this->id]);
        if($categoryId) {
            $query->andWhere(['category_id' => $categoryId]);
        }
        if($onlyCount) {
            return $query->count();
        }
        return $query
            ->orderBy(['is_manufactured_by' => SORT_DESC, Products::getRealEditSort($sort) => SORT_ASC])
            ->all();
    }

    public function findProductsByIds($productIds) {
        if(empty($productIds)) {
            return [];
        }
        $query = CompaniesProducts::find()
            ->select([
                'companies_products.*',
            ])
            ->active(true)
            ->andWhere(['company_id' => $this->id, 'product_id' => $productIds]);
        return $query
            ->orderBy(new Expression('FIELD(product_id,' . implode(',', $productIds) . ')'))
            ->all();
    }

    public function getPromoProducts($needleCount = 3) {
        $promoProducts = CompaniesProducts::getRandom($this->id, $needleCount);
        if(count($promoProducts) < $needleCount) {
            $yetNeedle = $needleCount - count($promoProducts);
            $promoProducts = array_merge(
                $promoProducts,
                CompaniesProducts::getRandom($this->id, $yetNeedle, false)
            );
        }
        return $promoProducts;
    }

    public function getAssortmentFile($isGetTemplate = true) {
        $assortmentFields = $this->autoAssortmentFields();
        $data = [ArrayHelper::getColumn($assortmentFields, 'comment')];
        if(!$isGetTemplate) {
            $data = array_merge($data, $this->getAssortmentArray());
        }

        $workBook = Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Assortment' => [
                    'data' => $data,
                    'titles' => array_keys($assortmentFields),
                    'styles' => [
                        '1' => [
                            'font' => ['bold' => true, 'size' => 12],
                            'alignment' => ['horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER],
                        ],
                        '2' => [
                            'font' => ['italic' => true, 'size' => 10],
                            'alignment' => [
                                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                                'vertical' => \PHPExcel_Style_Alignment::VERTICAL_TOP,
                            ],
                        ],
                    ],
                ],
            ]
        ]);
        $workBook->getWorkbook()->getActiveSheet()->getStyle('2')->getAlignment()->setWrapText(true);
        $workBook->getWorkbook()->getActiveSheet()->freezePane('A3');
        $i = 0;
        $abc = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        foreach($assortmentFields as $field) {
            $workBook->getWorkbook()->getActiveSheet()->getColumnDimension($abc[$i])->setWidth($field['columnWidth']);
            if(isset($field['format'])) {
                $workBook->getWorkbook()->getActiveSheet()->getStyle($abc[$i])->getNumberFormat()
                    ->setFormatCode($field['format']);
            }
            $i++;
        }
        return $workBook;
    }

    private function getAssortmentArray() {
        if(!in_array($this->type, [self::TYPE_STORE, self::TYPE_PROVIDER])) {
            return [];
        }
        $data = [];
        $fields = $this->autoAssortmentFields();
        foreach ($this->findProducts() as $product) {
            $item = [];
            /** @var $product CompaniesProducts */
            foreach($fields as $field) {
                $field = $field['field'];
                $field = explode('.', $field);
                if(count($field) > 1) {
                    $item[] = isset($product->{$field[0]}) && $product->{$field[0]}
                        ? $product->{$field[0]}->{$field[1]}
                        : (isset($field[2]) && isset($product->{$field[2]}) ? $product->{$field[2]} : '');
                } else {
                    $item[] = isset($product->{$field[0]}) ? $product->{$field[0]} : '';
                }
            }
            $data[] = $item;
        }
        return $data;
    }

    public static function getByFilter(CS $searchForm, $sortByRecommended = false) {
        $sort = $sortByRecommended ? ['users.is_recommended' => SORT_DESC] : [];

        $companies = Companies::find()
            ->select([
                'companies.*',
                self::totalRateField(),
            ])
            ->innerJoin('rates', 'companies.id = rates.company_id')
            ->active()
            ->orderBy($sort + $searchForm->getSort());

        if(!empty($searchForm->productId)) {
            /** @var Users $user */
            $user = Yii::$app->user->identity;
            $userId = $user ? $user->id : null;
            $companies->innerJoin(
                'companies_products',
                "
                    companies_products.company_id = companies.id
                    AND companies_products.product_id = {$searchForm->productId}
                ")
                ->andWhere(['companies_products.status' => CompaniesProducts::STATUS_ACTIVE])
                ->orWhere(['companies.user_id' => $userId])
                ->andWhere(['!=', 'companies_products.status', CompaniesProducts::STATUS_DELETED]);
        }

        if(!empty($searchForm->isRecommended) && $searchForm->isRecommended) {
            $companies->andWhere(['users.is_recommended' => 1]);
        }

        if(!empty($searchForm->q)) {
            $companies->andWhere(['like', 'users.legal_name', $searchForm->q])
                ->orWhere(['like', 'users.name', $searchForm->q]);
        }

        if(!empty($searchForm->m) && $searchForm->m) {
            $companies->andWhere(['companies.is_manufacturer' => 1]);
        }

        if(!empty($searchForm->c)) {
            $companies->andWhere(['companies.city_id' => $searchForm->c]);
        }

        $companies->andWhere(['companies.type' => $searchForm->type]);

        if($searchForm->type == Companies::TYPE_STORE) {
            if($searchForm->id) {
                $companies->andWhere(['companies.is_delivery' => 1]);
            }
            if($searchForm->mc) {
                if($userCity = Users::getCurrentUserCity()) {
                    $companies->andWhere(['companies.city_id' => $userCity]);
                }
            }
        }

        if(!empty($searchForm->productId)) {
            $companies->orWhere(['companies_products.is_manufactured_by' => 1]);
        }

        if(!empty($searchForm->cat)) {
            $companies->innerJoin('products', "
                products.manufacturer_id = companies.id
                AND products.is_active = 1
            ")
                ->innerJoin('categories products_categories', "products_categories.id = products.category_id")
                ->innerJoin('categories subcategories', "subcategories.id = products_categories.parent_id")
                ->innerJoin('categories', "
                    subcategories.parent_id = categories.id
                    AND categories.id = {$searchForm->cat}
                ");
        }

        $searchForm->found = $companies->count();

        if($limit = $searchForm->getOnPage()) {
            $companies->limit($limit);
            $page = !empty($searchForm->page) ? $searchForm->page : 1;
            $page = ceil(min($page, $searchForm->found / $limit));
            $companies->offset(($page - 1) * $limit);
        }

        return $companies->all();
    }

    public static function getHintsByName($query) {
        $companies = self::find()
            ->active()
            ->select(['companies.id', 'companies.user_id'])
            ->andWhere(['like', 'users.legal_name', $query])
            ->orWhere(['like', 'users.name', $query])
            ->all();

        $result = [];
        foreach ($companies as $company) {
            /** @var $company self */
            $result[] = [
                'id' => $company->id,
                'name' => $company->user->legalEntityVcard,
            ];
        }
        return $result;
    }

    public static function totalRateField() {
        return '(rates.approve_rate + rates.paid_rate + rates.sm_reg_rate + rates.le_reg_rate) totalRate';
    }

    public static function findOneIfExists($id, $currentUserRole) {
        $company = self::find()->active()->andWhere(['companies.id' => $id]);
        if($currentUserRole == Users::ROLE_BUYER) {
            $company->andWhere(['type' => self::TYPE_STORE]);
        }
        return $company->one();
    }

    public function getOtherUserCompanies($currentUserRole, $withCurrent = true) {
        $companies = self::find()
            ->active()
            ->andWhere(['user_id' => $this->user_id]);
        if(!$withCurrent) {
            $companies->andWhere(['!=', 'companies.id', $this->id]);
        }
        if($currentUserRole == Users::ROLE_BUYER) {
            $companies->andWhere(['type' => self::TYPE_STORE]);
        }
        return $companies->all();
    }

    public function getManufacturingCategories() {
        return Categories::find()
            ->select(['categories.*', 'COUNT(products.id) `visibleProductsCount`'])
            ->innerJoin('products', "
                products.category_id = categories.id
                AND products.manufacturer_id = {$this->id}
                AND products.is_active = 1
            ")
            ->groupBy(['categories.id'])
            ->where(['categories.level' => Categories::LEVEL_GOODSCATEGORY])
            ->all();
    }

    public function getManufacturingChapters() {
        return Categories::find()
            ->select(['categories.*', 'COUNT(products.id) `visibleProductsCount`'])
            ->innerJoin('categories subcategories', "subcategories.parent_id = categories.id")
            ->innerJoin('categories products_categories', "products_categories.parent_id = subcategories.id")
            ->innerJoin('products', " 
                products.category_id = products_categories.id 
                AND products.manufacturer_id = {$this->id} 
                AND products.is_active = 1 
            ")
            ->groupBy(['categories.id'])
            ->where(['categories.level' => Categories::LEVEL_CATEGORY])
            ->all();
    }

    public function getRandomCategoryProduct($categoryId) {
        return Products::find()
            ->innerJoin('categories', "categories.id = products.category_id AND categories.id = {$categoryId}")
            ->where(['manufacturer_id' => $this->id])
            ->orderBy(new Expression('RAND()'))
            ->limit(1)
            ->one();
    }
}
