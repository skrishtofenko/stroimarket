<?php
namespace frontend\models;

use Yii;
use frontend\models\forms\CatalogUploadForm;

class Catalogs extends \common\models\extended\Catalogs {
    public static function upload(CatalogUploadForm $catalogUploadForm) {
        $newFileName = self::generateFileName($catalogUploadForm->catalogFile);
        $newFilePath = self::touchPath($newFileName);
        $catalogUploadForm->catalogFile->saveAs($newFilePath . $newFileName);

        /** @var Users $user */
        /** @var Companies $company */
        $user = Yii::$app->user->identity;
        $company = $user->getCurrentCompany();

        $catalog = new self;
        $catalog->company_id = $company->id;
        $catalog->file_name = $newFileName;
        $catalog->upload_name = mb_substr($catalogUploadForm->catalogFile->name, 0, 255, 'utf-8');
        return $catalog->save();
    }
}
