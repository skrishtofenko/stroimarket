<?php
return [
    'main_title' => 'Stroymarket',
    'for_manufacturers' => 'For manufacturers',
    'for_providers' => 'For wholesale companies',
    'for_stores' => 'For retail stores',
    'for_pickers' => 'For wholesale buyers',
    'logout' => 'Logout',
    'registration' => 'Registration',
    'login' => 'Log In',
    'administration_phone' => 'Call us',
    'support' => 'Support',
    'faq' => 'FAQ',
    'privacy_policy' => 'Terms & Conditions',
    'search_products' => 'Search products',
    'close' => 'Close',
    'open' => 'Open',
    'info' => 'Info',
    'registration_info' => '
        At the moment, "Stroymarket" started working in test mode.
        To register your company,
        contact the "Stroymarket" administration by phone {phone} or email {email}
    ',
    'free_registration' => 'Register your company',
    'more_info' => 'Learn more',
    'profit_title' => 'What does “Stroymarket” offer you?',
    'user_placement_info' => 'At the moment, "Stroymarket" avilable only for Moscow.',

    'picker_title' => 'Russia’s united building and finishing materials online marketplace',
    'picker_subtitle' => 'Reduce costs, make purchases at the best prices, get discounts from suppliers from all over Russia',
    'picker_description' => 'United database of wholesale companies and manufacturers of building and finishing materials',
    'picker_registration_info' => '
        By registering on “Stroymarket”, you will gain access to a united national database of building and finishing materials manufacturers and wholesale companies. So, when choosing a particular product, you will see a complete list of suppliers of that product. Besides, we are constantly working on securing new discounts for our users. This would mean that by being a “Stroymarket” user, you get exclusive discounts from suppliers and manufacturers of construction and finishing materials throughout Russia.
    ',
    'picker_registration_info_1' => 'Search for new partners',
    'picker_registration_info_2' => 'Find the best supply prices',
    'picker_registration_info_3' => 'Get exclusive discounts from suppliers and manufacturers',
    'picker_purchases' => 'Make purchases at the best prices',
    'picker_purchases_info' => '
        An internal marketplace – a list of purchases made by wholesale buyers. Just announce that you want to make a purchase, and suppliers and manufacturers will send you their price and delivery quotations.
    ',
    'picker_profit_subtitle' => 'Reduce costs, make purchases at the best prices, get discounts from suppliers from all over Russia',
    'picker_profit_1' => 'Find new suppliers and agree on cooperation',
    'picker_profit_2' => 'Reduce costs by making purchases at the best prices',
    'picker_profit_3' => 'Get permanent discounts from suppliers throughout Russia',

    'provider_title' => 'Russia’s united building and finishing materials online marketplace',
    'provider_subtitle' => 'Be presented in a united database of suppliers and manufacturers, participate in procurements, boost your sales, find new customers',
    'provider_description' => 'A united database of building and finishing materials manufacturers, wholesale companies and retail stores',
    'provider_registration_info' => '
        After registering your company on “Stroymarket”, you will be included in a united database of building and finishing materials manufacturers and wholesale companies operating in Russia. This database is available to wholesale buyers. In addition, you will gain access to a united national database of building and finishing materials retail stores. So, whenever a wholesale buyer is choosing a particular product from “Stroymarket”, he will see your company in the list of suppliers of this product. In turn, when you are choosing a particular product, you will see the full list of stores offering this product.
    ',
    'provider_registration_info_1' => 'Be presented in a united database of building and finishing materials suppliers and manufacturers',
    'provider_registration_info_2' => 'Get access to the national database of building and finishing materials stores. In the database, you can sort by location and availability of a specific product',
    'provider_purchases' => 'Participate in procurements',
    'provider_purchases_info' => '
   An internal marketplace – a list of purchases made by wholesale buyers. When a wholesale buyer in the marketplace (a real estate developer, a retail store, etc.) requires supply of certain goods, he declares a purchase, and if this purchase is suitable for your product group, you will receive a notification by e-mail and will be able to contact the buyer directly or send your price and delivery quotations to the buyer through “Stroymarket”.
    ',
    'provider_profit_subtitle' => 'Be in a united database of suppliers and manufacturers, participate in procurements, boost your sales, and find new customers',
    'provider_profit_1' => 'Boost your sales',
    'provider_profit_2' => 'Increase your profit',
    'provider_profit_3' => 'Find new customers',

    'manufacturer_title' => 'Russia’s united building and finishing materials online marketplace',
    'manufacturer_subtitle' => 'Promote your products, be presented in a united manufacturer database, expand your geographic sales territories, boost your sales, and find new customers',
    'manufacturer_description' => 'A united database of building and finishing materials manufacturers, wholesale companies and stores',
    'manufacturer_registration_info' => '
        After registering your company on “Stroymarket”, your products will be included in a united catalog of building and finishing materials sold in Russia. Your company will also be included in a united database of building and finishing materials manufacturers operating in Russia. Wholesale buyers (wholesale companies, retail stores, etc.) can view these databases. In addition, you will gain access to a united national database of wholesale companies and stores in this sector. Thus, when a wholesale buyer wants to choose your product in the “Stroymarket” catalog, he will see you as a producer in the list of suppliers of this product. In turn, when you are choosing a particular product, you will see a complete list of wholesale companies and stores selling that product.
    ',
    'manufacturer_registration_info_1' => 'Be presented in a united database of building and finishing materials manufacturers. Wholesale buyers throughout Russia can access this database',
    'manufacturer_registration_info_2' => 'Get access to the national database of building and finishing materials wholesale companies and retail stores. In this database, you can sort clients by location and availability of a specific product',
    'manufacturer_purchases' => 'Participate in procurements',
    'manufacturer_purchases_info' => '
        An internal marketplace – a list of purchases made by wholesale buyers. When a wholesale buyer in the marketplace (a real estate developer, a retail store, etc.) requires supply of certain goods, he declares a purchase, and if this purchase is suitable for your product group, you will receive a notification by e-mail and will be able to contact the buyer directly or send your price and delivery quotations to the buyer through “Stroymarket”.
    ',
    'manufacturer_profit_subtitle' => 'Promote your products, be presented in a united manufacturer database, expand your geographic sales territories, boost your sales, and find new customers',
    'manufacturer_profit_1' => 'Expand your geographic sales territories',
    'manufacturer_profit_2' => 'Boost your sales',
    'manufacturer_profit_3' => 'Find new customers',

    'store_title' => 'Russia’s united building and finishing materials online marketplace',
    'store_subtitle' => 'Be presented in a united database of retail stores, boost your sales, reduce costs, make purchases at the best prices, get discounts from suppliers from all over Russia',
    'store_description' => 'A united catalog for retail buyers of all construction and finishing materials in Russia',
    'store_registration_info' => '
        By registering on “Stroymarket”, you will be added to a united national database of building and finishing materials stores. So, whenever buyers want to search or select a specific product in the “Stroymarket” catalog, they will see your store in the list of sellers of this product. And if you have delivery, they can make an order directly through “Stroymarket”, save the product or visit your online store.
    ',
    'store_registration_info_1' => 'Be presented as a store in a united database of construction and finishing materials',
    'store_registration_info_2' => 'New customers and strong boost in sales',
    'store_registration_info_3' => 'Online orders',
    'store_providers' => 'A united database of building and finishing materials manufacturers and wholesale companies',
    'store_providers_info' => '
       By registering on “Stroymarket”, you will gain access to a united national database of building and finishing materials manufacturers and wholesale companies. So, when choosing a particular product, you will see a complete list of suppliers of that product. Besides, we are constantly working on securing new discounts for our users. This would mean that by being a “Stroymarket” user, you get exclusive discounts from suppliers and manufacturers of construction and finishing materials throughout Russia.
    ',
    'store_providers_info_1' => 'Search for new partners',
    'store_providers_info_2' => 'An opportunity to find the best delivery prices',
    'store_providers_info_3' => 'Exclusive discounts from suppliers and manufacturers',
    'store_purchases' => 'Make purchases at the best prices',
    'store_purchases_info' => '
    An internal marketplace – a list of purchases made by wholesale buyers. Just announce that you want to make a purchase, and suppliers and manufacturers will send you their price and delivery quotations.
    ',
    'store_profit_subtitle' => 'Be presented in a united database of retail stores, boost your sales, reduce costs, make purchases at the best prices, get discounts from suppliers from all over Russia',
    'store_profit_1' => 'Expand your sales by registering your company in a national catalog',
    'store_profit_2' => 'Reduce costs by making purchases at the best prices',
    'store_profit_3' => 'Get permanent discounts from suppliers throughout Russia',
];