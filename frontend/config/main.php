<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'language' => 'ru-RU',
    'name' => 'Stroimarket',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'frontend\models\Users',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'loginUrl' => '\login'
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/<action:(agreement)>' => 'page/<action>',
                '/<action:(login|password-reset|logout)>' => 'profile/<action>',
                '/business' => '/site/business',
                '/business/<type:(provider|store|picker|manufacturer)>' => '/site/business',
                '/assortment/<categoryId:\d+>' => '/assortment/category',
                '/assortment/edit/<categoryId:\d+>' => '/assortment/edit',
                '/catalog/<categoryId:\d+>' => '/catalog/category',
                '/catalog/product/<productId:\d+>' => '/catalog/product',
                '/catalog/<type:(providers|stores)>' => '/catalog/companies',
                '/catalog/company/<companyId:\d+>' => '/catalog/company',
                '/catalog/company/<companyId:\d+>/assortment' => '/catalog/company-assortment',
                '/catalog/company/<companyId:\d+>/assortment/<categoryId:\d+>' => '/catalog/company-assortment',
                '/catalog/manufacturers/<id:\d+>' => '/catalog/manufacturer',
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/messages',
                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => 'app.php',
                    ],
                ],
                'landing*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'sys',
                    'fileMap' => [
                        'landing' => 'landing.php',
                    ],
                ],
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
        ],
    ],
    'params' => $params,
];
