<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $passwordResetForm \frontend\models\forms\PasswordResetForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление доступа';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="grid__column_2-5 grid__column_center">
        <section class="login">
            <h2 class="h2">
                Новый пароль
            </h2>
            <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form login__form']]);?>
                <?=$form->errorSummary($passwordResetForm, ['header' => false])?>
                <?=$form->field($passwordResetForm, 'password')
                    ->passwordInput(['class' => 'input', 'placeholder' => 'Введите новый пароль'])?>
                <?=$form->field($passwordResetForm, 'passwordRepeat')
                    ->passwordInput(['class' => 'input', 'placeholder' => 'Повторите новый пароль'])?>
                <?=Html::submitButton('Войти в личный кабинет', ['class' => 'button form__button'])?>
            <?php ActiveForm::end();?>
        </section>
    </div>
</div>