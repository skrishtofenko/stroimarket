<?php
use common\components\ImageManager\ImageManager;
use common\helpers\SKHelper;
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\Rates;
use frontend\models\Requisites;
use frontend\models\Deliveries;
use frontend\assets\pages\MyProfileAsset;
use frontend\assets\pages\CatalogAsset;

/** @var Companies $company */
/** @var Companies[] $companies */
/** @var string $page */
$page = isset($page) ? $page : 'dashboard';
/** @var Users $owner */
$owner = $company->user;
/** @var Rates $rates */
$rates = $company->rates;
/** @var Requisites $requisites */
$requisites = $owner->mainRequisites;
/** @var Users $currentUser */
$currentUser = Yii::$app->user->identity;
/** @var string $companyAddress */
$companyAddress = $company->addressAsText;
$this->title = $page == 'dashboard' ? 'Рабочий стол' : 'Каталог';
$companies = $company->getOtherUserCompanies($this->params['currentUserRole'], false);
?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<div class="customers profile">
    <h2 class="h2 customers__h2">Профиль компании</h2>

    <div class="grid">
        <div class="grid__column_2-3">
            <a class="link profile__prev-page" href="javascript:history.back()">
                Назад
            </a>
            <article class="company-card">
                <section class="company-card__info">
                    <div class="company-card__logo">
                        <img src="<?=ImageManager::getUrl($owner, 'logo', 140, 140, ImageManager::RESIZE_ZOOM_IN)?>" alt="" width="140">
                    </div>
                    <div>
                        <div class="company-card__name">
                            <b><?=$owner->displayName?></b>
                            <?php /*<span class="rating" data-value="<?=$company->rateAsNum?>" data-max="3"><span></span></span>*/?>
                            <p><?=$company->typeAsText?></p>
                        </div>
                        <p class="company-card__data">
                            Название юр. лица: <?=$owner->legalEntityVcard?><br>
                            Адрес: <?=$companyAddress?><br>
                            <?php if(!empty($company->phones_array)):?>
                                Телефон: <?=SKHelper::formatPhone($company->phones_array[0])?><br>
                            <?php endif;?>
                            Веб–сайт: <?=($owner->url ?: '—')?><br>
                            <?php if(!empty($company->emails_array)):?>
                                Электронная почта: <?=$company->emails_array[0]?>
                            <?php endif;?>
                            <?php //Главное контактное лицо: Гурченко Мария Петровна?>
                        </p>
                    </div>
                </section>
                <section class="company-card__description">
                    <p><?=$owner->description?></p>
                </section>
            </article>
        </div>
        <div class="grid__column_1-3">
            <div class="profile__sidebar">
                <?php if(!empty($companies)):?>
                    <select class="text-select text-select_right profile__places" id="place">
                        <option value="" selected hidden>Другие магазины сети</option>
                        <?php foreach ($companies as $otherCompany):?>
                            <option value="<?=$otherCompany->id?>"><?=$otherCompany->user->legal_name?>, <?=$otherCompany->addressAsText?></option>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>


                <?php if(!$company->is_imonly):?>
                    <div class="operating-time-wrapper profile__operating-time">
                        <?=$this->render('../partials/companySchedule', [
                            'company' => $company,
                        ])?>
                    </div>
                <?php endif;?>

                <div class="profile__map">
                    <script type="text/javascript">
                        ymaps.ready(init);
                        var myGeocoder;
                        var myMap;

                        function init(){
                            myGeocoder = ymaps.geocode('<?=$companyAddress?>');
                            myGeocoder.then(
                                function (res) {
                                    var myCoord = res.geoObjects.get(0).geometry.getCoordinates();
                                    myMap = new ymaps.Map("map", {
                                        center: myCoord,
                                        zoom: 16,
                                        controls: []
                                    });
                                    var myPlacemark = new ymaps.Placemark(myCoord, {
                                        hintContent: '<?=$companyAddress?>',
                                        balloonContent: '<?=$companyAddress?>'
                                    });
                                    myMap.geoObjects.add(myPlacemark);
                                    myMap.controls.add('zoomControl');
                                    myMap.controls.add('fullscreenControl');
                                },
                                function (err) {}
                            );
                        }
                    </script>
                    <div id="map" style="width: 100%; height: 170px;"></div>
                </div>
            </div>
        </div>
    </div>

    <?php $promoProducts = $company->getPromoProducts(4)?>
    <?php if(!empty($promoProducts)):?>
        <article class="products">
            <div class="products__heading">
                <h2>Ассортимент</h2>
                <a class="link" href="/catalog/company/<?=$company->id?>/assortment">Показать всё</a>
            </div>
            <?php if($company->catalog_updated_at):?>
                <p class="products__update-date">
                    Обновлен <?=date('d.m.Y', strtotime($company->catalog_updated_at))?>
                </p>
            <?php endif;?>
            <div class="grid">
                <?php foreach($promoProducts as $promoProduct):?>
                    <div class="grid__column_1-4">
                        <div class="product-card">
                            <img src="<?=ImageManager::getUrl($promoProduct->product, 'image', 120, 120, ImageManager::RESIZE_ZOOM_IN)?>" alt="" width="120" height="120">
                            <h3 class="product-card__name"><?=$promoProduct->product->fullName?></h3>
                            <p class="product-card__category"><?=$promoProduct->product->category->name?></p>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </article>
    <?php endif;?>

    <?php $companyDeliveries = $company->deliveries?>
    <?php if(!empty($companyDeliveries)):?>
        <?php
        $mappedDeliveries = [];
        foreach($companyDeliveries as $delivery) {
            $mappedDeliveries[] = [
                'city' => $delivery->city->name,
                'type' => $delivery->type,
            ];
        }
        usort($mappedDeliveries, function($a, $b) {
            if($a['city'] < $b['city']) return -1;
            if($a['city'] > $b['city']) return 1;
            return 0;
        });
        $deliveries = [];
        foreach($mappedDeliveries as $delivery) {
            $letter = mb_substr($delivery['city'], 0, 1, 'utf-8');
            $deliveries[$letter][] = $delivery;
        }
        $columnSize = ceil(count($deliveries) / 3);
        ?>
        <section class="delivery">
            <h2 class="delivery__heading">Доставка</h2>
            <div class="delivery__container">
                <div class="delivery__count-info">
                    Осуществляется в <?=count($companyDeliveries)?> населённы<?=SKHelper::ending(count($companyDeliveries), ['й', 'х', 'х'])?> пункт<?=SKHelper::ending(count($companyDeliveries), ['', 'а', 'ов'])?>
                </div>
                <div class="delivery__main" id="cities">
                    <?php for($i = 0; $i < 3; $i++):?>
                        <div class="delivery__column">
                            <?php $column = array_slice($deliveries, $i * $columnSize, $columnSize)?>
                            <?php if(!empty($column)):?>
                                <?php foreach($column as $letter => $columnDeliveries):?>
                                    <div class="delivery__block">
                                        <span class="delivery__letter"><?=$letter?></span>
                                        <?php foreach($columnDeliveries as $delivery):?>
                                            <p class="delivery__item">
                                                <?=$delivery['city']?>
                                                <?php if(in_array($delivery['type'], [Deliveries::TYPE_DELIVERY, Deliveries::TYPE_BOTH])):?>
                                                    <span class="delivery__legend">Д</span>
                                                <?php endif;?>
                                                <?php if(in_array($delivery['type'], [Deliveries::TYPE_PICKUP, Deliveries::TYPE_BOTH])):?>
                                                    <span class="delivery__legend">П</span>
                                                <?php endif;?>
                                            </p>
                                        <?php endforeach;?>
                                    </div>
                                <?php endforeach;?>
                            <?php endif;?>
                        </div>
                    <?php endfor;?>

                    <div class="delivery__column delivery__column_legend">
                        <div class="delivery__block">
                            <p class="delivery__item">
                                <span class="delivery__legend">Д</span> — до города
                            </p>
                            <p class="delivery__item">
                                <span class="delivery__legend">П</span> — по городу
                            </p>
                        </div>
                    </div>
                </div>

                <button class="delivery__show-btn" type="button"
                        data-show-more="Показать" data-show-less="Свернуть"
                        onclick="showMore.toggle('#cities')">
                    Показать
                </button>
            </div>
        </section>
    <?php endif;?>

    <?php /*
    <section class="reviews">
        <h2 class="reviews__heading">Отзывы</h2>

        <div class="layout-view">
            <span class="layout-view__title">Сортировать:</span>
            <a class="layout-view__sort-link">по дате</a>
            <a class="layout-view__sort-link layout-view__sort-link_down">по оценке</a>
        </div>

        <div class="grid">
            <div class="grid__column-3-4">
                <div class="review">
                    <header class="review__header">
                        <div class="review__rating">
                            <span class="rating" data-value="5" data-max="5"><span></span></span>
                        </div>
                        <p class="review__order-number">Номер заказа: 999999999999</p>
                        <time class="review__date" datetime="2017-10-30">30 октября 2017</time>
                    </header>

                    <p class="review__author">Роман Аркадьевич</p>

                    <div class="review__block">
                        <b>Достоинства:</b>
                        <p>Быстрота обработки. Утром заказал, вечером этого же дня забрал.</p>
                    </div>
                    <div class="review__block">
                        <b>Недостатки:</b>
                        <p>Оплата только наличными.</p>
                    </div>
                    <div class="review__block">
                        <b>Комментарий:</b>
                        <p>Уже второй раз заказываю в этом магазине. Все в норме. Цена ниж чем в других магазинах, качество
                            товара тоже самое.</p>
                    </div>
                </div>

                <div class="review">
                    <header class="review__header">
                        <div class="review__rating">
                            <span class="rating" data-value="5" data-max="5"><span></span></span>
                        </div>
                        <p class="review__order-number">Номер заказа: 999999999999</p>
                        <time class="review__date" datetime="2017-10-30">30 октября 2017</time>
                    </header>

                    <p class="review__author">Роман Аркадьевич</p>

                    <div class="review__block">
                        <b>Достоинства:</b>
                        <p>Быстрота обработки. Утром заказал, вечером этого же дня забрал.</p>
                    </div>
                    <div class="review__block">
                        <b>Недостатки:</b>
                        <p>Оплата только наличными.</p>
                    </div>
                    <div class="review__block">
                        <b>Комментарий:</b>
                        <p>Уже второй раз заказываю в этом магазине. Все в норме. Цена ниж чем в других магазинах, качество
                            товара тоже самое.</p>
                    </div>
                </div>

                <section class="pagination reviews__pagination">
                    <a class="pagination__item">Предыдущие</a>
                    <a class="pagination__item" href="javascript:void(0)">Следующие</a>
                </section>
            </div>

            <div class="grid__column_1-4">
                <ul class="list-reset rating-stats">
                    <li>
                        <span class="rating" data-value="5" data-max="5"><span></span></span>
                        <progress max="4951" value="3951"></progress>
                        <span class="rating-stats__count">3951</span>
                    </li>
                    <li>
                        <span class="rating" data-value="4" data-max="5"><span></span></span>
                        <progress max="4951" value="997"></progress>
                        <span class="rating-stats__count">997</span>
                    </li>
                    <li>
                        <span class="rating" data-value="3" data-max="5"><span></span></span>
                        <progress max="4951" value="3"></progress>
                        <span class="rating-stats__count">3</span>
                    </li>
                    <li>
                        <span class="rating" data-value="2" data-max="5"><span></span></span>
                        <progress max="4951" value="0"></progress>
                        <span class="rating-stats__count">0</span>
                    </li>
                    <li>
                        <span class="rating" data-value="1" data-max="5"><span></span></span>
                        <progress max="4951" value="0"></progress>
                        <span class="rating-stats__count">0</span>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    */?>
</div>
<?php
if($page == 'dashboard') {
    MyProfileAsset::register($this);
} else {
    CatalogAsset::register($this);
}
?>