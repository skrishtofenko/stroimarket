<?php
use common\components\ImageManager\ImageManager;
use common\helpers\SKHelper;
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\Rates;
use frontend\models\Requisites;
use common\models\CompaniesProducts;
use frontend\assets\pages\MyProfileAsset;
use frontend\assets\pages\CatalogAsset;

/** @var Companies $company */
/** @var Companies[] $companies */
/** @var string $page */
$page = isset($page) ? $page : 'dashboard';
/** @var Users $owner */
$owner = $company->user;
/** @var Rates $rates */
$rates = $company->rates;
/** @var Requisites $requisites */
$requisites = $owner->mainRequisites;
/** @var Users $currentUser */
$currentUser = Yii::$app->user->identity;
/** @var string $companyAddress */
$companyAddress = $company->addressAsText;
$this->title = $page == 'dashboard' ? 'Рабочий стол' : 'Каталог';
$companies = $company->getOtherUserCompanies($this->params['currentUserRole'], false);
?>
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<header class="dashboard__header">
    <h2 class="h2 dashboard__h2"><?=($page == 'dashboard' ? 'Моя компания' : 'Каталог')?></h2>
    <?php if($page == 'dashboard'):?>
        <h3 class="h3">Рабочий стол</h3>
    <?php else:?>
        <h3 class="h3 dashboard__divided-h3">
            <span><?=$company->typeAsText?></span>
            <span><?=$owner->legalEntityVcard?></span>
        </h3>
    <?php endif;?>
    <ul class="list-reset dashboard__tabs-titles" data-tabs-title="company">
        <li class="_is-active" data-tab="info">Общая информация</li>
        <?php /*<li data-tab="passport">Паспорт</li>*/?>
        <?php if($company->type != Companies::TYPE_PICKER && $owner->ownership_type_id):?>
            <li data-tab="requisites">Реквизиты</li>
        <?php endif;?>
        <?php /*if($currentUser->id != $owner->id):?>
            <li data-tab="send-message">Отправить сообщение</li>
        <?php endif;*/?>
    </ul>
</header>

<?php if($currentUser && $currentUser->role == Users::ROLE_COMPANY && $currentUser->status != Users::STATUS_ACTIVE && $currentUser->id != $owner->id):?>
    <div class="dashboard__forbidden">
        Оплатите доступ к системе, для того, чтобы видеть список поставщиков и магазинов.
    </div>
<?php else:?>
    <div data-tabs-content="company">
        <div data-tab="info">
            <article class="company-card">
                <section class="company-card__info">
                    <div class="company-card__logo">
                        <img src="<?=ImageManager::getUrl($owner, 'logo', 140, 140, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                    </div>
                    <div<?=(!empty($companies) ? ' data-accordion' : '')?>>
                        <div class="company-card__<?=(!empty($companies) ? 'chooser accordion-title' : 'name')?>">
                            <b><?=$owner->displayName?></b>
                            <?php /*<span class="rating" data-value="<?=$company->rateAsNum?>" data-max="3"><span></span></span>*/?>
                            <p><?=$company->typeAsText?></p>
                            <?php if(!empty($companies)):?>
                                <ul class="list-reset company-card__places accordion-content" hidden>
                                    <?php foreach ($companies as $otherCompany):?>
                                        <li data-company-id="<?=$otherCompany->id?>">
                                            <p><?=$otherCompany->typeAsText?></p>
                                            <span><?=$otherCompany->addressAsText?></span>
                                        </li>
                                    <?php endforeach;?>
                                </ul>
                            <?php endif;?>
                        </div>
                        <p class="company-card__data">
                            Название юр. лица: <?=$owner->legalEntityVcard?><br>
                            Адрес: <?=$companyAddress?><br>
                            <?php if(!empty($company->phones_array)):?>
                                Телефон: <?=SKHelper::formatPhone($company->phones_array[0])?><br>
                            <?php endif;?>
                            Веб–сайт: <?=($owner->url ?: '—')?><br>
                            <?php if(!empty($company->emails_array)):?>
                                Электронная почта: <?=$company->emails_array[0]?>
                            <?php endif;?>
                        </p>
                    </div>
                </section>
                <section class="company-card__description">
                    <p><?=$owner->description?></p>
                </section>
                <?php if($company->type != Companies::TYPE_PICKER && $owner->ownership_type_id):?>
                    <section class="company-card__map">
                        <script type="text/javascript">
                            ymaps.ready(init);
                            var myGeocoder;
                            var myMap;

                            function init(){
                                myGeocoder = ymaps.geocode('<?=$companyAddress?>');
                                myGeocoder.then(
                                    function (res) {
                                        var myCoord = res.geoObjects.get(0).geometry.getCoordinates();
                                        myMap = new ymaps.Map("map", {
                                            center: myCoord,
                                            zoom: 16,
                                            controls: []
                                        });
                                        var myPlacemark = new ymaps.Placemark(myCoord, {
                                            hintContent: '<?=$companyAddress?>',
                                            balloonContent: '<?=$companyAddress?>'
                                        });
                                        myMap.geoObjects.add(myPlacemark);
                                        myMap.controls.add('zoomControl');
                                        myMap.controls.add('rulerControl');
                                        myMap.controls.add('routeButtonControl');
                                        myMap.controls.add('fullscreenControl');
                                    },
                                    function (err) {}
                                );
                            }
                        </script>
                        <div id="map" style="width: 100%; height: 240px;"></div>
                    </section>
                <?php endif;?>

            </article>

            <?php if($company->type != Companies::TYPE_PICKER):?>
                <?php $promoProducts = $company->promoProducts?>
                <?php if(!empty($promoProducts)):?>
                    <article class="products">
                        <div class="products__heading">
                            <h2>Ассортимент</h2>
                            <a class="link" href="<?=(!$currentUser || $currentUser->id != $owner->id ? "/catalog/company/$company->id" : '')?>/assortment">Показать всё</a>
                        </div>
                        <div class="grid">
                            <?php foreach($promoProducts as $promoProduct):?>
                                <?php /** @var $promoProduct CompaniesProducts */?>
                                <div class="grid__column_1-3">
                                    <div class="product-card">
                                        <img src="<?=ImageManager::getUrl($promoProduct->product, 'image', 120, 120, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                                        <h3 class="product-card__name"><?=$promoProduct->product->fullName?></h3>
                                        <p class="product-card__category"><?=$promoProduct->product->category->name?></p>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </article>
                <?php endif;?>
            <?php endif;?>
        </div>

        <div data-tab="passport" hidden>
            <article class="company-card">
                <section class="company-card__info">
                    <div class="company-card__logo">
                        <img src="<?=ImageManager::getUrl($owner, 'logo', 140, 140, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                    </div>
                    <div>
                        <div class="company-card__name">
                            <b><?=$owner->legal_name?></b>
                            <p><?=$company->typeAsText?></p>
                        </div>
                        <hr class="hr company-card__hr">
                        <div class="company-card__assessment">
                            <div class="company-card__points">
                                <b>Итоговый балл</b>
                                <span class="rating" data-value="<?=$company->rateAsNum?>" data-max="3"><span></span></span>
                            </div>
                            <?php /*<div class="company-card__claims">
                                <b>Сумма претензий к компании</b>
                                <p>0 рублей</p>
                            </div>*/?>
                        </div>
                    </div>
                </section>
            </article>

            <article class="points">
                <h3 class="points__heading">Баллы</h3>
                <table class="points__table">
                    <thead>
                    <tr>
                        <th>Начисленные баллы</th>
                        <th>Критерий начисления</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <span><?=($rates->approve_rate + 0)?> балл<?=SKHelper::ending($rates->approve_rate, ['', 'а', 'ов'])?></span>
                        </td>
                        <td>
                            <b>Данные фирмы в «Строймаркете» совпадают с настоящими данными фирмы</b>
                            <p>Определяется по платёжным банковским документам (при оплате с р/с организации,
                                зарегистрированной в «Строймаркете»).<br>
                                Для физических лиц и ИП, не имеющих р/с, балл начисляется по нотариально заверенному Согласию
                                на обработку персональных данных.<br>
                                Максимальный балл по этому критерию: 1</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span><?=($rates->sm_reg_rate + 0)?> балл<?=SKHelper::ending($rates->sm_reg_rate, ['', 'а', 'ов'])?></span>
                            <p>Дата регистрации на «Строймаркете» <?=date('d.m.Y', strtotime($owner->created_at))?></p>
                        </td>
                        <td>
                            <b>За давность регистрации в «Строймаркете»</b>
                            <p>Балл начисляется только при наличии балла за "Данные фирмы". Начисляется по 0.1 балла за
                                каждые 180 дней с момента регистрации Участника в «Строймаркете».<br>
                                Максимальный балл по этому критерию: 0.5</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span><?=($rates->le_reg_rate + 0)?> балл<?=SKHelper::ending($rates->le_reg_rate, ['', 'а', 'ов'])?></span>
                            <p>Дата регистрации юридического лица <?=($owner->legal_registered_at ? date('d.m.Y', strtotime($owner->legal_registered_at)) : 'не указана')?></p>
                        </td>
                        <td>
                            <b>За время существования юридического лица или индивидуального предпринимателя</b>
                            <p>Рассчитывается только для юридических лиц и ИП резидентов РФ. Балл начисляется только при
                                наличии балла за "Данные фирмы". Начисляется по 0.1 балла за каждые 120 дней с момента
                                регистрации юридического лица (по данным ЕГРЮЛ, ЕГРИП). Максимальный балл по этому критерию:
                                1.0</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span><?=($rates->paid_rate + 0)?> балл<?=SKHelper::ending($rates->paid_rate, ['', 'а', 'ов'])?></span>
                            <p>Последняя дата платежа <?='22.08.2017' // todo: выводить правильную дату по последней платежке?></p>
                        </td>
                        <td>
                            <b>Свидетельство ведения фирмой текущей хозяйственной деятельности</b>
                            <p>Определяется по давности последней оплаты «Строймаркета» с р/с организации,
                                зарегистрированной в системе. Балл начисляется только при наличии балла за "Данные фирмы...".
                                Для ИП минимальный: 0.25.<br>
                                Максимальный балл по этому критерию: 0,5<br>
                                Правила расчёта: давность платежа не позже 190 дней от текущей даты - 0.5 балла, не позже 380
                                дней - 0.25 балла.</p>
                        </td>
                    </tr>
                    <?php /*
                    <tr>
                        <td>
                            <span>2 Балла</span>
                            <p>Сумма претензий: 0 руб.</p>
                        </td>
                        <td>
                            <b>За профессиональную деятельность на рынке</b>
                            <p>Вычисляется на основе длительности хозяйственной деятельности и наличия Претензий.<br>
                                Максимальный балл по этому критерию: 2</p>
                        </td>
                    </tr>
                    */?>
                    </tbody>
                </table>
            </article>
        </div>

        <?php if($company->type != Companies::TYPE_PICKER && $owner->ownership_type_id):?>
            <div data-tab="requisites" hidden>
                <table class="requisites">
                    <tbody>
                    <tr>
                        <td>Название юр. лица</td>
                        <td><?=$owner->legalEntityVcard?></td>
                    </tr>
                    <tr>
                        <td>ИНН</td>
                        <td><?=$owner->inn?></td>
                    </tr>
                    <tr>
                        <td>ОГРН</td>
                        <td><?=$owner->ogrn?></td>
                    </tr>
                    <tr>
                        <td>КПП</td>
                        <td><?=$owner->kpp?></td>
                    </tr>
                    <tr>
                        <td>ОКПО</td>
                        <td><?=$owner->okpo?></td>
                    </tr>
                    <tr>
                        <td>Расчетный счет</td>
                        <td><?=$requisites->account?></td>
                    </tr>
                    <tr>
                        <td>БИК</td>
                        <td><?=$requisites->bik?></td>
                    </tr>
                    <tr>
                        <td>Банк</td>
                        <td><?=$requisites->bank?></td>
                    </tr>
                    <tr>
                        <td>Корр. счет</td>
                        <td><?=$requisites->corr_account?></td>
                    </tr>
                    <tr>
                        <td>Юр. адрес</td>
                        <td><?=$requisites->legal_address?></td>
                    </tr>
                    <tr>
                        <td>Почтовый адрес</td>
                        <td><?=$requisites->post_address?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        <?php endif;?>

        <?php /*if($currentUser->id != $owner->id):?>
            <div data-tab="send-message" hidden>
                <section class="chat">
                    <!--<div class="chat__history chat__history_empty">
                      <p class="chat__no-messages">
                        Переписка с этой компанией пуста. Напишите сообщение чтобы начать диалог.
                      </p>
                    </div>-->
                    <div class="chat__history">
                        <div class="chat__message _is-minimized" data-message>
                            <p class="chat__message-sender">
                                Отправитель сообщения
                            </p>
                            <time class="chat__message-time" datetime="2017-10-03 20:32">
                                3 окт.
                            </time>
                            <div class="chat__message-text">
                                Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения
                                Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения
                                Текст сообщения
                            </div>
                        </div>
                        <div class="chat__message _is-minimized" data-message>
                            <p class="chat__message-sender">
                                Отправитель сообщения
                            </p>
                            <time class="chat__message-time" datetime="2017-10-04 21:33">
                                4 окт.
                            </time>
                            <div class="chat__message-text">
                                Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения
                                Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения
                                Текст сообщения
                            </div>
                        </div>
                        <div class="chat__message" data-message>
                            <p class="chat__message-sender">
                                Отправитель сообщения
                            </p>
                            <time class="chat__message-time" datetime="2017-10-04 22:34">
                                среда, 4 ноября, 22:34
                            </time>
                            <div class="chat__message-text">
                                Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения
                                Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения Текст сообщения
                                Текст сообщения
                            </div>
                        </div>
                    </div>
                    <form class="form">
                        <textarea class="input chat__input" rows="4" placeholder="Текст сообщения"></textarea>
                        <button class="button chat__send-btn" type="submit">
                            Отправить сообщение
                        </button>
                    </form>
                </section>
            </div>
        <?php endif;*/?>
    </div>
<?php endif;?>
<?php
if($page == 'dashboard') {
    MyProfileAsset::register($this);
} else {
    CatalogAsset::register($this);
}
?>