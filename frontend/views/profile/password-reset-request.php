<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $passwordResetRequestForm \frontend\models\forms\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление доступа';
$this->params['breadcrumbs'][] = $this->title;
?>
<a class="link prev-page" href="javascript:history.back()">
    Назад
</a>
<div class="grid">
    <div class="grid__column_2-5 grid__column_center">
        <section class="login">
            <h2 class="h2">
                Восстановить пароль
            </h2>
            <p class="login__text">
                Введите адрес электронной почты, связанный с вашим аккаунтом, и мы вышлем вам ссылку для изменения пароля.
            </p>
            <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form login__form']]);?>
                <?=$form->errorSummary($passwordResetRequestForm, ['header' => false])?>
                <?=$form->field($passwordResetRequestForm, 'email')
                    ->textInput(['class' => 'input', 'type' => 'email', 'placeholder' => 'Электронная почта'])?>
                <?=Html::submitButton('Получить ссылку', ['class' => 'button form__button'])?>
            <?php ActiveForm::end();?>
        </section>
    </div>
</div>