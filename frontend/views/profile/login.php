<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $loginForm \frontend\models\forms\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grid">
    <div class="grid__column_2-5 grid__column_center">
        <section class="login">
            <h2 class="h2">
                Войти или
                <a class="link" href="/registration">Зарегистрироваться</a>
            </h2>
            <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form']]);?>
                <?=$form->errorSummary($loginForm, ['header' => false])?>
                <?=$form->field($loginForm, 'email')
                    ->textInput(['class' => 'input', 'type' => 'email', 'placeholder' => 'Электронная почта'])?>
                <?=$form->field($loginForm, 'password')
                    ->passwordInput(['class' => 'input', 'placeholder' => 'Пароль'])?>
                <p class="">
                    <?=$form->field($loginForm, 'rememberMe')
                        ->checkbox(['class' => 'form__switcher', 'label' => false], false)?>
                    <label for="loginform-rememberme" data-checked="да" data-not-checked="нет">
                        Запомнить пароль?
                    </label>
                </p>
                <a class="link form__link" href="/password-reset">Я забыл(а) пароль</a>
                <?=$form->field($loginForm, 'isRegistrationFirstLogin')->hiddenInput()->label(false)?>
                <?=Html::submitButton('Войти в личный кабинет', ['class' => 'button form__button'])?>
            <?php ActiveForm::end();?>
        </section>
    </div>
</div>
