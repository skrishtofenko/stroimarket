<?php
use frontend\models\Categories;
use frontend\models\Products;
use frontend\models\forms\PS;

/* @var $this yii\web\View */
/* @var $category Categories */
/* @var $products Products[] */
/* @var $searchForm PS */
/* @var $foundCount integer */

$searchForm = $this->params['productSearch'];
$this->title = 'Каталог';
?>
<h2 class="h2 dashboard__h2">Каталог</h2>
<h3 class="h3 h3-selector">
    Товары
    <div class="dashboard__back-wrapper">
        <span class="link dashboard__back-link">Поиск <?=(!empty($searchForm->q) ? "&laquo;{$searchForm->q}&raquo; " : '')?>(<?=$searchForm->found?>)</span>
    </div>
</h3>

<div class="assortment">
    <div class="grid">
        <div class="grid__column_7-9">
            <section class="assortment__interactions">
                <?=$this->render('../partials/search/products/searchField')?>
                <?=$this->render('../partials/search/products/placeField')?>
            </section>

            <?php if(count($products)):?>
                <div class="layout-view">
                    <?=$this->render('../partials/search/products/sortField')?>
                    <?=$this->render('../partials/search/products/onPageField')?>
                    <?=$this->render('../partials/viewTypeToggle', ['selector' => 'product'])?>
                </div>

                <?=$this->render('../partials/productsList', [
                    'products' => $products,
                ])?>
                <?=$this->render('../partials/search/products/paginationField')?>
            <?php else:?>
                <div class="layout-view">
                    По <?=(!empty($searchForm->q) ? "запросу &laquo;{$searchForm->q}&raquo;" : 'вашему запросу')?> ничего не найдено.<br />
                    Попробуйте изменить параметры поиска.
                </div>
            <?php endif;?>
        </div>
        <div class="grid__column_2-9">
            <?=$this->render('../partials/search/products/sidebarFilter')?>
        </div>
    </div>
</div>

