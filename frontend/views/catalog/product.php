<?php
use common\helpers\SKHelper;
use common\components\ImageManager\ImageManager;
use frontend\models\Users;
use frontend\models\Products;
use frontend\models\Companies;
use frontend\models\CompaniesProducts;
use frontend\assets\pages\CatalogAsset;

/** @var $product Products */
/** @var $providers Companies|null */
/** @var $stores Companies */

$this->title = 'Каталог';
$companyTypes = [
    'providers' => 'Поставщики',
    'stores' => 'Магазины',
];
$characteristics =
array_filter($product->productsCharacteristics, function($characteristic) {
    return $characteristic->value !== '' && !is_null($characteristic->value);
});
?>
<?php if($this->params['currentUserRole'] == Users::ROLE_BUYER):?>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<?php endif;?>
<h2 class="h2 dashboard__h2">Каталог</h2>
<div class="flex">
    <h3 class="h3">Товары</h3>
    <div class="dashboard__back-wrapper">
        <a class="link dashboard__back-link" href="/catalog/<?=$product->category_id?>">Назад к списку товаров</a>
    </div>
</div>

<div class="assortment">
    <div class="product-sheet">
        <div class="product-sheet__img">
            <img src="<?=ImageManager::getUrl($product, 'image', 160, 160, ImageManager::RESIZE_ZOOM_IN)?>" alt="" />
        </div>
        <div class="product-sheet__info">
            <p class="product-sheet__name">
                <?=$product->fullName?>
            </p>
            <p class="product-sheet__type">
                <?=$product->category->name?>
            </p>
            <?php /*
            <p class="product-sheet__vendor-code">
                АРТИКУЛ СМ: <?=$product->id?>
            </p>
            */?>
            <?php /*<p class="product-sheet__definition">
                Выравнивающая, гипсовая основа
            </p>*/?>
        </div>
        <ul class="product-presence-tabs" data-tabs-title="info">
            <li class="_is-active" data-tab="description">Описание</li>
            <?php if(!empty($product->characteristics)):?>
                <li data-tab="characteristics">Характеристики</li>
            <?php endif;?>
        </ul>
    </div>

    <div data-tabs-content="info">
        <div data-tab="description">
            <div class="product-sheet__description _is-minimized" data-minimizable>
                <?=SKHelper::clearText($product->description)?>
                <button class="button product-sheet__expand-caption" type="button" data-show="Показать полностью" data-hide="Свернуть">
                    Показать полностью
                </button>
            </div>
        </div>

        <?php if(!empty($characteristics)):?>
            <div data-tab="characteristics" hidden>
                <div class="product-sheet__description _is-minimized" data-minimizable>
                    <div class="grid">
                        <?php
                        $half = ceil(count($characteristics) / 2);
                        echo $this->render('../partials/productCharacteristics', [
                            'characteristics' => array_slice($characteristics, 0, $half),
                        ]);
                        echo $this->render('../partials/productCharacteristics', [
                            'characteristics' => array_slice($characteristics, $half),
                        ]);
                        ?>
                    </div>
                    <button class="button product-sheet__expand-caption" type="button">Показать полностью</button>
                </div>
            </div>
        <?php endif;?>
    </div>

    <?php if($this->params['currentUserRole'] == Users::ROLE_BUYER):?>
        <?php if(!empty($stores)):?>
            <ul class="product-presence-tabs" data-tabs-title="presence">
                <li class="_is-active" data-tab="list">Список</li>
                <li data-tab="map" id="show-product-map">Карта</li>
            </ul>

            <div class="layout-view">
                <?=$this->render('../partials/search/companies/sortField', ['searchFormTitle' => 'storesCompaniesSearch'])?>
                <?=$this->render('../partials/search/companies/deliveryField')?>
            </div>

            <div data-tabs-content="presence">
                <div data-tab="list">
                    <ul class="product-providers">
                        <?php $retailStoresCount = 0;?>
                        <?php foreach($stores as $company):?>
                            <?php
                            /** @var $company Companies */
                            /** @var $companyProduct CompaniesProducts */
                            $user = $company->user;
                            $companyProduct = $product->getForCompany($company->id);
                            ?>
                            <li>
                                <div class="grid">
                                    <div class="grid__column_1-6">
                                        <div class="product-providers__logo">
                                            <img src="<?=ImageManager::getUrl($company->user, 'logo', 250, 250, ImageManager::RESIZE_ZOOM_OUT)?>" alt="">
                                        </div>
                                    </div>
                                    <div class="grid__column_5-12">
                                        <div class="product-providers__info">
                                            <div class="product-providers__title">
                                                <p class="product-providers__name">
                                                    <?=$user->legal_name?>
                                                </p>
                                                <?php if($company->is_imonly):?>
                                                    <a class="product-providers__link" href="<?=$company->im_url?>" target="_blank">
                                                        Перейти на сайт
                                                    </a>
                                                <?php endif;?>
                                            </div>
                                            <p class="product-providers__type">
                                                <?=$company->typeAsText?>
                                            </p>
                                            <p class="product-providers__contacts">
                                                <?php if(!empty($company->phones_array)):?>
                                                    <?=SKHelper::formatPhone($company->phones_array[0])?>
                                                <?php endif;?>
                                            </p>
                                            <?php /*<div class="rating-reviews">
                                                <span class="rating" data-value="<?=$company->rateAsNum?>" data-max="3"><span></span></span>
                                                <?php /*<span class="rating-reviews__reviews">Отзывы 99999</span>* /?>
                                            </div>*/?>
                                        </div>
                                    </div>
                                    <div class="grid__column_1-4">
                                        <?php /*if(!$company->is_imonly):?>
                                            <?=$this->render('../partials/companySchedule', [
                                                'company' => $company,
                                                'isTodayOnly' => true,
                                            ])?>
                                        <?php endif;*/?>
                                    </div>
                                    <div class="grid__column_1-6">
                                        <p class="product-providers__price">
                                            Цена <span><?=$companyProduct->cost?> руб./<?=$product->unitType->name?></span>
                                        </p>
                                        <?php $addonClass = $companyProduct->availability ? ' product-providers__availability_available' : ''?>
                                        <p class="product-providers__availability<?=$addonClass?>">
                                            <?=$companyProduct->availabilityAsText?>
                                        </p>
                                        <?php /*
                                        <div class="button-counter" data-cart-counter>
                                            <button class="button button-counter__button" type="button">
                                                В корзину
                                            </button>
                                            <div class="button-counter__counter">
                                                <input class="button-counter__count" type="number"
                                                       value="10" min="1" max="99"
                                                       data-cart-count readonly>
                                                <div class="button-counter__controls">
                                                    <button class="button-counter__control button-counter__control_increment"
                                                            type="button" data-cart-increment>
                                                        Увеличить количество
                                                    </button>
                                                    <button class="button-counter__control button-counter__control_decrement"
                                                            type="button" data-cart-decrement>
                                                        Уменьшить количество
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        */?>
                                    </div>
                                </div>
                            </li>
                            <?php if(!$company->is_imonly) $retailStoresCount++;?>
                        <?php endforeach;?>
                    </ul>
                </div>

                <div data-tab="map" hidden>
                    <div class="grid grid_collapse">
                        <div class="grid__column_5-12">
                            <?php if($retailStoresCount):?>
                                <ul class="product-providers product-providers_condensed" id="product-map-companies">
                                    <?php $addresses = []?>
                                    <?php foreach($stores as $company):?>
                                        <?php
                                        /** @var $company Companies */
                                        if($company->is_imonly) {
                                            continue;
                                        }
                                        /** @var $companyProduct CompaniesProducts */
                                        $user = $company->user;
                                        $companyProduct = $product->getForCompany($company->id);
                                        $companyAddress = $company->addressAsText;
                                        $addresses[] = $companyAddress;
                                        ?>
                                        <li data-accordion data-address="<?=$companyAddress?>">
                                            <div class="accordion-title">
                                                <div class="product-providers__logo">
                                                    <img src="<?=ImageManager::getUrl($company->user, 'logo', 60, 60, ImageManager::RESIZE_ZOOM_OUT)?>" alt="">
                                                </div>
                                                <div class="product-providers__info">
                                                    <div class="product-providers__title">
                                                        <p class="product-providers__name">
                                                            <?=$user->legal_name?>
                                                        </p>
                                                    </div>
                                                    <p class="product-providers__contacts">
                                                        <?=$companyAddress?>
                                                    </p>
                                                </div>
                                                <?php /*<div class="rating-reviews">
                                                    <span class="rating" data-value="<?=$company->rateAsNum?>" data-max="3"><span></span></span>
                                                    <?php /*<span class="rating-reviews__reviews">Отзывы 99999</span>* /?>
                                                </div>*/?>
                                                <div class="product-providers__aside">
                                                    <p class="product-providers__price">
                                                        <span><?=$companyProduct->cost?> руб./<?=$product->unitType->name?></span>
                                                    </p>
                                                    <?php $addonClass = $companyProduct->availability ? ' product-providers__availability_available' : ''?>
                                                    <p class="product-providers__availability<?=$addonClass?>">
                                                        <?=$companyProduct->availabilityAsText?>
                                                    </p>
                                                    <?php /*
                                                    <button class="button button-counter__button" type="button">
                                                        В корзину
                                                    </button>
                                                    */?>
                                                </div>
                                            </div>
                                            <div class="accordion-content" hidden>
                                                <p class="product-providers__contacts">
                                                    <?php if(!empty($company->phones_array)):?>
                                                        <?=SKHelper::formatPhone($company->phones_array[0])?>
                                                    <?php endif;?>
                                                </p>
                                                <?=$this->render('../partials/companySchedule', [
                                                    'company' => $company,
                                                    'isTodayOnly' => true,
                                                ])?>
                                                <?php if(!empty($user->url)):?>
                                                    <a class="product-providers__link" href="<?=$user->url?>" target="_blank">
                                                        Перейти на сайт
                                                    </a>
                                                <?php endif;?>
                                            </div>
                                        </li>
                                    <?php endforeach;?>
                                </ul>
                            <?php endif;?>
                        </div>

                        <div class="grid__column_7-12">
                            <div class="product-providers__map">
                                <script type="text/javascript">
                                    var productAddresses = ['<?=implode("','", $addresses)?>'];
                                </script>
                                <div id="map" style="width: 100%; height: 100%; min-height: 250px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;?>
    <?php else:?>
        <?php if(Yii::$app->user->identity->status == Users::STATUS_ACTIVE):?>
            <div class="grid">
                <div class="grid__column_2-3">
                    <ul class="product-presence-tabs" data-tabs-title="presence">
                        <?php $isActiveShown = false?>
                        <?php foreach($companyTypes as $companyType => $typeCaption):?>
                            <?php
                            $class = '';
                            if(!$isActiveShown) {
                                $class = '_is-active';
                                $isActiveShown = true;
                            }
                            ?>
                            <li class="<?=$class?>" data-tab="<?=$companyType?>"><?=$typeCaption?></li>
                        <?php endforeach;?>
                    </ul>
                </div>
                <div class="grid__column_1-3">
                    <section class="assortment__interactions">
                        <?=$this->render('../partials/search/companies/placeField', ['forProductPage' => true])?>
                    </section>
                </div>
            </div>

            <div data-tabs-content="presence">
                <?php $isActiveShown = false?>
                <?php foreach($companyTypes as $companyType => $typeCaption):?>
                    <?=$this->render('../partials/productCompanies', [
                        'product' => $product,
                        'companies' => $$companyType,
                        'type' => $companyType,
                        'hidden' => $isActiveShown,
                    ])?>
                    <?php if(!$isActiveShown) $isActiveShown = true?>
                <?php endforeach;?>
            </div>
        <?php else:?>
            <div class="product-providers product-providers_forbidden">
                Оплатите доступ к системе, для того, чтобы видеть список поставщиков и магазинов.
            </div>
        <?php endif;?>
    <?php endif;?>
</div>
<?php
CatalogAsset::register($this);
?>
