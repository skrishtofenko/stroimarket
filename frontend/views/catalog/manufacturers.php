<?php
use common\components\ImageManager\ImageManager;
use frontend\helpers\SearchHelper;
use frontend\models\Companies;
use frontend\models\Categories;

/** @var $manufacturers Companies[] */
/** @var $categories Categories[] */
$this->title = 'Каталог производителей';

$companies = [
    'recommended' => [],
    'others' => [],
];
foreach($manufacturers as $manufacturer) {
    $companies[$manufacturer->user->is_recommended ? 'recommended' : 'others'][] = $manufacturer;
}
?>

<h2 class="h2 dashboard__h2">Каталог</h2>
<div class="flex">
    <h3 class="h3">Производители</h3>
    <div class="dashboard__back-wrapper">
        <a class="link dashboard__back-link" href="/">Назад на главную</a>
    </div>
</div>

<div class="producers">
    <div class="grid">
        <div class="grid__column_3-4">
            <section>
                <?=$this->render('../partials/search/companies/searchField', ['fromPage' => 'manufacturers'])?>
            </section>
            <?php if(!empty($manufacturers)):?>
                <?php if(!empty($companies['recommended'])):?>
                    <ul class="list-reset producers__cards">
                        <?php foreach($companies['recommended'] as $company):?>
                            <?php /** @var $company Companies */?>
                            <li class="producers__card">
                                <a href="/catalog/manufacturers/<?=$company->id?>">
                                    <div class="producers__logo">
                                        <img src="<?=ImageManager::getUrl($company->user, 'logo', 140, 140, ImageManager::RESIZE_ZOOM_OUT)?>" alt="">
                                    </div>
                                </a>
                                <div>
                                    <div class="producers__name">
                                        <b>
                                            <a href="/catalog/manufacturers/<?=$company->id?>"><?=$company->user->name?></a>
                                        </b>
                                        <span class="producers__recommendation">
                                            Рекомендовано Строймаркетом
                                        </span>
                                    </div>
                                    <div class="producers__location">
                                        <?php
                                        $placement = [];
                                        if($company->country_id) {
                                            $placement[] = $company->country->name;
                                        }
                                        if($company->city_id) {
                                            $placement[] = $company->city->name;
                                        }
                                        ?>
                                        <?=implode(', ', $placement)?>
                                    </div>

                                    <ul class="list-reset producers__tags">
                                        <?php foreach($company->manufacturingChapters as $category):?>
                                            <li>
                                                <a class="link producers__tag-link" href="<?=SearchHelper::makeGetString('CS', null, null, ['cat' => $category->id])?>">
                                                    <?=$category->name?>
                                                </a>
                                            </li>
                                        <?php endforeach;?>
                                    </ul>
                                </div>
                            </li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>

                <?php if(!empty($companies['others'])):?>
                    <ul class="list-reset producers__cards producers__cards_grid">
                        <?php foreach($companies['others'] as $company):?>
                            <?php /** @var $company Companies */?>
                            <li class="producers__card">
                                <a href="/catalog/manufacturers/<?=$company->id?>">
                                    <div class="producers__logo">
                                        <img src="<?=ImageManager::getUrl($company->user, 'logo', 140, 140, ImageManager::RESIZE_ZOOM_OUT)?>" alt="" width="94" height="94">
                                    </div>
                                </a>
                                <div>
                                    <div class="producers__name">
                                        <b>
                                            <a href="/catalog/manufacturers/<?=$company->id?>"><?=$company->user->name?></a>
                                        </b>
                                    </div>
                                    <div class="producers__location">
                                        <?php if($company->country_id):?>
                                            <?=$company->country->name?>
                                        <?php endif;?>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach?>
                    </ul>
                <?php endif;?>
                <?=$this->render('../partials/search/companies/paginationField')?>
            <?php else:?>
                <div class="producers__not-found">
                    Пока нет производителей в данной категории
                </div>
            <?php endif;?>
        </div>

        <?php
        $searchForm = $this->params['companiesSearch'];
        $currentCategory = $searchForm->cat ?? null;
        ?>

        <div class="grid__column_1-4">
            <ul class="list-reset producers__categories">
                <?php foreach ($categories as $chapter):?>
                    <?php /** @var $chapter Categories */?>
                    <li>
                        <a class="producers__category">
                            <?=$chapter->name?>
                        </a>
                        <ol class="list-reset producers__subcategories">
                            <?php foreach ($chapter->children as $category):?>
                                <?php /** @var $category Categories */?>
                                <li>
                                    <?php
                                    $class = 'producers__subcategory' . ($category->id == $currentCategory ? ' producers__subcategory_active' : '');
                                    $href = $category->id == $currentCategory ? '' : ' href="' . SearchHelper::makeGetString('CS', null, null, ['cat' => $category->id]) . '"';
                                    ?>
                                    <a class="<?=$class?>"<?=$href?>>
                                        <?=$category->name?>
                                    </a>
                                </li>
                            <?php endforeach;?>
                        </ol>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
    </div>
</div>