<?php
use common\components\ImageManager\ImageManager;
use common\helpers\SKHelper;
use frontend\models\Companies;

/** @var $company Companies */
$this->title = 'Каталог производителей';
?>

<h2 class="h2 dashboard__h2">Каталог</h2>
<div class="flex">
    <h3 class="h3">
        Производители
    </h3>
    <div class="dashboard__back-wrapper">
        <a class="link dashboard__back-link" href="/catalog/manufacturers">
            Назад к списку производителей
        </a>
    </div>
</div>

<div class="producer">
    <div class="producers__card">
        <div class="producers__logo">
            <img src="<?=ImageManager::getUrl($company->user, 'logo', 140, 140, ImageManager::RESIZE_ZOOM_OUT)?>" alt="">
        </div>
        <div>
            <div class="producers__name">
                <b><?=$company->user->name?></b>
                <?php if($company->user->is_recommended):?>
                    <span class="producers__recommendation">
                      Рекомендовано Строймаркетом
                    </span>
                <?php endif;?>
            </div>
            <div class="producers__count">
                <?php
                $placement = [];
                if($company->country_id) {
                    $placement[] = $company->country->name;
                }
                if($company->city_id) {
                    $placement[] = $company->city->name;
                }
                ?>
                <?=implode(', ', $placement)?>
            </div>
            <div class="producers__site">
                <?php if($company->user->url):?>
                    <a class="link" href="<?=$company->user->url?>">
                        <?=$company->user->url?>
                    </a>
                <?php endif;?>
            </div>
        </div>
    </div>

    <h3 class="producer__title">
        О производителе
    </h3>
    <div class="producer__description _is-minimized" data-minimizable>
        <?php if(!empty($company->user->description)):?>
            <?=nl2br($company->user->description)?>
        <?php else:?>
            <p>Пока нет информации о производителе</p>
        <?php endif;?>
        <button class="button producer__expand" type="button" data-show="Показать полностью" data-hide="Свернуть">
            Показать полностью
        </button>
    </div>

    <h3 class="producer__title">
        Каталог товаров производителя
    </h3>
    <div class="producers _is-minimized" data-minimizable>
        <?php $categories = $company->manufacturingCategories?>
        <?php if(!empty($categories)):?>
            <ul class="list-reset producers__cards producers__cards_grid">
                <?php foreach($categories as $category):?>
                    <li class="producers__card">
                        <div class="producers__logo">
                            <img src="<?=ImageManager::getUrl(
                                $company->getRandomCategoryProduct($category->id),
                                'image',
                                140,
                                140,
                                ImageManager::RESIZE_ZOOM_IN
                            )?>" alt="">
                        </div>
                        <div>
                            <div class="producers__name">
                                <b>
                                    <a href="/catalog/<?=$category->id?>?PS[m][<?=$company->id?>]=1">
                                        <?=$category->name?>
                                    </a>
                                </b>
                            </div>
                            <div class="producers__count">
                                <?=$category->visibleProductsCount?> товар<?=SKHelper::ending($category->visibleProductsCount, ['', 'а', 'ов'])?>
                            </div>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        <?php else:?>
            <p>Пока нет информации об ассортименте производителя</p>
        <?php endif;?>
        <button class="button producer__expand" type="button" data-show="Показать полностью" data-hide="Свернуть">
            Показать полностью
        </button>
    </div>
</div>