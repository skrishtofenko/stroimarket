<?php
use frontend\models\Users;
use frontend\models\Categories;
use frontend\models\Products;

/* @var $this yii\web\View */
/* @var $category Categories */
/* @var $products Products[] */

$this->title = 'Каталог';
?>
<h2 class="h2 dashboard__h2">Каталог</h2>
<div class="flex">
    <h3 class="h3">Товары</h3>
    <div class="dashboard__back-wrapper">
        <a class="link dashboard__back-link" href="/catalog">Назад к списку категорий</a>
    </div>
</div>

<div class="assortment">
    <div class="grid">
        <div class="grid__column_<?=($this->params['currentUserRole'] == Users::ROLE_BUYER ? '3-4' : '7-9')?>">
            <section class="assortment__interactions">
                <?=$this->render('../partials/search/products/searchField', ['stayHere' => true])?>
                <?php if($this->params['currentUserRole'] == Users::ROLE_COMPANY):?>
                    <?=$this->render('../partials/search/products/placeField', ['stayHere' => true])?>
                <?php endif;?>
            </section>

            <div class="layout-view">
                <?=$this->render('../partials/search/products/sortField')?>
                <?=$this->render('../partials/search/products/onPageField')?>
                <?=$this->render('../partials/viewTypeToggle', ['selector' => 'product'])?>
            </div>

            <?=$this->render('../partials/productsList', [
                'products' => $products,
            ])?>
            <?=$this->render('../partials/search/products/paginationField')?>
        </div>
        <div class="grid__column_<?=($this->params['currentUserRole'] == Users::ROLE_BUYER ? '1-4' : '2-9')?>">
            <?=$this->render('../partials/search/products/sidebarFilter', [
                'stayHere' => true,
                'characteristics' => $category->characteristics,
                'manufacturers' => $category->manufacturers,
                'minCost' => $category->min_cost,
                'maxCost' => $category->max_cost,
            ])?>
        </div>
    </div>
</div>

