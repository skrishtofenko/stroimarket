<?php
use common\components\ImageManager\ImageManager;
use frontend\models\Users;
use frontend\models\Companies;

/** @var $companies Companies[] */
/** @var $type string */
$this->title = 'Каталог';
?>
<h2 class="h2 dashboard__h2">Каталог</h2>
<h3 class="h3 h3-selector"><?=Companies::types()[$type]?></h3>
<div class="catalog">
    <?php if(Yii::$app->user->identity->status == Users::STATUS_ACTIVE):?>
        <section class="catalog__interactions">
            <?=$this->render('../partials/search/companies/searchField')?>
            <?=$this->render('../partials/search/companies/placeField')?>
        </section>

        <div class="layout-view">
            <?=$this->render('../partials/search/companies/sortField')?>
            <?=$this->render('../partials/search/companies/manufacturerField')?>
            <?=$this->render('../partials/viewTypeToggle', ['selector' => 'companies'])?>
        </div>

        <ul class="list-reset catalog__panel _list-view" id="companies-list">
            <?php foreach ($companies as $company):?>
                <li class="company-item">
                    <div class="company-item__info">
                        <span class="company-item__type"><?=$company->typeAsText?></span>
                        <span class="rating" data-value="<?=$company->rateAsNum?>" data-max="3"><span></span></span>
                        <div class="company-item__logo">
                            <img src="<?=ImageManager::getUrl($company->user, 'logo', 160, 160, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                        </div>
                        <div>
                            <p class="company-item__name"><?=$company->user->legal_name?></p>
                            <p class="company-item__address"><?=$company->addressAsText?></p>
                        </div>
                    </div>
                    <a class="button button_action company-item__button" href="/catalog/company/<?=$company->id?>">
                        Смотреть
                    </a>
                </li>
            <?php endforeach;?>
        </ul>
    <?php else:?>
        <div class="dashboard__forbidden">
            Оплатите доступ к системе, для того, чтобы видеть список поставщиков и магазинов.
        </div>
    <?php endif;?>
</div>
