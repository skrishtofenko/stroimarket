<?php
/* @var $this yii\web\View */
/* @var $categoriesTree array */

$this->title = 'Каталог';
?>

<h2 class="h2 dashboard__h2">Каталог</h2>
<h3 class="h3 h3-selector">Товары</h3>

<div class="assortment">
    <section class="assortment__interactions">
        <?=$this->render('../partials/search/products/placeField', ['stayHere' => true])?>
        <?=$this->render('../partials/search/products/searchField')?>
        <?=$this->render('../partials/search/products/actionField', ['stayHere' => true])?>
    </section>

    <?=$this->render('../partials/fullCategoriesList', [
        'categories' => $categoriesTree,
        'categoryHref' => 'catalog',
    ])?>
</div>