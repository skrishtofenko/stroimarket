<?php
use common\helpers\SMHelper;
use frontend\models\Companies;

/** @var $isTodayOnly boolean */
/** @var $company Companies */
$isTodayOnly = isset($isTodayOnly) ? $isTodayOnly : false;
?>
<?php if($isTodayOnly):?>
    <?php
    $today = strtolower(date('D'));
    $field = "{$today}From";
    $from = $company->$field;
    $field = "{$today}To";
    $to = $company->$field;
    $field = "{$today}DinnerFrom";
    $dinnerFrom = $company->$field;
    $field = "{$today}DinnerTo";
    $dinnerTo = $company->$field;
    ?>
    <div class="product-providers__work-time">
        <p><span>Время работы:</span></p>
        <?php if($from && $to):?>
            <p>Сегодня <?=$from?>–<?=$to?></p>
            <?php if($dinnerFrom && $dinnerTo):?>
                <p>Обед <?=$dinnerFrom?>–<?=$dinnerTo?></p>
            <?php endif;?>
        <?php else:?>
            <p>Сегодня выходной</p>
        <?php endif;?>
    </div>
<?php else:?>
    <?php $scheduleValues = SMHelper::getCompanyScheduleValues($company)?>
    <table class="operating-time">
        <thead>
            <tr>
                <?php foreach($scheduleValues as $eng => $schedule):?>
                    <th<?=(!empty($schedule['classes']['header']) ? ' class="' . implode(' ', $schedule['classes']['header']) . '"' : '')?>><?=$schedule['ru']?></th>
                <?php endforeach;?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php foreach($scheduleValues as $eng => $schedule):?>
                    <td<?=(!empty($schedule['classes']['day']) ? ' class="' . implode(' ', $schedule['classes']['day']) . '"' : '')?>>
                        <?php if(!$schedule['isEmpty']):?>
                            <span><?=$schedule['from']?></span>
                            <span><?=$schedule['to']?></span>
                        <?php else:?>
                            <span></span>
                            <span></span>
                        <?php endif;?>
                    </td>
                <?php endforeach;?>
            </tr>
            <tr>
                <?php
                $dayNum = date('w');
                $dayNum = ($dayNum == 0 ? 7 : $dayNum);
                ?>
                <?php if($dayNum > 1):?>
                    <td colspan="<?=($dayNum - 1)?>">
                        <span></span>
                    </td>
                <?php endif;?>
                <td class="_is-current" colspan="1">
                    <span>обед</span>
                </td>
                <?php if($dayNum < 7):?>
                    <td colspan="<?=(7 - $dayNum)?>">
                        <span></span>
                    </td>
                <?php endif;?>
            </tr>
            <tr>
                <?php foreach($scheduleValues as $eng => $schedule):?>
                    <td<?=(!empty($schedule['classes']['dinner']) ? ' class="' . implode(' ', $schedule['classes']['dinner']) . '"' : '')?>>
                        <?php if(!$schedule['isDinnerEmpty']):?>
                            <span><?=$schedule['dinnerFrom']?></span>
                            <span><?=$schedule['dinnerTo']?></span>
                        <?php else:?>
                            <span></span>
                            <span></span>
                        <?php endif;?>
                    </td>
                <?php endforeach;?>
            </tr>
        </tbody>
    </table>
<?php endif;?>
