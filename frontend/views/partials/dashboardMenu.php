<?php
use frontend\models\Companies;
use frontend\models\Users;

/** @var Users $user */
$user = Yii::$app->user->identity;
$isMinimized = isset($this->params['leftMenuUnmaximizable']) && $this->params['leftMenuUnmaximizable'];
$menu = [];

if($user && $user->role == Users::ROLE_COMPANY) {
    $companyMenu = [
        'title' => 'Моя компания',
        'isCurrent' => in_array(Yii::$app->controller->id, ['profile', 'assortment']) && !$isMinimized,
        'icon' => 'company',
        'items' => [
            'Рабочий стол' => '/profile',
        ],
    ];
    if($user->mainCompany->type != Companies::TYPE_PICKER) {
        $companyMenu['items']['Ассортимент'] = '/assortment';
    }
    $companyMenu['items']['Претензии'] = [
        'link' => 'javascript:void(0)',
        'isInDevelop' => true,
    ];
    $menu[] = $companyMenu;
}

$catalogSubmenu = ['Товары' => '/catalog'];
if($user && $user->role == Users::ROLE_COMPANY) {
    $catalogSubmenu['Поставщики'] = '/catalog/providers';
}
$catalogSubmenu['Магазины'] = '/catalog/stores';
$menu[] = [
    'title' => 'Каталог',
    'isCurrent' => Yii::$app->controller->id == 'catalog' && !$isMinimized,
    'icon' => 'catalog',
    'items' => $catalogSubmenu,
];


if($user && $user->role == Users::ROLE_COMPANY) {
    $menu[] = [
        'title' => 'Закупки',
        'isCurrent' => Yii::$app->controller->id == 'my-purchases' && !$isMinimized,
        'icon' => 'purchases',
        'isInDevelop' => true,
        'items' => [
            'Список закупок' => 'javascript:void(0)',
            'Мои закупки' => 'javascript:void(0)',
        ],
    ];

    $settingsSubmenu = [
        'Аккаунт' => '/settings',
    ];
    if ($user->mainCompany->type == Companies::TYPE_PICKER && !$user->ownership_type_id) {
        $settingsSubmenu['Профиль'] = '/settings/profile';
    } else {
        $settingsSubmenu['Профиль компании'] = '/settings/company';
//    $settingsSubmenu['Контактные лица'] = 'javascript:void(0)';
    }
    $settingsSubmenu['Уведомления'] = '/settings/notices';
//    $settingsSubmenu['Оплата'] = '/settings/payment';
    if ($user->mainCompany->type != Companies::TYPE_PICKER || $user->ownership_type_id) {
        $settingsSubmenu['Реквизиты'] = '/settings/requisites';
    }
    $menu[] = [
        'title' => 'Настройки',
        'isCurrent' => Yii::$app->controller->id == 'settings' && !$isMinimized,
        'icon' => 'settings',
        'items' => $settingsSubmenu,
    ];
}
?>

<ul class="side-panel">
    <?php foreach ($menu as $item):?>
        <li class="side-panel__item side-panel__item_<?=$item['icon']?><?=(isset($item['isInDevelop']) && $item['isInDevelop'] ? ' side-panel__item_developing' : '')?><?=($item['isCurrent'] ? ' _is-open' : '')?>" data-accordion="side-menu">
            <span class="accordion-title"<?=(isset($item['isInDevelop']) && $item['isInDevelop'] ? ' data-develop="в разработке"' : '')?>><?=$item['title']?></span>
            <ul class="accordion-content"<?=(!$item['isCurrent'] ? ' hidden' : '')?>>
                <?php foreach ($item['items'] as $caption => $url):?>
                    <?php
                    $inDevelop = false;
                    if(is_array($url)) {
                        $inDevelop = $url['isInDevelop'];
                        $url = $url['link'];
                    }
                    ?>
                    <?php $href = ($url == Yii::$app->request->url ? '' : " href='$url'")?>
                    <li<?=($inDevelop ? ' class="_develop" data-develop="в разработке"' : '')?>><a<?=$href?>><?=$caption?></a></li>
                <?php endforeach;?>
            </ul>
        </li>
    <?php endforeach;?>
</ul>
