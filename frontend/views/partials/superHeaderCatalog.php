<?php
use frontend\models\Categories;
?>
<a class="link accordion-title" href="javascript:void(0)">
    Каталог
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 8" width="13" height="8">
        <path d="M6.5,7.3L6,7,0.5,1.5A0.7,0.7,0,0,1,1.5.5l5,5,5-5a0.7,0.7,0,0,1,1.1,1.1L7,7Z"/>
    </svg>
</a>
<ul class="accordion-content list-reset super-header__popup-menu" hidden>
    <?php foreach(Categories::getFullTree() as $chapter):?>
        <li data-accordion="level-1">
            <span class="accordion-title"><?=$chapter->name?></span>
            <ul class="accordion-content list-reset super-header__popup-menu" hidden>
                <?php foreach($chapter['children'] as $category):?>
                    <li data-accordion="level-2">
                        <span class="accordion-title"><?=$category->name?></span>
                        <ul class="accordion-content list-reset super-header__popup-menu" hidden>
                            <?php foreach($category['children'] as $subcategory):?>
                                <li data-accordion="level-3">
                                    <span class="accordion-title"><?=$subcategory->name?></span>
                                    <ul class="accordion-content list-reset super-header__popup-menu" hidden>
                                        <?php foreach($subcategory['children'] as $goodsCategory):?>
                                            <li>
                                                <a href="/catalog/<?=$goodsCategory->id?>"><?=$goodsCategory->name?></a>
                                            </li>
                                        <?php endforeach;?>
                                    </ul>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </li>
                <?php endforeach;?>
            </ul>
        </li>
    <?php endforeach;?>
</ul>
