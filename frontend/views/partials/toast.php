<?php
use common\components\FlashMessages;
use frontend\assets\lib\FlashMessageAsset;
?>
<script type="application/javascript">
    var flashTypes = ["<?=implode('","', array_keys(FlashMessages::FLASH_TYPES))?>"];
</script>
<?php foreach(FlashMessages::FLASH_TYPES as $messageType => $messageTypeTitle):?>
    <?php if(Yii::$app->session->hasFlash($messageType)):?>
        <div class="modal modal_middle" id="<?=$messageType?>">
            <div class="container modal__container">
                <div class="grid">
                    <div class="grid__column_1-3 grid__column_center">
                        <div class="modal__overlay"></div>
                        <div class="modal__body modal__body_contrast modal__body_compact">
                            <button class="modal__close" type="button">Закрыть</button>
                            <div class="modal__alert modal__alert_<?=$messageType?>">
                                <b class="modal__alert-title"><?=$messageTypeTitle?></b>
                                <p class="modal__alert-text">
                                    <?=Yii::$app->session->getFlash($messageType)?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>
<?php endforeach;?>
<?php FlashMessageAsset::register($this)?>
