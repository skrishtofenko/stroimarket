<?php
use common\components\ImageManager\ImageManager;
use frontend\models\Categories;

/** @var $categories Categories[] */
/** @var $columnsCount integer */
/** @var $ajaxCategoriesLinks boolean */
/** @var $categoryHref string */

$columnsCount = isset($columnsCount) ? $columnsCount : 3;
$ajaxCategoriesLinks = isset($ajaxCategoriesLinks) ? $ajaxCategoriesLinks : false;
$categoryHref = isset($categoryHref) ? $categoryHref : 'assortment';
?>
<?php foreach($categories as $chapter):?>
    <?php
    /** @var $chapter Categories */
    $openedShown = false;
    $addonClass = '';
    if(!$openedShown) {
        $addonClass = ' _is-open';
    }
    ?>
    <section class="assortment__category<?=$addonClass?>" data-accordion>
        <div class="assortment__category-name">
            <h4><?=$chapter->name?></h4>
            <button class="accordion-title" type="button" data-show="Показать" data-hide="Скрыть">
                <?=($openedShown ? 'Показать' : 'Скрыть')?>
            </button>
        </div>
        <div class="accordion-content"<?=($openedShown ? ' hidden' : '')?>>
            <div class="grid">
                <?php foreach($chapter->children as $category):?>
                    <?php /** @var $category Categories */?>
                    <div class="grid__column_1-<?=$columnsCount?>">
                        <div class="categories assortment__categories">
                            <?php $categoryImg = ImageManager::getUrl($category, 'image', 390, 270, ImageManager::RESIZE_ZOOM_IN)?>
                            <div class="categories__img" style="background-image: url(<?=$categoryImg?>);"></div>
                            <h4 class="categories__heading"><?=$category->name?></h4>
                            <ul class="categories__list" id="cg-<?=$category->id?>">
                                <?php foreach($category->children as $subcategory):?>
                                    <?php /** @var $subcategory Categories */?>
                                    <li data-accordion>
                                        <span class="accordion-title"><?=$subcategory->name?></span>
                                        <ul class="accordion-content" hidden>
                                            <?php foreach($subcategory->children as $goodscategory):?>
                                                <?php /** @var $goodscategory Categories */?>
                                                <li>
                                                    <?php
                                                    $linkParams
                                                        = $ajaxCategoriesLinks
                                                        ? "class='link ajax-category-link' data-category-id='{$goodscategory->id}'"
                                                        : 'class="link"';
                                                    ?>
                                                    <a <?=$linkParams?> href="/<?=$categoryHref?>/<?=$goodscategory->id?>" data-count="<?=$goodscategory->visibleProductsCount?>"><?=$goodscategory->name?></a>
                                                </li>
                                            <?php endforeach;?>
                                        </ul>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                            <?php if(count($category->children) > 5):?>
                                <button class="categories__show-more" type="button" data-show="Показать ещё" data-hide="Скрыть" onclick="categories.toggle('#cg-<?=$category->id?>')">
                                    Показать ещё
                                </button>
                            <?php endif;?>
                        </div>
                    </div>
                <?php endforeach;?>
                <div class="grid__column_1-<?=$columnsCount?>">
                    <div class="categories assortment__categories coming_soon">
                        Новые категории товаров появятся совсем скоро
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php //$openedShown = true;?>
<?php endforeach;?>
