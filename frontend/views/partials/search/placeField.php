<?php
use yii\bootstrap\ActiveForm;
use frontend\models\Cities;
use frontend\models\forms\PS;
use frontend\models\forms\CS;
use frontend\assets\lib\CityChooserAsset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm PS|CS */
/* @var $action string */

$cities = Cities::getMapWithTop();
?>
<button class="button button_action assortment__place-chooser" type="button" onclick="modal.open('#choose-city')">
    <?php
    $selected = 'По всем городам';
    if(!empty($searchForm->c)) {
        if(count($searchForm->c) == 1) {
            $selected = Cities::getList()[array_keys($searchForm->c)[0]];
        } else {
            $selected = 'Выбрано городов: ' . count($searchForm->c);
        }
    }
    ?>
    <span><?=$selected?></span>
</button>

<div class="modal modal_middle" id="choose-city" data-choose-cities>
    <div class="container modal__container">
        <div class="grid">
            <div class="grid__column_1-2 grid__column_center">
                <div class="modal__overlay"></div>
                <header class="modal__header modal__header_compact">
                    <button class="modal__close" type="button">Закрыть</button>
                    <h3 class="modal__heading">
                        Выберите город
                    </h3>
                </header>
                <div class="modal__body modal__body_compact">
                    <div class="choose-city">
                        <input class="input choose-city__search" id="city-chooser-search" type="search" placeholder="Поиск">
                        <?php $form = ActiveForm::begin([
                            'action' => $action,
                            'method' => 'get',
                            'fieldConfig' => [
                                'template' => '{input}',
                                'options' => ['tag' => false]
                            ],
                        ]);?>
                            <div class="form choose-city__form">
                                <fieldset class="_is-hidden">
                                    <legend>Выбранные города</legend>
                                </fieldset>
                                <?php if(!empty($cities['top'])):?>
                                    <fieldset>
                                        <legend>Топ города</legend>
                                        <?php foreach($cities['top'] as $city):?>
                                            <?php /** @var $city Cities */ ?>
                                            <p>
                                                <?=$form->field($searchForm, "c[$city->id]")
                                                    ->checkbox(['id' => "city-$city->id", 'uncheck' => null, 'data-id' => "city-$city->id", 'data-name' => mb_strtolower($city->name, 'utf-8')], false)?>
                                                <label for="city-<?=$city->id?>"><?=$city->name?></label>
                                            </p>
                                        <?php endforeach;?>
                                    </fieldset>
                                <?php endif;?>
                                <?php if(!empty($cities['other'])):?>
                                    <?php
                                    $firstLetter = ucfirst(mb_substr($cities['other'][0]->name, 0, 1, 'utf-8'));
                                    ?>
                                    <fieldset>
                                        <legend><?=$firstLetter?></legend>
                                        <?php foreach($cities['other'] as $city):?>
                                            <?php /** @var $city Cities */ ?>
                                            <?php $newFirstLetter = ucfirst(mb_substr($city->name, 0, 1, 'utf-8'))?>
                                            <?php if($newFirstLetter != $firstLetter):?>
                                                <?php $firstLetter = $newFirstLetter?>
                                                </fieldset>
                                                <fieldset>
                                                    <legend><?=$firstLetter?></legend>
                                            <?php endif;?>
                                            <p>
                                                <?=$form->field($searchForm, "c[$city->id]")
                                                    ->checkbox(['id' => "city-$city->id", 'data-name' => mb_strtolower($city->name, 'utf-8'), 'uncheck' => null], false)?>
                                                <label for="city-<?=$city->id?>"><?=$city->name?></label>
                                            </p>
                                        <?php endforeach;?>
                                    </fieldset>
                                <?php endif;?>
                            </div>
                            <div class="grid grid_small">
                                <div class="grid__column_1-3">
                                    <button class="button button_secondary form__button" type="button" onclick="modal.close('#choose-city')">
                                        Отменить
                                    </button>
                                </div>
                                <div class="grid__column_2-3">
                                    <button class="button form__button" type="button" onclick="$(this).parents('form').submit()">
                                        Подтвердить
                                    </button>
                                </div>
                            </div>
                        <?php ActiveForm::end();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
CityChooserAsset::register($this);
?>
