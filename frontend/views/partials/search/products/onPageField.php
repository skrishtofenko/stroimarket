<?php
use frontend\helpers\SearchHelper;
use frontend\models\forms\PS;

/* @var $this yii\web\View */
/* @var $searchForm PS */

$searchForm = $this->params['productSearch'];

$currentOnPage = isset($searchForm->op) ? $searchForm->op : PS::DEFAULT_ON_PAGE;
?>
<div class="layout-view__count">
    <?php foreach(PS::AVAILABLE_ON_PAGE as $count):?>
        <?php
        $href = SearchHelper::makeGetString('PS', null, null, ['op' => $count]);
        $addonClass = '';
        if($currentOnPage == $count) {
            $addonClass = ' layout-view__count-link_active';
        }
        ?>
        <a class="layout-view__count-link<?=$addonClass?>" href="<?=$href?>"><?=($count ?: 'все')?></a>
    <?php endforeach;?>
</div>