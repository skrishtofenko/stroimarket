<?php
use yii\bootstrap\ActiveForm;
use frontend\helpers\SearchHelper;
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\Characteristics;
use frontend\models\forms\PS;
use frontend\assets\lib\ProductSearchAsset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm PS */
/* @var $stayHere boolean */
/* @var $characteristics Characteristics[]|null */
/* @var $minCost integer|null */
/* @var $maxCost integer|null */

$stayHere = isset($stayHere) ? $stayHere : false;
$action = $stayHere ? '/' . Yii::$app->request->pathInfo : '/catalog/search';
$action .= SearchHelper::makeGetString('PS', ['a', 'ch', 'm', 'd', 'p', 'pf', 'pt']);
$searchForm = $this->params['productSearch'];
$manufacturers = isset($manufacturers) ? $manufacturers : Companies::getManufacturersList();
$manufacturersFirst5 = array_slice($manufacturers, 0, 5, true);
$manufacturersOthers = array_slice($manufacturers, 5, null, true);
$characteristics = isset($characteristics) ? $characteristics : [];
$minCost = isset($minCost) ? floor($minCost) : 0;
$maxCost = isset($maxCost) ? ceil($maxCost) : 0;
?>
<?php $form = ActiveForm::begin([
    'action' => $action,
    'method' => 'get',
    'id' => 'sidebar-product-search-form',
    'fieldConfig' => [
        'template' => '{input}',
        'options' => ['tag' => false]
    ],
    'options' => ['class' => 'search-input-form w100']
]);?>
    <div class="aside-filter">
        <?php if($this->params['currentUserRole'] == Users::ROLE_BUYER):?>
            <?php if($minCost && $maxCost && $minCost != $maxCost):?>
                <script type="application/javascript">
                    var minCost = <?=$minCost?>,
                        maxCost = <?=$maxCost?>,
                        curMinCost = <?=(isset($searchForm->pf) && !empty($searchForm->pf) ? $searchForm->pf : $minCost)?>,
                        curMaxCost = <?=(isset($searchForm->pt) && !empty($searchForm->pt) ? $searchForm->pt : $maxCost)?>;
                </script>
                <p class="aside-filter__caption">
                    Цена, руб
                </p>
                <div class="range-filter">
                    <div class="range-filter__inputs-wrapper">
                        <?=$form->field($searchForm, "pf")
                            ->textInput(['class' => 'input range-filter__input', 'id' => 'range-input-from', 'placeholder' => 'от'])?>
                        <?=$form->field($searchForm, "pt")
                            ->textInput(['class' => 'input range-filter__input', 'id' => 'range-input-to', 'placeholder' => 'до'])?>
                    </div>
                    <div class="range-filter__slider" id="range-slider"></div>
                </div>
            <?php endif;?>

            <?php /*if(Users::getCurrentUserCity()):?>
                <p class="aside-filter__caption">
                    Доставка
                </p>
                <ul class="aside-filter__list">
                    <li>
                        <?=$form->field($searchForm, "d")
                            ->checkbox(['class' => 'checkbox', 'uncheck' => null], false)?>
                        <label for="ps-d">Есть доставка по моему городу</label>
                    </li>
                    <li>
                        <?=$form->field($searchForm, "p")
                            ->checkbox(['class' => 'checkbox', 'uncheck' => null], false)?>
                        <label for="ps-p">Магазины из других городов с доставкой в мой город</label>
                    </li>
                </ul>
            <?php endif;*/?>
        <?php endif;?>
        <?php if(!empty($manufacturers)):?>
            <p class="aside-filter__caption">
                Производитель
            </p>
            <ul class="aside-filter__list">
                <?php foreach($manufacturersFirst5 as $manufacturerId => $manufacturerName):?>
                    <li>
                        <?=$form->field($searchForm, "m[$manufacturerId]")
                            ->checkbox(['class' => 'checkbox', 'id' => "manufacturer-$manufacturerId", 'uncheck' => null], false)?>
                        <label for='manufacturer-<?=$manufacturerId?>'><?=$manufacturerName?></label>
                    </li>
                <?php endforeach;?>
            </ul>
            <?php if(!empty($manufacturersOthers)):?>
            <div class="" data-accordion>
                <ul class="aside-filter__list accordion-content" hidden>
                    <?php foreach($manufacturersOthers as $manufacturerId => $manufacturerName):?>
                        <li>
                            <?=$form->field($searchForm, "m[$manufacturerId]")
                                ->checkbox(['class' => 'checkbox', 'id' => "manufacturer-$manufacturerId", 'uncheck' => null], false)?>
                            <label for='manufacturer-<?=$manufacturerId?>'><?=$manufacturerName?></label>
                        </li>
                    <?php endforeach;?>
                </ul>
                <span class="accordion-title aside-filter__show-all" data-show="Показать всё" data-hide="Скрыть">
                    Показать всё
                </span>
            </div>
        <?php endif;?>
        <?php endif;?>

        <?php if(!empty($characteristics)):?>
            <?php foreach($characteristics as $characteristic):?>
                <?php if(in_array($characteristic->type, [Characteristics::TYPE_LIST, Characteristics::TYPE_BOOLEAN])):?>
                    <p class="aside-filter__caption">
                        <?=$characteristic->name?>
                    </p>
                    <ul class="aside-filter__list">
                        <?php if($characteristic->type == Characteristics::TYPE_LIST):?>
                            <?php foreach($characteristic->getOptions() as $value):?>
                                <li>
                                    <?=$form->field($searchForm, "ch[$characteristic->id][$value]")
                                        ->checkbox(['class' => 'checkbox', 'id' => "characteristics-{$characteristic->id}-$value", 'uncheck' => null], false)?>
                                    <label for='characteristics-<?=$characteristic->id?>-<?=$value?>'><?=$value?></label>
                                </li>
                            <?php endforeach;?>
                        <?php endif;?>
                        <?php if($characteristic->type == Characteristics::TYPE_BOOLEAN):?>
                            <?=$form->field($searchForm, "ch[$characteristic->id]")
                                ->radioList($characteristic->getOptions(), [
                                    'item' => function($index, $label, $name, $checked, $value) use ($characteristic) {
                                        $checked = $checked ? ' checked' : '';
                                        $label = $label ?: 'Не важно';
                                        return "<li>
                                                <input type='radio' class='checkbox' id='characteristic-{$characteristic->id}-$value' name='$name' value='$value'$checked>
                                                <label for='characteristic-{$characteristic->id}-$value'>$label</label>
                                            </li>";
                                        },
                                    ])
                                ->label(false);?>
                        <?php endif;?>
                    </ul>
                <?php endif;?>
            <?php endforeach;?>
        <?php endif;?>

        <?php if($this->params['currentUserRole'] == Users::ROLE_COMPANY):?>
            <p class="aside-filter__caption">
                Акция
            </p>
            <?=$form->field($searchForm, 'a')
                ->checkbox(['class' => 'checkbox', 'uncheck' => null], false)?>
            <label for="ps-a">Наличие акции</label>
        <?php endif;?>
        <a class="aside-filter__discard" href="<?=$action?>">
            <span>Сбросить фильтры</span>
        </a>
    </div>
<?php ActiveForm::end();?>
<?php
ProductSearchAsset::register($this);
?>


