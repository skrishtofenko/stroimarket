<?php
use yii\bootstrap\ActiveForm;
use frontend\helpers\SearchHelper;
use frontend\models\forms\PS;
use frontend\assets\lib\ProductSearchAsset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm PS */
/* @var $stayHere boolean */

$stayHere = isset($stayHere) ? $stayHere : false;
$action = $stayHere ? '/' . Yii::$app->request->pathInfo : '/catalog/search';
$action .= SearchHelper::makeGetString('PS', ['a']);
$searchForm = $this->params['productSearch'];
?>
<?php $form = ActiveForm::begin([
    'action' => $action,
    'method' => 'get',
    'fieldConfig' => [
        'template' => '{input}',
        'options' => ['tag' => false]
    ],
    'options' => ['class' => 'search-input-form']
]);?>
    <?=$form->field($searchForm, 'a')
        ->checkbox(['class' => 'switcher assortment__switcher'], false)?>
    <label for="ps-a" data-checked="акция" data-not-checked="акция"></label>
<?php ActiveForm::end();?>
<?php
ProductSearchAsset::register($this);
?>


