<?php
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use kartik\typeahead\Typeahead;
use frontend\helpers\SearchHelper;
use frontend\models\forms\PS;
use frontend\assets\lib\ProductSearchAsset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm PS */
/* @var $stayHere boolean */
/* @var $forLanding boolean */

$forLanding = isset($forLanding) ? $forLanding : false;
$stayHere = isset($stayHere) ? $stayHere : false;
$action = $stayHere ? '/' . Yii::$app->request->pathInfo : '/catalog/search';
$action .= SearchHelper::makeGetString('PS', ['q']);
$searchForm = $this->params['productSearch'];
$formOptions = $forLanding ? [] : ['class' => 'search-input-form w100'];
$placeholder = $forLanding ? 'Что вы ищете? Например: Knauf Ротбанд или Tarkett Cinema Мерлин' : 'Поиск';
?>
<?php $form = ActiveForm::begin([
    'action' => $action,
    'method' => 'get',
    'fieldConfig' => [
        'template' => '{input}',
        'options' => ['tag' => false]
    ],
    'options' => $formOptions,
]);?>
    <?php if($forLanding):?>
        <div class="input-row landing-promo__input-row">
    <?php endif;?>
    <?=$form->field($searchForm, 'q')->widget(Typeahead::classname(), [
            'pluginOptions' => ['highlight' => true],
            'scrollable' => true,
            'options' => [
                'placeholder' => $placeholder,
                'class' => 'input',
                'type' => 'search',
                'required' => true,
            ],
            'dataset' => [[
                'display' => 'name',
                'limit' => 1000,
                'remote' => [
                    'url' => '/catalog/products-hints?q=%QUERY',
                    'wildcard' => '%QUERY'
                ],
                'templates' => [
                    'suggestion' => new JsExpression("function (data) {return '<p>' + data.name + '</p>';}")
                ]
            ]]
        ])?>
    <?php if($forLanding):?>
            <button class="button search-products" type="submit">
                Найти
            </button>
        </div>
    <?php endif;?>
<?php ActiveForm::end();?>
<?php
ProductSearchAsset::register($this);
?>
