<?php
use yii\bootstrap\ActiveForm;
use frontend\helpers\SearchHelper;
use frontend\models\forms\PS;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm PS */
/* @var $isOwner boolean */

$searchForm = $this->params['productSearch'];

$currentSortField = isset($searchForm->s) ? $searchForm->s : PS::DEFAULT_SORT_FIELD;
$currentSortOrder = isset($searchForm->so) ? $searchForm->so : PS::DEFAULT_SORT_ORDER;
$isOwner = isset($isOwner) ? $isOwner : false;
?>
<span class="layout-view__title">Сортировать:</span>
<?php foreach(PS::getAvailableSortFields($this->params['currentUserRole']) as $sortField => $caption):?>
    <?php
    if($isOwner && $sortField != 'name') {
        continue;
    }
    $sortOrder = $currentSortField == $sortField ? $currentSortOrder * -1 : PS::DEFAULT_SORT_ORDER;
    $href = SearchHelper::makeGetString('PS', null, null, ['s' => $sortField, 'so' => $sortOrder]);
    $addonClass = '';
    if($currentSortField == $sortField) {
        $addonClass = ' layout-view__sort-link_' . ($currentSortOrder == PS::DEFAULT_SORT_ORDER ? 'down' : 'up');
    }
    ?>
    <a class="layout-view__sort-link<?=$addonClass?>" href="<?=$href?>"><?=$caption?></a>
<?php endforeach;?>
