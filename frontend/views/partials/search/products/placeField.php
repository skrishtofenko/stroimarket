<?php
use yii\bootstrap\ActiveForm;
use frontend\helpers\SearchHelper;
use frontend\models\forms\PS;
use frontend\assets\lib\ProductSearchAsset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm PS */
/* @var $stayHere boolean */

$stayHere = isset($stayHere) ? $stayHere : false;
$action = $stayHere ? '/' . Yii::$app->request->pathInfo : '/catalog/search';
$action .= SearchHelper::makeGetString('PS', ['c']);
$searchForm = $this->params['productSearch'];

echo $this->render('../placeField', [
    'searchForm' => $searchForm,
    'action' => $action,
]);
ProductSearchAsset::register($this);
