<?php
use yii\bootstrap\ActiveForm;
use frontend\helpers\SearchHelper;
use frontend\models\Companies;
use frontend\models\forms\CS;
use frontend\assets\lib\CompanySearchAsset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm CS */
$searchForm = $this->params['companiesSearch'];
?>
<?php if($searchForm->type == Companies::TYPE_PROVIDER):?>
    <?php $form = ActiveForm::begin([
        'action' => '/' . Yii::$app->request->pathInfo . SearchHelper::makeGetString('CS', ['m']),
        'method' => 'get',
        'fieldConfig' => [
            'template' => '{input}',
            'options' => ['tag' => false]
        ],
        'options' => ['class' => 'search-input-form w100']
    ]);?>
        <?=$form->field($searchForm, 'm')
            ->checkbox(['class' => 'checkbox layout-view__checkbox', 'uncheck' => null], false)?>
        <label for="cs-m">Производитель</label>
    <?php ActiveForm::end();?>
<?php endif;?>
<?php
CompanySearchAsset::register($this);
?>
