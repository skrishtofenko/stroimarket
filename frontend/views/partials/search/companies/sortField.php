<?php
use yii\bootstrap\ActiveForm;
use frontend\helpers\SearchHelper;
use frontend\models\Users;
use frontend\models\forms\CS;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchFormTitle string */
/* @var $searchForm CS */

$searchFormTitle = isset($searchFormTitle) ? $searchFormTitle : 'companiesSearch';
$searchFormTitleAbbr = preg_replace('/[^A-Z]/','', ucfirst($searchFormTitle));

$baseHref = '/' . Yii::$app->request->pathInfo;
$searchForm = $this->params[$searchFormTitle];

$currentSortField = isset($searchForm->s) ? $searchForm->s : CS::DEFAULT_SORT_FIELD;
$currentSortOrder = isset($searchForm->so) ? $searchForm->so : CS::DEFAULT_SORT_ORDER;
?>
<span class="layout-view__title">Сортировать:</span>
<?php foreach(CS::getAvailableSortFields($this->params['currentUserRole'] == Users::ROLE_BUYER) as $sortField => $caption):?>
    <?php
    $sortOrder = $currentSortField == $sortField ? $currentSortOrder * -1 : CS::DEFAULT_SORT_ORDER;
    $href = SearchHelper::makeGetString($searchFormTitleAbbr, null, null, ['s' => $sortField, 'so' => $sortOrder]);
    $addonClass = '';
    if($currentSortField == $sortField) {
        $addonClass = ' layout-view__sort-link_' . ($currentSortOrder == CS::DEFAULT_SORT_ORDER ? 'down' : 'up');
    }
    ?>
    <a class="layout-view__sort-link<?=$addonClass?>" href="<?=$href?>"><?=$caption?></a>
<?php endforeach;?>
