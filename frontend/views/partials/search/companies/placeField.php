<?php
use yii\bootstrap\ActiveForm;
use frontend\helpers\SearchHelper;
use frontend\models\forms\CS;
use frontend\assets\lib\CompanySearchAsset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm CS */
/* @var $forProductPage boolean */

$forProductPage = isset($forProductPage) ? $forProductPage : false;

if($forProductPage) {
    $mainCompaniesForm = 'providersCompaniesSearch';
    $clearModels = ['SCS'];
} else {
    $mainCompaniesForm = 'companiesSearch';
    $clearModels = [];
}
$mainCompaniesModel = preg_replace('/[^A-Z]/','', ucfirst($mainCompaniesForm));
$clearModels[] = $mainCompaniesModel;

$action = '/' . Yii::$app->request->pathInfo;
$action .= SearchHelper::makeGetString($clearModels, ['c']);
$searchForm = $this->params[$mainCompaniesForm];

echo $this->render('../placeField', [
    'searchForm' => $searchForm,
    'action' => $action,
]);
CompanySearchAsset::register($this);

