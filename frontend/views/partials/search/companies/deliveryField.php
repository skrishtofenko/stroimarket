<?php
use yii\bootstrap\ActiveForm;
use frontend\helpers\SearchHelper;
use frontend\models\Users;
use frontend\models\forms\SCS;
use frontend\assets\lib\CompanySearchAsset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm SCS */
$searchForm = $this->params['storesCompaniesSearch'];
?>
<?php $form = ActiveForm::begin([
    'action' => '/' . Yii::$app->request->pathInfo . SearchHelper::makeGetString('SCS', ['mc', 'id']),
    'method' => 'get',
    'fieldConfig' => [
        'template' => '{input}',
        'options' => ['tag' => false]
    ],
    'options' => ['class' => 'layout-view__right-side'],
]);?>
    <?php if(Users::getCurrentUserCity()):?>
        <?=$form->field($searchForm, 'mc')
            ->checkbox(['class' => 'checkbox layout-view__checkbox', 'uncheck' => null], false)?>
        <label for="scs-mc">Только мой город</label>
    <?php endif;?>

    <?=$form->field($searchForm, 'id')
        ->checkbox(['class' => 'checkbox layout-view__checkbox', 'uncheck' => null], false)?>
    <label for="scs-id">Есть доставка</label>
<?php ActiveForm::end();?>
<?php
CompanySearchAsset::register($this);
?>
