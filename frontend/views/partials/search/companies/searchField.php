<?php
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;
use kartik\typeahead\Typeahead;
use frontend\helpers\SearchHelper;
use frontend\models\forms\CS;
use frontend\assets\lib\CompanySearchAsset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $searchForm CS */
/* @var $fromPage string */

$action = '/' . Yii::$app->request->pathInfo . SearchHelper::makeGetString('CS', ['q']);
$searchForm = $this->params['companiesSearch'];
$fromPage = isset($fromPage) ? $fromPage : 'company';
?>
<?php $form = ActiveForm::begin([
    'action' => $action,
    'method' => 'get',
    'fieldConfig' => [
        'template' => '{input}',
        'options' => ['tag' => false]
    ],
    'options' => ['class' => 'search-input-form w100']
]);?>
    <?=$form->field($searchForm, 'q')->widget(Typeahead::classname(), [
            'pluginOptions' => ['highlight' => true],
            'scrollable' => true,
            'options' => [
                'placeholder' => 'Поиск',
                'class' => 'input',
                'type' => 'search',
            ],
            'dataset' => [[
                'display' => 'name',
                'limit' => 1000,
                'remote' => [
                    'url' => '/catalog/companies-hints?q=%QUERY',
                    'wildcard' => '%QUERY'
                ],
                'templates' => [
                    'suggestion' => new JsExpression("function (data) {return '<p>' + data.name + '</p>';}")
                ]
            ]]
        ])?>
<?php ActiveForm::end();?>
<script type="text/javascript">
    var searchResultPage = '<?=$fromPage?>';
</script>
<?php
CompanySearchAsset::register($this);
?>
