<?php
use frontend\helpers\SearchHelper;
use frontend\models\forms\PS;
use frontend\models\forms\CS;

/* @var $this yii\web\View */
/* @var $searchForm PS|CS */
/* @var $form string */
/* @var $formClass string */
/* @var $cssClass string */

$searchForm = $this->params[$form];

$pagesCount = $searchForm->op ? ceil($searchForm->found / $searchForm->op) : 1;
$currentPage = isset($searchForm->page) ? $searchForm->page : 1;
$currentPage = min($currentPage, $pagesCount);

$visiblePages = [1];
if($pagesCount > 1) {
    $visiblePages = [];
    if($pagesCount < 8) {
        for($i = 1; $i <= $pagesCount; $i++) {
            $visiblePages[] = $i;
        }
    } else {
        if($currentPage > 1) $visiblePages[] = 1;
        if($currentPage > 5) $visiblePages[] = '...';
        $startNumber = $currentPage > 5 ? $currentPage - 2 : 2;
        for($i = $startNumber; $i < $currentPage; $i++) {
            $visiblePages[] = $i;
        }
        $visiblePages[] = $currentPage;
        $endNumber = $currentPage <= $pagesCount - 5 ? $currentPage + 2 : $pagesCount - 1;
        for($i = $currentPage + 1; $i <= $endNumber; $i++) {
            $visiblePages[] = $i;
        }
        if($currentPage <= $pagesCount - 5) $visiblePages[] = '...';
        if($currentPage < $pagesCount) $visiblePages[] = $pagesCount;
    }
}

if(!function_exists('makePagingHref')) {
    function makePagingHref ($condition, $page, $formClass) {
        if($condition) {
            $href = SearchHelper::makeGetString($formClass, null, null, ['page' => $page]);
            return " href='$href'";
        }
        return '';
    }
}
?>
<section class="pagination <?=$cssClass?>__pagination">
    <?php $href = makePagingHref($currentPage > 1, $currentPage - 1, $formClass)?>
    <a class="pagination__item"<?=$href?>>Предыдущая</a>
    <?php foreach($visiblePages as $page):?>
        <?php if($page == '...'):?>
            <a class="pagination__item">…</a>
        <?php else:?>
            <?php $href = makePagingHref($currentPage != $page, $page, $formClass)?>
            <a class="pagination__item<?=($page == $currentPage ? ' _is-current' : '')?>"<?=$href?>><?=$page?></a>
        <?php endif;?>
    <?php endforeach;?>
    <?php $href = makePagingHref($currentPage < $pagesCount, $currentPage + 1, $formClass)?>
    <a class="pagination__item"<?=$href?>>Следующая</a>
</section>
