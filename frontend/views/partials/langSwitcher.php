<?php
use frontend\models\Users;

$languages = Users::USER_LANGUAGES;
$currentLocale = Yii::$app->language;
$languageShortName = $languages[Users::DEFAULT_USER_LANGUAGE]['shortName'];
foreach($languages as $language) {
    if($language['locale'] == $currentLocale) {
        $languageShortName = $language['shortName'];
    }
}
?>
<li class="user-actions__item lang-switcher lang-switcher_<?=$languageShortName?>">
    <button class="button lang-switcher__button" type="button">
        <?=$languages[$languageShortName]['selfName']?>
    </button>
    <ul class="list-reset lang-switcher__list">
        <?php foreach($languages as $language):?>
            <?php $href = ($language['shortName'] != $languageShortName ? " href='/site/set-user-language/?lang={$language['shortName']}'" : '')?>
            <li><a<?=$href?>><?=$language['selfName']?></a></li>
        <?php endforeach;?>
    </ul>
</li>
