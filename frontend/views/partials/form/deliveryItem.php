<?php
use frontend\models\forms\CompanyInfoForm;
use frontend\models\Deliveries;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $company CompanyInfoForm */
/* @var $cities array */
/* @var $selectedCityId integer */
/* @var $selectedTypeId integer */

$selectedCityId = isset($selectedCityId) ? $selectedCityId : null;
$citiesDropDownOptions = ['class' => 'select form__select delivery-city'];
if($selectedCityId) {
    $citiesDropDownOptions['value'] = $selectedCityId;
}

$selectedTypeId = isset($selectedTypeId) ? $selectedTypeId : null;
$typesDropDownOptions = ['class' => 'select form__select delivery-type'];
if($selectedTypeId) {
    $typesDropDownOptions['value'] = $selectedTypeId;
}
?>
<div class="registration-delivery-item">
    <?=$form->field($company, "delivery_city_id[]")
        ->dropDownList($cities, $citiesDropDownOptions)
        ->label(false);?>
    <?=$form->field($company, "delivery_type[]")
        ->dropDownList(Deliveries::types(), $typesDropDownOptions)
        ->label(false);?>
</div>
