<?php
use common\helpers\SMHelper;
use frontend\models\Companies;
use frontend\assets\lib\TimeMaskAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $company Companies */

$leftColumnClass = isset($leftColumnClass) ? $leftColumnClass : '5-9';
$columnsClasses = SMHelper::makeColumnClasses($leftColumnClass);

$daysOfWeek = [
    1 => [
        'nameRus' => 'Понедельник',
        'eng3' => 'mon'
    ], [
        'nameRus' => 'Вторник',
        'eng3' => 'tue'
    ], [
        'nameRus' => 'Среда',
        'eng3' => 'wed'
    ], [
        'nameRus' => 'Четверг',
        'eng3' => 'thu'
    ], [
        'nameRus' => 'Пятница',
        'eng3' => 'fri'
    ], [
        'nameRus' => 'Суббота',
        'eng3' => 'sat'
    ], [
        'nameRus' => 'Воскресенье',
        'eng3' => 'sun'
    ],
];
?>

<div class="grid form__row address-block">
    <div class="grid__column_<?=$columnsClasses['left']?>">
        <p class="form__label">
            Время работы (местное):
        </p>
    </div>
    <div class="grid__column_<?=$columnsClasses['right']?>">
        <button class="button button_action"
                id="set-operating-time"
                type="button" onclick="modal.open('#operating-time')">
            Установить время работы
        </button>
        <div class="operating-time-wrapper"></div>
    </div>
</div>

<div class="modal" id="operating-time">
    <div class="container modal__container">
        <div class="grid">
            <div class="grid__column_1-2 grid__column_center">
                <div class="modal__overlay"></div>
                <header class="modal__header">
                    <button class="modal__close" type="button">Закрыть</button>
                    <h3 class="modal__heading">
                        Время работы
                    </h3>
                </header>
                <div class="modal__body">
                    <ul class="list-reset registration__time-input">
                        <?php foreach ($daysOfWeek as $num => $day):?>
                            <li>
                                <b><?=$day['nameRus']?></b>
                                <div class="grid grid_medium">
                                    <div class="grid__column_1-2">
                                        <p>Рабочие часы:</p>
                                        <div class="grid grid_small">
                                            <div class="grid__column_1-2">
                                                <?=$form->field($company, "{$day['eng3']}From")
                                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "От", 'data-id' => "work-start-$num"])?>
                                            </div>
                                            <div class="grid__column_1-2">
                                                <?=$form->field($company, "{$day['eng3']}To")
                                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "До", 'data-id' => "work-end-$num"])?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid__column_1-2">
                                        <p>Обеденные часы:</p>
                                        <div class="grid grid_small">
                                            <div class="grid__column_1-2">
                                                <?=$form->field($company, "{$day['eng3']}DinnerFrom")
                                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "От", 'data-id' => "lunch-start-$num"])?>
                                            </div>
                                            <div class="grid__column_1-2">
                                                <?=$form->field($company, "{$day['eng3']}DinnerTo")
                                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "До", 'data-id' => "lunch-end-$num"])?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach;?>
                    </ul>
                    <button class="button" type="button" onclick="operatingTime.save()">
                        Сохранить и закрыть
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<template id="operating-time-template">
    <table class="operating-time registration__operating-time">
        <thead>
        <tr>
            <th>пн</th>
            <th>вт</th>
            <th>ср</th>
            <th>чт</th>
            <th>пт</th>
            <th>сб</th>
            <th>вс</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <span data-id="work-start-1"></span>
                <span data-id="work-end-1"></span>
            </td>
            <td>
                <span data-id="work-start-2"></span>
                <span data-id="work-end-2"></span>
            </td>
            <td>
                <span data-id="work-start-3"></span>
                <span data-id="work-end-3"></span>
            </td>
            <td>
                <span data-id="work-start-4"></span>
                <span data-id="work-end-4"></span>
            </td>
            <td>
                <span data-id="work-start-5"></span>
                <span data-id="work-end-5"></span>
            </td>
            <td>
                <span data-id="work-start-6"></span>
                <span data-id="work-end-6"></span>
            </td>
            <td>
                <span data-id="work-start-7"></span>
                <span data-id="work-end-7"></span>
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <span>обед</span>
            </td>
        </tr>
        <tr>
            <td>
                <span data-id="lunch-start-1"></span>
                <span data-id="lunch-end-1"></span>
            </td>
            <td>
                <span data-id="lunch-start-2"></span>
                <span data-id="lunch-end-2"></span>
            </td>
            <td>
                <span data-id="lunch-start-3"></span>
                <span data-id="lunch-end-3"></span>
            </td>
            <td>
                <span data-id="lunch-start-4"></span>
                <span data-id="lunch-end-4"></span>
            </td>
            <td>
                <span data-id="lunch-start-5"></span>
                <span data-id="lunch-end-5"></span>
            </td>
            <td>
                <span data-id="lunch-start-6"></span>
                <span data-id="lunch-end-6"></span>
            </td>
            <td>
                <span data-id="lunch-start-7"></span>
                <span data-id="lunch-end-7"></span>
            </td>
        </tr>
        </tbody>
    </table>
</template>
<template id="edit-operating-time">
    <button class="button registration__edit-time"
            type="button" onclick="modal.open('#operating-time')">
        Редактировать
    </button>
</template>
<?php
TimeMaskAsset::register($this);
?>

