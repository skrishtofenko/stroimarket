<?php
use common\helpers\SMHelper;
use frontend\models\forms\CompanyInfoForm;
use frontend\assets\lib\DeliveryBlockAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $company CompanyInfoForm */
/* @var $cities array */

$leftColumnClass = isset($leftColumnClass) ? $leftColumnClass : '5-9';
$columnsClasses = SMHelper::makeColumnClasses($leftColumnClass);
?>

<template id="registration-delivery-template">
    <div class="grid form__row">
        <div class="grid__column_<?=$columnsClasses['left']?>">
            <p class="form__label">
                Доставка:
            </p>
        </div>
        <div class="grid__column_<?=$columnsClasses['right']?>">
            <button class="button registration__add-delivery" type="button" id="add-delivery-button" onclick="addDeliveryItem()">
                Добавить еще
            </button>
        </div>
    </div>
</template>
<template id="delivery-item-template">
    <?=$this->render('deliveryItem', [
        'form' => $form,
        'company' => $company,
        'cities' => $cities,
    ])?>
</template>

<div class="form__row">
    <?=$form->field($company, 'is_delivery')
        ->checkbox(['class' => 'form__switcher', 'onchange' => "registrationDelivery.toggle(event)"], false)?>
    <label for="companyinfoform-is_delivery" data-checked="да" data-not-checked="нет">
        Имеется доставка:
    </label>
</div>
<div class="form__row registration-delivery-wrapper"><?php if(!empty($company->delivery_city_id)):?>
    <div class="grid form__row">
        <div class="grid__column_<?=$columnsClasses['left']?>">
            <p class="form__label">
                Доставка
            </p>
        </div>
        <div class="grid__column_<?=$columnsClasses['right']?>">
            <?php foreach($company->delivery_city_id as $key => $cityId):?>
                <?=$this->render('deliveryItem', [
                    'form' => $form,
                    'company' => $company,
                    'cities' => $cities,
                    'selectedCityId' => $cityId,
                    'selectedTypeId' => $company->delivery_type[$key],
                ])?>
            <?php endforeach;?>
            <button class="button registration__add-delivery" type="button" id="add-delivery-button" onclick="addDeliveryItem()">
                Добавить еще
            </button>
        </div>
    </div>
<?php endif;?></div>
<?php
DeliveryBlockAsset::register($this);
?>
