<?php
use yii\helpers\Html;
use common\helpers\SMHelper;
use common\components\ImageManager\ImageManager;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model yii\db\ActiveRecord */
/* @var $attribute string */
/* @var $maxFileSize integer */
/* @var $leftColumnClass string */
/* @var $caption string */

$caption = isset($caption) ? $caption : 'Логотип';
$leftColumnClass = isset($leftColumnClass) ? $leftColumnClass : '5-9';
$columnsClasses = SMHelper::makeColumnClasses($leftColumnClass);
$inputId = Html::getInputId($model, "{$attribute}Image");
?>
<div class="grid form__row form__row_top">
    <div class="grid__column_<?=$columnsClasses['left']?>">
        <p class="form__label">
            <?=$caption?>:
        </p>
        <p class="form__label-caption">
            Файл формата .jpg, .gif или .png
            Размер файла не более <?=$maxFileSize?>МБ.
        </p>
    </div>
    <div class="grid__column_<?=$columnsClasses['right']?>">
        <div class="grid grid_medium">
            <div class="grid__column_1-2 registration__logo">
                <?php
                $labelStyle = '';
                $inputClass = 'input';
                if($model->$attribute) {
                    $labelStyle = ' style="background-image:url(' . ImageManager::getUrl($model, $attribute, null, 200) . ');"';
                    $inputClass .= ' _is-chosen';
                }
                ?>
                <?=$form->field($model, "{$attribute}Image")
                    ->fileInput(['class' => $inputClass, 'onchange' => "checkUploadImage(event, $maxFileSize)"])?>
                <label class="input" for="<?=$inputId?>"<?=$labelStyle?>>
                    <span>Нет логотипа</span>
                </label>
            </div>
            <div class="grid__column_1-2">
                <label class="button button_action" for="<?=$inputId?>">
                    Загрузить
                </label>
                <button class="button button_action_secondary"
                        type="button"
                        onclick="filePreview.remove('<?=$inputId?>')">
                    <span>Удалить</span>
                </button>
                <div class="image-errors"></div>
            </div>
        </div>
    </div>
</div>
