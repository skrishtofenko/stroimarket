<?php
use frontend\models\forms\CompanyInfoForm;
use frontend\models\Cities;
use frontend\assets\lib\CityChooserAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $company CompanyInfoForm */
/* @var $cities array */
/* @var $isWithWrapper boolean */

$isWithWrapper = isset($isWithWrapper) ? $isWithWrapper : true;
$cities = Cities::getListWithTop();
?>

<?=($isWithWrapper ? '<div class="grid grid_small">' : '')?>
    <div class="grid__column_<?=($isWithWrapper ? '1-1' : '4-7')?>">
        <button class="button button_action" id="city-chooser-button" type="button" onclick="modal.open('#choose-city')">
            Выберите город
        </button>
    </div>
    <div class="modal modal_middle" id="choose-city">
        <div class="container modal__container">
            <div class="grid">
                <div class="grid__column_1-2 grid__column_center">
                    <div class="modal__overlay"></div>
                    <header class="modal__header modal__header_compact">
                        <button class="modal__close" type="button">Закрыть</button>
                        <h3 class="modal__heading">
                            Выберите город
                        </h3>
                    </header>
                    <div class="modal__body modal__body_compact">
                        <div class="choose-city">
                            <input class="input choose-city__search" id="city-chooser-search" type="search" placeholder="Поиск">
                            <div class="form choose-city__form">
                                <?php if(!empty($cities['top'])):?>
                                    <?=$this->render('cityChooserFieldset', [
                                        'form' => $form,
                                        'company' => $company,
                                        'cities' => $cities['top'],
                                        'letter' => 'Топ города',
                                    ]);?>
                                <?php endif;?>
                                <?php
                                if(!empty($cities['other'])) {
                                    $firstLetter = '';
                                    $citiesBlock = [];
                                    foreach ($cities['other'] as $cityId => $cityName) {
                                        $newFirstLetter = ucfirst(mb_substr($cityName, 0, 1, 'utf-8'));
                                        if ($newFirstLetter != $firstLetter && $firstLetter != '') {
                                            echo $this->render('cityChooserFieldset', [
                                                'form' => $form,
                                                'company' => $company,
                                                'cities' => $citiesBlock,
                                                'letter' => $firstLetter,
                                            ]);
                                            $citiesBlock = [];
                                        }
                                        $citiesBlock[$cityId] = $cityName;
                                        $firstLetter = $newFirstLetter;
                                    }
                                    echo $this->render('cityChooserFieldset', [
                                        'form' => $form,
                                        'company' => $company,
                                        'cities' => $citiesBlock,
                                        'letter' => $firstLetter,
                                    ]);
                                }
                                echo $form->field($company, 'city_id')->hiddenInput()->label(false);
                                ?>
                            </div>
                            <div class="grid grid_small">
                                <div class="grid__column_3-5 grid__column_center">
                                    <button class="button form__button" type="button" onclick="modal.close('#choose-city');">
                                        Подтвердить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?=($isWithWrapper ? '</div>' : '')?>
<?php
CityChooserAsset::register($this);
?>
