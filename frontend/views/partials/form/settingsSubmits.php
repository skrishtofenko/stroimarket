<?php
use yii\helpers\Html;
use common\helpers\SMHelper;

/* @var $this yii\web\View */
/* @var $submitCaption string */
/* @var $totalClass string */
/* @var $leftColumnClass string */

$submitCaption = isset($submitCaption) ? $submitCaption : 'Сохранить изменения';
$totalClass = isset($totalClass) ? $totalClass : '4-9';
$leftColumnClass = isset($leftColumnClass) ? $leftColumnClass : '2-3';
$columnsClasses = SMHelper::makeColumnClasses($leftColumnClass);
?>
<div class="grid">
    <div class="grid__column_<?=$totalClass?> grid__column_right">
        <div class="grid grid_small">
            <div class="grid__column_<?=$columnsClasses['left']?>">
                <?=Html::submitButton($submitCaption, ['class' => 'button form__button'])?>
            </div>
            <div class="grid__column_<?=$columnsClasses['right']?>">
                <button class="button button_secondary form__button" type="button" onclick="window.location.href = '/profile'">
                    Отменить
                </button>
            </div>
        </div>
    </div>
</div>
