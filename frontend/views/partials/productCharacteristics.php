<?php
use frontend\models\ProductsCharacteristics;

/** @var $characteristics ProductsCharacteristics[] */
?>
<div class="grid__column_1-2">
    <table class="product-sheet__characteristics" width="100%">
        <tbody>
        <?php foreach($characteristics as $characteristic):?>
            <tr>
                <td width="50%"><?=$characteristic->characteristic->name?></td>
                <td width="50%"><span><?=$characteristic->niceValue()?></span></td>
            </tr>
        <?php endforeach;?>
        </tbody>
    </table>
</div>
