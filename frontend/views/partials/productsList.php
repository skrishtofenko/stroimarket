<?php
use frontend\models\Products;
use frontend\models\Companies;
use frontend\models\CompaniesProducts;

/* @var $products CompaniesProducts[]|Products[] */
/* @var $isOwner boolean */
/* @var $isAssortment boolean */
/* @var $company Companies */

$company = isset($company) ? $company : null;
$isOwner = isset($isOwner) ? $isOwner : false;
$isAssortment = isset($isAssortment) ? $isAssortment : false;
?>

<ul class="assortment__products _list-view" id="product-list">
    <?php foreach($products as $product):?>
        <?=$this->render('../partials/productInList', [
            'isOwner' => $isOwner,
            'isAssortment' => $isAssortment,
            'product' => $isOwner ? $product->product : $product,
            'companyProduct' => $isOwner ? $product : null,
            'company' => $company,
        ])?>
    <?php endforeach;?>
</ul>