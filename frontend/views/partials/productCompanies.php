<?php
use common\helpers\SKHelper;
use common\components\ImageManager\ImageManager;
use frontend\models\Companies;
use frontend\models\Products;

/** @var $product Products */
/** @var $companies Companies[] */
/** @var $type string */
/** @var $hidden boolean */
$forms = ['providers' => 'providersCompaniesSearch', 'stores' => 'storesCompaniesSearch'];
$form = $forms[$type];
?>
<div data-tab="<?=$type?>"<?=($hidden ? ' hidden' : '')?>>
    <div class="layout-view layout-view_no-margin">
        <?=$this->render('../partials/search/companies/sortField', ['searchFormTitle' => $form])?>
    </div>
    <ul class="product-providers">
        <?php foreach($companies as $company):?>
            <?php $user = $company->user;?>
            <li>
                <div class="product-providers__logo">
                    <img src="<?=ImageManager::getUrl($company->user, 'logo', 150, 150, ImageManager::RESIZE_ZOOM_OUT)?>" alt="">
                </div>
                <div class="product-providers__info">
                    <div class="product-providers__title">
                        <p class="product-providers__name">
                            <?=$user->legal_name?>
                        </p>
                        <?php if(!empty($user->url)):?>
                            <a class="product-providers__link" href="<?=$user->url?>" target="_blank">
                                Перейти на сайт
                            </a>
                        <?php endif;?>
                    </div>
                    <p class="product-providers__type">
                        <?=$company->typeAsText?>
                    </p>
                    <p class="product-providers__contacts">
                        <?=$company->addressAsText?><br>
                        <?php if(!empty($company->phones_array)):?>
                            <?=SKHelper::formatPhone($company->phones_array[0])?>
                        <?php endif;?>
                    </p>
                    <?php /*<span class="rating" data-value="<?=$company->rateAsNum?>" data-max="3"><span></span></span>*/?>
                </div>
                <div class="product-providers__aside">
                    <a class="button product-providers__button" href="/catalog/company/<?=$company->id?>">
                        Открыть профиль
                    </a>
                    <?php $companyProduct = $product->getForCompany($company->id)?>
                    <?php if($companyProduct->is_action):?>
                        <p class="product-providers__promotion">
                            Акция: <?=$companyProduct->cost?> руб/<?=$product->unitType->name?>
                        </p>
                    <?php endif;?>
                    <?php /*<p class="product-providers__multiplicity">
                        Кратность: <?=$companyProduct->multiplicity?>
                    </p>*/?>
                </div>
            </li>
        <?php endforeach;?>
    </ul>
</div>