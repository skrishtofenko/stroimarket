<?php
use common\helpers\SKHelper;
use common\components\ImageManager\ImageManager;
use frontend\models\Users;
use frontend\models\Products;
use frontend\models\Companies;
use frontend\models\CompaniesProducts;

/* @var $product Products */
/* @var $companyProduct CompaniesProducts|null */
/* @var $isOwner boolean */
/* @var $isAssortment boolean */
/* @var $company Companies */
?>
<li class="category-item">
    <?php if($this->params['currentUserRole'] == Users::ROLE_COMPANY && !$isAssortment && $product->actionsCnt):?>
        <div class="category-item__badge">Акции</div>
    <?php endif;?>
    <div class="category-item__img">
        <img src="<?=ImageManager::getUrl($product, 'image', 160, 160, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
    </div>
    <div class="category-item__info">
        <p class="category-item__name">
            <?=$product->fullName?>
        </p>
        <?php if(!$isOwner):?>
            <p class="category-item__type">
                <?=$product->category->name?>
            </p>
        <?php endif;?>
        <?php /*
        <p class="category-item__vendor-code">
            АРТИКУЛ СМ: <?=$product->id?>
        </p>
        */?>
        <?php if($isOwner):?>
            <p class="category-item__breadcrumbs">
                <?=$product->fullCategory?>
            </p>
        <?php else:?>
            <p class="category-item__description">
                <?=SKHelper::smartCut(strip_tags($product->description))?>
            </p>
        <?php endif;?>
    </div>
    <div class="category-item__aside<?=($this->params['currentUserRole'] == Users::ROLE_BUYER ? ' category-item__aside_centered' : '')?>">
        <?php if($this->params['currentUserRole'] == Users::ROLE_BUYER):?>
            <?php if($product->minCost):?>
                <p class="category-item__price">
                    <?=$product->minCost?> руб./<?=$product->unitType->name?>
                </p>
            <?php endif;?>
            <?php /*
            <p class="category-item__count">
                В ассортименте: <span><?=$product->assortment_count?></span>
            </p>
            */?>
            <p class="category-item__count">
                Количество предложений: <span><?=$product->availability_count?></span>
            </p>
            <a class="button button_action_secondary" href="/catalog/product/<?=$product->id?>">
                Купить
            </a>
        <?php elseif($isAssortment):?>
            <?php if($isOwner):?>
                <p class="category-item__data">
                    Ваш артикул: <?=$companyProduct->vendor_code?><br>
                    <?php /*if($companyProduct->company->type == Companies::TYPE_PROVIDER):?>
                        Кратность: <?=$companyProduct->multiplicity?> <?=$product->unitType->name?>.
                    <?php endif;*/?>
                </p>
            <?php endif;?>
            <?php $companyProduct = $companyProduct ?: $product->getForCompany($company->id)?>
            <?php if($companyProduct->is_action):?>
                <p class="category-item__promo">
                    Акция: <?=$companyProduct->cost?> руб/<?=$product->unitType->name?>
                </p>
            <?php endif;?>
        <?php else:?>
            <a class="button button_action_secondary" href="/catalog/product/<?=$product->id?>">
                Открыть
            </a>
            <p class="category-item__counts">
                Поставщики: <span><?=$product->providerCnt?></span><br>
                Магазины: <span><?=$product->storeCnt?></span>
            </p>
        <?php endif;?>
    </div>
</li>