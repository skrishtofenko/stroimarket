<?php
/** @var $isLanding boolean */

$noSuperHeader = isset($this->params['noSuperHeader']) ? $this->params['noSuperHeader'] : false;
$isLanding = isset($isLanding) ? $isLanding : false;
?>
<?php if(!$noSuperHeader):?>
    <div class="super-header<?=($isLanding ? ' super-header_landing' : '')?>">
        <div class="container super-header__container">
            <ul class="list-reset super-header__menu">
                <?php /*
                <li data-accordion>
                    <?=$this->render('superHeaderCatalog.php')?>
                </li>
                */?>
                <li><a class="link" href="/business">Для опта и бизнеса</a></li>
                <li><a class="link" href="tel:<?=Yii::$app->params['globalSettings']['contactPhone']?>"><?=Yii::$app->params['globalSettings']['contactPhone']?></a></li>
            </ul>
            <?=$this->render('userPlacement.php')?>
        </div>
    </div>
<?php endif;?>