<?php
use yii\bootstrap\ActiveForm;
use frontend\models\Cities;
use frontend\models\forms\UserPlacementForm;
use frontend\assets\lib\CityChooserAsset;

$currentCity = isset($this->params['currentUserCity']) ? $this->params['currentUserCity'] : false;
$needsApprove = isset($this->params['needsApproveUserCity']) ? $this->params['needsApproveUserCity'] : false;
$cities = Cities::getListWithTop();
/** @var UserPlacementForm $placementForm */
$placementForm = $this->params['userPlacement'];
?>

<div class="super-header__geo-container">
    <?php /*<button class="super-header__geo" type="button" onclick="modal.open('#choose-main-user-city'); deleteElement('.city-confirmation')">*/?>
    <button class="super-header__geo" type="button" onclick="modal.open('#user-placement-info')">
        <?=($currentCity ?: 'Местоположение')?>
    </button>
    <?php if($needsApprove):?>
        <div class="city-confirmation">
            <p>Ваш город</p>
            <b><?=$currentCity?></b>
            <div class="grid grid_small">
                <div class="grid__column_4-9">
                    <button class="button button_secondary" type="button" onclick="modal.open('#choose-main-user-city')">
                        Нет, другой
                    </button>
                </div>
                <div class="grid__column_5-9">
                    <button class="button" type="button" onclick="deleteElement('.city-confirmation')">
                        Да, спасибо
                    </button>
                </div>
            </div>
        </div>
    <?php endif;?>
    <div class="modal modal_middle" id="choose-main-user-city">
        <div class="container modal__container">
            <div class="grid">
                <div class="grid__column_1-2 grid__column_center">
                    <div class="modal__overlay"></div>
                    <header class="modal__header modal__header_compact">
                        <button class="modal__close" type="button">Закрыть</button>
                        <h3 class="modal__heading">
                            Выберите город
                        </h3>
                    </header>
                    <div class="modal__body modal__body_compact">
                        <div class="choose-city">
                            <input class="input choose-city__search" id="city-chooser-search" type="search" placeholder="Поиск">
                            <?php $form = ActiveForm::begin([
                                'action' => '/site/set-user-city',
                                'method' => 'post',
                                'fieldConfig' => [
                                    'template' => '{input}',
                                    'options' => ['tag' => false]
                                ],
                            ]);?>
                                <div class="form choose-city__form">
                                    <?php
                                    if(!empty($cities['top'])) {
                                        echo $this->render('userPlacementFieldset', [
                                            'form' => $form,
                                            'placementForm' => $placementForm,
                                            'cities' => $cities['top'],
                                            'letter' => 'Топ города',
                                        ]);
                                    }
                                    if(!empty($cities['other'])) {
                                        $firstLetter = '';
                                        $citiesBlock = [];
                                        foreach ($cities['other'] as $cityId => $cityName) {
                                            $newFirstLetter = ucfirst(mb_substr($cityName, 0, 1, 'utf-8'));
                                            if ($newFirstLetter != $firstLetter && $firstLetter != '') {
                                                echo $this->render('userPlacementFieldset', [
                                                    'form' => $form,
                                                    'placementForm' => $placementForm,
                                                    'cities' => $citiesBlock,
                                                    'letter' => $firstLetter,
                                                ]);
                                                $citiesBlock = [];
                                            }
                                            $citiesBlock[$cityId] = $cityName;
                                            $firstLetter = $newFirstLetter;
                                        }
                                        echo $this->render('userPlacementFieldset', [
                                            'form' => $form,
                                            'placementForm' => $placementForm,
                                            'cities' => $citiesBlock,
                                            'letter' => $firstLetter,
                                        ]);
                                    }
                                    echo $form->field($placementForm, 'city_id')->hiddenInput()->label(false);
                                    ?>
                                </div>

                                <div class="grid grid_small">
                                    <div class="grid__column_1-3">
                                        <button class="button button_secondary form__button" type="button"
                                                onclick="modal.close('#choose-main-user-city')">
                                            Отменить
                                        </button>
                                    </div>
                                    <div class="grid__column_2-3">
                                        <button class="button form__button" type="submit">
                                            Подтвердить
                                        </button>
                                    </div>
                                </div>
                            <?php ActiveForm::end();?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal modal_middle" id="user-placement-info">
    <div class="container modal__container">
        <div class="grid">
            <div class="grid__column_1-3 grid__column_center">
                <div class="modal__overlay"></div>
                <div class="modal__body modal__body_contrast modal__body_compact">
                    <button class="modal__close" type="button"><?=Yii::t('landing', 'close')?></button>
                    <div class="modal__alert modal__alert_info">
                        <b class="modal__alert-title"><?=Yii::t('landing', 'info')?></b>
                        <p class="modal__alert-text">
                            <?=Yii::t('landing', 'user_placement_info')?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
CityChooserAsset::register($this);
?>
