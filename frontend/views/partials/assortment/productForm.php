<?php
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\bootstrap\ActiveForm;
use kartik\typeahead\Typeahead;
use frontend\models\Companies;
use frontend\models\Vocabularies;
use frontend\models\forms\ProductForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $productForm ProductForm */
/* @var $categoriesTree array */
/* @var $company Companies */
?>

<div class="modal" id="add-product">
    <div class="container modal__container">
        <div class="grid">
            <div class="grid__column_1-2 grid__column_center">
                <div class="modal__overlay"></div>
                <header class="modal__header">
                    <button class="modal__close" type="button">Закрыть</button>
                    <h3 class="modal__heading" id="productform-title">
                        Добавить товар
                    </h3>
                </header>
                <div class="modal__body">
                    <?php $form = ActiveForm::begin(['action' => '/assortment/save-product', 'fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form add-product', 'id' => 'product-form']]);?>
                        <h4 class="add-product__subheading">Соответствие</h4>
                        <div id="addproduct-conformity-empty">
                            <button class="button button_action_secondary" type="button" onclick="modal.open('#select-match')">
                                Выбрать соответствие
                            </button>
                            <p class="add-product__footnote add-product__footnote_center">
                                Не нашли этот товар в каталоге?
                                <a class="link" href="/assortment/no-match">Сообщите нам</a>
                            </p>
                        </div>
                        <div id="addproduct-conformity-selected">
                            <div class="add-product__chosen">
                                <span>Выбрано</span>
                                <a class="add-product__edit" onclick="modal.open('#select-match')" id="change-conformity-button">Изменить</a>
                            </div>
                            <div class="add-product__chosen-product">
                            </div>
                        </div>

                        <h4 class="add-product__subheading">Основное</h4>
                        <?=$form->field($productForm, 'name')
                            ->textInput(['maxlength' => true, 'class' => "input add-product__input", 'placeholder' => "Наименование в вашей базе"])?>
                        <?=$form->field($productForm, 'vendorCode')
                            ->textInput(['maxlength' => true, 'class' => "input add-product__input", 'placeholder' => "Артикул в вашей базе"])?>
                        <p class="add-product__footnote">
                            Внимание! По этим полям происходит привязка ваших товаров к базе товаров Строймаркета, во избежание
                            потери соответствия — не изменяйте их.
                        </p>

                        <?php if($company->type != Companies::TYPE_PROVIDER):?>
                            <?=$form->field($productForm, 'url')
                                ->textInput(['maxlength' => true, 'class' => "input add-product__input", 'placeholder' => "Ссылка"])?>
                            <p class="add-product__footnote">
                                Если у вас есть интернет-магазин — добавьте ссылку на данный товар в вашем интернет-магазине
                            </p>
                        <?php endif;?>

                        <h4 class="add-product__subheading">Параметры</h4>
                        <?php /*<div class="grid grid_collapse add-product__parameters">
                            <?php if($company->type == Companies::TYPE_PROVIDER):?>
                                <?=$form->field($productForm, 'multiplicity')
                                    ->textInput(['class' => "input", 'placeholder' => "Кратность"])?>
                                <?=$form->field($productForm, "unitTypeId")
                                    ->dropDownList(Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_UNITS]['items'], ['class' => 'select'])
                                    ->label(false);?>
                            <?php endif;?>
                        </div>*/?>
                        <div class="grid grid_collapse add-product__parameters">
                            <?php if($company->type == Companies::TYPE_PROVIDER):?>
                                <?=$form->field($productForm, 'isAction')
                                    ->checkbox(['class' => 'switcher add-product__switcher'], false)?>
                                <label for="productform-isaction" data-checked="акция" data-not-checked="акция"></label>
                                <div class="grid__column_1-2">
                                    <?=$form->field($productForm, 'cost')
                                        ->textInput(['class' => "input", 'placeholder' => "Цена в рублях"])?>
                                    <span>за</span>
                                </div>
                                <div class="grid__column_1-2">
                                    <?=$form->field($productForm, 'cnt')
                                        ->textInput(['class' => "input", 'placeholder' => "Количество"])?>
                                    <span id="unit-type"></span>
                                </div>
                            <?php else:?>
                                <div class="grid__column_1-1">
                                    <?=$form->field($productForm, 'cost')
                                        ->textInput(['class' => "input", 'placeholder' => "Цена в рублях", 'type' => 'number'])?>
                                    <span>за 1 <span id="unit-type">шт</span></span>
                                </div>
                            <?php endif;?>
                        </div>
                        <?php if($company->type == Companies::TYPE_PROVIDER):?>
                            <p class="add-product__footnote">
                                Если вы объявите акцию, то всем компаниям, заинтересованным в получении акций по данной товарной
                                группе, будут отправлены уведомления.
                            </p>
                        <?php else:?>
                            <div class="add-product__switcher">
                                <?=$form->field($productForm, 'isInStock')
                                    ->checkbox(['class' => 'form__switcher'], false)?>
                                <label for="productform-isinstock" data-checked="да" data-not-checked="нет">
                                    Есть в наличии?
                                </label>
                            </div>
                            <div class="add-product__switcher" id="is-by-order-switcher">
                                <?=$form->field($productForm, 'isByOrder')
                                    ->checkbox(['class' => 'form__switcher'], false)?>
                                <label for="productform-isbyorder" data-checked="да" data-not-checked="нет">
                                    Есть под заказ?
                                </label>
                            </div>
                        <?php endif;?>

                        <h4 class="add-product__subheading">Видимость</h4>
                        <div class="add-product__switcher">
                            <?=$form->field($productForm, 'isPromo')
                                ->checkbox(['class' => 'form__switcher'], false)?>
                            <label for="productform-ispromo" data-checked="да" data-not-checked="нет">
                                Показывать на главной странице компании?
                            </label>
                            <p class="add-product__footnote">
                                Товары, у которых данный переключатель включен, показываются среди 3-х товаров в разделе общая
                                информация в профиле вашей компании.
                            </p>
                        </div>
                        <div class="add-product__switcher" id="show-in-catalog-button">
                            <?=$form->field($productForm, 'isVisible')
                                ->checkbox(['class' => 'form__switcher'], false)?>
                            <label for="productform-isvisible" data-checked="да" data-not-checked="нет">
                                Показывать в каталоге?
                            </label>
                            <p class="add-product__footnote">
                                Если вы выключите переключатель, то данный товар в вашем ассортименте будете видеть только вы.
                            </p>
                        </div>

                        <div class="grid">
                            <div class="grid__column_2-5 grid__column_center">
                                <?=$form->field($productForm, 'productId')->hiddenInput()?>
                                <?=$form->field($productForm, 'id')->hiddenInput()?>
                                <?=Html::submitButton('Добавить', ['class' => 'button add-product__button', 'disabled' => true, 'id' => 'product-submit-button'])?>
                            </div>
                        </div>
                    <?php ActiveForm::end();?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="select-match">
    <div class="container modal__container">
        <div class="grid">
            <div class="grid__column_4-7 grid__column_center">
                <div class="modal__overlay"></div>
                <header class="modal__header">
                    <button class="modal__close" type="button">Закрыть</button>
                    <h3 class="modal__heading">
                        Добавить товар
                    </h3>
                </header>
                <div class="modal__body modal__body_contrast modal__body_categories add-match">
                    <div class="add-match__nav">
                        <button class="button add-match__back" type="button" onclick="returnToCategories();">
                            Назад
                        </button>
                        <form class="form add-match__search-form">
                            <?=Typeahead::widget([
                                'name' => 'q',
                                'pluginOptions' => ['highlight' => true],
                                'scrollable' => true,
                                'options' => [
                                    'placeholder' => 'Поиск по товарам',
                                    'class' => 'input',
                                    'type' => 'search',
                                    'id' => 'conformity-search-field',
                                ],
                                'dataset' => [[
                                    'display' => 'name',
                                    'limit' => 1000,
                                    'remote' => [
                                        'url' => '/catalog/products-hints?q=%QUERY',
                                        'wildcard' => '%QUERY'
                                    ],
                                    'templates' => [
                                        'suggestion' => new JsExpression("function (data) {return '<p>' + data.name + '</p>';}")
                                    ]
                                ]]
                            ])?>
                            <button class="button add-match__search-button" type="submit">
                                Поиск
                            </button>
                        </form>
                    </div>
                    <div id="add-match-replaceable">
                        <div class="assortment add-match__assortment">
                            <?=$this->render('../fullCategoriesList', [
                                'categories' => $categoriesTree,
                                'columnsCount' => 2,
                                'ajaxCategoriesLinks' => true,
                            ])?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>