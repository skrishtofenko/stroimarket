<?php
use frontend\models\Users;

/* @var $noAction boolean|null */
?>
<?=$this->render('../search/products/searchField', ['stayHere' => true])?>
<?php if((!isset($noAction) || !$noAction) && $this->params['currentUserRole'] != Users::ROLE_BUYER):?>
    <?=$this->render('../search/products/actionField', ['stayHere' => true])?>
<?php endif;?>
