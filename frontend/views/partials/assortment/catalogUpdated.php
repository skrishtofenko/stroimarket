<?php
use frontend\models\Companies;

/* @var $company Companies */
/* @var $currentMode string */

$currentMode = isset($currentMode) ? $currentMode : '';
$classPrefix = $currentMode ? "$currentMode-" : '';
?>
<?php if($company->catalog_updated_at):?>
    <p class="<?=$classPrefix?>assortment__update-date">
        Обновлен <?=date('d.m.y', strtotime($company->catalog_updated_at))?>
    </p>
<?php endif;?>

