<?php
/* @var $currentMode string */
$currentMode = isset($currentMode) ? $currentMode : '';
?>
<select class="select assortment__select" id="mode">
    <option value=""<?=($currentMode == '' ? ' selected' : '')?>>Режим "Каталог"</option>
    <option value="edit"<?=($currentMode == 'edit' ? ' selected' : '')?>>Режим "Редактирование"</option>
</select>
