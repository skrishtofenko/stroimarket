<?php
use yii\widgets\ActiveForm;
use frontend\models\Companies;
use frontend\models\forms\CatalogUploadForm;

/* @var $company Companies */
/* @var $currentMode string */
/* @var $noAction boolean|null */
/* @var $catalogUploadForm CatalogUploadForm|null */

$currentMode = isset($currentMode) ? $currentMode : '';
$classPrefix = $currentMode ? "$currentMode-" : '';
?>
<?=$this->render('catalogUpdated', [
    'company' => $company,
    'currentMode' => $currentMode,
])?>

<section class="<?=$classPrefix?>assortment__interactions">
    <?php if(!Yii::$app->user->isGuest && Yii::$app->user->identity->id == $company->user_id):?>
        <?=$this->render('modeSwitcher', [
            'currentMode' => $currentMode,
        ])?>
    <?php endif;?>
    <?php if($currentMode == 'edit'):?>
        <p>
            В этом режиме нельзя раскрыть боковое меню
        </p>
        <button class="button button_action" type="button" onclick="modal.open('#add-product'); clearProductForm();">
            Добавить товар
        </button>
        <?php $form = ActiveForm::begin(['action' => '/assortment/upload', 'fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['hidden' => true]]);?>
            <?=$form->field($catalogUploadForm, "catalogFile")
                ->fileInput(['onchange' => "validateAndUpload(event)"])?>
        <?php ActiveForm::end();?>
        <select class="select select_right assortment-actions" id="action">
            <option value="0" selected hidden>Действие</option>
            <option value="/assortment/get-assortment-template">Скачать шаблон</option>
            <option value="/assortment/get-full-assortment">Скачать базу Строймаркета</option>
            <option value="upload-file">Загрузить мой ассортимент в Строймаркет</option>
            <option value="/assortment/get-my-assortment">Скачать мой ассортимент</option>
        </select>
        <?php /*<button class="button button_action" type="button" disabled>
            Сохранить изменения
        </button>*/?>
    <?php else:?>
        <?=$this->render('searchString', [
            'noAction' => isset($noAction) ? $noAction : false,
        ])?>
    <?php endif;?>
</section>
<section>
    <div class="file-errors"></div>
</section>