<?php
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\Categories;

/* @var $user Users */
/* @var $company Companies */
/* @var $category Categories|null */
/* @var $productsCount int|null */
/** @var Users $currentUser */
$currentUser = Yii::$app->user->identity;

$this->title = 'Ассортимент';
$companies = $company->getOtherUserCompanies($this->params['currentUserRole'], true);
?>
<h2 class="h2 dashboard__h2"><?=($currentUser && $user->id == $currentUser->id ? 'Моя компания' : $user->legalEntityVcard)?></h2>
<div class="flex">
    <h3 class="h3 h3-selector">
        <span>Ассортимент</span>
        <?php if(count($companies) > 1):?>
            <select class="text-select" id="place"<?=(!$currentUser || $user->id != $currentUser->id ? ' data-othercompany="1"' : '')?>>
                <?php foreach($companies as $otherCompany):?>
                    <?php /** @var $otherCompany Companies */?>
                    <option value="<?=$otherCompany->id?>"<?=($otherCompany->id == $company->id ? ' selected' : '')?>><?=$otherCompany->typeAsText?>, <?=$otherCompany->addressAsText?></option>
                <?php endforeach;?>
            </select>
        <?php endif;?>
    </h3>
    <?php if(isset($category) && $category):?>
        <div class="dashboard__back-wrapper">
            <a class="link dashboard__back-link" href="<?=(!$currentUser || $user->id != $currentUser->id ? "/catalog/company/$company->id" : '')?>/assortment">Назад к списку категорий</a>
        </div>
    <?php endif;?>
</div>
