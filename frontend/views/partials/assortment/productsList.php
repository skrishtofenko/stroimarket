<?php
use common\components\ImageManager\ImageManager;
use frontend\models\Products;

/** @var $products Products[] */
$half = ceil(count($products) / 2);
$i = 1;
?>
<div class="assortment add-match__products">
    <div class="grid">
        <?php if(!empty($products)):?>
            <div class="grid__column_1-2">
                <?php foreach($products as $product):?>
                    <div class="product-card add-match__product-card" id="conformity-product-<?=$product->id?>">
                        <?php $productImg = ImageManager::getUrl($product, 'image', 160, 160, ImageManager::RESIZE_ZOOM_IN)?>
                        <img src="<?=$productImg?>" alt="" width="160" height="160">
                        <h3 class="product-card__name"><?=$product->fullName?></h3>
                        <p class="product-card__vendor-code">Артикул СМ: <?=$product->id?></p>
                        <p class="product-card__category">
                            <?=$product->fullCategory?>
                        </p>
                        <button class="button button_action_secondary product-card__button" type="button" onclick="chooseConformity(<?=$product->id?>, '<?=$product->unitType->name?>')">
                            Выбрать
                        </button>
                        <a class="link product-card__link" href="/catalog/product/<?=$product->id?>" target="_blank">
                            Посмотреть товар полностью
                        </a>
                        <div class="hidden-info" hidden>
                            <img src="<?=$productImg?>" alt="">
                            <b><?=$product->fullName?></b>
                            <i>Артикул СМ: <?=$product->id?></i>
                            <p><?=$product->fullCategory?></p>
                        </div>
                    </div>
                    <?php if($i == $half):?>
                        </div>
                        <div class="grid__column_1-2">
                    <?php endif;?>
                    <?php $i++?>
                <?php endforeach;?>
            </div>
        <?php else:?>
            <div class="grid__column_1-1 nothing-found">
                Ничего не найдено
            </div>
        <?php endif;?>
    </div>
</div>