<?php
use frontend\models\forms\UserPlacementForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $placementForm UserPlacementForm */
/* @var $cities array */
/* @var $letter string */
?>
<fieldset>
    <legend><?=$letter?></legend>
    <?php foreach($cities as $cityId => $cityName):?>
        <p>
            <?=$form->field($placementForm, 'city_id', ['template' => '{input}'])
                ->radio([
                    'value' => $cityId,
                    'uncheck' => null,
                    'id' => "city-$cityId",
                    'data-name' => mb_strtolower($cityName, 'utf-8')
                ], false)
                ->label(false)?>
            <label for='city-<?=$cityId?>'><?=$cityName?></label>
        </p>
    <?php endforeach;?>
</fieldset>
