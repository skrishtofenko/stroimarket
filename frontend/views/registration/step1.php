<?php
use yii\helpers\Html;
use frontend\models\Companies;

$this->title = 'Регистрация';
?>
<div class="registration">
    <b class="h2 registration__heading">
        В качестве кого вы хотите зарегистрироваться на Строймаркете?
    </b>
    <?=Html::beginForm('', 'post')?>
    <div class="grid">
        <div class="grid__column_1-3">
            <div class="registration__block">
                <h3 class="registration__type">
                    В качестве поставщика
                </h3>
                <p class="registration__clarification">
                    Вы — производитель или оптовая компания и продаете оптом товары розничным магазинам или другим оптовым компаниям. Если кроме этого вы также торгуете в розницу, хотите зарегистрироваться в качестве розничного магазина и продавать свои товары через «Строймаркет» — зарегистрируйтесь отдельно в качестве розничного магазина.
                </p>
                <?=Html::submitButton('Зарегистрироваться', [
                    'class' => 'button registration__button',
                    'name' => 'type',
                    'value' => Companies::TYPE_PROVIDER
                ])?>
            </div>
        </div>
        <div class="grid__column_1-3">
            <div class="registration__block">
                <h3 class="registration__type">
                    В качестве розничного магазина
                </h3>
                <p class="registration__clarification">
                    Вы не продаете свои товары другим магазинам, торгуете только в мелкий опт и розницу. Если у вас сеть магазинов — каждый нужно зарегистрировать отдельно.
                </p>
                <?=Html::submitButton('Зарегистрироваться', [
                    'class' => 'button registration__button',
                    'name' => 'type',
                    'value' => Companies::TYPE_STORE
                ])?>
            </div>
        </div>
        <div class="grid__column_1-3">
            <div class="registration__block">
                <h3 class="registration__type">
                    В качестве оптового покупателя
                </h3>
                <p class="registration__clarification">
                    Вы не продаете товары вообще, регистрируетесь на «Строймаркете»  только для поиска поставщиков, проведения тендеров и т.д.
                </p>
                <?=Html::submitButton('Зарегистрироваться', [
                    'class' => 'button registration__button',
                    'name' => 'type',
                    'value' => Companies::TYPE_PICKER
                ])?>
            </div>
        </div>
    </div>
    <?=Html::endForm()?>
    <p class="registration__has-account">
        Уже есть аккаунт?
        <a class="link" href="/login">Войти</a>
    </p>
</div>
