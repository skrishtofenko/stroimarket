<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Users;
use frontend\models\forms\CompanyInfoForm;
use frontend\assets\pages\RegistrationStep4Asset;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $user Users */
/* @var $companyInfoForm CompanyInfoForm */
/* @var $cities array */

$this->title = 'Общая информация';
?>

<div class="grid">
    <div class="grid__column_1-3">
        <section class="registration__panel">
            <b class="registration__name">
                <?=$user->legalEntityVcard?>
            </b>
            <p class="registration__text">
                Продолжение регистрации
            </p>
            <div class="registration__progress">
                <p class="registration__checkbox registration__checkbox_current">
                    <input id="progress_info" type="checkbox" disabled>
                    <label for="progress_info">Общая информация</label>
                </p>
            </div>
        </section>
    </div>
    <div class="grid__column_7-12">
        <div class="registration">
            <h2 class="h2 registration__h2">Общая информация</h2>
            <?php $form = ActiveForm::begin(['options' => ['class' => 'form registration__form'], 'fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]]]);?>
                <div class="grid form__row">
                    <div class="grid__column_3-7">
                        <label class="form__label" for="users-fio">
                            ФИО:
                        </label>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$form->field($companyInfoForm, 'fio')
                            ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Текст"])?>
                    </div>
                </div>
                <div class="grid form__row">
                    <div class="grid__column_3-7">
                        <label class="form__label" for="companies-emails_array-0">
                            Электронная почта:
                        </label>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$form->field($companyInfoForm, "email")
                            ->textInput(['maxlength' => true, 'class' => "input", 'type' => 'email']);?>
                    </div>
                </div>
                <div class="grid form__row">
                    <div class="grid__column_3-7">
                        <label class="form__label" for="companies-phones_array-0">
                            Телефон:
                        </label>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$form->field($companyInfoForm, "phone")
                            ->textInput(['class' => "input", 'type' => 'tel']);?>
                    </div>
                </div>
                <div class="grid form__row">
                    <div class="grid__column_3-7">
                        <p class="form__label">
                            Город:
                        </p>
                    </div>
                    <?=$this->render('../partials/form/cityChooser', [
                        'form' => $form,
                        'company' => $companyInfoForm,
                        'isWithWrapper' => false,
                    ])?>
                </div>
                <?=$this->render('../partials/form/imageInput', [
                    'form' => $form,
                    'model' => $user,
                    'attribute' => 'logo',
                    'maxFileSize' => Users::imagesParams()['logo']['maxSizeInMB'],
                    'leftColumnClass' => '3-7',
                    'caption' => 'Фото',
                ])?>
                <div class="grid">
                    <div class="grid__column_3-7 grid__column_right">
                        <?=Html::submitButton('Продолжить', ['class' => 'button form__button'])?>
                    </div>
                </div>
            <?php ActiveForm::end();?>
        </div>
    </div>
</div>
<?php
RegistrationStep4Asset::register($this);
?>
