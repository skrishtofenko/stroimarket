<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\helpers\SMHelper;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $loginForm \frontend\models\forms\LoginForm */
/* @var $user \frontend\models\Users */

$company = $user->companies[0];
$regType = SMHelper::companyTypeInGenitive($company->type);
$this->title = 'Регистрация в качестве ' . $regType;
?>
<div class="registration">
    <div class="grid">
        <div class="grid__column_2-5 grid__column_center">
            <div class="registration__step">
                <h2 class="h2">
                    <?=Html::encode($this->title)?>
                </h2>
                <p class="link">
                    <?php if($user->ownership_type_id):?>
                        Компания <?=$user->legalEntityVcard?> зарегистрирована
                    <?php else:?>
                        <?=$user->fio?> зарегистрирован
                    <?php endif;?>
                </p>
                <p class="registration__caption">
                    По адресу <?=$user->email?> отправлено письмо с паролем для входа в личный кабинет.
                </p>
                <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form registration__form']]);?>
                    <div class="grid form__row">
                        <div class="grid__column_2-5">
                            <label class="form__label" for="registration_name">Пароль:</label>
                        </div>
                        <div class="grid__column_3-5">
                            <?=$form->field($loginForm, 'password')
                                ->passwordInput(['class' => 'input', 'placeholder' => 'Введите полученный пароль'])?>
                        </div>
                    </div>
                    <?=$form->field($loginForm, 'rememberMe')
                        ->checkbox(['class' => 'form__switcher', 'label' => false], false)?>
                    <label for="loginform-rememberme" data-checked="да" data-not-checked="нет">Запомнить пароль?</label>
                    <?=$form->field($loginForm, 'email')->hiddenInput()->label(false)?>
                    <?=$form->field($loginForm, 'isRegistrationFirstLogin')->hiddenInput()->label(false)?>
                    <?=Html::submitButton('Войти в личный кабинет', ['class' => 'button form__button'])?>
                <?php ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>
