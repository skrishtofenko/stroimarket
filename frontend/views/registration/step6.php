<?php
use frontend\models\Companies;

/* @var $this yii\web\View */
/* @var $company \frontend\models\Companies */
$tips = [
    'assortment' => [
        'title' => 'Заполните ассортимент',
        'text' => 'Для того чтобы другие компании смогли найти вас в общем каталоге, заполните ваш ассортимент.'
    ],
    'requisites' => [
        'title' => 'Подтвердите реквизиты',
        'text' => 'Для того чтобы профиль и ассортимент вашей компании появились в общем каталоге и стали доступны другим пользователям, вам необходимо подтвердить ваши реквизиты в настройках личного кабинета.'
    ],
    'start' => [
        'title' => 'Начинайте работать',
        'text' => 'Находите новых клиентов, участвуйте в закупках, расширяйте рынок сбыта и увеличивайте прибыль.'
    ],
];
if($company->type == Companies::TYPE_STORE) {
    if(!$company->is_main) {
        unset($tips['requisites']);
    }
    $tips['assortment']['text'] = 'Для того чтобы пользователи смогли найти вас в общем каталоге, заполните ваш ассортимент.';
    $tips['start']['text'] = 'Получайте новых клиентов, совершайте закупки по самым выгодным ценам, получайте скидки от поставщиков со всей России, расширяйте сбыт, снижайте затраты, увеличивайте прибыль.';
}
if($company->type == Companies::TYPE_PICKER) {
    unset($tips['assortment']);
    $tips['requisites']['text'] = 'Для того чтобы вы появились в общей базе и стали доступны другим пользователям, вам необходимо подтвердить ваши реквизиты в настройках личного кабинета.';
    $tips['start']['text'] = 'Снижайте затраты, совершайте закупки по самым выгодным ценам и получайте скидки от поставщиков со всей России.';
}

$this->title = 'Регистрация завершена';
?>
<div class="grid">
    <div class="grid__column_<?=(count($tips) == 3 ? '5-6' : '2-3')?> grid__column_center">
        <div class="registration registration_finish">
            <div class="registration__block registration__block_finish">
                <b class="h2 registration__heading">
                    Регистрация завершена
                </b>
                <p class="registration__what-next">
                    Что дальше?
                </p>

                <div class="grid grid_large">
                    <?php $i = 1;?>
                    <?php foreach ($tips as $tip):?>
                        <div class="grid__column_<?=(count($tips) == 3 ? '1-3' : '3-7')?><?=(count($tips) == 2 && $i == 2 ? ' grid__column_right' : '')?>">
                            <h3 class="registration__next-step">
                                <?=$tip['title']?>
                            </h3>
                            <p class="registration__clarification">
                                <?=$tip['text']?>
                            </p>
                        </div>
                        <?php $i++?>
                    <?php endforeach;?>
                </div>

                <div class="grid grid_large">
                    <div class="grid__column_<?=(count($tips) == 3 ? '1-3' : '3-7')?> grid__column_center">
                        <a class="button registration__button registration__button_finish" href="/profile">
                            Перейти в личный кабинет
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>