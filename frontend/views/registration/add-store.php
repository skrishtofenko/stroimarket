<?php
use yii\bootstrap\ActiveForm;
use frontend\models\Vocabularies;
use frontend\models\forms\CompanyInfoForm;
use frontend\assets\pages\RegistrationStep4Asset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user \frontend\models\Users */
/* @var $companyInfoForm CompanyInfoForm */
/* @var $cities array */

$this->title = 'Добавить магазин';
?>
<div class="grid">
    <div class="grid__column_5-6 grid__column_center">
        <div class="dashboard">
            <h2 class="h2 dashboard__h2"><?=$user->legalEntityVcard?></h2>
            <h3 class="h3 dashboard__h3">Создать магазин</h3>

            <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form dashboard__form']]);?>
                <div class="form__row">
                    <?=$form->field($companyInfoForm, 'is_imonly')
                        ->checkbox(['class' => 'form__switcher'], false)?>
                    <label for="companyinfoform-is_imonly" data-checked="да" data-not-checked="нет">
                        Нет фактического адреса — только интернет магазин:
                    </label>
                </div>

                <div class="grid form__row form__row_top address-block">
                    <div class="grid__column_3-5">
                        <p class="form__label">
                            Расположение фирмы:
                        </p>
                        <p class="form__label-caption">
                            Внимание! Проверьте адрес внимательно, так как после завершения процедуры добавления магазина вы
                            сможете изменить адрес, только обратившись в администрацию Строймаркета
                        </p>
                    </div>
                    <div class="grid__column_2-5">
                        <?=$this->render('../partials/form/cityChooser', [
                            'form' => $form,
                            'company' => $companyInfoForm,
                            'cities' => $cities,
                        ])?>
                        <div class="grid grid_small">
                            <div class="grid__column_1-2">
                                <?=$form->field($companyInfoForm, 'street')
                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Улица"])?>
                            </div>
                            <div class="grid__column_1-2">
                                <?=$form->field($companyInfoForm, 'house')
                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Дом"])?>
                            </div>
                        </div>
                        <div class="grid grid_small">
                            <div class="grid__column_1-2">
                                <?=$form->field($companyInfoForm, 'room_type_id')->dropDownList(
                                    Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_ROOMS]['items'], [
                                        'class' => 'select form__select',
                                        'prompt' => 'Помещение',
                                    ]
                                )?>
                            </div>
                            <div class="grid__column_1-2">
                                <?=$form->field($companyInfoForm, 'room_number')
                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Номер"])?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="grid form__row form__row_top im-block" style="display: none;">
                    <div class="grid__column_3-5">
                        <label class="form__label" for="companyinfoform-im_url">
                            Адрес интернет-магазина:
                        </label>
                    </div>
                    <div class="grid__column_2-5">
                        <?=$form->field($companyInfoForm, "im_url")
                            ->textInput(['class' => "input", 'type' => 'url']);?>
                    </div>
                </div>

                <div class="grid form__row">
                    <div class="grid__column_3-5">
                        <label class="form__label" for="companyinfoform-phone">
                            Контактный телефон:
                        </label>
                    </div>
                    <div class="grid__column_2-5">
                        <?=$form->field($companyInfoForm, "phone")
                            ->textInput(['class' => "input", 'type' => 'tel']);?>
                    </div>
                </div>

                <div class="grid form__row">
                    <div class="grid__column_3-5">
                        <label class="form__label" for="companyinfoform-email">
                            Контактный email:
                        </label>
                    </div>
                    <div class="grid__column_2-5">
                        <?=$form->field($companyInfoForm, "email")
                            ->textInput(['maxlength' => true, 'class' => "input", 'type' => 'email']);?>
                    </div>
                </div>

                <?=$this->render('../partials/form/deliveryBlock', [
                    'form' => $form,
                    'company' => $companyInfoForm,
                    'cities' => $cities,
                    'leftColumnClass' => '3-5',
                ])?>

                <?=$this->render('../partials/form/scheduleBlock', [
                    'form' => $form,
                    'company' => $companyInfoForm,
                    'leftColumnClass' => '3-5',
                ])?>

                <?=$this->render('../partials/form/settingsSubmits', [
                    'submitCaption' => 'Подтвердить',
                    'totalClass' => '1-3',
                    'leftColumnClass' => '3-5',
                ])?>
            <?php ActiveForm::end();?>
        </div>
    </div>
</div>
<?php
RegistrationStep4Asset::register($this);
?>
