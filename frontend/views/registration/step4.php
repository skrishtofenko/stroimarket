<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\Vocabularies;
use frontend\models\forms\CompanyInfoForm;
use frontend\assets\pages\RegistrationStep4Asset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user Users */
/* @var $companyInfoForm CompanyInfoForm */
/* @var $cities array */

$this->title = 'Общая информация';
?>

<div class="grid">
    <div class="grid__column_1-3">
        <section class="registration__panel">
            <b class="registration__name">
                <?=$user->legalEntityVcard?>
            </b>
            <p class="registration__text">
                Продолжение регистрации
            </p>
            <div class="registration__progress">
                <p class="registration__checkbox registration__checkbox_current">
                    <input id="progress_info" type="checkbox" disabled>
                    <label for="progress_info">Общая информация</label>
                </p>
                <p class="registration__checkbox">
                    <input id="progress_requisites" type="checkbox" disabled>
                    <label for="progress_requisites">Реквизиты</label>
                </p>
            </div>
        </section>
    </div>
    <div class="grid__column_7-12">
        <div class="registration">
            <h2 class="h2 registration__h2">Общая информация</h2>
            <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form registration__form']]);?>
                <div class="grid form__row">
                    <div class="grid__column_3-7">
                        <label class="form__label" for="companyinfoform-name">
                            Название фирмы:
                        </label>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$form->field($companyInfoForm, 'name')
                            ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Именно название фирмы, а не юр. лица"])?>
                    </div>
                </div>

                <?=$this->render('../partials/form/imageInput', [
                    'form' => $form,
                    'model' => $user,
                    'attribute' => 'logo',
                    'maxFileSize' => Users::imagesParams()['logo']['maxSizeInMB'],
                    'leftColumnClass' => '3-7',
                ])?>
            
                <div class="grid form__row">
                    <div class="grid__column_3-7">
                        <label class="form__label" for="companyinfoform-description">
                            Информация об организации:
                        </label>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$form->field($companyInfoForm, 'description')
                            ->textInput(['class' => "input", 'placeholder' => "Текст"])?>
                    </div>
                </div>
                <div class="grid form__row">
                    <div class="grid__column_3-7">
                        <label class="form__label" for="companyinfoform-url">
                            Веб-сайт:
                        </label>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$form->field($companyInfoForm, 'url')
                            ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Укажите сайт в формате https://www.-", 'type' => 'url'])?>
                    </div>
                </div>
                <hr class="hr">
                <?php if($companyInfoForm->type == Companies::TYPE_STORE):?>
                    <div class="grid form__row">
                        <div class="grid__column_6-7">
                            <p class="registration__hint">
                                Если у вас сеть магазинов — добавить другие магазины сети вы сможете по окончании регистрации
                                в личном кабинете
                            </p>
                        </div>
                    </div>
                <?php endif;?>
                <?php if($companyInfoForm->type == Companies::TYPE_STORE):?>
                    <div class="form__row">
                        <?=$form->field($companyInfoForm, 'is_imonly')
                            ->checkbox(['class' => 'form__switcher'], false)?>
                        <label for="companyinfoform-is_imonly" data-checked="да" data-not-checked="нет">
                            Нет фактического адреса — только интернет магазин:
                        </label>
                    </div>
                <?php endif;?>
                <div class="grid form__row form__row_top address-block">
                    <div class="grid__column_3-7">
                        <p class="form__label">
                            Расположение фирмы:
                        </p>
                        <p class="form__label-caption">
                            Внимание! Проверьте адрес внимательно, так как после завершения регистрации вы сможете изменить
                            адрес, только обратившись в администрацию Строймаркета
                        </p>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$this->render('../partials/form/cityChooser', [
                            'form' => $form,
                            'company' => $companyInfoForm,
                            'cities' => $cities,
                        ])?>
                        <div class="grid grid_small">
                            <div class="grid__column_1-2">
                                <?=$form->field($companyInfoForm, 'street')
                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Улица"])?>
                            </div>
                            <div class="grid__column_1-2">
                                <?=$form->field($companyInfoForm, 'house')
                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Дом"])?>
                            </div>
                        </div>
                        <div class="grid grid_small">
                            <div class="grid__column_1-2">
                                <?=$form->field($companyInfoForm, 'room_type_id')->dropDownList(
                                    Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_ROOMS]['items'], [
                                        'class' => 'select form__select',
                                        'prompt' => 'Помещение'
                                    ]
                                )?>
                            </div>
                            <div class="grid__column_1-2">
                                <?=$form->field($companyInfoForm, 'room_number')
                                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Номер"])?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid form__row form__row_top im-block" style="display: none;">
                    <div class="grid__column_3-7">
                        <label class="form__label" for="companyinfoform-im_url">
                            Адрес интернет-магазина:
                        </label>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$form->field($companyInfoForm, "im_url")
                            ->textInput(['class' => "input", 'type' => 'url']);?>
                    </div>
                </div>
                <div class="grid form__row">
                    <div class="grid__column_3-7">
                        <label class="form__label" for="companyinfoform-phone">
                            Контактный телефон:
                        </label>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$form->field($companyInfoForm, "phone")
                            ->textInput(['class' => "input", 'type' => 'tel']);?>
                    </div>
                </div>
                <div class="grid form__row">
                    <div class="grid__column_3-7">
                        <label class="form__label" for="companyinfoform-email">
                            Контактный email:
                        </label>
                    </div>
                    <div class="grid__column_4-7">
                        <?=$form->field($companyInfoForm, "email")
                            ->textInput(['maxlength' => true, 'class' => "input", 'type' => 'email']);?>
                    </div>
                </div>
                <?php if($companyInfoForm->type == Companies::TYPE_STORE):?>
                    <?=$this->render('../partials/form/deliveryBlock', [
                        'form' => $form,
                        'company' => $companyInfoForm,
                        'cities' => $cities,
                        'leftColumnClass' => '3-7',
                    ])?>

                    <?=$this->render('../partials/form/scheduleBlock', [
                        'form' => $form,
                        'company' => $companyInfoForm,
                        'leftColumnClass' => '3-7',
                    ])?>
                <?php endif;?>
                <div class="grid">
                    <div class="grid__column_3-7 grid__column_right">
                        <?=Html::submitButton('Продолжить', ['class' => 'button form__button'])?>
                    </div>
                </div>
            <?php ActiveForm::end();?>
        </div>
    </div>
</div>
<?php
RegistrationStep4Asset::register($this);
?>
