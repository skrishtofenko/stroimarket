<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Users;
use frontend\models\forms\RequisitesInfoForm;
use frontend\assets\pages\RegistrationStep5Asset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user Users */
/* @var $requisitesInfoForm RequisitesInfoForm */

$this->title = 'Реквизиты';
?>
<div class="grid">
    <div class="grid__column_1-3">
        <section class="registration__panel">
            <b class="registration__name">
                <?=$user->legalEntityVcard?>
            </b>
            <p class="registration__text">
                Продолжение регистрации
            </p>
            <div class="registration__progress">
                <p class="registration__checkbox active">
                    <input id="progress_info" type="checkbox" checked disabled>
                    <label for="progress_info">Общая информация</label>
                </p>
                <p class="registration__checkbox registration__checkbox_current">
                    <input id="progress_requisites" type="checkbox" disabled>
                    <label for="progress_requisites">Реквизиты</label>
                </p>
            </div>
        </section>
    </div>
    <div class="grid__column_7-12">
        <div class="registration">
            <h2 class="h2 registration__h2">Реквизиты</h2>
            <div class="grid">
                <div class="grid__column_6-7">
                    <p class="registration__hint">
                        Для того, чтобы другие пользователи видели, что вы реальная фирма,
                        а профиль и ассортимент вашей компании появились в общей базе и стали доступны другим пользователям,
                        вам необходимо указать реквизиты и по окончании регистрации подтвердить их в настройках личного кабинета
                    </p>
                </div>
            </div>
            <?php $form = ActiveForm::begin([
                'fieldConfig' => [
                    'template' => '
                        <div class="grid form__row">
                            <div class="grid__column_3-7">
                                {label}
                            </div>
                            <div class="grid__column_4-7">
                                <p class="form__field">
                                    {input}
                                    <span class="form__error">
                                        <b>Ошибка ввода</b>
                                        <span></span>
                                    </span>
                                </p>
                            </div>
                        </div>
                    ',
                    'options' => ['tag' => false],
                    'labelOptions' => ['class' => 'form__label'],
                ],
                'options' => [
                    'class' => 'form registration__form',
                    'id' => 'requisites-settings-form'
                ]
            ]);?>
                <?=$form->field($requisitesInfoForm, 'inn')
                    ->textInput(['maxlength' => true, 'class' => "input"])?>
                <?=$form->field($requisitesInfoForm, 'ogrn')
                    ->textInput(['maxlength' => true, 'class' => "input"])?>
                <?=$form->field($requisitesInfoForm, 'kpp')
                    ->textInput(['maxlength' => true, 'class' => "input"])?>
                <?=$form->field($requisitesInfoForm, 'okpo')
                    ->textInput(['maxlength' => true, 'class' => "input"])?>
                <?=$form->field($requisitesInfoForm, 'account')
                    ->textInput(['maxlength' => true, 'class' => "input"])?>
                <?=$form->field($requisitesInfoForm, 'bik')
                    ->textInput(['maxlength' => true, 'class' => "input"])?>
                <?=$form->field($requisitesInfoForm, 'bank')
                    ->textInput(['maxlength' => true, 'class' => "input"])?>
                <?=$form->field($requisitesInfoForm, 'corr_account')
                    ->textInput(['maxlength' => true, 'class' => "input"])?>
                <?=$form->field($requisitesInfoForm, 'legal_address')
                    ->textarea(['class' => "input", 'rows' => 1])?>
                <?=$form->field($requisitesInfoForm, 'post_address')
                    ->textarea(['class' => "input", 'rows' => 1])?>
                <div class="grid">
                    <div class="grid__column_3-7 grid__column_right">
                        <?=Html::submitButton('Завершить регистрацию', ['class' => 'button form__button'])?>
                    </div>
                </div>
            <?php ActiveForm::end();?>
        </div>
    </div>
</div>
<?php
RegistrationStep5Asset::register($this);
?>