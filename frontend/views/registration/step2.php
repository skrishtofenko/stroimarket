<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\helpers\SMHelper;
use frontend\models\Companies;
use frontend\assets\pages\RegistrationStep2Asset;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $registrationStep2Form \frontend\models\forms\RegistrationStep2Form */

$regType = SMHelper::companyTypeInGenitive($registrationStep2Form->type);
$this->title = 'Регистрация в качестве ' . $regType;
?>
<div class="registration">
    <div class="grid">
        <div class="grid__column_1-2 grid__column_center">
            <div class="registration__step">
                <h2 class="h2">
                    <?=$this->title?>
                </h2>
                <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]]]);?>
                    <?php if($registrationStep2Form->type == Companies::TYPE_PROVIDER):?>
                        <p class="">
                            <?=$form->field($registrationStep2Form, 'isManufacturer')
                                ->checkbox(['class' => 'form__switcher', 'label' => false])?>
                            <label for="registrationstep2form-ismanufacturer" data-checked="да" data-not-checked="нет">Вы являетесь производителем:</label>
                        </p>
                    <?php endif;?>
                    <?php if($registrationStep2Form->type == Companies::TYPE_PICKER):?>
                        <p class="">
                            <?=$form->field($registrationStep2Form, 'isLegalEntity')
                                ->checkbox(['class' => 'form__switcher', 'label' => false])?>
                            <label for="registrationstep2form-islegalentity" data-checked="да" data-not-checked="нет">Вы являетесь юридическим лицом:</label>
                        </p>
                    <?php endif;?>
                    <div class="grid" id="legal-entity-block">
                        <div class="grid__column_1-3">
                            <?=$form->field($registrationStep2Form, 'ownershipTypeId')
                                ->dropDownList(
                                    Yii::$app->params['vocabulariesFront']['legal_entities']['items'], [
                                        'class' => 'select form__select'
                                    ]
                                )?>
                        </div>
                        <div class="grid__column_2-3">
                            <?=$form->field($registrationStep2Form, 'legalEntityName')
                                ->textInput(['maxlength' => true, 'class' => 'input', 'placeholder' => 'Название юр.лица без кавычек'])?>
                        </div>
                    </div>
                    <b class="registration__subheading">
                        Администратор аккаунта
                    </b>
                    <p class="registration__clarification">
                        Эти данные будут видны только администрации «Строймаркета» и будут использоваться в качестве вашего
                        логина для входа. Убедительная просьба не указывать здесь контакты, предназначенные для ваших клиентов
                        или контрагентов, такие контакты вы сможете указать далее.
                    </p>
                    <div class="grid form__row">
                        <div class="grid__column_1-2">
                            <label class="form__label" for="registration_name">ФИО:</label>
                        </div>
                        <div class="grid__column_1-2">
                            <?=$form->field($registrationStep2Form, 'fio')
                                ->textInput(['maxlength' => true, 'placeholder' => 'Администратор аккаунта', 'class' => 'input'])?>
                        </div>
                    </div>
                    <div class="grid form__row">
                        <div class="grid__column_1-2">
                            <label class="form__label" for="registration_email">Электронная почта:</label>
                        </div>
                        <div class="grid__column_1-2">
                            <?=$form->field($registrationStep2Form, 'email')
                                ->textInput(['maxlength' => true, 'placeholder' => 'Email для входа в аккаунт', 'class' => 'input', 'type' => 'email'])?>
                        </div>
                    </div>
                    <div class="grid form__row">
                        <div class="grid__column_1-2">
                            <label class="form__label" for="registration_phone-code">Телефон:</label>
                        </div>
                        <div class="grid__column_1-2">
                            <p class="input-row">
                                <input class="input registration__phone-country"
                                       disabled
                                       value="+7">
                                <?=$form->field($registrationStep2Form, 'phoneCode')
                                    ->textInput(['class' => 'input registration__phone-code', 'placeholder' => 'Код'])?>
                                <?=$form->field($registrationStep2Form, 'phoneNumber')
                                    ->textInput(['class' => 'input registration__phone-number', 'placeholder' => 'Номер'])?>
                            </p>
                        </div>
                    </div>
                    <div class="grid">
                        <div class="grid__column_1-2">
                            <p class="registration__register-conditions">
                                Нажимая «Зарегистрировать компанию», вы подтверждаете, что ознакомлены и полностью согласны с
                                <a class="link" href="/agreement" target="_blank">условиями пользования сайта</a>.
                            </p>
                        </div>
                        <div class="grid__column_1-2">
                            <?=Html::submitButton('Зарегистрировать компанию', ['class' => 'button form__button'])?>
                        </div>
                    </div>
                    <?=$form->field($registrationStep2Form, 'type')->hiddenInput()?>
                    <?=Html::hiddenInput('type', $registrationStep2Form->type)?>
                <?php ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>
<?php
RegistrationStep2Asset::register($this);
?>
