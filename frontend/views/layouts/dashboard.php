<?php
use yii\helpers\Html;
use common\components\ImageManager\ImageManager;
use frontend\assets\StroimarketAsset;
use frontend\models\Users;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var Users $user */

$currentUserRole = $this->params['currentUserRole'];
$user = Yii::$app->user->identity;
$isMinimized = isset($this->params['leftMenuUnmaximizable']) && $this->params['leftMenuUnmaximizable'];
$isWhiteBody = isset($this->params['isWhiteBody']) ? $this->params['isWhiteBody'] : false;
StroimarketAsset::register($this);
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title><?=Html::encode($this->title)?></title>
        <link rel="apple-touch-icon" sizes="180x180" href="/static/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon-32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon-16.png">
        <link rel="manifest" href="/static/manifest.json">
        <?=Html::csrfMetaTags()?>
        <?php $this->head()?>
    </head>
    <body>
        <header class="page-header">
            <?php if($currentUserRole == Users::ROLE_BUYER):?>
                <?=$this->render('../partials/superHeader.php')?>
            <?php endif;?>
            <div class="container page-header__container">
                <div class="grid page-header__grid">
                    <?php if($currentUserRole == Users::ROLE_COMPANY):?>
                        <div class="grid__column_side-panel menu-btn-wrapper<?=($isMinimized ? ' _is-minimized' : '')?>">
                            <a class="logo logo_panel" href="/">Строймаркет</a>
                            <button class="menu-btn"
                                    type="button"
                                    onclick="<?=(!$isMinimized ? 'sidePanel.toggle()' : '')?>"></button>
                        </div>
                    <?php else:?>
                        <div class="grid__column_1-4">
                            <a class="logo" href="/">Строймаркет</a>
                        </div>
                    <?php endif;?>
                    <div class="grid__column">
                        <?php if($user):?>
                            <ul class="list-reset user-actions">
                                <?php /*
                                <li class="user-actions__item user-actions__item_notifications" data-accordion>
                                    <a class="accordion-title link user-actions__link">
                                        Уведомления
                                    </a>
                                    <div class="accordion-content notification-popup" hidden>
                                        <header class="notification-popup__header">
                                            Уведомления
                                        </header>
                                        <ul class="notification-popup__list">
                                            <li>
                                                <a class="link notification-popup__link" href="javascript:void(0)">
                                                    <div class="notification-popup__data">
                                                        <p class="notification-popup__caption">Закупка №111</p>
                                                        <p class="notification-popup__text">Новое предложение по закупке</p>
                                                    </div>
                                                    <time class="notification-popup__date" datetime="2017-10-20 12:36">
                                                        сегодня<br>
                                                        12:36
                                                    </time>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="link notification-popup__link" href="javascript:void(0)">
                                                    <div class="notification-popup__data">
                                                        <p class="notification-popup__caption">Претензия №123</p>
                                                        <p class="notification-popup__text">Новое предложение по закупке</p>
                                                    </div>
                                                    <time class="notification-popup__date" datetime="2017-10-15 15:10">
                                                        15.10.17<br>
                                                        15:10
                                                    </time>
                                                </a>
                                            </li>
                                        </ul>
                                        <a class="link notification-popup__show-more" href="javascript:void(0)">
                                            Показать все
                                        </a>
                                    </div>
                                </li>
                                <li class="user-actions__item user-actions__item_messages">
                                    <a class="link user-actions__link" href="javascript:void(0)">
                                        Сообщения
                                    </a>
                                </li>
                                */?>
                                <li class="user-actions__item user-actions__item_profile">
                                    <a class="link user-actions__link" href="/profile">
                                        <?=$user->displayName?>
                                        <?php $logoUrl = ImageManager::getUrl($user, 'logo', null, 44)?>
                                        <span class="user-actions__avatar" style="background-image: url(<?=$logoUrl?>);"></span>
                                    </a>
                                </li>
                                <li class="user-actions__item user-actions__item_logout">
                                    <a class="link user-actions__link" href="/logout">
                                        Выйти
                                    </a>
                                </li>
                            </ul>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </header>
        <main<?=($isWhiteBody ? ' class="main main_white"' : '')?>>
            <div class="container">
                <?php if($currentUserRole == Users::ROLE_COMPANY):?>
                    <div class="grid">
                        <div class="grid__column_side-panel<?=($isMinimized ? ' _is-minimized' : '')?>">
                            <?=$this->render('../partials/dashboardMenu.php')?>
                        </div>
                        <div class="grid__column">
                            <div class="dashboard">
                                <?=$content?>
                            </div>
                        </div>
                    </div>
                <?php else:?>
                    <div class="dashboard">
                        <?=$content?>
                    </div>
                <?php endif;?>
            </div>
        </main>
        <?=$this->render('../partials/toast.php')?>
        <?php $this->endBody()?>
    </body>
</html>
<?php $this->endPage()?>