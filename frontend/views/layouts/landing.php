<?php
use yii\helpers\Html;
use common\components\ImageManager\ImageManager;
use frontend\assets\pages\LandingAsset;
use frontend\models\Users;

/* @var $this \yii\web\View */
/* @var $content string */
/* @var Users $user */

$currentUserRole = $this->params['currentUserRole'] ?? false;
$isShowRegistration = $this->params['isShowRegistration'] ?? true;
$isCustomer = isset($this->params['isCustomer']) ? $this->params['isCustomer'] : false;
$user = Yii::$app->user->identity;
$this->title = Yii::t('landing', 'main_title');

$landings = [
    '/business/manufacturer' => Yii::t('landing', 'for_manufacturers'),
    '/business/provider' => Yii::t('landing', 'for_providers'),
    '/business/store' => Yii::t('landing', 'for_stores'),
    '/business/picker' => Yii::t('landing', 'for_pickers'),
];

LandingAsset::register($this);
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="google-site-verification" content="JhntY9SWhCd47LHfTbcHeBmhyO4sIQbq1xtKp8Jviy8" />
        <title><?=Html::encode($this->title)?></title>
        <link rel="apple-touch-icon" sizes="180x180" href="/static/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon-32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon-16.png">
        <link rel="manifest" href="/static/manifest.json">
        <?=Html::csrfMetaTags()?>
        <?php $this->head()?>
    </head>
    <body>
        <header class="landing-header">
            <?php if($currentUserRole == Users::ROLE_BUYER):?>
                <?=$this->render('../partials/superHeader.php', ['isLanding' => true])?>
            <?php endif;?>
            <div class="container landing-header__container">
                <a class="logo landing-header__logo" href="/"><?=Yii::t('landing', 'main_title')?></a>
                <?php if(Yii::$app->request->url != '/'):?>
                    <select class="text-select landing-header__select" id="for-whom">
                        <?php foreach($landings as $url => $caption):?>
                            <option value="<?=$url?>"<?=(Yii::$app->request->url == $url ? ' selected' : '')?>><?=$caption?></option>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
                <?php if($currentUserRole == Users::ROLE_COMPANY):?>
                    <a class="link user-actions__link landing-header__phone-link" href="tel:<?=Yii::$app->params['globalSettings']['contactPhone']?>">
                        <?=Yii::$app->params['globalSettings']['contactPhone']?>
                    </a>
                <?php endif;?>
                <ul class="list-reset user-actions landing-header__user-actions">
                    <?php /*if($currentUserRole == Users::ROLE_BUYER):?>
                        <li class="user-actions__item user-actions__item_icon">
                            <a class="link user-actions__link" href="javascript:void(0)">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                                     viewBox="0 0 19 20" width="19" height="20">
                                    <use xlink:href="/static/img/header-icons.svg#cart"></use>
                                </svg>
                                Корзина
                            </a>
                        </li>
                    <?php endif;*/?>
                    <?php if($user):?>
                        <li class="user-actions__item user-actions__item_profile">
                            <a class="link user-actions__link" href="/profile">
                                <?=$user->displayName?>
                                <?php $logoUrl = ImageManager::getUrl($user, 'logo', null, 44)?>
                                <span class="user-actions__avatar" style="background-image: url(<?=$logoUrl?>);"></span>
                            </a>
                        </li>
                        <li class="user-actions__item user-actions__item_logout">
                            <a class="link user-actions__link" href="/logout">
                                <?=Yii::t('landing', 'logout')?>
                            </a>
                        </li>
                    <?php else:?>
                        <?php if($isShowRegistration):?>
                            <li class="user-actions__item">
                                <a class="link user-actions__link" <?php //href="/registration"?> onclick="modal.open('#registration-info')">
                                    <?=Yii::t('landing', 'registration')?>
                                </a>
                            </li>
                            <li class="user-actions__item user-actions__item_login">
                                <a class="link user-actions__link"<?php //href="/login"?> onclick="modal.open('#registration-info')">
                                    <?=Yii::t('landing', 'login')?>
                                </a>
                            </li>
                        <?php endif;?>
                    <?php endif;?>
                    <?=$this->render('../partials/langSwitcher.php')?>
                </ul>
            </div>
        </header>
        <?=$content?>
        <footer class="landing-footer">
            <div class="container">
                <a class="logo landing-footer__logo"><?=Yii::t('landing', 'main_title')?></a>
                <div class="grid">
                    <div class="grid__column_1-3">
                        <p class="landing-footer__contacts">
                            <?=Yii::t('landing', 'administration_phone')?>
                            <span><?=Yii::$app->params['globalSettings']['contactPhone']?></span>
                        </p>
                        <p class="landing-footer__contacts">
                            <?=Yii::t('landing', 'support')?>
                            <span><?=Yii::$app->params['globalSettings']['contactEmail']?></span>
                        </p>
                        <p class="landing-footer__contacts">
                            © <?=Yii::t('landing', 'main_title')?>
                        </p>
                        <?php /*
                        <p class="landing-footer__contacts">
                            999999, г. Город, ул. Улица, д. 99, строение 99, офис 999
                        </p>
                        <p class="landing-footer__contacts">
                            Почтовый адрес: 999999, г. Город, ул. Улица, д. 99, строение 99, офис 999
                        </p>
                        <p class="landing-footer__contacts">
                            ИНН: 1234567890, ОГРН: 12345678901234
                        </p>
                        */?>
                    </div>

                    <div class="grid__column_1-3">
                        <ul class="list-reset landing-footer__list">
                            <li>
                                <a href="javascript:void(0)"><?=Yii::t('landing', 'faq')?></a>
                            </li>
                            <li>
                                <a href="/agreement"><?=Yii::t('landing', 'privacy_policy')?></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><?=Yii::t('landing', 'search_products')?></a>
                            </li>
                        </ul>
                    </div>

                    <div class="grid__column_1-3">
                        <ul class="list-reset landing-footer__list">
                            <?php foreach($landings as $url => $caption):?>
                                <li>
                                    <a href="<?=$url?>"><?=$caption?></a>
                                </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal modal_middle" id="registration-info">
            <div class="container modal__container">
                <div class="grid">
                    <div class="grid__column_1-3 grid__column_center">
                        <div class="modal__overlay"></div>
                        <div class="modal__body modal__body_contrast modal__body_compact">
                            <button class="modal__close" type="button"><?=Yii::t('landing', 'close')?></button>
                            <div class="modal__alert modal__alert_info">
                                <b class="modal__alert-title"><?=Yii::t('landing', 'info')?></b>
                                <p class="modal__alert-text">
                                    <?=Yii::t('landing', 'registration_info', [
                                        'phone' => Yii::$app->params['globalSettings']['contactPhone'],
                                        'email' => Yii::$app->params['globalSettings']['contactEmail'],
                                    ])?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $this->endBody()?>
    </body>
</html>
<?php $this->endPage()?>