<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\StroimarketAsset;

StroimarketAsset::register($this);
?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title><?=Html::encode($this->title)?></title>
        <link rel="apple-touch-icon" sizes="180x180" href="/static/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/static/favicon-32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/static/favicon-16.png">
        <link rel="manifest" href="/static/manifest.json">
        <?=Html::csrfMetaTags()?>
        <?php $this->head()?>
    </head>
    <body class="body body_fullscreen">
        <main>
            <?=$content?>
        </main>
        <?php $this->endBody()?>
    </body>
</html>
<?php $this->endPage()?>