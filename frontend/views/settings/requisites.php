<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use frontend\models\Users;
use frontend\models\forms\RequisitesInfoForm;
use frontend\assets\lib\FormValidationAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user Users */
/* @var $requisitesInfoForm RequisitesInfoForm */

$this->title = 'Реквизиты | Настройки';
?>
<h2 class="h2 dashboard__h2">Настройки</h2>
<h3 class="h3">Реквизиты</h3>

<?php $form = ActiveForm::begin([
    'fieldConfig' => [
        'template' => '
            <div class="grid form__row">
                <div class="grid__column_1-3">
                    {label}
                </div>
                <div class="grid__column_2-3">
                    <p class="form__field">
                        {input}
                        <span class="form__error">
                            <b>Ошибка ввода</b>
                            <span></span>
                        </span>
                    </p>
                </div>
            </div>
        ',
        'options' => ['tag' => false],
        'labelOptions' => ['class' => 'form__label'],
    ],
    'options' => [
        'class' => 'form dashboard__form',
        'id' => 'requisites-settings-form',
    ]
]);?>
    <div class="grid">
        <div class="grid__column_1-1">
            <p class="form__label-caption">
                <?php if($user->status != Users::STATUS_ACTIVE):?>
                    У вас нет балла за соответствие данных фирмы.
                <?php else:?>
                    У вас есть балл за соответствие данных фирмы — название фирмы и реквизиты вы можете изменить
                    только через сотрудников финансового отдела «Строймаркета» и при условии, что учредители в старой
                    и новой организациях полностью совпадают, либо учредитель старой организации имеет более 50% в
                    новой организации.
                <?php endif;?>
            </p>
        </div>
    </div>

    <fieldset>
        <?php if($user->status != Users::STATUS_ACTIVE):?>
            <?=$form->field($requisitesInfoForm, 'legal_name')
                ->textInput(['maxlength' => true, 'class' => "input"])?>
            <?=$form->field($requisitesInfoForm, 'inn')
                ->textInput(['maxlength' => true, 'class' => "input"])?>
            <?=$form->field($requisitesInfoForm, 'ogrn')
                ->textInput(['maxlength' => true, 'class' => "input"])?>
            <?=$form->field($requisitesInfoForm, 'kpp')
                ->textInput(['maxlength' => true, 'class' => "input"])?>
            <?=$form->field($requisitesInfoForm, 'okpo')
                ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?php else:?>
            <div class="grid form__row form__row_readonly">
                <div class="grid__column_1-3">
                    <label class="form__label" for="requisites_name">
                        Название юрлица:
                    </label>
                </div>
                <div class="grid__column_2-3">
                    <input class="input input_text" id="requisites_name" type="text"
                           placeholder="" value="<?=Html::encode($user->legalEntityVcard)?>" readonly>
                </div>
            </div>

            <div class="grid form__row form__row_readonly">
                <div class="grid__column_1-3">
                    <label class="form__label" for="requisites_sf1">
                        ИНН:
                    </label>
                </div>
                <div class="grid__column_2-3">
                    <input class="input input_text" id="requisites_sf1" type="text"
                           placeholder="" value="<?=$user->inn?>" readonly>
                </div>
            </div>

            <div class="grid form__row form__row_readonly">
                <div class="grid__column_1-3">
                    <label class="form__label" for="requisites_sf2">
                        ОГРН:
                    </label>
                </div>
                <div class="grid__column_2-3">
                    <input class="input input_text" id="requisites_sf2" type="text"
                           placeholder="" value="<?=$user->ogrn?>" readonly>
                </div>
            </div>

            <div class="grid form__row form__row_readonly">
                <div class="grid__column_1-3">
                    <label class="form__label" for="requisites_sf3">
                        КПП:
                    </label>
                </div>
                <div class="grid__column_2-3">
                    <input class="input input_text" id="requisites_sf3" type="text"
                           placeholder="" value="<?=$user->kpp?>" readonly>
                </div>
            </div>

            <div class="grid form__row form__row_readonly">
                <div class="grid__column_1-3">
                    <label class="form__label" for="requisites_sf4">
                        ОКПО:
                    </label>
                </div>
                <div class="grid__column_2-3">
                    <input class="input input_text" id="requisites_sf4" type="text"
                           placeholder="" value="<?=$user->okpo?>" readonly>
                </div>
            </div>
        <?php endif;?>

        <?=$form->field($requisitesInfoForm, 'account')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($requisitesInfoForm, 'bik')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($requisitesInfoForm, 'bank')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($requisitesInfoForm, 'corr_account')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($requisitesInfoForm, 'legal_address')
            ->textarea(['class' => "input", 'rows' => 1])?>
        <?=$form->field($requisitesInfoForm, 'post_address')
            ->textarea(['class' => "input", 'rows' => 1])?>
    </fieldset>

    <?=$this->render('../partials/form/settingsSubmits')?>
<?php ActiveForm::end();?>
<?php
FormValidationAsset::register($this);
?>
