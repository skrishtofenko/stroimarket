<?php
use yii\bootstrap\ActiveForm;
use common\components\ImageManager\ImageManager;
use frontend\models\Users;
use frontend\models\Notices;
use frontend\models\Categories;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $notices Notices */
/* @var $user Users */
/* @var $categoriesTree array */

$this->title = 'Уведомления | Настройки';
?>
<h2 class="h2 dashboard__h2">Настройки</h2>
<h3 class="h3">Уведомления</h3>

<?php $form = ActiveForm::begin([
    'fieldConfig' => [
        'template' => '
            <div class="">
                {input}
                {label}
            </div>
        ',
        'options' => ['tag' => false],
        'labelOptions' => ['class' => 'form__label', 'data-checked' => "включить", 'data-not-checked' => "выключить"],
    ],
    'options' => [
        'class' => 'form dashboard__form',
        'id' => 'requisites-settings-form',
    ]
]);?>
    <fieldset>
        <legend>Email</legend>

        <?=$form->field($notices, 'messages')
            ->checkbox(['class' => "form__switcher form__switcher_condensed form__switcher_wide"], false)?>
        <?php if($user->ownership_type_id && $user->hasStore):?>
            <?=$form->field($notices, 'orders')
                ->checkbox(['class' => "form__switcher form__switcher_condensed form__switcher_wide"], false)?>
            <?=$form->field($notices, 'reviews')
                ->checkbox(['class' => "form__switcher form__switcher_condensed form__switcher_wide"], false)?>
        <?php endif;?>
        <?=$form->field($notices, 'specoffers')
            ->checkbox(['class' => "form__switcher form__switcher_condensed form__switcher_wide"], false)?>
        <?=$form->field($notices, 'actions', ['template' => '
            <div class="relative">
                {input}
                {label}
                <a class="link notices__category-chooser" onclick="modal.open(\'#categories\')">
                    Выбрать категории
                </a>
            </div>
        '])
            ->checkbox(['class' => "form__switcher form__switcher_condensed form__switcher_wide",], false)?>

        <?=$this->render('../partials/form/settingsSubmits')?>
    </fieldset>

    <div class="modal" id="categories">
        <div class="container modal__container">
            <div class="grid">
                <div class="grid__column_4-7 grid__column_center">
                    <div class="modal__overlay"></div>
                    <header class="modal__header">
                        <button class="modal__close" type="button">Закрыть</button>
                        <h3 class="modal__heading">
                            Выбрать категории
                        </h3>
                    </header>
                    <div class="modal__body modal__body_contrast modal__body_categories">
                        <div class="grid">
                            <div class="grid__column_1-2">
                                <?php
                                $totalCategoriesCount = 0;
                                foreach($categoriesTree as $chapter) {
                                    /** @var $chapter Categories */
                                    $totalCategoriesCount += count($chapter->children);
                                }
                                $half = ceil($totalCategoriesCount / 2);
                                $i = 1;
                                ?>
                                <?=$form->field($notices, "categoriesIds[]")->hiddenInput()->label(false)?>
                                <?php foreach($categoriesTree as $chapter):?>
                                    <?php /** @var $chapter Categories */?>
                                    <?php foreach ($chapter->children as $category):?>
                                        <?php /** @var $category Categories */?>
                                        <div class="categories">
                                            <?php $categoryImg = ImageManager::getUrl($category, 'image', 180, 120, ImageManager::RESIZE_ZOOM_IN)?>
                                            <div class="categories__img" style="background-image: url(<?=$categoryImg?>);"></div>
                                            <h4 class="categories__heading"><?=$category->name?></h4>
                                            <ul class="categories__list" id="cg-<?=$category->id?>">
                                                <?php foreach($category->children as $subcategory):?>
                                                    <?php /** @var $subcategory Categories */?>
                                                    <li data-accordion>
                                                        <span class="accordion-title"><?=$subcategory->name?></span>
                                                        <ul class="accordion-content" hidden>
                                                            <?php foreach($subcategory->children as $goodscategory):?>
                                                                <?php /** @var $goodscategory Categories */?>
                                                                <li>
                                                                    <?=$form->field($notices, "categoriesIds[$goodscategory->id]", ['template' => '{input}'])
                                                                        ->checkbox([
                                                                            'id' => "categoryCB-{$goodscategory->id}",
                                                                            'uncheck' => null,
                                                                        ], false)->label(false)?>
                                                                    <label for='categoryCB-<?=$goodscategory->id?>'><?=$goodscategory->name?></label>
                                                                </li>
                                                            <?php endforeach;?>
                                                        </ul>
                                                    </li>
                                                <?php endforeach;?>
                                            </ul>
                                            <?php if(count($category->children) > 5):?>
                                                <button class="categories__show-more" type="button" onclick="categories.open('#cg-<?=$category->id?>')">
                                                    Показать ещё
                                                </button>
                                            <?php endif;?>
                                        </div>
                                        <?php if($i == $half):?>
                                            </div>
                                            <div class="grid__column_1-2">
                                        <?php endif;?>
                                        <?php $i++?>
                                    <?php endforeach;?>
                                <?php endforeach;?>
                            </div>
                            <div class="grid grid_small">
                                <div class="grid__column_3-5 grid__column_center">
                                    <button class="button form__button" type="button" onclick="modal.close('#categories')">
                                        Подтвердить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php ActiveForm::end();?>
