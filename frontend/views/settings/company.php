<?php
use yii\bootstrap\ActiveForm;
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\Vocabularies;
use frontend\models\forms\CompanyInfoForm;
use frontend\assets\pages\SettingsCompanyAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user Users */
/* @var $company Companies */
/* @var $companyInfoForm CompanyInfoForm */
/* @var $cities array */

$this->title = 'Профиль компании | Настройки';
?>
<h2 class="h2 dashboard__h2">Настройки</h2>
<h3 class="h3">Профиль компании</h3>

<?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form dashboard__form']]);?>
    <div class="grid form__row">
        <div class="grid__column_1-1">
            <p class="form__label-caption">
                <?php if($user->status != Users::STATUS_ACTIVE):?>
                    У вас нет балла за соответствие названия компании.
                <?php else:?>
                    У вас есть балл за соответствие названия компании — теперь название фирмы вы можете изменить только
                    через сотрудников финансового отдела «Строймаркета» и при условии, что учредители в старой и новой
                    организациях полностью совпадают, либо учредитель старой организации имеет более 50% в новой
                    организации.
                <?php endif;?>
            </p>
        </div>
    </div>
    <div class="grid form__row">
        <div class="grid__column_5-9">
            <label class="form__label" for="companyinfoform-legal_name">
                Название компании:
            </label>
        </div>
        <div class="grid__column_4-9">
            <?php if($user->status != Users::STATUS_ACTIVE):?>
                <?=$form->field($companyInfoForm, 'name')
                    ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Текст", 'required' => true])?>
            <?php else:?>
                <input class="input input_text" id="companyinfoform-legal_name" type="text"
                       value="<?=$user->legal_name?>" readonly>
            <?php endif;?>
        </div>
    </div>

    <?=$this->render('../partials/form/imageInput', [
        'form' => $form,
        'model' => $user,
        'attribute' => 'logo',
        'maxFileSize' => Users::imagesParams()['logo']['maxSizeInMB'],
    ])?>

    <div class="grid form__row">
        <div class="grid__column_5-9">
            <label class="form__label" for="companyinfoform-description">
                Информация об организации:
            </label>
        </div>
        <div class="grid__column_4-9">
            <?=$form->field($companyInfoForm, 'description')
                ->textInput(['class' => "input", 'placeholder' => "Текст"])?>
        </div>
    </div>
    <div class="grid form__row">
        <div class="grid__column_5-9">
            <label class="form__label" for="companyinfoform-url">
                Веб-сайт:
            </label>
        </div>
        <div class="grid__column_4-9">
            <?=$form->field($companyInfoForm, 'url')
                ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => "Текст", 'type' => 'url'])?>
        </div>
    </div>

    <hr class="hr">

    <div class="form__row text-selector">
        <span>Параметры для:</span>
        <select class="text-select" id="place">
            <?php foreach($user->companies as $otherCompany):?>
                <?php /** @var $otherCompany Companies */?>
                <option value="<?=$otherCompany->id?>"<?=($otherCompany->id == $company->id ? ' selected' : '')?>><?=$otherCompany->typeAsText?>, <?=$otherCompany->addressAsText?></option>
            <?php endforeach;?>
        </select>
    </div>

    <?php /*if($companyInfoForm->type == Companies::TYPE_STORE):?>
        <div class="form__row">
            <?=$form->field($companyInfoForm, 'is_imonly')
                ->checkbox(['class' => 'form__switcher'], false)?>
            <label for="companyinfoform-is_imonly" data-checked="да" data-not-checked="нет">
                Нет фактического адреса — только интернет магазин:
            </label>
        </div>
    <?php endif;*/?>
    <?php if($companyInfoForm->type != Companies::TYPE_STORE || !$companyInfoForm->is_imonly):?>
        <div class="grid form__row form__row_top address-block">
            <div class="grid__column_5-9">
                <p class="form__label">
                    Расположение фирмы:
                </p>
                <p class="form__label-caption">
                    Для того, чтобы изменить адрес или удалить магазин, обратитесь в администрацию «Строймаркета
                </p>
            </div>
            <div class="grid__column_4-9">
                <div class="grid grid_small">
                    <div class="grid__column_1-1">
                        <button class="button button_action" type="button" readonly disabled>
                            <?=$company->city_id ? $company->city->name : '&nbsp;'?>
                        </button>
                        <input type="text" id="profile_city" value="<?=$company->city_id ? $company->city->name : '&nbsp;'?>" hidden>
                    </div>
                </div>
                <div class="grid grid_small">
                    <div class="grid__column_1-2">
                        <input class="input" type="text"
                               placeholder="Улица" value="<?=$companyInfoForm->street?>"
                               readonly required>
                    </div>
                    <div class="grid__column_1-2">
                        <input class="input" type="text"
                               placeholder="Дом" value="<?=$companyInfoForm->house?>"
                               readonly required>
                    </div>
                </div>
                <div class="grid grid_small">
                    <div class="grid__column_1-2">
                        <select class="select form__select" id="room-type" disabled>
                            <?php foreach (Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_ROOMS]['items'] as $roomTypeId => $roomTypeName):?>
                                <option<?=($roomTypeId == $companyInfoForm->room_type_id ? ' selected' : '')?>><?=$roomTypeName?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                    <div class="grid__column_1-2">
                        <input class="input" type="text"
                               placeholder="Номер" value="<?=$companyInfoForm->room_number?>"
                               readonly required>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>

    <?php if($companyInfoForm->type == Companies::TYPE_STORE && $companyInfoForm->is_imonly):?>
        <div class="grid form__row">
            <div class="grid__column_5-9">
                <label class="form__label" for="companyinfoform-im_url">
                    Адрес интернет-магазина:
                </label>
            </div>
            <div class="grid__column_4-9">
                <input class="input" type="url" id="companyinfoform-im_url"
                       placeholder="Адрес интернет-магазина" value="<?=$companyInfoForm->im_url?>"
                       readonly required>
            </div>
        </div>
    <?php endif;?>
    <div class="grid form__row">
        <div class="grid__column_5-9">
            <label class="form__label" for="companyinfoform-phone">
                Контактный телефон:
            </label>
        </div>
        <div class="grid__column_4-9">
            <?=$form->field($companyInfoForm, "phone")
                ->textInput(['class' => "input", 'required' => true, 'type' => 'tel']);?>
        </div>
    </div>
    <div class="grid form__row">
        <div class="grid__column_5-9">
            <label class="form__label" for="companyinfoform-email">
                Контактный email:
            </label>
        </div>
        <div class="grid__column_4-9">
            <?=$form->field($companyInfoForm, "email")
                ->textInput(['maxlength' => true, 'class' => "input", 'required' => true, 'type' => 'email']);?>
        </div>
    </div>
    <?php if($company->type == Companies::TYPE_STORE):?>
        <?=$this->render('../partials/form/deliveryBlock', [
            'form' => $form,
            'company' => $companyInfoForm,
            'cities' => $cities,
        ])?>

        <?php if(!$company->is_imonly):?>
            <?=$this->render('../partials/form/scheduleBlock', [
                'form' => $form,
                'company' => $companyInfoForm,
            ])?>
        <?php endif;?>
    <?php endif;?>

    <hr class="hr">

    <?php if($user->mainCompany->type != Companies::TYPE_PICKER):?>
        <div class="grid form__row form__row_top">
            <div class="grid__column_5-9">
                <p class="form__label">
                    <?=($user->mainCompany->type == Companies::TYPE_PROVIDER ? 'Создать магазин' : 'Добавить магазин')?>:
                </p>
                <p class="form__label-caption">
                    <?php if($user->mainCompany->type == Companies::TYPE_PROVIDER):?>
                        Если хотите продавать товары через это юр. лиц обычным покупателям — зарегистрируйтесь как магазин.
                    <?php else:?>
                        Если у вас несколько магазинов зарегистрированных на одно юр. лицо — добавьте остальные магазины
                    <?php endif;?>
                </p>
            </div>
            <div class="grid__column_4-9">
                <div class="grid">
                    <div class="grid__column_1-1">
                        <button class="button button_action" type="button"
                                onclick="modal.open('#create-shop')">
                            <?=($user->mainCompany->type == Companies::TYPE_PROVIDER ? 'Зарегистрироваться как магазин' : 'Добавить магазин')?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>

    <?=$this->render('../partials/form/settingsSubmits')?>
<?php ActiveForm::end();?>

<div class="modal" id="create-shop">
    <div class="container modal__container">
        <div class="grid">
            <div class="grid__column_1-2 grid__column_center">
                <div class="modal__overlay"></div>
                <header class="modal__header">
                    <button class="modal__close" type="button">Закрыть</button>
                    <h3 class="modal__heading">
                        Создать магазин
                    </h3>
                </header>
                <div class="modal__body">
                    <p>Вы уверены, что хотите создать магазин? Это действие нельзя отменить.</p>
                    <div class="grid grid_small">
                        <div class="grid__column_2-3 grid__column_right">
                            <div class="grid grid_small">
                                <div class="grid__column_2-5">
                                    <button class="button button_secondary" type="button" onclick="modal.close('#create-shop')">
                                        Отменить
                                    </button>
                                </div>
                                <div class="grid__column_3-5">
                                    <button class="button" type="button" onclick="window.location.href='/registration/add-store'">
                                        Подтвердить
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
SettingsCompanyAsset::register($this);
?>
