<?php
use yii\bootstrap\ActiveForm;
use frontend\models\Users;
use frontend\models\Requisites;
use frontend\assets\pages\SettingsPaymentAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user Users */
/* @var $requisites Requisites */

$this->title = 'Оплата | Настройки';
?>
<h2 class="h2 dashboard__h2">Выставление счёта</h2>

<?php $form = ActiveForm::begin([
    'fieldConfig' => [
        'template' => '
            <div class="grid form__row">
                <div class="grid__column_1-3">
                    {label}:
                </div>
                <div class="grid__column_2-3">
                    <p class="form__field">
                        {input}
                        <span class="form__error">
                            <b>Ошибка ввода</b>
                            <span></span>
                        </span>
                    </p>
                </div>
            </div>
        ',
        'options' => ['tag' => false],
        'labelOptions' => ['class' => 'form__label'],
    ],
    'options' => [
        'class' => 'form dashboard__form',
        'id' => 'payment-settings-form',
    ]
]);?>
    <div class="grid">
        <div class="grid__column_1-1">
            <p class="form__label-caption">
                Для подтверждения реквизитов фирмы, оплатите проверку именно с расчетного счета фирмы, данные
                которой указаны вами в личном кабинете.
            </p>
        </div>
    </div>

    <fieldset>
        <div class="grid form__row">
            <div class="grid__column_1-3">
                <label class="form__label" for="invoicing_amount">
                    Сумма платежа:
                </label>
            </div>
            <div class="grid__column_2-3">
                <input class="input" id="invoicing_amount" type="text"
                       placeholder="" value="5 690.00 рублей">
                <input class="checkbox invoicing__checkbox" id="invoicing_taxation" type="checkbox" checked>
                <label for="invoicing_taxation">Общая система налогообложения</label>
                <p class="form__label-caption invoicing__caption">
                    Внимание! Организации–платильщики НДС обязательно поставьте галочку.
                </p>
            </div>
        </div>

        <div class="grid form__row">
            <div class="grid__column_1-3">
                <label class="form__label" for="invoicing_license">
                    Лицензия:
                </label>
            </div>
            <div class="grid__column_2-3">
                <input class="input" id="invoicing_license" type="text"
                       placeholder="" value="Перевозчик">
            </div>
        </div>

        <div class="grid form__row">
            <div class="grid__column_1-3">
                <label class="form__label" for="invoicing_period">
                    Период:
                </label>
            </div>
            <div class="grid__column_2-3">
                <input class="input" id="invoicing_period" type="text"
                       placeholder="" value="90 дней">
            </div>
        </div>

        <div class="grid form__row">
            <div class="grid__column_1-3">
                <label class="form__label" for="invoicing_beginning">
                    Начало периода:
                </label>
            </div>
            <div class="grid__column_2-3">
                <input class="input" id="invoicing_beginning" type="text"
                       placeholder="" value="С момента окончания предыдущего оплаченного периода">
            </div>
        </div>

        <div class="grid form__row">
            <div class="grid__column_1-3">
                <label class="form__label" for="invoicing_payment">
                    Способ оплаты:
                </label>
            </div>
            <div class="grid__column_2-3">
                <input class="input" id="invoicing_payment" type="text"
                       placeholder="" value="Банковский перевод юридических лиц в рублях">
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Ваши реквизиты</legend>
        <div class="grid form__row">
            <div class="grid__column_1-1">
                <p class="form__label-caption invoicing__caption">
                    Будьте внимательны! Реквизиты заполняются согласно Вашим учредительным документам.
                </p>
            </div>
        </div>

        <?php /*
        <div class="grid form__row">
            <div class="grid__column_1-3">
                <label class="form__label">
                    Заполнить форму:
                </label>
            </div>
            <div class="grid__column_2-3">
                <select class="select form__select">
                    <option value="" selected>Выберите реквизиты</option>
                    <option value="">...</option>
                </select>
            </div>
        </div>
        */?>
        <?=$form->field($user, 'legal_name')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($user, 'inn')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($user, 'ogrn')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($user, 'kpp')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($user, 'okpo')
            ->textInput(['maxlength' => true, 'class' => "input"])?>

        <?=$form->field($requisites, 'account')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($requisites, 'bik')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($requisites, 'bank')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($requisites, 'corr_account')
            ->textInput(['maxlength' => true, 'class' => "input"])?>
        <?=$form->field($requisites, 'legal_address')
            ->textarea(['class' => "input", 'rows' => 1])?>
        <?=$form->field($requisites, 'post_address')
            ->textarea(['class' => "input", 'rows' => 1])?>
    </fieldset>

    <fieldset>
        <legend>Информация о фирме</legend>

        <?=$form->field($user, 'fio')
            ->textInput(['maxlength' => true, 'class' => "input", 'placeholder' => 'ФИО'])->label('Ф.И.О. контактного лица')?>
        <?=$form->field($user, 'phone')
            ->textInput(['class' => "input", 'placeholder' => 'Телефон', 'type' => 'tel'])->label('Телефон с кодом города')?>

        <div class="grid form__row">
            <div class="grid__column_1-3">
                <label class="form__label" for="invoicing_fax">
                    Факс:
                </label>
            </div>
            <div class="grid__column_2-3">
                <input class="input" id="invoicing_fax" type="tel"
                       placeholder="Факс" value="">
            </div>
        </div>
    </fieldset>

    <?=$this->render('../partials/form/settingsSubmits', [
        'submitCaption' => 'Выписать счет'
    ])?>
<?php ActiveForm::end();?>
<?php
SettingsPaymentAsset::register($this);
?>