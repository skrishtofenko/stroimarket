<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Users;
use frontend\models\Vocabularies;
use frontend\assets\pages\SettingsAccountAsset;
use frontend\models\forms\AccountSettingsForm;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $user Users */
/* @var $accountSettingsForm AccountSettingsForm */

$this->title = 'Аккаунт | Настройки';
?>
<h2 class="h2 dashboard__h2">Настройки</h2>
<h3 class="h3">Аккаунт</h3>

<?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form dashboard__form']]);?>
    <fieldset>
        <legend>Администратор аккаунта</legend>
        <div class="grid form__row">
            <div class="grid__column_5-9">
                <label class="form__label" for="accountsettingsform-fio">
                    ФИО администратора:
                </label>
            </div>
            <div class="grid__column_4-9">
                <?=$form->field($accountSettingsForm, 'fio')
                    ->textInput(['maxlength' => true, 'class' => 'input'])?>
            </div>
        </div>
        <div class="grid form__row">
            <div class="grid__column_5-9">
                <label class="form__label" for="accountsettingsform-phone">
                    Контактный телефон администратора:
                </label>
            </div>
            <div class="grid__column_4-9">
                <?=$form->field($accountSettingsForm, 'phone')
                    ->textInput(['class' => 'input', 'type' => 'tel'])?>
            </div>
        </div>
        <div class="grid form__row">
            <div class="grid__column_5-9">
                <label class="form__label" for="accountsettingsform-email">
                    Электронная почта администратора:
                </label>
            </div>
            <div class="grid__column_4-9">
                <?=$form->field($user, 'email')
                    ->textInput(['maxlength' => true, 'class' => 'input', 'type' => 'email', 'readonly' => true])?>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <?php if($user->ownership_type_id):?>
            <legend>Подтверждение данных фирмы</legend>
            <div class="grid form__row form__row_top">
                <div class="grid__column_5-9">
                    <p class="form__label">
                        Документально:
                    </p>
                    <p class="form__label-caption">
                        <?php if($user->status == Users::STATUS_ACTIVE):?>
                            Данные фирмы подтверждены
                        <?php elseif(!empty($user->checking_file) && $user->status != Users::STATUS_DECLINED):?>
                            Документ загружен, ожидайте проверки администратора
                        <?php else:?>
                            Вы не имеете балла за соответствие данных фирмы. Для того, чтоб подтвердить данные, пожалуйста,
                            загрузите отсканированную копию свидетельства ИНН вашего юридического лица или копию ОГРНИП для
                            индивидуальных предпринимателей.
                        <?php endif;?>
                    </p>
                </div>
                <?php if(($user->status != Users::STATUS_ACTIVE) && (empty($user->checking_file) || $user->status == Users::STATUS_DECLINED)):?>
                    <div class="grid__column_4-9">
                        <div class="grid grid_medium">
                            <div class="grid__column_1-2 registration__logo">
                                <?=Html::dropDownList('doc-type', null, Yii::$app->params['vocabularies'][Vocabularies::VOCABULARY_DOCS]['items'], ['class' => 'select', 'id' => 'doc-type'])?>
                            </div>
                            <div class="grid__column_1-2">
                                <label class="button button_action" for="users-checking_fileimage">
                                    Загрузить
                                </label>
                                <?=$form->field($user, "checking_fileImage")
                                    ->fileInput(['class' => 'input', 'onchange' => "checkUploadImage(event)"])?>
                                <input class="input" type="file" id="account_document">
                            </div>
                            <div class="image-comment">После загрузки документа нажмите "Сохранить изменения"</div>
                            <div class="image-errors"></div>
                        </div>
                    </div>
                <?php endif;?>
            </div>
            <div class="grid form__row form__row_top">
                <div class="grid__column_5-9">
                    <p class="form__label">
                        Оплатой:
                    </p>
                    <p class="form__label-caption">
                        <?php if($user->status == Users::STATUS_ACTIVE):?>
                            Данные фирмы подтверждены
                        <?php else:?>
                            Вы не имеете балла за соответствие данных фирмы. Для того, чтоб подтвердить данные, пожалуйста,
                            оплатите услуги «Строймаркета» с расчетного счета организации, данные которой указаны в разделе
                            Настройки / Реквизиты.
                        <?php endif;?>
                    </p>
                </div>
                <?php if($user->status != Users::STATUS_ACTIVE):?>
                    <div class="grid__column_4-9">
                        <div class="grid">
                            <div class="grid__column_1-1">
                                <button id="button-create-bill" class="button button_action" type="button">
                                    Выставить счет
                                </button>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        <?php endif;?>
        <?=$this->render('../partials/form/settingsSubmits')?>
    </fieldset>
<?php ActiveForm::end();?>
<?php
SettingsAccountAsset::register($this);
?>
