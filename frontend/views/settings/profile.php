<?php
use yii\bootstrap\ActiveForm;
use frontend\models\Users;
use frontend\models\forms\CompanyInfoForm;
use frontend\assets\pages\SettingsProfileAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $user Users */
/* @var $companyInfoForm CompanyInfoForm */

$this->title = 'Профиль | Настройки';
?>
<h2 class="h2 dashboard__h2">Настройки</h2>
<h3 class="h3">Профиль</h3>

<?php $form = ActiveForm::begin(['fieldConfig' => ['template' => '{input}', 'options' => ['tag' => false]], 'options' => ['class' => 'form dashboard__form']]);?>
    <fieldset>
        <div class="grid form__row">
            <div class="grid__column_5-9">
                <label class="form__label" for="companyinfoform-fio">
                    ФИО:
                </label>
            </div>
            <div class="grid__column_4-9">
                <?=$form->field($companyInfoForm, 'fio')
                    ->textInput(['maxlength' => true, 'class' => 'input'])?>
            </div>
        </div>

        <div class="grid form__row">
            <div class="grid__column_5-9">
                <label class="form__label" for="companyinfoform-phone">
                    Контактный телефон:
                </label>
            </div>
            <div class="grid__column_4-9">
                <?=$form->field($companyInfoForm, 'phone')
                    ->textInput(['class' => 'input', 'type' => 'tel'])?>
            </div>
        </div>

        <div class="grid form__row">
            <div class="grid__column_5-9">
                <label class="form__label" for="companyinfoform-email">
                    Электронная почта:
                </label>
            </div>
            <div class="grid__column_4-9">
                <?=$form->field($companyInfoForm, 'email')
                    ->textInput(['maxlength' => true, 'class' => 'input', 'type' => 'email'])?>
            </div>
        </div>

        <?=$this->render('../partials/form/imageInput', [
            'form' => $form,
            'model' => $user,
            'attribute' => 'logo',
            'maxFileSize' => Users::imagesParams()['logo']['maxSizeInMB'],
            'caption' => $user->ownership_type_id ? 'Логотип' : 'Фото',
        ])?>

        <?=$this->render('../partials/form/settingsSubmits')?>
    </fieldset>
<?php ActiveForm::end();?>
<?php
SettingsProfileAsset::register($this);
?>
