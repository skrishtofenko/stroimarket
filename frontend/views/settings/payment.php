<?php
use frontend\models\Users;

/* @var $this yii\web\View */
/* @var $user Users */

$this->title = 'Оплата | Настройки';
?>
<h2 class="h2 dashboard__h2">Настройки</h2>
<h3 class="h3">Оплата</h3>

<form class="form dashboard__form">
    <fieldset>
        <legend>Оплаченные периоды</legend>
        <div class="grid form__row">
            <label class="form__label">
                <?php if($user->mainCompany->is_paid):?>
                    Доступ оплачен до:
                    <?php $date = date('d.m.y', strtotime($user->mainCompany->paid_till))?>
                    <input class="input input_inline" type="text" value="<?=$date?>" readonly>
                <?php else:?>
                    Доступ не оплачен
                <?php endif;?>
            </label>
        </div>
    </fieldset>

    <fieldset>
        <legend>Способы оплаты</legend>
        <div class="grid grid_large">
            <div class="grid__column_1-2">
                <div class="payment-type">
                    <input id="payment-type_1" name="type" value="account" type="radio" checked>
                    <label for="payment-type_1">
                        Безналичный платёж с расчетного счета организации
                    </label>
                    <p class="payment-type__caption">
                        Внимание! Для того, чтобы в паспорте фирмы иметь балл «Свидетельство ведения фирмой текущей
                        хозяйственной деятельности» необходимо пользоваться данным способом оплаты каждые 190 дней.
                    </p>
                </div>
            </div>
            <div class="grid__column_1-2">
                <div class="payment-type">
                    <input id="payment-type_2" name="type" value="card" type="radio" disabled>
                    <label for="payment-type_2">
                        Кредитная карта
                    </label>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="grid">
        <div class="grid__column_1-3 grid__column_right">
            <button class="button form__button" type="submit">
                Перейти на страницу оплаты
            </button>
        </div>
    </div>
</form>
