<?php
use frontend\models\Users;
use frontend\models\Bills;

/* @var $this yii\web\View */
/* @var $user Users */
/* @var $bill Bills */

$this->title = 'Оплата | Настройки';
?>
<h2 class="h2 dashboard__h2">Счет выписан</h2>

<form class="form dashboard__form" action="/profile">
    <div class="grid">
        <div class="grid__column_1-1">
            <p class="form__label-caption">
                Теперь вам необходимо распечатать перечисленные ниже документы и передать их в бухгалтерию вашей
                компании на оплату.
            </p>
        </div>
    </div>

    <fieldset>
        <div class="grid form__row">
            <div class="grid__column_1-3">
                <p class="form__label">
                    Документы для распечатки:
                </p>
            </div>
            <div class="grid__column_2-3">
                <p class="invoicing__link">
                    <a class="link" href="/settings/bill?id=<?=$bill->id?>">
                        Счет на оплату
                    </a>
                </p>
                <p class="invoicing__link">
                    <a class="link" href="javascript:void(0)">
                        Сублицензионный договор–оферта
                    </a>
                </p>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <p class="form__label-caption invoicing__caption">
            Средний срок поступления оплаты на наш р/с 1–2 банковских дня.
        </p>
    </fieldset>

    <div class="grid">
        <div class="grid__column_1-3 grid__column_right">
            <button class="button form__button" type="submit">
                Завершить и закрыть
            </button>
        </div>
    </div>
</form>
