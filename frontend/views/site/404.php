<?php
/* @var $this yii\web\View */

$this->title = 'Страница не найдена';
?>
<div class="page-404">
    <div class="container page-404__container">
        <a class="logo page-404__logo" href="/">Строймаркет</a>
        <p class="page-404__not-found">
            Страницы не существует
        </p>
        <div class="grid">
            <div class="grid__column_1-6 grid__column_right">
                <a class="button" href="javascript:history.back()">
                    Вернуться назад
                </a>
            </div>
        </div>
    </div>
</div>
