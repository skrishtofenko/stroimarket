<?php
use frontend\assets\lib\BindSmoothScrollAsset;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<main>
    <section class="landing-promo landing-promo_full-screen">
        <div class="container landing-promo__container" style="background: linear-gradient(rgba(0,0,0,.65), rgba(0,0,0,.65)), #455056 url(/img/landing-picker.jpg) no-repeat center / cover">
            <h2 class="landing-promo__heading">
                <?=Yii::t('landing', 'picker_title')?>
            </h2>
            <div class="grid">
                <div class="grid__column_5-6 grid__column_center">
                    <p class="landing-promo__lead">
                        <?=Yii::t('landing', 'picker_subtitle')?>
                    </p>
                </div>
            </div>
            <div class="grid">
                <div class="grid__column_1-3 grid__column_center">
                    <a class="button landing-promo__button" <?php //href="/registration"?> onclick="modal.open('#registration-info')">
                        <?=Yii::t('landing', 'free_registration')?>
                    </a>
                </div>
            </div>
            <a class="link landing-promo__more-info" data-scroll href="#moreinfo">
                <?=Yii::t('landing', 'more_info')?>
            </a>
        </div>
    </section>

    <section class="landing-block" id="moreinfo">
        <div class="container">
            <div class="grid">
                <div class="grid__column_1-2">
                    <div class="landing-block__img landing-block__img_left">
                        <img src="/static/img/assets/l-3@1x.png" srcset="/static/img/assets/l-3@2x.png 2x" alt="">
                    </div>
                </div>
                <div class="grid__column_1-2" data-scroll="right">
                    <h3 class="landing-block__title">
                        <?=Yii::t('landing', 'picker_description')?>
                    </h3>
                    <p class="landing-block__text">
                        <?=Yii::t('landing', 'picker_registration_info')?>
                    </p>
                    <ul class="landing-block__list">
                        <li>
                            <?=Yii::t('landing', 'picker_registration_info_1')?>
                        </li>
                        <li>
                            <?=Yii::t('landing', 'picker_registration_info_2')?>
                        </li>
                        <li>
                            <?=Yii::t('landing', 'picker_registration_info_3')?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="landing-promo">
        <div class="container landing-promo__container" style="background: url(/img/purchase-bg.jpg) no-repeat center / cover;">
            <h2 class="landing-promo__heading">
                <?=Yii::t('landing', 'picker_purchases')?>
            </h2>
            <div class="grid">
                <div class="grid__column_5-6 grid__column_center">
                    <p class="landing-promo__lead">
                        <?=Yii::t('landing', 'picker_purchases_info')?>
                    </p>
                </div>
            </div>
            <div class="grid">
                <div class="grid__column_1-6 grid__column_center">
                    <a class="button landing-promo__button" href="javascript:void(0)">
                        <?=Yii::t('landing', 'open')?>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="landing-block landing-block_center">
        <div class="container">
            <div class="grid">
                <div class="grid__column_1-2 grid__column_center">
                    <h3 class="landing-block__title">
                        <?=Yii::t('landing', 'profit_title')?>
                    </h3>
                </div>
                <div class="grid__column_2-3 grid__column_center">
                    <p class="landing-block__text">
                        <?=Yii::t('landing', 'picker_profit_subtitle')?>
                    </p>
                </div>
            </div>

            <div class="grid landing-block__features">
                <div class="grid__column_1-3">
                    <div class="landing-block__feature landing-block__feature_catalog">
                        <?=Yii::t('landing', 'picker_profit_1')?>
                    </div>
                </div>
                <div class="grid__column_1-3">
                    <div class="landing-block__feature landing-block__feature_reduce-costs">
                        <?=Yii::t('landing', 'picker_profit_2')?>
                    </div>
                </div>
                <div class="grid__column_1-3">
                    <div class="landing-block__feature landing-block__feature_discounts">
                        <?=Yii::t('landing', 'picker_profit_3')?>
                    </div>
                </div>
            </div>

            <div class="grid">
                <div class="grid__column_1-3 grid__column_center">
                    <a class="button" <?php //href="/registration"?> onclick="modal.open('#registration-info')">
                        <?=Yii::t('landing', 'free_registration')?>
                    </a>
                </div>
            </div>
        </div>
    </section>
</main>
<?php BindSmoothScrollAsset::register($this)?>
