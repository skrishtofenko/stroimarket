<?php
/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<main>
    <ul class="list-reset expander" data-expander>
        <li class="expander__item _is-active" data-expander-item style="background-image: url(/img/landing-provider.jpg)">
            <div class="container">
                <h2 class="expander__title">
                    Поставщику и производителю
                </h2>
                <a class="link expander__link" href="/business/provider">
                    Перейти в раздел
                </a>
                <p class="expander__description">
                    Находитесь в единой базе поставщиков и производителей, участвуйте в закупках, расширяйте сбыт,
                    находите новых клиентов и получайте о них оценку от других участников рынка
                </p>
            </div>
        </li>
        <li class="expander__item" data-expander-item style="background-image: url(/img/landing-store.jpg)">
            <div class="container">
                <h2 class="expander__title">
                    Магазину
                </h2>
                <a class="link expander__link" href="/business/store">
                    Перейти в раздел
                </a>
                <p class="expander__description">
                    Находитесь в единой базе поставщиков и производителей, участвуйте в закупках, расширяйте сбыт,
                    находите новых клиентов и получайте о них оценку от других участников рынка
                </p>
            </div>
        </li>
        <li class="expander__item" data-expander-item style="background-image: url(/img/landing-picker.jpg)">
            <div class="container">
                <h2 class="expander__title">
                    Оптовому покупателю
                </h2>
                <a class="link expander__link" href="/business/picker">
                    Перейти в раздел
                </a>
                <p class="expander__description">
                    Находитесь в единой базе поставщиков и производителей, участвуйте в закупках, расширяйте сбыт
                    и находите новых клиентов
                </p>
            </div>
        </li>
        <li class="expander__item" data-expander-item style="background-image: url(/img/landing-contractor.jpg)">
            <div class="container">
                <h2 class="expander__title">
                    Строительному подрядчику
                </h2>
                <a class="link expander__link">
                    Раздел в разработке
                </a>
                <p class="expander__description">
                    Находитесь в единой базе поставщиков и производителей, участвуйте в закупках, расширяйте сбыт,
                    находите новых клиентов и получайте о них оценку от других участников рынка
                </p>
            </div>
        </li>
    </ul>
</main>