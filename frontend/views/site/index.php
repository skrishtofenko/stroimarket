<?php
use common\components\ImageManager\ImageManager;
use frontend\models\Categories;
use frontend\models\forms\PS;
use frontend\models\Companies;
use frontend\assets\lib\BindSmoothScrollAsset;
use frontend\assets\lib\BindTinySliderAsset;

/* @var $this yii\web\View */
/* @var $manufacturers Companies[] */

$this->title = Yii::$app->name;
?>
<main>
    <section class="landing-promo landing-promo_full-screen">
        <div class="container landing-promo__container" style="background: linear-gradient(rgba(0,0,0,.65), rgba(0,0,0,.65)), #455056 url(/img/landing-customer.jpg) no-repeat center / cover">
            <h2 class="landing-promo__heading">
                Единая&nbsp;база всех&nbsp;скидочных и&nbsp;стоковых&nbsp;предложений&nbsp;Москвы на&nbsp;строительные и&nbsp;отделочные&nbsp;материалы
            </h2>
            <div class="grid">
                <div class="grid__column_2-3 grid__column_center">
                    <p class="landing-promo__lead">
                        Найдите наиболее выгодные предложения и&nbsp;сэкономьте на&nbsp;закупке до&nbsp;70%
                    </p>
                </div>
            </div>
            <div class="grid">
                <div class="grid__column_2-3 grid__column_center">
                    <?=$this->render('../partials/search/products/searchField', ['forLanding' => true])?>
                </div>
            </div>
            <a class="link landing-promo__more-info" data-scroll href="#moreinfo">
                Перейти к каталогу
            </a>
        </div>
    </section>

    <?php /*<section class="landing-block landing-block_center">
        <div class="container">
            <div class="grid">
                <div class="grid__column_1-2 grid__column_center">
                    <h3 class="landing-block__title">
                        Что вам дает «Строймаркет»?
                    </h3>
                </div>
            </div>

            <div class="grid landing-block__features">
                <div class="grid__column_1-3">
                    <div class="landing-block__feature landing-block__feature_time">
                        <b>Экономия времени</b>
                        <p>Экономьте время и силы при поиске строительных и отделочных материалов</p>
                    </div>
                </div>
                <div class="grid__column_1-3">
                    <div class="landing-block__feature landing-block__feature_money">
                        <b>Экономия денег</b>
                        <p>Сравнивайте цены в разных магазинах и совершайте выгодные покупки</p>
                    </div>
                </div>
                <div class="grid__column_1-3">
                    <div class="landing-block__feature landing-block__feature_guarantees">
                        <b>Гарантии</b>
                        <p>Бронируйте товары онлайн или оформите доставку</p>
                    </div>
                </div>
            </div>

            <?php /*
            <div class="grid">
                <div class="grid__column_1-3 grid__column_center">
                    <a class="button" href="/registration">
                        Зарегистрироваться бесплатно
                    </a>
                </div>
            </div>
            * /?>
        </div>
    </section>*/?>

    <section class="landing-block landing-block_center">
        <div class="container">
            <div class="grid landing-block__factoids">
                <div class="grid__column_1-3">
                    <?php /*<div class="landing-block__factoid">
                        <span>более</span>
                        <b>200&nbsp;000</b>
                        товаров
                    </div>*/?>
                    <div class="landing-block__factoid">
                        <span>до</span>
                        <b>70%</b>
                        экономии
                    </div>
                </div>
                <div class="grid__column_1-3">
                    <div class="landing-block__factoid">
                        <span>более</span>
                        <b>300</b>
                        магазинов
                    </div>
                </div>
                <div class="grid__column_1-3">
                    <?php /*<div class="landing-block__factoid">
                        <span>более</span>
                        <b>3&nbsp;500</b>
                        производителей
                    </div>*/?>
                    <div class="landing-block__factoid">
                        <span>более</span>
                        <b>2&nbsp;000</b>
                        предложений
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php /*
    <section class="landing-block landing-block_center">
        <div class="container producers">
            <b class="producers__title">
                Выбирайте лучшее, сравнивая товары более чем 3500 производителей
            </b>
            <ul class="list-reset producers__list">
                <?php foreach ($manufacturers as $manufacturer):?>
                    <li>
                        <img src="<?=ImageManager::getUrl($manufacturer->user, 'logo', 90, 90, ImageManager::RESIZE_ZOOM_OUT)?>" alt="">
                    </li>
                <?php endforeach;?>
            </ul>
            <button class="button producers__more" type="button" onclick="location.href='/catalog/manufacturers'">
                Открыть каталог производителей
            </button>
        </div>
    </section>
    */?>

    <section class="landing-block landing-block_assortment" id="moreinfo">
        <div class="container">
            <h2 class="h2 landing-block__h2">Каталог</h2>
            <div class="assortment">
                <?=$this->render('../partials/fullCategoriesList', [
                    'categories' => Categories::getFullTree(new PS()),
                    'categoryHref' => 'catalog',
                    'columnsCount' => 4,
                ])?>
            </div>
        </div>
    </section>
</main>
<?php
BindSmoothScrollAsset::register($this);
BindTinySliderAsset::register($this);
?>
