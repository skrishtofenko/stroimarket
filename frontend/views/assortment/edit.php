<?php
use common\components\ImageManager\ImageManager;
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\CompaniesProducts;
use frontend\models\forms\ProductForm;
use frontend\models\forms\CatalogUploadForm;
use frontend\assets\pages\AssortmentAsset;

/* @var $this yii\web\View */
/* @var $user Users */
/* @var $company Companies */
/* @var $products CompaniesProducts */
/* @var $productForm ProductForm */
/* @var $catalogUploadForm CatalogUploadForm */
/* @var $categoriesTree array */
/* @var $sort string */

$this->title = 'Ассортимент';
?>
<?=$this->render('../partials/assortment/header', [
    'user' => $user,
    'company' => $company,
])?>

<div class="edit-assortment">
    <?=$this->render('../partials/assortment/top', [
        'company' => $company,
        'currentMode' => 'edit',
        'catalogUploadForm' => $catalogUploadForm,
    ])?>
    <table class="edit-assortment__table">
        <thead>
            <tr>
                <th>Соответствие</th>
                <th<?=($sort == 'art' ? ' class="_is-active"' : '')?>><a class="link"<?=($sort != 'art' ? ' href="?sort=art"' : '')?>>Артикул</a></th>
                <th<?=($sort == 'sm_art' ? ' class="_is-active"' : '')?>><a class="link"<?=($sort != 'sm_art' ? ' href="?sort=sm_art"' : '')?>>Артикул СМ</a></th>
                <th<?=($sort == 'product' ? ' class="_is-active"' : '')?>><a class="link"<?=($sort != 'product' ? ' href="?sort=product"' : '')?>>Товар</a></th>
                <th<?=($sort == 'category' ? ' class="_is-active"' : '')?>><a class="link"<?=($sort != 'category' ? ' href="?sort=category"' : '')?>>Категория</a></th>
                <?php if($company->type == Companies::TYPE_STORE):?>
                    <th>Наличие</th>
                <?php endif;?>
                <th>Обновлен</th>
                <?php if($company->type == Companies::TYPE_STORE):?>
                    <th>Цена</th>
                <?php else:?>
                    <th>Акция</th>
                <?php endif;?>
                <th>Действие</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($products as $companyProduct):?>
                <?php /** @var $companyProduct CompaniesProducts */?>
                <tr>
                    <td<?=($companyProduct->product ? ' data-accordion="product"' : '')?>>
                        <?php if($companyProduct->product):?>
                            <span class="accordion-title at-conformity at-conformity_got"></span>
                            <div class="accordion-content" hidden>
                                <img src="<?=ImageManager::getUrl($companyProduct->product, 'image', 80, 80, ImageManager::RESIZE_ZOOM_IN)?>">
                                <b><?=$companyProduct->product->fullName?></b>
                                <i>Артикул СМ: <?=$companyProduct->product->id?></i>
                                <p><?=$companyProduct->product->fullCategory?></p>
                            </div>
                        <?php else:?>
                            <button class="at-conformity at-conformity_choose" type="button" onclick="modal.open('#add-product'); fillProductForm(<?=$companyProduct->id?>);">
                                Выбрать
                            </button>
                        <?php endif;?>
                    </td>
                    <td><?=($companyProduct->vendor_code ?: '—')?></td>
                    <td><?=$companyProduct->product ? $companyProduct->product->id : '—'?></td>
                    <td><?=($companyProduct->product ? $companyProduct->product->fullName : (!empty($companyProduct->name) ? $companyProduct->name : '—'))?></td>
                    <td<?=($companyProduct->product ? ' class="at-mute"' : '')?>>
                        <?=($companyProduct->product ? $companyProduct->product->fullCategory : '—')?>
                    </td>
                    <?php if($company->type == Companies::TYPE_STORE):?>
                        <td><?=($companyProduct->is_in_stock ? 'В наличии' : ($companyProduct->is_by_order ? 'Под  заказ' : 'Нет в наличии'))?></td>
                    <?php endif;?>
                    <td><?=date('d.m.y', strtotime($companyProduct->updated_at))?></td>
                    <?php if($company->type == Companies::TYPE_STORE):?>
                        <td><?=$companyProduct->cost?> руб</td>
                    <?php else:?>
                        <td>
                            <?php if($companyProduct->is_action):?>
                            <?=$companyProduct->cost?> руб / <?=$companyProduct->cnt?> <?=($companyProduct->product ? $companyProduct->product->unitType->name : 'ед.')?>
                            <?php else:?>
                                —
                            <?php endif;?>
                        </td>
                    <?php endif;?>
                    <td class="at-action" data-accordion="action">
                        <span class="accordion-title"></span>
                        <ul class="accordion-content" hidden>
                            <?php if(!$companyProduct->is_manufactured_by):?>
                                <li onclick="if(confirm('Вы действительно хотите удалить этот товар?')) {window.location.href='/assortment/remove?id=<?=$companyProduct->id?>'}">Удалить товар</li>
                            <?php endif;?>
                            <li onclick="accordion.close(this.parentNode.parentNode); modal.open('#add-product'); fillProductForm(<?=$companyProduct->id?>);">
                                Редактировать
                            </li>
                            <li>Открыть товар</li>
                        </ul>
                        <div id="product<?=$companyProduct->id?>" hidden>
                            <?php if($companyProduct->product):?>
                                <div id="product-<?=$companyProduct->id?>-conformity-info">
                                    <img src="<?=ImageManager::getUrl($companyProduct->product, 'image', 80, 80, ImageManager::RESIZE_ZOOM_IN)?>" alt="">
                                    <b><?=$companyProduct->product->fullName?></b>
                                    <i>Артикул СМ: <?=$companyProduct->product_id?></i>
                                    <p><?=$companyProduct->product->fullCategory?></p>
                                </div>
                            <?php endif;?>
                            <input type="hidden" id="product-<?=$companyProduct->id?>-productid" value="<?=$companyProduct->product_id?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-id" value="<?=$companyProduct->id?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-name" value="<?=$companyProduct->name?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-vendorcode" value="<?=$companyProduct->vendor_code?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-url" value="<?=$companyProduct->url?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-cost" value="<?=$companyProduct->cost?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-cnt" value="<?=$companyProduct->cnt?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-isvisible" value="<?=$companyProduct->status?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-ispromo" value="<?=$companyProduct->is_promo?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-companyType" value="<?=$company->type?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-updatedAt" value="<?=date('d.m.y', strtotime($companyProduct->updated_at))?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-unittype" value="<?=($companyProduct->product ? $companyProduct->product->unitType->name : 'шт.')?>" />
                            <input type="hidden" id="product-<?=$companyProduct->id?>-manufactured" value="<?=$companyProduct->is_manufactured_by?>" />
                            <?php if($company->type == Companies::TYPE_PROVIDER):?>
                                <input type="hidden" id="product-<?=$companyProduct->id?>-isaction" value="<?=$companyProduct->is_action?>" />
                                <input type="hidden" id="product-<?=$companyProduct->id?>-multiplicity" value="<?=$companyProduct->multiplicity?>" />
                                <input type="hidden" id="product-<?=$companyProduct->id?>-unitTypeId" value="<?=$companyProduct->unit_type_id?>" />
                            <?php else:?>
                                <input type="hidden" id="product-<?=$companyProduct->id?>-isinstock" value="<?=$companyProduct->is_in_stock?>" />
                                <input type="hidden" id="product-<?=$companyProduct->id?>-isbyorder" value="<?=$companyProduct->is_by_order?>" />
                            <?php endif;?>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <?php /*<section class="edit-assortment__pagination">
        <a>Предыдущая</a>
        <a class="_is-current">1</a>
        <a href="javascript:void(0)">2</a>
        <a href="javascript:void(0)">…</a>
        <a href="javascript:void(0)">24</a>
        <a href="javascript:void(0)">25</a>
        <a href="javascript:void(0)">Следующая</a>
    </section>*/?>
</div>
<?=$this->render('../partials/assortment/productForm', [
    'productForm' => $productForm,
    'categoriesTree' => $categoriesTree,
    'company' => $company,
])?>
<?php
AssortmentAsset::register($this);
?>