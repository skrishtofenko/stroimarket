<?php
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\Categories;
use frontend\models\CompaniesProducts;
use frontend\assets\pages\AssortmentAsset;

/* @var $this yii\web\View */
/* @var $user Users */
/* @var $company Companies */
/* @var $category Categories */
/* @var $products CompaniesProducts[] */
/** @var Users $currentUser */
$currentUser = Yii::$app->user->identity;

$this->title = 'Ассортимент';
?>
<?=$this->render('../partials/assortment/header', [
    'user' => $user,
    'company' => $company,
    'category' => $category,
    'productsCount' => count($products),
])?>
<div class="assortment">
    <?=$this->render('../partials/assortment/catalogUpdated', ['company' => $company])?>
    <div class="grid">
        <div class="grid__column_7-9">
            <section class="assortment__interactions">
                <?php if($currentUser && $currentUser->id == $company->user_id):?>
                    <?=$this->render('../partials/assortment/modeSwitcher')?>
                <?php endif;?>
                <?=$this->render('../partials/assortment/searchString', ['noAction' => true])?>
            </section>
            <div class="layout-view">
                <?=$this->render('../partials/search/products/sortField', ['isOwner' => true])?>
                <?=$this->render('../partials/viewTypeToggle', ['selector' => 'product'])?>
            </div>

            <?=$this->render('../partials/productsList', [
                'products' => $products,
                'isOwner' => $currentUser && $currentUser->id == $user->id,
                'isAssortment' => true,
                'company' => $company,
            ])?>
        </div>
        <div class="grid__column_2-9">
            <?=$this->render('../partials/search/products/sidebarFilter', [
                'stayHere' => true,
                'characteristics' => $category->characteristics,
                'manufacturers' => $category->manufacturers,
            ])?>
        </div>
    </div>
</div>
<?php
AssortmentAsset::register($this);
?>
