<?php
use frontend\models\Users;
use frontend\models\Companies;
use frontend\assets\pages\AssortmentAsset;

/* @var $this yii\web\View */
/* @var $user Users */
/* @var $company Companies */
/* @var $companies Companies[] */
/* @var $categoriesTree array */
/** @var Users $currentUser */
$currentUser = Yii::$app->user->identity;

$this->title = 'Ассортимент';
?>
<?=$this->render('../partials/assortment/header', [
    'user' => $user,
    'company' => $company,
])?>

<div class="assortment">
    <?=$this->render('../partials/assortment/top', [
        'company' => $company,
    ])?>
    <?=$this->render('../partials/fullCategoriesList', [
        'categories' => $categoriesTree,
        'categoryHref' => $currentUser && $currentUser->id == $user->id ? null : "catalog/company/$company->id/assortment",
    ])?>
</div>
<?php
AssortmentAsset::register($this);
?>
