<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use frontend\models\Users;
use frontend\models\Products;
use frontend\models\Categories;
use frontend\models\Companies;
use frontend\models\forms\PS;
use frontend\models\forms\CS;
use frontend\models\forms\PCS;
use frontend\models\forms\SCS;

class CatalogController extends AppController {
    public $layout = 'dashboard';

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => [],
                'allow' => true,
            ],
        ];
        return $behaviors;
    }

    public function actionIndex() {
        /** @var PS $searchForm */
        $searchForm = &$this->productsSearchForm;
        $searchForm->load(Yii::$app->request->get());
        return $this->render('index', [
            'categoriesTree' => Categories::getFullTree($searchForm),
        ]);
    }

    public function actionCategory($categoryId) {
        $category = Categories::findOne($categoryId);
        if(!$category || $category->level != Categories::LEVEL_GOODSCATEGORY) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        /** @var PS $searchForm */
        $searchForm = &$this->productsSearchForm;
        $searchForm->load(Yii::$app->request->get());
        $searchForm->cat = $categoryId;
        return $this->render('category', [
            'category' => $category,
            'products' => Products::getByFilter($searchForm, Products::GET_SEARCH_RESULT_WITH_COUNT),
        ]);
    }

    public function actionProduct($productId) {
        if(!$product = Products::findOne(['id' => $productId, 'is_active' => 1])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $this->view->params['isWhiteBody'] = true;
        /** @var PCS $providersSearchForm */
        $providersSearchForm = &$this->providersSearchForm;
        $providersSearchForm->load(Yii::$app->request->get());
        $providersSearchForm->type = Companies::TYPE_PROVIDER;
        $providersSearchForm->productId = $productId;
        /** @var SCS $storesSearchForm */
        $storesSearchForm = &$this->storesSearchForm;
        $storesSearchForm->load(Yii::$app->request->get());
        $storesSearchForm->type = Companies::TYPE_STORE;
        $storesSearchForm->productId = $productId;
        if(isset($providersSearchForm->c)) {
            $storesSearchForm->c = $providersSearchForm->c;
        } else {
            unset($storesSearchForm->c);
        }
        return $this->render('product', [
            'product' => $product,
            'providers' => $this->currentUserRole == Users::ROLE_COMPANY ? Companies::getByFilter($providersSearchForm) : null,
            'stores' => Companies::getByFilter($storesSearchForm),
        ]);
    }

    public function actionCompanies($type) {
        $type = rtrim($type, 's');
        if(!isset(Companies::types()[$type])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        /** @var CS $companiesSearchForm */
        $companiesSearchForm = &$this->companiesSearchForm;
        $companiesSearchForm->load(Yii::$app->request->get());
        $companiesSearchForm->type = $type;
        return $this->render('companies', [
            'companies' => Companies::getByFilter($companiesSearchForm),
            'type' => $type,
        ]);
    }

    public function actionCompany($companyId) {
        /** @var $company Companies */
        if(!$company = Companies::findOneIfExists($companyId, $this->currentUserRole)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        return $this->render($this->currentUserRole == Users::ROLE_BUYER ? '//profile/store' : '//profile/index', [
            'company' => $company,
            'page' => 'catalog',
        ]);
    }

    public function actionCompanyAssortment($companyId, $categoryId = null) {
        /** @var $company Companies */
        if(!$company = Companies::findOneIfExists($companyId, $this->currentUserRole)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        /** @var PS $searchForm */
        $searchForm = &$this->productsSearchForm;
        $searchForm->load(Yii::$app->request->get());
        $user = Users::findOne($company->user_id);
        if($categoryId) {
            $category = Categories::findOne($categoryId);
            if(!$category || $category->level != Categories::LEVEL_GOODSCATEGORY) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            $searchForm->cat = $categoryId;
            $searchForm->companyId = $company->id;
            return $this->render('//assortment/category', [
                'user' => $user,
                'company' => $company,
                'category' => $category,
                'products' => Products::getByFilter($searchForm),
            ]);
        }
        return $this->render('//assortment/index', [
            'user' => $user,
            'company' => $company,
            'categoriesTree' => $company->getCategoriesTreeWithCounts($searchForm)
        ]);
    }

    public function actionSearch() {
        /** @var PS $searchForm */
        $searchForm = &$this->productsSearchForm;
        if($searchForm->load(Yii::$app->request->get())) {
            return $this->render('search', [
                'products' => Products::getByFilter($searchForm, Products::GET_SEARCH_RESULT_WITH_COUNT),
            ]);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionProductsHints($q) {
        if(!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        echo Json::encode(Products::getHintsByName($q));
    }

    public function actionCompaniesHints($q) {
        if(!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        echo Json::encode(Companies::getHintsByName($q));
    }

    public function actionManufacturers() {
        /** @var CS $manufacturersSearchForm */
        $manufacturersSearchForm = &$this->companiesSearchForm;
        $manufacturersSearchForm->load(Yii::$app->request->get());
        $manufacturersSearchForm->type = 'provider';
        $manufacturersSearchForm->m = 1;
        return $this->render('manufacturers', [
            'manufacturers' => Companies::getByFilter($manufacturersSearchForm, true),
            'categories' => Categories::getFullTree(),
        ]);
    }

    public function actionManufacturer($id) {
        if(!$company = Companies::find()->active()->andWhere(['companies.id' => $id])->one()) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $this->view->params['isWhiteBody'] = true;
        return $this->render('manufacturer', [
            'company' => $company,
        ]);
    }
}
