<?php
namespace frontend\controllers;

use Yii;
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\forms\CS;
use frontend\models\forms\UserPlacementForm;

class SiteController extends AppController {
    public $layout = 'landing';

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => [],
                'allow' => true,
            ],
        ];
        return $behaviors;
    }

    public function actionIndex() {
        /** @var CS $manufacturersSearchForm */
        $manufacturersSearchForm = &$this->companiesSearchForm;
        $manufacturersSearchForm->type = 'provider';
        $manufacturersSearchForm->m = 1;
        $manufacturersSearchForm->isRecommended = 1;
        $this->view->params['isShowRegistration'] = false;
        return $this->render('index', [
            'manufacturers' => Companies::getByFilter($manufacturersSearchForm),
        ]);
    }

    public function actionBusiness($type = null) {
        $this->view->params['noSuperHeader'] = true;
        return $this->render($type ?: 'manufacturer');
    }

    public function actionError() {
        $this->layout = 'error';
        return $this->render('404');
    }

    public function actionSetUserCity() {
        if(Yii::$app->request->isPost) {
            /** @var UserPlacementForm $placementForm */
            $placementForm = &$this->userPlacementForm;
            $placementForm->load(Yii::$app->request->post());
            if($placementForm->validate()) {
                Users::setCurrentUserCity($placementForm->city_id);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionSetUserLanguage($lang) {
        Users::setCurrentUserLanguage($lang);
        return $this->redirect(Yii::$app->request->referrer);
    }
}
