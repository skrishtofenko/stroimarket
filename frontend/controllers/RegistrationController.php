<?php
namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use common\components\FlashMessages;
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\Cities;
use frontend\models\forms\RegistrationStep2Form;
use frontend\models\forms\LoginForm;
use frontend\models\forms\CompanyInfoForm;
use frontend\models\forms\RequisitesInfoForm;

class RegistrationController extends AppController {
    public $layout = 'clear';

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['index', 'login'],
                'allow' => true,
                'roles' => ['?'],
            ],
            [
                'actions' => [],
                'allow' => true,
                'roles' => ['@'],
                'matchCallback' => function ($rule, $action) {
                    return Yii::$app->user->identity->role == Users::ROLE_COMPANY;
                }
            ],
        ];
        return $behaviors;
    }

    public function actionIndex() {
        if(!Yii::$app->user->isGuest) {
            return $this->redirect(['/profile']);
        }
        if(Yii::$app->request->isPost && $type = Yii::$app->request->post('type', false)) {
            if(isset(Companies::types()[$type])) {
                $registrationStep2Form = new RegistrationStep2Form();
                $registrationStep2Form->isLegalEntity = 1;
                if($registrationStep2Form->load(Yii::$app->request->post())) {
                    if($registrationStep2Form->validate()) {
                        if($token = Users::signUp($registrationStep2Form)) {
                            return $this->redirect(['login?token=' . $token]);
                        }
                    } else {
                        if(isset($registrationStep2Form->errors['email']) && $registrationStep2Form->errors['email'][0] == 'This email address has already been taken.') {
                            Yii::$app->session->setFlash(FlashMessages::FLASH_FAILED, 'Пользователь с таким email уже зарегистрирован, попробуйте <a href="/login">войти</a>');
                        }
                    }
                }
                $registrationStep2Form->type = $type;
                return $this->render("step2", [
                    'registrationStep2Form' => $registrationStep2Form,
                ]);
            }
        }
        return $this->render('step1');
    }

    public function actionLogin($token) {
        if(!Yii::$app->user->isGuest) {
            return $this->goBack();
        }
        if(!$user = Users::findOne(['registration_token' => $token])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $loginForm = new LoginForm();
        $loginForm->isRegistrationFirstLogin = 1;
        if($loginForm->load(Yii::$app->request->post())) {
            if($loginForm->validate() && Users::login($loginForm)) {
                return $this->redirect(['company']);
            }
            return $this->goBack();
        } else {
            $loginForm->email = $user->email;
            return $this->render('step3', [
                'loginForm' => $loginForm,
                'user' => $user,
            ]);
        }
    }

    public function actionCompany() {
        /** @var $user Users */
        $user = Yii::$app->user->identity;
        if($user->last_completed_registration_step >= Users::COMPLETED_REG_STEP) {
            return $this->redirect('/profile');
        }
        $companyInfoForm = new CompanyInfoForm();
        $companyInfoForm->fill($user);
        if($companyInfoForm->load(Yii::$app->request->post()) && $companyInfoForm->validate()) {
            if($user->updateCompanyInfo($companyInfoForm, true)) {
                $nextStep = ($companyInfoForm->type == Companies::TYPE_PICKER && !$user->ownership_type_id ? 'finish' : 'requisites');
                return $this->redirect([$nextStep]);
            }
        }
        $view = 'step4' . ($companyInfoForm->type == Companies::TYPE_PICKER && !$user->ownership_type_id ? '-picker' : '');
        return $this->render($view, [
            'user' => $user,
            'companyInfoForm' => $companyInfoForm,
            'cities' => Cities::getList(),
        ]);
    }

    public function actionRequisites() {
        /** @var $user Users */
        $user = Yii::$app->user->identity;
        if($user->last_completed_registration_step >= Users::COMPLETED_REG_STEP) {
            return $this->redirect('/profile');
        }
        $requisitesInfoForm = new RequisitesInfoForm();
        $requisitesInfoForm->fill($user);
        if($requisitesInfoForm->load(Yii::$app->request->post()) && $requisitesInfoForm->validate()) {
            if($user->updateRequisitesInfo($requisitesInfoForm, true)) {
                return $this->redirect(['finish']);
            }
        }
        return $this->render('step5', [
            'user' => $user,
            'requisitesInfoForm' => $requisitesInfoForm,
        ]);
    }

    public function actionFinish($company = false) {
        $company = $company ? Companies::findOne($company) : false;
        $company = $company ?: Yii::$app->user->identity->mainCompany;
        return $this->render('step6', [
            'company' => $company,
        ]);
    }

    public function actionAddStore() {
        $this->throwNonLegal404();
        /** @var $user Users */
        $user = Yii::$app->user->identity;
        $mainCompany = $user->mainCompany;
        if(!in_array($mainCompany->type, [Companies::TYPE_STORE, Companies::TYPE_PROVIDER])) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $company = new Companies();
        $companyInfoForm = new CompanyInfoForm();
        $companyInfoForm->fill($user, $company);
        $companyInfoForm->type = Companies::TYPE_STORE;
        $companyInfoForm->name = '_';
        if($companyInfoForm->load(Yii::$app->request->post()) && $companyInfoForm->validate()) {
            $companyId = $user->addStore($companyInfoForm, $company);
            return $this->redirect(['finish?company=' . $companyId]);
        }
        return $this->render('add-store', [
            'user' => $user,
            'companyInfoForm' => $companyInfoForm,
            'cities' => Cities::getList(),
        ]);
    }
}
