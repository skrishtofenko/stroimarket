<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use common\components\FlashMessages;
use frontend\models\Users;
use frontend\models\Products;
use frontend\models\Catalogs;
use frontend\models\Companies;
use frontend\models\Categories;
use frontend\models\CompaniesProducts;
use frontend\models\forms\PS;
use frontend\models\forms\ProductForm;
use frontend\models\forms\CatalogUploadForm;

class AssortmentController extends AppController {
    public $layout = 'dashboard';

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => [],
                'allow' => true,
                'roles' => ['@'],
                'matchCallback' => function ($rule, $action) {
                    /** @var Users $user */
                    $user = Yii::$app->user->identity;
                    return $user->role == Users::ROLE_COMPANY && in_array($user->mainCompany->type, [Companies::TYPE_PROVIDER, Companies::TYPE_STORE]);
                }
            ],
        ];
        return $behaviors;
    }

    public function actionIndex() {
        /** @var $user Users */
        $user = Yii::$app->user->identity;
        /** @var $company Companies */
        $company = $user->getCurrentCompany();
        if($user->status != Users::STATUS_ACTIVE && $user->mainCompany->type != Companies::TYPE_PICKER) {
            Yii::$app->session->setFlash(FlashMessages::FLASH_INFO, 'Внимание! Для того чтобы профиль и ассортимент вашей компании появились в общем каталоге и стали доступны другим пользователям, вам необходимо подтвердить ваши реквизиты в настройках личного кабинета');
        }
        /** @var PS $searchForm */
        $searchForm = &$this->productsSearchForm;
        $searchForm->load(Yii::$app->request->get());
        return $this->render('index', [
            'user' => $user,
            'company' => $company,
            'categoriesTree' => $company->getCategoriesTreeWithCounts($searchForm)
        ]);
    }

    public function actionEdit($categoryId = null) {
        $this->view->params['leftMenuUnmaximizable'] = true;
        if($categoryId) {
            $category = Categories::findOne($categoryId);
            if (!$category || $category->level != Categories::LEVEL_GOODSCATEGORY) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
        /** @var $user Users */
        $user = Yii::$app->user->identity;
        /** @var $company Companies */
        $company = $user->getCurrentCompany();
        $sort = Yii::$app->request->get('sort', Products::DEFAULT_EDIT_SORT);
        return $this->render('edit', [
            'user' => $user,
            'company' => $company,
            'products' => $company->findProducts($categoryId, false, $sort),
            'productForm' => new ProductForm(),
            'categoriesTree' => Categories::getFullTree(null, true),
            'catalogUploadForm' => new CatalogUploadForm(),
            'sort' => array_search(Products::getRealEditSort($sort), Products::EDIT_SORTS),
        ]);
    }

    public function actionCategory($categoryId) {
        /** @var $user Users */
        $user = Yii::$app->user->identity;
        $category = Categories::findOne($categoryId);
        if(!$category || $category->level != Categories::LEVEL_GOODSCATEGORY) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        /** @var $company Companies */
        $company = $user->getCurrentCompany();
        /** @var PS $searchForm */
        $searchForm = &$this->productsSearchForm;
        $searchForm->load(Yii::$app->request->get());
        $searchForm->cat = $categoryId;
        $searchForm->companyId = $company->id;
        $products = Products::getByFilter($searchForm);
        return $this->render('category', [
            'user' => $user,
            'company' => $company,
            'category' => $category,
            'products' => $company->findProductsByIds(ArrayHelper::getColumn($products, 'id')),
        ]);
    }

    public function actionGetAssortmentTemplate() {
        /**
         * @var $user Users
         * @var $company Companies
         */
        $user = Yii::$app->user->identity;
        $company = $user->getCurrentCompany();
        return $company
            ->getAssortmentFile()
            ->send('Stroimarket_Assortment_Template_For_' . ucfirst($company->type) . '.xlsx');
    }

    public function actionGetFullAssortment() {
        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                'Produccts' => [
                    'class' => 'codemix\excelexport\ActiveExcelSheet',
                    'query' => Products::find()->where(['is_active' => 1])->orderBy(['category_id' => SORT_ASC, 'name' => SORT_ASC]),
                    'attributes' => [
                        'id',
                        'name',
                        'category.name',
                    ],
                    'titles' => [
                        'Артикул',
                        'Наименование',
                        'Категория',
                    ]
                ]
            ],
        ]);

        $file->send('Stroimarket_Full_Assortment.xlsx');
    }

    public function actionGetMyAssortment() {
        /**
         * @var $user Users
         * @var $company Companies
         */
        $user = Yii::$app->user->identity;
        $company = $user->getCurrentCompany();
        $company
            ->getAssortmentFile(false)
            ->send('Stroimarket_Assortment.xlsx');
    }

    public function actionProductsForConformity($categoryId) {
        if(!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if(!$categoryId || !is_numeric($categoryId)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $category = Categories::findOne($categoryId);
        if(!$category || $category->level != Categories::LEVEL_GOODSCATEGORY) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $this->layout = 'blank';
        return $this->renderPartial('/partials/assortment/productsList.php', [
            'products' => Products::getForConformity($categoryId),
        ]);
    }

    public function actionProductsForConformityByQuery($query) {
        if(!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        if(!$query || !is_string($query)) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $this->layout = 'blank';
        return $this->renderPartial('/partials/assortment/productsList.php', [
            'products' => Products::getForConformity(null, $query),
        ]);
    }

    public function actionSaveProduct() {
        $productForm = new ProductForm();
        if($productForm->load(Yii::$app->request->post()) && $productForm->validate()) {
            CompaniesProducts::safeCreateOrUpdate($productForm);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionRemove($id) {
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        /** @var Companies $company */
        $company = $user->getCurrentCompany();
        if($company->type != Companies::TYPE_PICKER) {
            $product = CompaniesProducts::findOne($id);
            if($product && $product->company_id == $company->id && !$product->is_manufactured_by) {
                $product->updateAttributes(['status' => CompaniesProducts::STATUS_DELETED]);
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionNoMatch() {
        return $this->render('no-match');
    }

    public function actionUpload() {
        $assortmentForm = new CatalogUploadForm();
        if(Yii::$app->request->isPost) {
            $assortmentForm->catalogFile = UploadedFile::getInstance($assortmentForm, 'catalogFile');
            if($assortmentForm->validate()) {
                if(Catalogs::upload($assortmentForm)) {
                    Yii::$app->session->setFlash(FlashMessages::FLASH_INFO, 'Файл ассортимента был загружен в систему, в ближайшее время произойдет его обработка и обновление товаров вашего ассортимента, а по окончании обновления вам на почту придет письмо с информацией о результате. До получения письма с информацией о результатах загрузки, воздержитесь от изменений в ассортименте во избежание потери данных и неверной загрузки товаров.');
                }
            }
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
}
