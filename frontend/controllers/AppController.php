<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use frontend\models\Users;
use frontend\models\Cities;
use frontend\models\forms\PS;
use frontend\models\forms\CS;
use frontend\models\forms\PCS;
use frontend\models\forms\SCS;
use frontend\models\forms\UserPlacementForm;

class AppController extends Controller {
    protected $productsSearchForm;
    protected $companiesSearchForm;
    protected $providersSearchForm;
    protected $storesSearchForm;

    protected $userPlacementForm;

    protected $currentUserRole;

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => []
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }

    public function actions() {
        return [];
    }

    protected function regRedirect() {
        if($this->id == 'registration' && in_array($this->action->id, ['company', 'requisites'])) {
            return true;
        }
        if($this->id == 'profile' && in_array($this->action->id, ['remove-logo'])) {
            return true;
        }
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        switch ($user->last_completed_registration_step) {
            case Users::REGISTERED_REG_STEP:
                return $this->redirect(['/registration/company']);
                break;
            case Users::FILLED_COMPANY_REG_STEP:
                return $this->redirect(['/registration/requisites']);
                break;
        }
        return false;
    }

    public function beforeAction($action) {
        if (!parent::beforeAction($action)) {
            return false;
        }
        if(!Yii::$app->user->isGuest) {
            $this->regRedirect();
        }

        $this->setSearchForms();
        $this->setUserRole();
        $this->setUserCity();
        $this->setAppLanguage();
        return true;
    }

    protected function throwNonIndividual404() {
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        if($user->ownership_type_id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function throwNonLegal404() {
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        if(!$user->ownership_type_id) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    private function setSearchForms() {
        $this->view->params['productSearch'] = new PS();
        $this->view->params['companiesSearch'] = new CS();
        $this->view->params['providersCompaniesSearch'] = new PCS();
        $this->view->params['storesCompaniesSearch'] = new SCS();
        $this->productsSearchForm = &$this->view->params['productSearch'];
        $this->companiesSearchForm = &$this->view->params['companiesSearch'];
        $this->providersSearchForm = &$this->view->params['providersCompaniesSearch'];
        $this->storesSearchForm = &$this->view->params['storesCompaniesSearch'];
    }

    private function setUserRole() {
        $this->currentUserRole
            = Yii::$app->user->isGuest
            ? Users::ROLE_BUYER
            : Yii::$app->user->identity->role;
        $this->view->params['currentUserRole'] = $this->currentUserRole;
    }

    private function setUserCity() {
        if($this->currentUserRole == Users::ROLE_BUYER) {
            $city = Users::setCurrentUserCity(); // todo: drop this line when will connected other cities and uncomment next lines
//            if(!$city = Users::getCurrentUserCity()) {
//                $city = Users::setCurrentUserCity();
//                $this->view->params['needsApproveUserCity'] = true;
//            }
            $city = Cities::findOne($city);
            $this->view->params['currentUserCity'] = $city->name;
            $this->userPlacementForm = new UserPlacementForm();
            $this->userPlacementForm->city_id = $city->id;
            $this->view->params['userPlacement'] = &$this->userPlacementForm;
        }
    }

    private function setAppLanguage() {
        Yii::$app->language = Users::getCurrentUserLanguage();
    }
}