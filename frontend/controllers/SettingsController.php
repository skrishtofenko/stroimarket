<?php
namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use common\components\BillGenerator\BillGenerator;
use frontend\models\Users;
use frontend\models\Bills;
use frontend\models\Cities;
use frontend\models\Categories;
use frontend\models\forms\AccountSettingsForm;
use frontend\models\forms\CompanyInfoForm;
use frontend\models\forms\RequisitesInfoForm;

class SettingsController extends AppController {
    public $layout = 'dashboard';

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => [],
                'allow' => true,
                'roles' => ['@'],
                'matchCallback' => function ($rule, $action) {
                    return Yii::$app->user->identity->role == Users::ROLE_COMPANY;
                }
            ],
        ];
        return $behaviors;
    }

    public function actionIndex() {
        /** @var $user Users */
        $user = Yii::$app->user->identity;
        $accountSettingsForm = new AccountSettingsForm();
        $accountSettingsForm->fill($user);
        if($accountSettingsForm->load(Yii::$app->request->post()) && $accountSettingsForm->validate()) {
            $user->updateAccount($accountSettingsForm);
        }
        return $this->render('index', [
            'user' => $user,
            'accountSettingsForm' => $accountSettingsForm,
        ]);
    }

    public function actionCreateRegistrationBill() {
        /** @var $user Users */
        $user = Yii::$app->user->identity;
        BillGenerator::generateBill($user->getOrCreateRegistrationBill());
    }

    public function actionCompany() {
        $this->throwNonLegal404();

        /** @var $user Users */
        $user = Yii::$app->user->identity;
        $company = $user->getCurrentCompany();
        $companyInfoForm = new CompanyInfoForm();
        $companyInfoForm->fill($user, $company);
        $companyInfoForm->isOnSettings = true;
        if($companyInfoForm->load(Yii::$app->request->post()) && $companyInfoForm->validate()) {
            $user->updateCompanyInfo($companyInfoForm, false, $company);
        }
        return $this->render('company', [
            'user' => $user,
            'companyInfoForm' => $companyInfoForm,
            'company' => $company,
            'cities' => Cities::getList(),
        ]);
    }

    public function actionProfile() {
        $this->throwNonIndividual404();

        /** @var $user Users */
        $user = Yii::$app->user->identity;
        $companyInfoForm = new CompanyInfoForm();
        $companyInfoForm->fill($user);
        $companyInfoForm->isOnSettings = true;
        if($companyInfoForm->load(Yii::$app->request->post()) && $companyInfoForm->validate()) {
            $user->updateCompanyInfo($companyInfoForm);
        }
        return $this->render('profile', [
            'user' => $user,
            'companyInfoForm' => $companyInfoForm,
        ]);
    }

    public function actionRequisites() {
        $this->throwNonLegal404();

        /** @var $user Users */
        $user = Yii::$app->user->identity;
        $requisitesInfoForm = new RequisitesInfoForm();
        $requisitesInfoForm->fill($user);
        if($requisitesInfoForm->load(Yii::$app->request->post()) && $requisitesInfoForm->validate()) {
            $user->updateRequisitesInfo($requisitesInfoForm);
        }
        return $this->render('requisites', [
            'user' => $user,
            'requisitesInfoForm' => $requisitesInfoForm,
        ]);
    }

    public function actionNotices() {
        /** @var $user Users */
        /** @var $notices \frontend\models\Notices */
        $user = Yii::$app->user->identity;
        $notices = $user->notices;
        $notices->fillCategories();
        if($notices->load(Yii::$app->request->post()) && $notices->validate()) {
            if($notices->saveWithCategories()) {
                return $this->redirect(['notices']);
            }
        }
        return $this->render('notices', [
            'user' => $user,
            'notices' => $notices,
            'categoriesTree' => Categories::getFullTree(null, true),
        ]);
    }

    public function actionPayment($type = false) {
        /** @var $user Users */
        $user = Yii::$app->user->identity;
        if(!$type || !in_array($type, ['account', 'card'])) {
            return $this->render('payment', [
                'user' => $user,
            ]);
        }

        $requisites = $user->mainRequisites;
        if($requisites->load(Yii::$app->request->post())) {
            if($requisites->validate()) {
                $bill = Bills::createForSubscription($requisites->createIfChanged());
                return $this->render("payment-$type-complete", [
                    'user' => $user,
                    'bill' => $bill,
                ]);
            }
        }
        return $this->render("payment-$type", [
            'user' => $user,
            'requisites' => $requisites,
        ]);
    }

    public function actionBill($id) {
        $this->throwNonLegal404();

        /** @var $user Users */
        $user = Yii::$app->user->identity;
        if($bill = Bills::findOne($id)) {
            if($bill->requisite->user_id == $user->id) {
                return BillGenerator::generateBill($bill);
            }
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
