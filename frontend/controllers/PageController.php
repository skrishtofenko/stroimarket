<?php
namespace frontend\controllers;

class PageController extends AppController {
    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['agreement'],
                'allow' => true,
            ],
        ];
        return $behaviors;
    }

	public function actionAgreement() {
        $this->layout = 'clear';
		return $this->render('agreement');
	}
}
