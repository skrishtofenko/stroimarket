<?php
namespace frontend\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use common\components\FlashMessages;
use frontend\models\Users;
use frontend\models\Companies;
use frontend\models\forms\LoginForm;
use frontend\models\forms\PasswordResetRequestForm;
use frontend\models\forms\PasswordResetForm;

class ProfileController extends AppController {
    public $layout = 'clear';

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['access']['rules'] = [
            [
                'actions' => ['login', 'password-reset'],
                'allow' => true,
                'roles' => ['?'],
            ],
            [
                'actions' => ['logout'],
                'allow' => true,
                'roles' => ['@'],
            ],
            [
                'actions' => [],
                'allow' => true,
                'roles' => ['@'],
                'matchCallback' => function ($rule, $action) {
                    return Yii::$app->user->identity->role == Users::ROLE_COMPANY;
                }
            ],
        ];
        return $behaviors;
    }

    public function actionIndex() {
        $this->layout = 'dashboard';
        /** @var Users $user */
        $user = Yii::$app->user->identity;
        $currentCompany = $user->getCurrentCompany();
        $companies = [];
        foreach ($user->companies as $company) {
            if($company->id != $currentCompany->id) {
                $companies[] = $company;
            }
        }
        if($user->status != Users::STATUS_ACTIVE && $user->mainCompany->type != Companies::TYPE_PICKER) {
            Yii::$app->session->setFlash(FlashMessages::FLASH_INFO, 'Внимание! Для того чтобы профиль и ассортимент вашей компании появились в общем каталоге и стали доступны другим пользователям, вам необходимо подтвердить ваши реквизиты в настройках личного кабинета');
        }
        return $this->render('index', [
            'company' => $currentCompany,
            'companies' => $companies,
        ]);
    }

    public function actionLogin() {
        if(!Yii::$app->user->isGuest) {
            return $this->regRedirect() || $this->redirect(['/profile']);
        }

        $loginForm = new LoginForm();
        if($loginForm->load(Yii::$app->request->post())) {
            if($loginForm->validate() && Users::login($loginForm)) {
                return $this->regRedirect() || $this->redirect(['/profile']);
            }
        }
        $loginForm->isRegistrationFirstLogin = 0;
        return $this->render('login', [
            'loginForm' => $loginForm,
        ]);
    }

    public function actionLogout() {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionPasswordReset($token = false) {
        if($token) {
            if(!$user = Users::findByPasswordResetToken($token)) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            $passwordResetForm = new PasswordResetForm();
            if($passwordResetForm->load(Yii::$app->request->post())) {
                $passwordResetForm->token = $token;
                if($passwordResetForm->validate() && Users::passwordReset($passwordResetForm)) {
                    return $this->redirect(['/login']);
                }
            }
            return $this->render('password-reset', [
                'passwordResetForm' => $passwordResetForm,
            ]);
        }
        $passwordResetRequestForm = new PasswordResetRequestForm();
        if($passwordResetRequestForm->load(Yii::$app->request->post()) && $passwordResetRequestForm->validate()) {
            if(Users::requestPasswordReset($passwordResetRequestForm)) {
                Yii::$app->session->setFlash(FlashMessages::FLASH_INFO, "Готово! Для восстановления пароля, перейдите по ссылке, которую мы отправили на указанный вами адрес электронной почты.");
            } else {
                Yii::$app->session->setFlash(FlashMessages::FLASH_FAILED, 'К сожалению, восстановление пароля сейчас невозможно. Повторите попытку позже.');
            }
        }
        return $this->render('password-reset-request', [
            'passwordResetRequestForm' => $passwordResetRequestForm,
        ]);
    }

    public function actionRemoveLogo() {
        if(Yii::$app->request->isAjax && Yii::$app->request->isPost) {
            /** @var $user Users */
            $user = Yii::$app->user->identity;
            $user->updateAttributes(['logo' => null]);
        }
        return Json::encode('ok');
    }

    public function actionSwitchCompany($companyId = false) {
        if($companyId) {
            Yii::$app->session->set('currentCompany', $companyId);
        }
        $this->redirect(Yii::$app->request->referrer);
    }
}
