<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class StroimarketAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'static/css/style.css',
        'css/addon.style.css',
    ];
    public $js = [
        'static/js/lib/autosize.min.js',
        'static/js/main.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
