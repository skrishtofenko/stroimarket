<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class UploadImageAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/imageUpload';
    public $css = [
        'imageUpload.css'
    ];
    public $js = [
        'imageUpload.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
