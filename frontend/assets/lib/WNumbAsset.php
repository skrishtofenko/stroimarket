<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class WNumbAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/static/js/lib';
    public $css = [];
    public $js = [
        'wNumb.js',
    ];
    public $depends = [];
}