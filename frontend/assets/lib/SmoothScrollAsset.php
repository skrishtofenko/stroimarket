<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class SmoothScrollAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/static/js/lib';
    public $css = [];
    public $js = [
        'smooth-scroll.min.js',
    ];
    public $depends = [];
}