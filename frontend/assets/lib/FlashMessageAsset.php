<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class FlashMessageAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/flashMessage';
    public $css = [];
    public $js = [
        'flashMessage.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\DropKickAsset',
    ];
}