<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class InputMaskAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/static/js/lib';
    public $css = [];
    public $js = [
        'imask.min.js',
    ];
    public $depends = [];
}