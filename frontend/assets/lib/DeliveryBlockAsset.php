<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class DeliveryBlockAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/deliveryBlock';
    public $css = [];
    public $js = [
        'deliveryBlock.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}