<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class ScrollRevealAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/static/js/lib';
    public $css = [];
    public $js = [
        'scrollreveal.min.js',
    ];
    public $depends = [];
}