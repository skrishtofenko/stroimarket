<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class TinySliderAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/static/js/lib';
    public $css = [];
    public $js = [
        'tiny-slider.min.js',
    ];
    public $depends = [];
}