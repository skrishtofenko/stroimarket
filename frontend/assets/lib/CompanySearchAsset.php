<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class CompanySearchAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/companySearch';
    public $css = [];
    public $js = [
        'companySearch.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}