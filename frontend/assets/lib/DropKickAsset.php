<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class DropKickAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/static/js/lib';
    public $css = [];
    public $js = [
        'dropkick.js',
    ];
    public $depends = [];
}