<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class ProductSearchAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/productSearch';
    public $css = [];
    public $js = [
        'productSearch.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\NoUiSliderAsset',
        'frontend\assets\lib\WNumbAsset',
    ];
}