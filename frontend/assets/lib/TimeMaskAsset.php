<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class TimeMaskAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/timeMask';
    public $css = [];
    public $js = [
        'timeMask.js',
    ];
    public $depends = [
        'frontend\assets\lib\InputMaskAsset',
    ];
}