<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class BindSmoothScrollAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/bindSmoothScroll';
    public $css = [];
    public $js = [
        'bindSmoothScroll.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\SmoothScrollAsset',
    ];
}