<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class CityChooserAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/cityChooser';
    public $css = [];
    public $js = [
        'cityChooser.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}