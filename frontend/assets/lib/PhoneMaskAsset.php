<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class PhoneMaskAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/phoneMask';
    public $css = [];
    public $js = [
        'phoneMask.js'
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\widgets\MaskedInputAsset',
    ];
}
