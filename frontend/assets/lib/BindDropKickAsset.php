<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class BindDropKickAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/bindDropKick';
    public $css = [];
    public $js = [
        'bindDropKick.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\DropKickAsset',
    ];
}