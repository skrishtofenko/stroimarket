<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class BindTinySliderAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/bindTinySlider';
    public $css = [];
    public $js = [
        'bindTinySlider.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\TinySliderAsset',
    ];
}