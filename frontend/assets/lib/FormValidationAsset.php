<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class FormValidationAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/lib/formValidation';
    public $css = [];
    public $js = [
        'formValidation.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}