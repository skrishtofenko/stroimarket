<?php
namespace frontend\assets\lib;

use yii\web\AssetBundle;

class NoUiSliderAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/static/js/lib';
    public $css = [];
    public $js = [
        'nouislider.min.js',
    ];
    public $depends = [];
}