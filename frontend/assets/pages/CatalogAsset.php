<?php
namespace frontend\assets\pages;

use yii\web\AssetBundle;

class CatalogAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/pages';
    public $css = [];
    public $js = [
        'catalog.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\BindDropKickAsset',
    ];
}