<?php
namespace frontend\assets\pages;

use yii\web\AssetBundle;

class RegistrationStep5Asset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/pages';
    public $css = [];
    public $js = [
        'registrationStep5.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\FormValidationAsset',
    ];
}