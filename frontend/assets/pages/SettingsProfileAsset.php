<?php
namespace frontend\assets\pages;

use yii\web\AssetBundle;

class SettingsProfileAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/pages';
    public $css = [];
    public $js = [
        'settingsProfile.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\PhoneMaskAsset',
        'frontend\assets\lib\UploadImageAsset',
    ];
}