<?php
namespace frontend\assets\pages;

use yii\web\AssetBundle;

class RegistrationStep2Asset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/pages';
    public $css = [];
    public $js = [
        'registrationStep2.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\BindDropKickAsset',
        'frontend\assets\lib\PhoneMaskAsset',
    ];
}