<?php
namespace frontend\assets\pages;

use yii\web\AssetBundle;

class SettingsCompanyAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/pages';
    public $css = [];
    public $js = [
        'settingsCompany.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\BindDropKickAsset',
        'frontend\assets\lib\UploadImageAsset',
        'frontend\assets\lib\PhoneMaskAsset',
    ];
}