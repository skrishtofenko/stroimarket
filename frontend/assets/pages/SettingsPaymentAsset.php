<?php
namespace frontend\assets\pages;

use yii\web\AssetBundle;

class SettingsPaymentAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/pages';
    public $css = [];
    public $js = [
        'settingsPayment.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\PhoneMaskAsset',
    ];
}