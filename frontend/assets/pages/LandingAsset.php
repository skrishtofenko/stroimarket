<?php
namespace frontend\assets\pages;

use yii\web\AssetBundle;

class LandingAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/pages';
    public $css = [];
    public $js = [
        'landing.js',
    ];
    public $depends = [
        'frontend\assets\StroimarketAsset',
        'frontend\assets\lib\ScrollRevealAsset',
        'frontend\assets\lib\BindDropKickAsset',
    ];
}