<?php
namespace frontend\assets\pages;

use yii\web\AssetBundle;

class MyProfileAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/pages';
    public $css = [];
    public $js = [
        'myProfile.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}