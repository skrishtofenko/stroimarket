<?php
namespace frontend\assets\pages;

use yii\web\AssetBundle;

class AssortmentAsset extends AssetBundle {
    public $sourcePath = '@frontend/web/js/pages';
    public $css = [
        'assortment.css',
    ];
    public $js = [
        'assortment.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'frontend\assets\lib\BindDropKickAsset',
    ];
}