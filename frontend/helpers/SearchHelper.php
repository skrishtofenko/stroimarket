<?php
namespace frontend\helpers;

use Yii;

class SearchHelper {
    /**
     * @param $searchFormFields string|array
     * @param $excludeParams array|null
     * @param $saveOnlyParams array|null
     * @param $addOrReplaceParams array|null
     * @return string
     * */
    public static function makeGetString($searchFormFields, $excludeParams = null, $saveOnlyParams = null, $addOrReplaceParams = null) {
        if(!is_array($searchFormFields)) {
            $searchFormFields = [$searchFormFields];
        }
        $inputQueryString = Yii::$app->request->queryString;
        parse_str($inputQueryString, $queryArray);
        foreach($searchFormFields as $searchFormField) {
            $querySubArray = isset($queryArray[$searchFormField]) ? $queryArray[$searchFormField] : [];
            if(!empty($querySubArray)) {
                if (is_array($excludeParams)) {
                    $querySubArray = array_diff_key($querySubArray, array_flip($excludeParams));
                } elseif (is_array($saveOnlyParams)) {
                    $querySubArray = array_intersect_key($querySubArray, array_flip($saveOnlyParams));
                }
            }
            if(is_array($addOrReplaceParams) && !empty($addOrReplaceParams)) {
                $querySubArray = array_merge($querySubArray, $addOrReplaceParams);
            }
            $queryArray[$searchFormField] = $querySubArray;
        }
        return !empty($queryArray) ? '?' . http_build_query($queryArray) : '';
    }
}
