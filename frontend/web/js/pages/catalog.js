var myMap;
if(ymaps) {
    ymaps.ready(bindMap);
}

$(function() {
    $('ul.company-card__places li').on('click', function() {
        window.location.href = '/catalog/company/' + $(this).data('company-id');
    });

    $('select.profile__places').on('change', function() {
        window.location.href = '/catalog/company/' + $(this).val();
    });
});

function bindMap() {
    $('#show-product-map').on('click', function() {
        if(!myMap) {
            var domCompanies = $('ul#product-map-companies li'),
                geocoders = [];

            myMap = new ymaps.Map("map", {
                center: [55.755814, 37.617635],
                zoom: 16,
                controls: []
            });
            myMap.controls.add('zoomControl');
            myMap.controls.add('rulerControl');
            myMap.controls.add('routeButtonControl');
            myMap.controls.add('fullscreenControl');

            domCompanies.each(function(index, item) {
                geocoders.push(ymaps.geocode($(item).data('address')));
            });

            if(geocoders.length) {
                Promise.all(geocoders).then(geoObjects => {
                    geoObjects.forEach(function(geoObject, index) {
                        geoObject = geoObject.geoObjects.get(0);
                        var placemark = new ymaps.Placemark(geoObject.geometry.getCoordinates(), {
                            hintContent: geoObject.properties.get('balloonContent'),
                            balloonContent: geoObject.properties.get('balloonContent')
                        });

                        placemark.events.add('mouseenter', function(e) {
                            e.get('target').options.set('preset', 'islands#greenIcon');
                            // если нужна обратная реакция с метки на DOM-элемент
                            // $(domCompanies[index]).css('background-color', 'red');
                        })
                        .add('mouseleave', function(e) {
                            e.get('target').options.unset('preset');
                            // $(domCompanies[index]).css('background-color', 'white');
                        });

                        $(domCompanies[index]).on('mouseover', function() {
                            placemark.options.set('preset', 'islands#greenIcon');
                        }).on('mouseout', function() {
                            placemark.options.unset('preset');
                        });
                        myMap.geoObjects.add(placemark);
                    });
                    myMap.setBounds(myMap.geoObjects.getBounds());
                    if(geoObjects.length === 1) {
                        myMap.setZoom(16);
                    }
                });
            }
        }
    });
}
