$(function() {
    $('select#place').on('change', function() {
        window.location.href = '/profile/switch-company?companyId=' + $(this).val();
    });

    $('#companyinfoform-is_imonly').on('change', function() {
        if($(this).prop('checked')) {
            $('.address-block').slideUp();
        } else {
            $('.address-block').slideDown();
        }
    });

    bindPhoneMask('#companyinfoform-phone');

    operatingTime.save();
});
