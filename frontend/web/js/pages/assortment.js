var categoriesHtml = '';

$(function() {
    $('#mode').on('change', function() {
        var page = $(this).val();
        var currentCategoryId = parseInt(window.location.href.split('/').splice(-1, 1));
        window.location.href = '/assortment' + (page.length ? '/' + page : page) + (currentCategoryId ? '/' + currentCategoryId : '');
    });

    $('select.assortment-actions').on('change', function() {
        var url = $(this).val();
        if(url === 'upload-file') {
            $('#cataloguploadform-catalogfile').trigger('click');
        } else {
            if(url && url !== '0') {
                window.location.href = url;
            }
        }
        return false;
    });

    $(document).on('click', 'a.ajax-category-link', function(e) {
        e.preventDefault();
        var categoryId = parseInt($(this).data('category-id'));
        if(categoryId) {
            $.ajax({
                url: '/assortment/products-for-conformity?categoryId=' + categoryId,
                dataType: 'html',
                type: 'GET',
                success: showProductsList
            });
        }
    });

    $('#conformity-search-field').bind('typeahead:select', function(ev, suggestion) {
        getProductsListByName(suggestion.name);
    }).on('keydown', function(e) {
        if(e.which === 13) {
            e.preventDefault();
            if($(this).val() !== '') {
                getProductsListByName($(this).val());
            }
        }
    });

    $('button[type=submit].add-match__search-button').on('click', function(e) {
        e.preventDefault();
        var input = $('#conformity-search-field');
        if(input.val() !== '') {
            getProductsListByName(input.val());
        }
    });

    $('select#place').on('change', function() {
        var newHref = '/profile/switch-company?companyId=' + $(this).val();
        var data = $(this).data();
        if(typeof data.othercompany !== 'undefined' && parseInt(data.othercompany) === 1) {
            var currentHref = window.location.href;
            newHref = currentHref.replace(/company\/(\d)+\/assortment/g, "company/" + $(this).val() + "/assortment");
        }
        window.location.href = newHref;
    });

    $('#productform-isinstock').on('change', function() {
        setByOrderVisibility();
    });
});

var textInputs = ['name', 'vendorcode', 'url', 'multiplicity', 'cost', 'cnt', 'id', 'productid'];
var checkboxes = ['action', 'promo', 'visible', 'instock', 'byorder'];

function clearProductForm() {
    textInputs.forEach(function(textInput) {
        var selector = '#productform-' + textInput;
        $(selector).val('');
    });
    checkboxes.forEach(function(checkbox) {
        var selector = '#productform-is' + checkbox;
        var cb = $(selector);
        if(cb.prop('checked')) {
            cb.trigger('click');
        }
    });
    $('#productform-isvisible').trigger('click');
    $('#productform-title').html('Добавить товар');
    $('#addproduct-conformity-selected').hide();
    $('#addproduct-conformity-empty').show();
    $('#product-submit-button').prop('disabled', true).text('Добавить');
    $('#unit-type').text('шт.');
}

function fillProductForm(productId) {
    textInputs.forEach(function(textInput) {
        var formSelector = '#productform-' + textInput;
        var productSelector = '#product-' + productId + '-' + textInput;
        var formInput = $(formSelector);
        var productInput = $(productSelector);
        if(formInput && productInput) {
            formInput.val(productInput.val());
        }
    });
    checkboxes.forEach(function(checkbox) {
        var formSelector = '#productform-is' + checkbox;
        var productSelector = '#product-' + productId + '-is' + checkbox;
        var formInput = $(formSelector);
        var productInput = $(productSelector);
        if(formInput && productInput) {
            if((productInput.val() === '1' && !formInput.prop('checked')) || (productInput.val() === '0' && formInput.prop('checked'))) {
                formInput.trigger('click');
            }
        }
    });
    setByOrderVisibility();
    var submitButton = $('#product-submit-button');
    submitButton.text('Готово');
    var updatedAt = $('#product-' + productId + '-updatedAt').val();
    $('#productform-title').html('Редактировать товар (обновлен ' + updatedAt + ')');
    $('#unit-type').text($('#product-' + productId + '-unittype').val());
    var productChosen = parseInt($('#product-' + productId + '-productid').val());
    var selectedConformitySelector = '#addproduct-conformity-selected';
    if(productChosen) {
        var conformityInfo = $('#product-' + productId + '-conformity-info').html();
        $(selectedConformitySelector).show();
        $(selectedConformitySelector).find('div.add-product__chosen-product').html(conformityInfo);
        $('#addproduct-conformity-empty').hide();
        submitButton.prop('disabled', false);
    } else {
        $(selectedConformitySelector).hide();
        $(selectedConformitySelector).find('div.add-product__chosen-product').html('');
        $('#addproduct-conformity-empty').show();
        submitButton.prop('disabled', true);
    }
    var manufacturedBy = parseInt($('#product-' + productId + '-manufactured').val());
    if(manufacturedBy === 1) {
        $('#change-conformity-button, #show-in-catalog-button').hide();
    } else {
        $('#change-conformity-button, #show-in-catalog-button').show();
    }
}

function returnToCategories() {
    if(categoriesHtml !== '') {
        $('div#add-match-replaceable').html(categoriesHtml);
        accordion.init([].concat(_toConsumableArray(document.querySelectorAll(`#add-match-replaceable [data-accordion]`))));
        $('#conformity-search-field').val('');
        categoriesHtml = '';
    }
}

function chooseConformity(productId, unitType) {
    var conformityInfo = $('#conformity-product-' + productId).find('div.hidden-info').html();
    var selectedConformitySelector = '#addproduct-conformity-selected';
    $(selectedConformitySelector).show();
    $(selectedConformitySelector).find('div.add-product__chosen-product').html(conformityInfo);
    $('#addproduct-conformity-empty').hide();
    $('#unit-type').text(unitType);
    returnToCategories();
    modal.close('#select-match');
    $('#productform-productid').val(productId);
    $('#product-submit-button').prop('disabled', false);
}

function validateAndUpload(event) {
    var input = $(event.target.files[0]);
    if(checkUploadCatalogFile(input)) {
        $(event.target).parent().submit();
    }
    return false;
}

function checkUploadCatalogFile(input, maxFileSizeInMB) {
    var isExtValid = false,
        validTypes = ['vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'vnd.ms-excel'], // xlsx, xls
        actualFileSize = 0,
        errors = [],
        fileErrorsDiv = $('div.file-errors'),
        file = input[0];

    if(typeof maxFileSizeInMB === 'undefined') {
        maxFileSizeInMB = 10;
    }

    fileErrorsDiv.html('');

    if($.browser && $.browser.msie) {
        var objFSO = new ActiveXObject("Scripting.FileSystemObject"),
            sPath = $(file)[0].value,
            objFile = objFSO.getFile(sPath);
        actualFileSize = objFile.size;
    } else {
        actualFileSize = file.size;
    }

    if(actualFileSize > 1024 * 1024 * maxFileSizeInMB) {
        errors.push("Размер файла должен быть не более " + maxFileSizeInMB + " МБ");
    }

    for(var i in validTypes) {
        if('application/' + validTypes[i] === file.type) {
            isExtValid = true;
        }
    }
    if(!isExtValid) {
        errors.push("Файл должен иметь один из форматов xls или xlsx");
    }

    if(errors.length > 0)  {
        input.val("");
        fileErrorsDiv.html(errors.join('<br />'));
        return false;
    }
    return true;
}

function getProductsListByName(q) {
    $.ajax({
        url: '/assortment/products-for-conformity-by-query?query=' + q,
        dataType: 'html',
        type: 'GET',
        success: showProductsList
    });
}

function showProductsList(html) {
    var replaceable = $('div#add-match-replaceable');
    if(categoriesHtml === '') {
        categoriesHtml = replaceable.html();
    }
    replaceable.html(html);
}

function setByOrderVisibility() {
    var byOrderSwitcher = $('#is-by-order-switcher');
    if($('#productform-isinstock').prop('checked')) {
        byOrderSwitcher.slideUp();
        var byOrderSwitcherInput = $('#productform-isbyorder');
        if(byOrderSwitcherInput.prop('checked')) {
            byOrderSwitcherInput.trigger('click');
        }
    } else {
        byOrderSwitcher.slideDown();
    }
}