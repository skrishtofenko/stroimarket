document.addEventListener('DOMContentLoaded', function(){
    window.sr = ScrollReveal();
    sr.reveal('[data-scroll="left"]', {
        origin: 'left',
        distance: '200px',
        duration: 1000,
        scale: 1
    });
    sr.reveal('[data-scroll="right"]', {
        origin: 'right',
        distance: '200px',
        duration: 1000,
        scale: 1
    });
});

$(function() {
    $('#for-whom').on('change', function() {
        window.location.href = $(this).val();
    });
});