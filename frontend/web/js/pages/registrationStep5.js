$(function() {
    $('p.registration__checkbox.active label').on('click', function() {
        var inputs = $('#requisites-settings-form').find('input[type=text], textarea');
        var serial = [];
        inputs.each(function() {
            serial.push(encodeURIComponent($(this).prop('id')) + '&' + encodeURIComponent($(this).val()));
        });
        document.cookie = "step5form=" + serial.join('||') + "; path=/;";
        window.location.href = '/registration/company';
    });

    fillFormFromCookie();
});

function fillFormFromCookie() {
    var cookies = document.cookie.split(';');
    var step5form = false;
    for(var i in cookies) {
        var cookie = cookies[i].split('=');
        if(cookie[0] === 'step5form' && cookie[1]) {
            step5form = cookie[1];
        }
    }
    if(step5form) {
        var fields = step5form.split('||');
        for(var j in fields) {
            var field = fields[j].split('&');
            var input = $('#' + decodeURIComponent(field[0]));
            if(input.length) {
                input.val(decodeURIComponent(field[1]))
            }
        }
    }
    var date = new Date(0);
    document.cookie = "step5form=; path=/; expires=" + date.toUTCString();
}
