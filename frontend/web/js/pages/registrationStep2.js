$(function() {
    $('#registrationstep2form-islegalentity').on('change', function() {
        if(!$(this).prop('checked')) {
            $('#legal-entity-block').slideUp();
        } else {
            $('#legal-entity-block').slideDown();
        }
    });

    bindPhoneCodeMask('#registrationstep2form-phonecode');
    bindPhoneNumberMask('#registrationstep2form-phonenumber');
});