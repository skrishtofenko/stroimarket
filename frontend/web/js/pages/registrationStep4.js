$(function() {
    bindPhoneMask('#companyinfoform-phone');

    $('#companyinfoform-is_imonly').on('change', function() {
        if($(this).prop('checked')) {
            $('.address-block').slideUp();
            $('.im-block').slideDown();
        } else {
            $('.address-block').slideDown();
            $('.im-block').slideUp();
        }
    });

    operatingTime.save();
});

