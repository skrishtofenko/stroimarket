var operatingTimeModal = document.getElementById('operating-time');
var timeInputs = Array.prototype.slice.call(operatingTimeModal.querySelectorAll('input[data-id]'));

var timeMaskOptions = {
    mask: 'HH:TT',
    groups: {
        HH: new IMask.MaskedPattern.Group.Range([0, 23]),
        TT: new IMask.MaskedPattern.Group.Range([0, 59])
    }
};

var maskedInputs = {};
timeInputs.forEach(function (timeInput, i) {
    maskedInputs[i] = new IMask(timeInput, timeMaskOptions)
});