$(function() {
    $('#cs-q').bind('typeahead:select', function(ev, suggestion) {
        var companyId = parseInt(suggestion.id);
        if(companyId) {
            window.location.href = '/catalog/' + searchResultPage + '/' + companyId;
        }
    }).on('keydown', function(e) {
        if(e.which === 13) {
            $(this).parents('form')[0].submit();
        }
    });

    $("#cs-m, #scs-id, #scs-mc").on('change', function() {
        $(this).parents('form')[0].submit();
    });
});