var cityDKs = [];

$(function() {
    $('#companyinfoform-is_delivery').on('change', function() {
        if($(this).prop('checked')) {
            disableSelectedCities();
        }
    });

    $(document).on('change', 'select.delivery-city', function() {
        disableSelectedCities();
    });

    disableSelectedCities();
});

function addDeliveryItem() {
    registrationDelivery.addItem();
    disableSelectedCities();
}

function bindDeliveryDropKick() {
    unbindDeliveryDropKick();
    var citySelectors = $('.registration-delivery-wrapper select.delivery-city');
    if(citySelectors.length) {
        citySelectors.each(function (i, citySelector) {
            citySelector = $(citySelector);
            var newCitySelectorId = 'citySelector' + cityDKs.length;
            citySelector.prop('id', newCitySelectorId);
            cityDKs.push(new Dropkick(document.querySelector("#" + newCitySelectorId)));
        });
    }
    $('select.delivery-type').dropkick({});
}

function unbindDeliveryDropKick() {
    if(cityDKs.length) {
        cityDKs.forEach(function(cityDK) {
            cityDK.dispose();
        });
        cityDKs = [];
    }
}

function pickSelectedCities() {
    var citySelectors = $('.registration-delivery-wrapper select.delivery-city');
    var selectedCities = [];
    citySelectors.each(function (i, citySelector) {
        citySelector = $(citySelector);
        if(selectedCities.indexOf(citySelector.val()) >= 0) {
            var selected = false;
            $(citySelector).find('option').each(function (j, option) {
                option = $(option);
                if(selectedCities.indexOf(option.val()) < 0 && !selected) {
                    option.prop('selected', true);
                    selected = true;
                    selectedCities.push(option.val());
                }
            });
            if(!selected) {
                citySelector.parents('div.registration-delivery-item').remove();
                $('#add-delivery-button').slideUp();
            }
        } else {
            selectedCities.push(citySelector.val());
        }
        if(selectedCities.length === $(citySelector).find('option').length) {
            $('#add-delivery-button').slideUp();
        }
    });
    return selectedCities;
}

function disableSelectedCities() {
    var citySelectors = $('.registration-delivery-wrapper select.delivery-city');
    if(citySelectors.length) {
        citySelectors.find('option').prop('disabled', false);

        var selectedCities = pickSelectedCities();
        citySelectors.each(function(i, citySelector) {
            citySelector = $(citySelector);
            $(citySelector).find('option').each(function(j, option) {
                option = $(option);
                if(selectedCities.indexOf(option.val()) >= 0 && !option.prop('selected')) {
                    option.prop('disabled', true)
                }
            });
        });
    }
    bindDeliveryDropKick();
}
