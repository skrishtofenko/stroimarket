function checkUploadImage(event, maxFileSizeInMB) {
    var isExtValid = false,
        validExtensions = ['jpg', 'jpeg', 'png', 'gif'],
        actualFileSize = 0,
        errors = [],
        imageErrorsDiv = $('div.image-errors'),
        input = $(event.target.files[0]),
        file = input[0];

    if(typeof maxFileSizeInMB === 'undefined') {
        maxFileSizeInMB = 5;
    }

    imageErrorsDiv.html('');

    if($.browser && $.browser.msie) {
        var objFSO = new ActiveXObject("Scripting.FileSystemObject"),
            sPath = $(file)[0].value,
            objFile = objFSO.getFile(sPath);
        actualFileSize = objFile.size;
    } else {
        actualFileSize = file.size;
    }

    if(actualFileSize > 1024 * 1024 * maxFileSizeInMB) {
        errors.push("Размер файла должен быть не более " + maxFileSizeInMB + " МБ");
    }

    for(var i in validExtensions) {
        if('image/' + validExtensions[i] === file.type) {
            isExtValid = true;
        }
    }
    if(!isExtValid) {
        errors.push("Файл должен иметь один из форматов: " + validExtensions.join(', '));
    }

    if(errors.length > 0)  {
        input.val("");
        imageErrorsDiv.html(errors.join('<br />'));
        filePreview.remove(event.target.id);
    } else {
        filePreview.show(event);
    }
    return false;
}