function bindPhoneMask(selector) {
    $(selector).inputmask({
        'mask': '+7 (999) 999-99-99',
        'removeMaskOnSubmit': true,
        'autoUnmask': true
    });
}

function bindPhoneCodeMask(selector) {
    $(selector).inputmask({
        'mask': '999',
        'removeMaskOnSubmit': true,
        'autoUnmask': true
    });
}

function bindPhoneNumberMask(selector) {
    $(selector).inputmask({
        'mask': '999-99-99',
        'removeMaskOnSubmit': true,
        'autoUnmask': true
    });
}