$(function() {
    $('#requisites-settings-form').on('afterValidateAttribute', function(event, attribute, message) {
        var errorSpan = $($('#' + attribute.id).siblings('span')[0]);
        var errorTextSpan = errorSpan.find('span');
        if(message.length) {
            errorSpan.addClass('showable');
            errorTextSpan.text(message[0]);
        } else {
            errorSpan.removeClass('showable');
            errorTextSpan.text('');
        }
    });

    $('input, textarea').on('mouseenter', function() {
        if(!$(this).prop('readonly')) {
            var errorSpan = $($('#' + $(this).prop('id')).siblings('span')[0]);
            if (errorSpan.hasClass('showable')) {
                errorSpan.show();
            }
        }
    }).on('mouseleave', function() {
        if(!$(this).prop('readonly')) {
            var errorSpan = $($('#' + $(this).prop('id')).siblings('span')[0]);
            if (!errorSpan.is(':hover')) {
                errorSpan.hide();
            }
        }
    });

    $('span.form__error').on('mouseleave', function() {
        if($(this).hasClass('showable')) {
            var input = $($(this).siblings('input, textarea')[0]);
            if (!input.is(':hover')) {
                $(this).hide();
            }
        }
    });
});
