$(function() {
    $('#ps-q').bind('typeahead:select', function(ev, suggestion) {
        var productId = parseInt(suggestion.id);
        if(productId) {
            window.location.href = '/catalog/product/' + productId;
        }
    }).on('keydown', function(e) {
        if(e.which === 13) {
            $(this).parents('form')[0].submit();
        }
    });

    $('button.search-products[type=submit]').on('click', function(e) {
        if($('#ps-q').val() === '') {
            e.preventDefault();
        }
    });

    $("#ps-a").on('change', function() {
        $(this).parents('form')[0].submit();
    });

    $('form#sidebar-product-search-form input[type=checkbox], form#sidebar-product-search-form input[type=radio]').on('change', function() {
        $(this).parents('form')[0].submit();
    });
});

var inputFrom = document.getElementById('range-input-from'),
    inputTo = document.getElementById('range-input-to'),
    slider = document.getElementById('range-slider');

if(inputFrom && inputTo) {
    var inputs = [inputFrom, inputTo];
    noUiSlider.create(slider, {
        start: [curMinCost, curMaxCost],
        step: 1,
        connect: true,
        range: {
            'min': minCost,
            'max': maxCost
        },
        format: wNumb({
            decimals: 0
        })
    });

    slider.noUiSlider.on('update', function (values, handle) {
        inputs[handle].value = values[handle];
    });

    function setSliderHandle(i, value) {
        var r = [null, null];
        r[i] = value;
        slider.noUiSlider.set(r);
    }

    inputs.forEach(function (input, handle) {
        input.addEventListener('change', function () {
            setSliderHandle(handle, this.value);
        });

        input.addEventListener('keydown', function (e) {
            var values = slider.noUiSlider.get();
            var value = Number(values[handle]);
            var steps = slider.noUiSlider.steps();
            var step = steps[handle];
            var position;

            switch (e.which) {
                case 13:
                    setSliderHandle(handle, this.value);
                    break;

                case 38:
                    position = step[1];
                    if (position === false) {
                        position = 1;
                    }
                    if (position !== null) {
                        setSliderHandle(handle, value + position);
                    }
                    break;

                case 40:
                    position = step[0];
                    if (position === false) {
                        position = 1;
                    }
                    if (position !== null) {
                        setSliderHandle(handle, value - position);
                    }
                    break;
            }
        });
    });
}