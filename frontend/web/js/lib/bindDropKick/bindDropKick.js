var dropKicks = {};
var dropKickSelectors = [
    '#place',
    '#action',
    '#room-type',
    '#companyinfoform-room_type_id',
    '#registrationstep2form-ownershiptypeid',
    '#doc-type',
    '#mode',
    '#productform-unittypeid',
    '#for-whom'
];

$(function() {
    bindAllDropCicks();
});

function bindAllDropCicks() {
    dropKickSelectors.forEach(function(selector) {
        if($(selector).length) {
            var selectorType = selector[0];
            var selectName = selector.substr(1) + 'Select';
            switch (selectorType) {
                case '#':
                    if (typeof dropKicks[selectName] === 'undefined') {
                        dropKicks[selectName] = new Dropkick(selector, {});
                    }
                    break;
                case '.':
                    $(selector).dropkick({});
                    break;
            }
        }
    });
}