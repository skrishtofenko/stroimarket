$(function() {
    var chooserForm = document.querySelector('.choose-city__form');
    if(chooserForm) {
        var cities = [].concat(_toConsumableArray(chooserForm.querySelectorAll('input[type=checkbox]')));
        cities.forEach(function (city) {
            citiesChooser.action(city, true);
        });
    }

    $('div.choose-city input[type=radio]').on('change', function() {
        $('#userplacementform-city_id').val($(this).val());
        $('#companyinfoform-city_id').val($(this).val());
        var cityName = 'Выберите город';
        var selected = document.querySelectorAll('.choose-city__form input[type=radio]:checked + label');
        if(selected.length) {
            cityName = selected[0].innerText;
        }
        $('#city-chooser-button').text(cityName);
    });

    $('#city-chooser-search').on('keyup', function() {
        filterCitiesList($(this).val());
    })
});

function filterCitiesList(query) {
    query = query.trim().toLowerCase();
    var cityChooserForm = $('div.choose-city__form');
    var fieldsets = cityChooserForm.find('fieldset');
    fieldsets.each(function() {
        $(this).find('p').prop('hidden', false);
        $(this).prop('hidden', false);
    });
    if(query !== '') {
        fieldsets.each(function() {
            var inputs = $(this).find('p');
            var hiddenInputsCount = 0;
            inputs.each(function() {
                var input = $(this).find('input[type=checkbox], input[type=radio]');
                if(input.data('name').indexOf(query) !== 0) {
                    $(this).prop('hidden', true);
                    hiddenInputsCount++;
                }
            });
            if(hiddenInputsCount === inputs.length) {
                $(this).prop('hidden', true);
            }
        });
    }
}
